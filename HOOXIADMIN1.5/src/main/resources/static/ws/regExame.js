//공백 확인
const CHECK_SPACE = /\s+/;

//숫자 확인
const ONLY_NUMBER = /^[0-9]*$/;

//영문 확인
const ONLY_ENGLISH = /^[a-zA-Z]*$/;

//한글 확인
const ONLY_KOREA = /^[ㄱ-ㅎ|가-힣|]*$/;

//한글 영어 숫자
const ONLY_KO_EN__NUMBER = /^[ㄱ-ㅎ|가-힣|a-z|A-Z|0-9|]+$/;

//이메일 확인
const EMAIL_CHECK = /\w+@\w+\.\w+(\.\w+)?/;

//전화번호 확인
const TEL_NUMBERS_CHECK = /^\d{2,3}-\d{3,4}-\d{4}$/;

//핸드폰 확인
const PHONE_NUMBERS_CHECK = /^01(?:0|1|[6-9])-(?:\d{3}|\d{4})-\d{4}$/;

//주민번화 확인
const IDENTITY_CHECK = /\d{6}\-[1-4]\d{6}/;

//우편번호 확인
const POSTAL_CODE_CHECK = /^\d{3}-\d{2}$/;

















