/*
 * 2020.02.24 js 첫 수정
 * botui 0.3.9
 * A JS library to build the UI for your bot
 * https://botui.org
 *
 * Copyright 2019, Moin Uddin
 * Released under the MIT license.
 */

(function (root, factory) {
    "use strict";
    if (typeof define === 'function' && define.amd) {
      define([], function () {
        return (root.BotUI = factory(root));
      });
    } else {
      root.BotUI = factory(root);
    }
  }
  (typeof window !== 'undefined' ? window : this, function (root, undefined) {
    "use strict";

    var BotUI = (function (id, opts) {

      opts = opts || {};

      if (!id) {
        throw Error('BotUI: Container id is required as first argument.');
      }

      if (!document.getElementById(id)) {
        throw Error('BotUI: Element with id #' + id + ' does not exist.');
      }

      if (!root.Vue && !opts.vue) {
        throw Error('BotUI: Vue is required but not found.');
      }

      var _botApp, // current vue instance.
        _options = {
          debug: false,
          fontawesome: false,
          searchselect: false,
          slickCarousel: true,
          response: true
        },
        _container, // the outermost Element. Needed to scroll to bottom, for now.
        _interface = {}, // methods returned by a BotUI() instance.
        _actionResolve,
        _markDownRegex = {
          /* igm은 그냥 정규식임을 알려주는 것(?)
          실제로 봐야하는 부분은 /  / 사이 내용 */
          icon: /!\(([^\)]+)\)/igm, // !(icon)
          image: /!\[(.*?)\]\((.*?)\)/igm, // ![aleternate text](src)
          /* 이미지는 []와 ()를 필수적으로 가지고 있어야 한다. */
          link: /!\[([^\[]+)\]\(([^\)]+)\)(\^?)/igm // [text](link) ^ can be added at end to set the target as 'blank'
          /* 링크는 ()를 필수적으로 가지고 있어야 한다.
          이미지와 함께 사용될 때는 [![aleternate text](src)](링크)형태를 갖는다) */
        },
        _fontAwesome = 'https://use.fontawesome.com/ea731dcb6f.js',
        _esPromisePollyfill = 'https://cdn.jsdelivr.net/es6-promise/4.1.0/es6-promise.min.js', // mostly for IE
        _searchselect = "https://unpkg.com/vue-select@2.4.0/dist/vue-select.js";

      root.Vue = root.Vue || opts.vue;

      // merge opts passed to constructor with _options
      for (var prop in _options) {
        if (opts.hasOwnProperty(prop)) {
          _options[prop] = opts[prop];
        }
      }

      if (!root.Promise && typeof Promise === "undefined" && !opts.promise) {
        loadScript(_esPromisePollyfill);
      }

      /* 링크가 들어간 부분은 아래 함수를 통해 수정 됨.  */
      function _linkReplacer(match, $1, $2, $3) {
        var _target = $3 ? 'blank' : ''; // check if '^' sign is present with link syntax
        return "<a class='botui-message-content-link' target='" + _target + "' href='" + $2 + "'>" + $1 + "</a>";
      }

      /* 이미지, 아이콘 형식으로 들어올 경우, 해당 문자열을 아래와 같이 변 */
      function _parseMarkDown(text) {
        return text
          .replace(_markDownRegex.image, "<img class='botui-message-content-image' src='$2' alt='$1' />")
          .replace(_markDownRegex.icon, "<i class='botui-icon botui-message-content-icon fa fa-$1'></i>")
          .replace(_markDownRegex.link, _linkReplacer)
      }

      function _linebreak(text) {
        var textTmp1 = text;
        var textTmp2 = text;

        textTmp1.replace('/(?:\r\n|\r|\n)/g', '<br />');
        textTmp2.split('\n').join('<br />');

        return text;
      }

      function loadScript(src, cb) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = src;

        if (cb) {
          script.onload = cb;
        }

        document.body.appendChild(script);
      }

      function _handleAction(text) {
        if (_instance.action.addMessage) {
          _interface.message.human({
            delay: 0,
            content: text
          });
        }
        _instance.action.show = !_instance.action.autoHide;
      }
      var template = '';
      
      
      /* 채팅창을 꾸며주는 div 구성을 아래 함수를 통해 꾸며준다. 필수적인 작업 */
      function SettingTemplate() {
        /* 기존 초기 값 */
        // var template = '<div class=\"botui botui-container\" v-botui-container><div class=\"botui-messages-container\"><div v-for=\"msg in messages\" class=\"botui-message\" :class=\"msg.cssClass\" v-botui-scroll><transition name=\"slide-fade\"><div v-if=\"msg.visible\"><div v-if=\"msg.photo && !msg.loading\" :class=\"[\'profil\', \'profile\', {human: msg.human, \'agent\': !msg.human}]\"> <img :src=\"msg.photo\" :class=\"[{human: msg.human, \'agent\': !msg.human}]\"></div><div v-if=\ "msg.type== \'text\'\" :class=\ "[{human: msg.human, \'botui-message-content\':true}, msg.type]\" @click=\"handle_action_button(msg.type)\"><span v-if=\ "msg.type== \'text\'\" v-text=\"msg.content\" v-botui-markdown></span><span v-if=\ "msg.type==\'html\'\" v-html=\"msg.content\"></span></div><div v-if=\ "msg.type== \'single\'\" :class=\"[{human: msg.human, \'botui-message-content\': true}, msg.type]\"><span v-if=\"msg.type == \'text\'\" v-text=\"msg.content\" v-botui-markdown></span><span v-if=\"msg.type == \'html\'\" v-html=\"msg.content\"></span> <iframe v-if=\"msg.type == \'passion\'\" :src=\"msg.content\" frameborder=\"0\" allowfullscreen></iframe><span v-if=\ "msg.type==  \'single\'\" v-text=\"msg.content\" v-botui-markdownOutput></span></div><div v-if=\ "msg.type== \'multiple\'\" ><section class="regular slider" v-botui-markdownOutput></section></div><div v-if=\ "msg.type== \'quick\'\" class=\"botui-actions-buttons\" :class=\"action.cssClass\" v-botui-markdownQuick></div></div></transition><div v-if=\"msg.photo && msg.loading && !msg.human\" :class=\"[\'profil\', \'profile\', {human: msg.human, \'agent\': !msg.human}]\"> <img :src=\"msg.photo\" :class=\"[{human: msg.human, \'agent\': !msg.human}]\"></div><div v-if=\"msg.loading\" class=\"botui-message-content loading\"><i class=\"dot\"></i><i class=\"dot\"></i><i class=\"dot\"></i></div></div></div><div class=\"botui-actions-container\"><transition name=\"slide-fade\"><div v-if=\"action.show\" v-botui-scroll><form v-if=\"action.type == \'text\'\" class=\"botui-actions-text\" @submit.prevent=\"handle_action_text()\" :class=\"action.cssClass\"><i v-if=\"action.text.icon\" class=\"botui-icon botui-action-text-icon fa\" :class=\"\'fa-\' + action.text.icon\"></i> <input type=\"text\" ref=\"input\" :type=\"action.text.sub_type\" v-model=\"action.text.value\" class=\"botui-actions-text-input\" :placeholder=\"action.text.placeholder\" :size=\"action.text.size\" :value=\" action.text.value\" :class=\"action.text.cssClass\" required v-focus/></form><form v-if=\"action.type == \'select\'\" class=\"botui-actions-select\" @submit.prevent=\"handle_action_select()\" :class=\"action.cssClass\"><i v-if=\"action.select.icon\" class=\"botui-icon botui-action-select-icon fa\" :class=\"\'fa-\' + action.select.icon\"></i><v-select v-if=\"action.select.searchselect && !action.select.multipleselect\" v-model=\"action.select.value\" :value=\"action.select.value\" :placeholder=\"action.select.placeholder\" class=\"botui-actions-text-searchselect\" :label=\"action.select.label\" :options=\"action.select.options\"></v-select><v-select v-else-if=\"action.select.searchselect && action.select.multipleselect\" multiple v-model=\"action.select.value\" :value=\"action.select.value\" :placeholder=\"action.select.placeholder\" class=\"botui-actions-text-searchselect\" :label=\"action.select.label\" :options=\"action.select.options\"></v-select> <select v-else v-model=\"action.select.value\" class=\"botui-actions-text-select\" :placeholder=\"action.select.placeholder\" :size=\"action.select.size\" :class=\"action.select.cssClass\" required v-focus><option v-for=\"option in action.select.options\" :class=\"action.select.optionClass\" v-bind:value=\"option.value\" :disabled=\"(option.value == \'\')?true:false\" :selected=\"(action.select.value == option.value)?\'selected\':\'\'\"> {{ option.text }}</option></select> <button type=\"submit\" :class=\"{\'botui-actions-buttons-button\': !!action.select.button, \'botui-actions-select-submit\': !action.select.button}\"><i v-if=\"action.select.button && action.select.button.icon\" class=\"botui-icon botui-action-button-icon fa\" :class=\"\'fa-\' + action.select.button.icon\"></i> <span>{{(action.select.button && action.select.button.label) || \'Ok\'}}</span></button></form><div v-if=\"action.type == \'button\'\" class=\"botui-actions-buttons\" :class=\"action.cssClass\"> <button type=\"button\" :class=\"button.cssClass\" class=\"botui-actions-buttons-button\" v-botui-scroll v-for=\"button in action.button.buttons\" @click=\"handle_action_button(button)\"><i v-if=\"button.icon\" class=\"botui-icon botui-action-button-icon fa\" :class=\"\'fa-\' + button.icon\"></i> {{button.text}}</button></div><form v-if=\"action.type == \'buttontext\'\" class=\"botui-actions-text\" @submit.prevent=\"handle_action_text()\" :class=\"action.cssClass\"><i v-if=\"action.text.icon\" class=\"botui-icon botui-action-text-icon fa\" :class=\"\'fa-\' + action.text.icon\"></i> <input type=\"text\" ref=\"input\" :type=\"action.text.sub_type\" v-model=\"action.text.value\" class=\"botui-actions-text-input\" :placeholder=\"action.text.placeholder\" :size=\"action.text.size\" :value=\"action.text.value\" :class=\"action.text.cssClass\" required v-focus/><div class=\"botui-actions-buttons\" :class=\"action.cssClass\"> <button type=\"button\" :class=\"button.cssClass\" class=\"botui-actions-buttons-button\" v-for=\"button in action.button.buttons\" @click=\"handle_action_button(button)\" autofocus><i v-if=\"button.icon\" class=\"botui-icon botui-action-button-icon fa\" :class=\"\'fa-\' + button.icon\"></i> {{button.text}}</button></div></form></div></transition></div></div>';

        /*현재 값  ---------------------------------------------------------------------------------------------------------------  */
        template += '<div class=\"botui botui-container\" v-botui-container><div id=\"message-container\" class=\"botui-messages-container\">'
        template += '<div name=\"botui-message-response\" v-for=\"(msg, index) in messages\" class=\"botui-message\" :class=\"msg.cssClass\" v-botui-scroll>';
        template += '<transition name=\"slide-fade\"><div v-if=\"msg.visible\">';
        /* 기존 - 이미지 출력 사용 */
        template += '<div v-if=\"msg.photo && !msg.loading\" :class=\"[\'profil\', \'profile\', {human: msg.human, \'agent\': !msg.human}]\">';
        template += '<img :src=\"msg.photo\" :class=\"[{human: msg.human, \'agent\': !msg.human}]\"></div>';
        /* 기존 - 텍스트 출력 사용. 버튼이나 입력이 들어왔을 때 사용자 입력으로 보여지는 부분은 여기서 꾸밈 */
        template += '<div v-if=\ "msg.type== \'text\'\" :class=\ "[{human: msg.human, \'botui-message-content\':true}, msg.type]\" @click=\"handle_action_button(msg.type)\">';
        template += '<span v-if=\ "msg.type== \'text\'\" v-text=\"msg.content\" v-botui-markdown></span>';
        template += '<span v-if=\ "msg.type==\'html\'\" v-html=\"msg.content\"></span></div>';

        /* 챗봇 이미지와 이름 삽입 */
        template += '<div v-if = \"msg.order == 1" class=\"profil profile agent\"><img class=\"agent\"></div>';
        template += '<div v-if = \"msg.order == 1" class=\"botui-message-name text\"><span>챗봇</span><span id="botui-time"><a v-text=msg.time></a></span></div>';
       

        /* single 세팅--------------------------------------------------------------------------------------------------------------- */
        
        // template += '<div v-if=\ "msg.type== \'single\'\" :class=\"[{human: msg.human, \'botui-message-content\': true}, msg.type]\">';
        template += '<div v-if=\"msg.type== \'single\' && msg.order == 1\" class=\"botui-message-content\">';
        
        template += '<input type=\"hidden\" id=\"idValue_single\" v-html=\"msg.groupId\" ><div id=\"singleReply\" v-for=\"response in msg.reply\" ><input type=\"hidden\" id=\"singleFormId\" v-html="response.formId"><span id="singleResponse">';
        template += '<img id=\"singleImg\" v-if=\"response.content != null\" :src=\"response.content\" class=\"botui-message-content-image\">';
        template += '<div id=\"singleLabel\"  v-html =\"response.label\" ></div>';

        /* 버튼 처리 */
        template += '<div v-if=\"response.button != null\"  v-for=\ "btn in response.button\"><input name=\"singleBtnValue\" type=\"hidden\" v-html=\"btn.value\"><input type=\"hidden\" v-html=\"btn.btnId\" name=\"singleBtnId\"></div><div name=\"singleButtons\" v-if=\"response.button != null\"  v-for=\ "btn in response.button\" >';
        template += '<button name=\"singleBlockBtn\" v-if=\"btn.type == \'message\' \" class=\"botui-output-buttons-button\" @click=\"handle_button(btn)\">{{btn.text}}</button>';
        template += '<button name=\"singlePhoneBtn\" v-else-if=\"btn.type == \'phone\' \" class=\"botui-output-buttons-button\" @click=\"handle_button_phone(btn)\">{{btn.text}}</button>';
        template += '<button name=\"singleWebLinkBtn\" v-else class=\"botui-output-buttons-alink\"  @click=\"handle_button_web(btn)\">{{btn.text}}</button>';
        template += '</div>';
        /* 버튼 처리 */

        template += '</span></div></div>';
        /* single 세팅 끝 --------------------------------------------------------------------------------------------------------------- */
        
       /* single 프로필 없는 --------------------------------------------------------------------------------------------------------------- */
        
        // template += '<div v-if=\ "msg.type== \'single\'\" :class=\"[{human: msg.human, \'botui-message-content\': true}, msg.type]\">';
        template += '<div v-if=\ "msg.type== \'single\' && msg.order != 1\" class=\"botui-message-content-noprofil\"><input type=\"hidden\" id=\"idValue_noProfil\" v-html=\"msg.groupId\" >';
        template += '<div v-for=\ "response in msg.reply\" ><span id=\"noProfilResponse\">';
        template += '<input type=\"hidden\" id=\"noProfilFormId\" v-html="response.formId"><img id=\"noProfilImage\" v-if=\"response.content != null\" :src=\"response.content\" class=\"botui-message-content-image\">';
        template += '<div title=\"noProfilLabel\" id=\"noProfilLabel\"  v-html =\"response.label\"></div>';

        /* 버튼 처리 */
        template += '<div v-if=\"response.button != null\"  v-for=\ "btn in response.button\"><input name=\"noProfilBtnValue\" type=\"hidden\" v-html=\"btn.value\"><input type=\"hidden\" v-html=\"btn.btnId\" name=\"noProfilBtnId\"></div><div name=\"noProfilButtons\" v-if=\"response.button != null\"  v-for=\ "btn in response.button\">';
        template += '<button name=\"noProfilBlockBtn\" v-if=\"btn.type == \'message\' \" class=\"botui-output-buttons-button\" @click=\"handle_button(btn)\">{{btn.text}}</button>';
        template += '<button name=\"noProfilPhoneBtn\" v-else-if=\"btn.type == \'phone\' \" class=\"botui-output-buttons-alink\" @click=\"handle_button_phone(btn)\">{{btn.text}}</button>';
        template += '<button name=\"noProfilWebLinkBtn\" v-else class=\"botui-output-buttons-alink\" @click=\"handle_button_web(btn)\">{{btn.text}}</button>';
        template += '</div>';
        /* 버튼 처리 */

        template += '</span></div></div>';
        /* single 세팅 끝 --------------------------------------------------------------------------------------------------------------- */
        // template += '</div></div>';

        

        /* multiple 세팅--------------------------------------------------------------------------------------------------------------- */
        template += '<div v-if=\ "msg.type== \'multiple\'\" ><input type=\"hidden\" id=\"idValue_multiple\" v-html=\"msg.groupId\"><section class="regular" >';
        template += '<div v-for=\ "response in msg.reply\" ><div name=\"botuiMultiple\" class=\"botui-message-content-multiple\"><input type=\"hidden\" name=\"multipleFormId\" v-html=\"response.formId\"><span name=\"multipleResponse\">';
        template += '<img name=\"multipleImg\" v-if=\"response.content != null\" :src=\"response.content\" class=\"botui-message-content-image\">';
        template += '<div name=\"multipleLabel\" class=\"\" v-text =\"response.label\" ></div>';

        /* 버튼 처리 */
        template += '<div v-if=\"response.button != null\"  v-for=\ "btn in response.button\"><input name=\"multipleButtonValue\" type=\"hidden\" v-html=\"btn.value\"><input type=\"hidden\" v-html=\"btn.btnId\" name=\"multipleBtnId\"></div><div class=\"multipleButton\" v-if=\"response.button != null\"  v-for=\ "btn in response.button\" >';
        // template += '<button v-if=\"response.button != null\" v-for=\ "btn in response.button\" class=\"botui-output-buttons-button\" @click=\"handle_button(btn)\">{{btn.text}}</button>';
        template += '<button name="multipleBlockBtn" v-if=\"btn.type == \'message\' \" class=\"botui-output-buttons-button\" @click=\"handle_button(btn)\">{{btn.text}}</button>';
        template += '<button name="multiplePhoneBtn" v-else-if=\"btn.type == \'phone\' \" class=\"botui-output-buttons-alink\" @click=\"handle_button_phone(btn)\">{{btn.text}}</button>';
        template += '<button name="multipleWebLinkBtn" v-else class=\"botui-output-buttons-alink\" @click=\"handle_button_web(btn)\">{{btn.text}}</a></button>';
        template += '</div>';
        /* 버튼 처리 */

        template += '</span></div></div>';
        // </section></div>';
        /* multiple 세팅 끝 --------------------------------------------------------------------------------------------------------------- */


        /* 이 사이에 캐로셀로 나갈 아이들 세팅  퀵리플라이*/
        template += '</section></div>';
        template += '<div v-for=\"quick in msg.quick\"><input name=\"quickValue\" type=\"hidden\" v-html=\"quick.value\"></div><div v-if=\ "msg.type== \'quick\'\" class=\"botui-quick-buttons\"><section id=\"quickReplies\" class="quickRegular" >';
        template += '<button name=\"quickReplies\" v-for=\ "quick in msg.quick\" type=\"button\" class=\"botui-actions-buttons-button\" v-botui-scroll  @click=\"handle_quick(quick)\" > {{quick.text}} </button></section></div></div></transition>';
        template += '<div v-if=\"msg.photo && msg.loading && !msg.human\" :class=\"[\'profil\', \'profile\', {human: msg.human, \'agent\': !msg.human}]\"> ';
        template += '<img :src=\"msg.photo\" :class=\"[{human: msg.human, \'agent\': !msg.human}]\"></div>';
        template += '</div></div>';

        /* 출력이 관한 template 완료 */
        
        /* 입력에 관한 template 시작 */
        template += '<div class=\"botui-actions-container\"><transition name=\"slide-fade\">';
        template += '<div v-if=\"action.show\" v-botui-scroll>';
        template += '<form v-if=\"action.type == \'text\'\" class=\"botui-actions-text\" @submit.prevent=\"handle_action_text()\" :class=\"action.cssClass\">';
        template += '<i v-if=\"action.text.icon\" class=\"botui-icon botui-action-text-icon fa\" :class=\"\'fa-\' + action.text.icon\"></i>';
        template += '<input type=\"text\" ref=\"input\" :type=\"action.text.sub_type\" v-model=\"action.text.value\" class=\"botui-actions-text-input\" :placeholder=\"action.text.placeholder\" :size=\"action.text.size\" :value=\" action.text.value\" :class=\"action.text.cssClass\" required v-focus/></form>';
        template += '</form>';

        template += '<div v-if=\"action.type == \'button\'\" class=\"botui-actions-buttons\" :class=\"action.cssClass\">';
        template += '<button type=\"button\" :class=\"button.cssClass\" class=\"botui-actions-buttons-button\" v-botui-scroll v-for=\"button in action.button.buttons\" @click=\"handle_action_button(button)\">';
        template += '<i v-if=\"button.icon\" class=\"botui-icon botui-action-button-icon fa\" :class=\"\'fa-\' + button.icon\"></i> {{button.text}}</button></div>';

        template += '<form v-if=\"action.type == \'buttontext\'\" class=\"botui-actions-text\" @submit.prevent=\"handle_action_text()\" :class=\"action.cssClass\">';
        template += '<i v-if=\"action.text.icon\" class=\"botui-icon botui-action-text-icon fa\" :class=\"\'fa-\' + action.text.icon\"></i>';
        template += '<input type=\"text\" ref=\"input\" :type=\"action.text.sub_type\" v-model=\"action.text.value\" class=\"botui-actions-text-input\" :placeholder=\"action.text.placeholder\" :size=\"action.text.size\" :value=\"action.text.value\" :class=\"action.text.cssClass\" required v-focus/>';
        template += '<div class=\"botui-actions-buttons\" :class=\"action.cssClass\">';
        template += '<button type=\"button\" :class=\"button.cssClass\" class=\"botui-actions-buttons-button\" v-for=\"button in action.button.buttons\" @click=\"handle_action_button(button)\" autofocus>';
        template += '<i v-if=\"button.icon\" class=\"botui-icon botui-action-button-icon fa\" :class=\"\'fa-\' + button.icon\"></i> {{button.text}}</button></div></form></div></transition>';
        template += '</div></div>';

        /* 입력에 관한 template 완료 */
        
        
        
        
        return template;
      }
      
      
      
      var _botuiComponent = {
        /* 전역변수인 bot-ui의 값이 _botuiComponent 값에 담긴 값으로 세팅됨. */
        /* 모든 값들의 결과 값이 이쪽으로 전달되어 여기서 세팅됨.  */
       template: SettingTemplate(),
        data: function () {
          return {
            action: {
              text: {
                size: 50,
                placeholder: 'Write here ..'
                
              },
              button: {},
              show: false,
              type: 'text',
              autoHide: true,
              addMessage: true
            },
            messages: []
          };
        },
        computed: {
          isMobile: function () {
            return root.innerWidth && root.innerWidth <= 768;
          }
        },
        methods: {
          /* BUTTON이 클릭되면 넘어오는 곳 -  기존 현재 사용안함.*/
          handle_action_button: function (button) {
            for (var i = 0; i < this.action.button.buttons.length; i++) {
              if (this.action.button.buttons[i].value == button.value && typeof (this.action.button.buttons[i].event) == 'function') {
                this.action.button.buttons[i].event(button);
                if (this.action.button.buttons[i].actionStop) return false;
                break;
              }
            }

            _handleAction(button.text);

            var defaultActionObj = {
              type: 'button',
              text: button.text,
              value: button.value
            };

            for (var eachProperty in button) {
              if (button.hasOwnProperty(eachProperty)) {
                if (eachProperty !== 'type' && eachProperty !== 'text' && eachProperty !== 'value') {
                  defaultActionObj[eachProperty] = button[eachProperty];
                }
              }
            }

            _actionResolve(defaultActionObj);
          },
          


              
          
          
          /* 퀵 리플라이 아닌 버튼 형태 */
          handle_button: function (button) {
            inputValue = button.value;
            deviceType = button.device;
            pushType = "button";

            _handleAction(button.text);

            var defaultActionObj = {
              type: 'button',
              text: button.text,
              value: button.value,
              device: button.device
            };


            _actionResolve(defaultActionObj);
          },
          handle_button_phone: function(button){
             inputValue = button.value;
              deviceType = button.device;
              pushType = "phone";

              
              console.log(button.value);
              
              var defaultActionObj = {
                type: 'phone',
                text: button.text,
                value: button.value,
                device: button.device
              };
              return location.href="tel:"+button.value
          },
          handle_button_web: function(button){
             inputValue = button.value;
              deviceType = button.device;
              pushType = "link";

              
              console.log(button.value);
              
              var defaultActionObj = {
                type: 'link',
                text: button.text,
                value: button.value,
                device: button.device
              };
              return location.href=button.value
          }

          ,
          /* 퀵 리플라이 버튼 형태 */
          handle_quick: function (button) {
            inputValue = button.value;
            deviceType = button.device;
            pushType = "quick";
            _handleAction(button.text);


            var defaultActionObj = {
              type: 'quick',
              text: button.text,
              value: button.value,
              device: button.device
            };


            _actionResolve(defaultActionObj);
          },

          /* 입력 받은 값 핸들링 하는 곳, TEXT */
          handle_action_text: function () {
            pushType = "input";
            if (!this.action.text.value) return;
            inputValue = this.action.text.value;

            _handleAction(this.action.text.value);
            _actionResolve({
              type: 'text',
              value: this.action.text.value
            });
            this.action.text.value = '';
          },

          /* 입력 받은 값 핸들링 하는 곳, SELECT */
          handle_action_select: function () {
            if (this.action.select.searchselect && !this.action.select.multipleselect) {
              if (!this.action.select.value.value) return;
              _handleAction(this.action.select.value[this.action.select.label]);
              _actionResolve({
                type: 'text',
                value: this.action.select.value.value,
                text: this.action.select.value.text,
                obj: this.action.select.value
              });
            }
            if (this.action.select.searchselect && this.action.select.multipleselect) {
              if (!this.action.select.value) return;
              var values = new Array();
              var labels = new Array();
              for (var i = 0; i < this.action.select.value.length; i++) {
                values.push(this.action.select.value[i].value);
                labels.push(this.action.select.value[i][this.action.select.label]);
              }
              _handleAction(labels.join(', '));
              _actionResolve({
                type: 'text',
                value: values.join(', '),
                text: labels.join(', '),
                obj: this.action.select.value
              });
            } else {
              if (!this.action.select.value) return;
              for (var i = 0; i < this.action.select.options.length; i++) { // Find select title
                if (this.action.select.options[i].value == this.action.select.value) {
                  _handleAction(this.action.select.options[i].text);
                  _actionResolve({
                    type: 'text',
                    value: this.action.select.value,
                    text: this.action.select.options[i].text
                  });
                }
              }
            }
          }
        }
      };

      /* 이미지와 텍스트가 추가된 사항은 이 함수로 들어오게 되어있음. */
      root.Vue.directive('botui-markdown', function (el, binding) {
        if (binding.value == 'false') return; // v-botui-markdown="false";
        el.innerHTML = _parseMarkDown(el.textContent);

      });


      /* 줄바꿈 치환. */
      root.Vue.directive('botui-linebreak', function (el, binding) {
        if (binding.value == 'false') return; // v-botui-markdown="false";
        el.innerHTML = _linebreak(el.textContent);
      });

      /* ======================output형 출력에 필요한 부분 세팅하기 위해 내가 추가한 부분 시작============================= */
      /* 카드형은 이쪽으로 들어올 수 있게끔 작업 */
      root.Vue.directive('botui-markdownOutput', function (el, binding) {
        // if (binding.value == 'false') return;
        // /* 임시 변수 값에 넣은 후 전역 변수에 있는 값은 초기 */
        // if (outputConponent != null && outputConponent.length != conponentIdx) {
        //   /* 1. 순서대로 처리 */
        //   var outputConponentTmp = outputConponent[conponentIdx];
        //   el.textContent = _parseOutput(outputConponentTmp);
        //   /* 2. 해당 배열에 들어가 있는 기록 삭제 및 다음 값을 가져 올 수 있게 conponentIdx 증가 */
        //   conponentIdx = conponentIdx + 1;
        // } else {
        //   binding.value = 'false'
        //   outputConponent = null;
        //   conponentIdx = 0;
        // }

        /* ################################################################################################################ */
      });


      /* 퀵리플라이 이쪽으로 들어올 수 있게끔 작업 */
      root.Vue.directive('botui-markdownQuick', function (el, binding) {
        if (binding.value == 'false') return;
        /* 임시 변수 값에 넣은 후 전역 변수에 있는 값은 초기 */
        if (quickReplyConponent != null && quickReplyConponent.length != quickReplyConponentIdx) {
          /* 1. 순서대로 처리 */
          var quickReplyConponentTmp = quickReplyConponent[quickReplyConponentIdx].quick;
          el.innerHTML = _parseQuick(quickReplyConponentTmp);
          /* 2. 해당 배열에 들어가 있는 기록 삭제 및 다음 값을 가져 올 수 있게 conponentIdx 증가 */
          quickReplyConponentIdx = quickReplyConponentIdx + 1;
        } else {
          binding.value = 'false'
          quickReplyConponent = null;
          quickReplyConponentIdx = 0;
        }
        /* ################################################################################################################ */
      });

      /* ======================END output형 출력에 필요한 부분 세팅하기 위해 내가 추가한 부분 END======================== */
      root.Vue.directive('botui-scroll', {
        inserted: function (el) {
          _container.scrollTop = _container.scrollHeight;
          el.scrollIntoView(true);
        }
      });

      root.Vue.directive('focus', {
        inserted: function (el) {
          el.focus();
        }
      });

      root.Vue.directive('botui-container', {
        inserted: function (el) {
          _container = el;
        }
      });

      /* _botuiComponent 컴포넌트를 bot-ui라는 명의 전역변수로 생성 */
      _botApp = new root.Vue({
        components: {
          'bot-ui': _botuiComponent
        }
      }).$mount('#' + id);
      /* _botApp 으로 생성된 객체의 첫번째 인자를 인스턴스로 만듦 */
      var _instance = _botApp.$children[0]; // to access the component's data

      /* 여기서 부터 메세지를 출력 세팅하는 곳 */
      function _addMessage(_msg) {
        /* if (!_msg.loading && !_msg.content) {
          throw Error('BotUI: "content" is requir/.ed in a non-loading message object.');
        } */

        _msg.type = _msg.type || 'text';
        _msg.visible = (_msg.delay || _msg.loading) ? false : true;
        var _index = _instance.messages.push(_msg) - 1;

        return new Promise(function (resolve, reject) {
          /* script를 import */
          /* 로딩 시간 있이 보여줄 지 말지 결정하는 부분 */
          setTimeout(function () {
            if (_msg.delay) {
              _msg.visible = true;

              if (_msg.loading) {
                _msg.loading = false;
              }
            }
            resolve(_index);
          }, _msg.delay || 0);
        });
      }
      /* ======================output형 출력에 필요한 부분 세팅하기 위해 내가 추가한 부분 시작============================= */
      /* 여기서 부터 메세지를 출력 세팅하는 곳 */
      /* 전역변수를 선언해 그 변수를 순서 확인 4번으로 넘겨줌 */
      var outputConponent = [];
      var quickReplyConponent = [];
      var quickReplyConponentIdx = 0;

      function _addOutputMessage(_msg) {
        outputConponent.push(_msg);


        /* if (!_msg.loading && !_msg.content) {
          throw Error('BotUI: "content" is requir/.ed in a non-loading message object.');
        } */

        /* type따라 정리 값이 달라짐 조건문 추가 */
        if (_msg.type == 'multiple') {
          _msg.type = _msg.type || 'multiple';
        } else {
          _msg.type = _msg.type || 'single';
        }
        _msg.visible = (_msg.delay || _msg.loading) ? false : true;


        /* 이미지 ,라벨, 버튼 세팅 */
        /*         _msg.imgSrc = _msg.imgSrc;
                _msg.label = _msg.label; */
        /* 카드형 리스트에 추가된 버튼 값 가져오기 */
        /* 버튼 넘어 왔을 때 처리 */
        /* for (var i = 0; i < _msg.button.length; i++) {
          if (_msg.button[i].text != null && _msg.buttoin[i].value != null) {
          }
        } */
        var _index = _instance.messages.push(_msg) - 1;

        /* 로딩 시간 있이 보여줄 지 말지 결정하는 부분 
        로딩이 없을 경우, 그대로 새로운 Promoise 객체를 생성 후 반환*/
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            if (_msg.delay) {
              _msg.visible = true;

              if (_msg.loading) {
                _msg.loading = false;
              }
            }
            resolve(_index);
          }, _msg.delay || 0);
        });
      }

      function _addQuickMessage(_msg) {
        quickReplyConponent.push(_msg);


        /* if (!_msg.loading && !_msg.content) {
          throw Error('BotUI: "content" is requir/.ed in a non-loading message object.');
        } */

        _msg.type = _msg.type || 'quick';
        _msg.visible = (_msg.delay || _msg.loading) ? false : true;


        /* 이미지 ,라벨, 버튼 세팅 */
        /*         _msg.imgSrc = _msg.imgSrc;
                _msg.label = _msg.label; */
        /* 카드형 리스트에 추가된 버튼 값 가져오기 */
        /* 버튼 넘어 왔을 때 처리 */
        /* for (var i = 0; i < _msg.button.length; i++) {
          if (_msg.button[i].text != null && _msg.buttoin[i].value != null) {
          }
        } */
        var _index = _instance.messages.push(_msg) - 1;

        /* 로딩 시간 있이 보여줄 지 말지 결정하는 부분 
        로딩이 없을 경우, 그대로 새로운 Promoise 객체를 생성 후 반환*/
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            if (_msg.delay) {
              _msg.visible = true;
              if (_msg.loading) {
                _msg.loading = false;
              }
            }
            resolve(_index);
          }, _msg.delay || 0);
        });
      }

      /* ======================END output형 출력에 필요한 부분 세팅하기 위해 내가 추가한 부분 END ============================= */
      //유기훈-------------------------------- 
      var inputValue;
      var deviceType;
      var pushType;
      //var time = document.getElementById('botui-time');
     
      //---------------------
      function _checkOpts(_opts) {

        if (typeof _opts === 'string') {
          _opts = {
            content: _opts
          };
        }
        return _opts || {};
      }

      /* 출력 담당  */
      _interface.message = {
        add: function (addOpts) {
          return _addMessage(_checkOpts(addOpts));
        },
        bot: function (addOpts) {
            addOpts = _checkOpts(addOpts);
            return _addMessage(addOpts);
          }
          /* 카드형 출력에 필요한 부분 추가 */
          ,
        output: function (addOpts) {
          addOpts.chatbot = true;
          return _addOutputMessage(_checkOpts(addOpts));
        },
        quick: function (addOpts) {
          return _addQuickMessage(_checkOpts(addOpts));
        },
        human: function (addOpts) {
          addOpts = _checkOpts(addOpts);
          addOpts.human = true;
          return _addMessage(addOpts);
        },
        get: function (index) {
          return Promise.resolve(_instance.messages[index]);
        },
        remove: function (index) {
          _instance.messages.splice(index, 1);
          return Promise.resolve();
        },
        update: function (index, msg) { // only content can be updated, not the message type.
          var _msg = _instance.messages[index];
          _msg.content = msg.content;
          _msg.visible = !msg.loading;
          _msg.loading = !!msg.loading;
          return Promise.resolve(msg.content);
        },
        removeAll: function () {
          _instance.messages.splice(0, _instance.messages.length);
          return Promise.resolve();
        }
      };

      /* 입력 담당  */

      /* 역할
      1. 입력을 받는다.
      2. 버튼을 보여준다.
      3. selectbox와 같은 작업을 보여준다.
       */
      function mergeAtoB(objA, objB) {
        for (var prop in objA) {
          if (!objB.hasOwnProperty(prop)) {
            objB[prop] = objA[prop];
          }
        }
      }

      function _checkAction(_opts) {
        if (!_opts.action && !_opts.actionButton && !_opts.actionText) {
          throw Error('BotUI: "action" property is required.');
        }
      }

      function _showActions(_opts) {
        _checkAction(_opts);

        /* autoHide 
        true : 버튼일 경우, 한 번 누르고 삭제 된다.
        false : 버튼을 누르고도 사라지지 않는다. */
        mergeAtoB({
          type: 'text',
          cssClass: '',
          autoHide: false,
          addMessage: true
        }, _opts);

        _instance.action.type = _opts.type;
        _instance.action.cssClass = _opts.cssClass;
        _instance.action.autoHide = _opts.autoHide;
        _instance.action.addMessage = _opts.addMessage;

        return new Promise(function (resolve, reject) {
          _actionResolve = resolve; // resolved when action is performed, i.e: button clicked, text submitted, etc.
          setTimeout(function () {
            _instance.action.show = true;
          }, _opts.delay || 0);
        });
      };
      _interface.action = {
        show: _showActions,
        hide: function () {
          _instance.action.show = true;
          return Promise.resolve();
        },
        text: function (_opts) {
          /* 입력창  */
          _checkAction(_opts);
          _instance.action.text = _opts.action;
          return _showActions(_opts);
        },
        button: function (_opts) {
          /* 버튼 출력 창 */
          _checkAction(_opts);
          _opts.type = 'button';
          _instance.action.button.buttons = _opts.action;
          return _showActions(_opts);
        }
      };

      if (_options.fontawesome) {
        loadScript(_fontAwesome);
      }

      if (_options.searchselect) {
        loadScript(_searchselect, function () {
          Vue.component('v-select', VueSelect.VueSelect);
        });
      }

      /* carousel을 위한 script */
      if (_options.slickCarousel) {
        loadScriptSlickCarouselRun();
        loadQuickSlickRun();
      }

      if (_options.response) {
        firstResponseSetting();
        responseSetting();
        deviceCheck();
        
        
      }
      /* 캐로셀 만드는 곳 처음 로딩 될 때 1회 생성 */
      function loadScriptSlickCarouselRun() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        //  script.innerHTML = '$(document).on(\'ready\', function() {$(\".regular\").slick({dots : false,infinite : false,slidesToShow : 1.5,slidesToScroll : 1});});';
        script.innerHTML = ' function initSlider() {$(\".regular\").not(\'.slick-initialized\').slick({dots : false,arrows:true,infinite : false,slidesToShow : 1.028,slidesToScroll : 1})};';
        document.body.appendChild(script);
      }
      
      function loadQuickSlickRun() {
          var script = document.createElement('script');
          script.type = 'text/javascript';
          //  script.innerHTML = '$(document).on(\'ready\', function() {$(\".regular\").slick({dots : false,infinite : false,slidesToShow : 1.5,slidesToScroll : 1});});';
          script.innerHTML = ' function initQuickSlider() {$(\".quickRegular\").not(\'.slick-initialized\').slick({dots : false,arrows:true,infinite : false,slidesToShow: 1.845,slidesToScroll : 2})};';
          document.body.appendChild(script);
        }

      // -------------------- 기훈 추가
      function firstResponseSetting() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.innerHTML = 'function UpResponse_first() {axios.get("/first/").then(function(response) {var arrList = response.data; console.log(JSON.stringify(arrList)); var len = arrList.length; for (var i = 0; i < len; i++) { if (arrList[i].type == "multiple") {botui.message.output(arrList[i]).then(function(res) {initSlider(); initQuickSlider();})} else {botui.message.output(arrList[i]).then(function(res) {   })}} })}';
        document.body.appendChild(script);
      }
      // -------------------- 기훈 추가
      function responseSetting() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.innerHTML = 'function input() {botui.action.text({ action : { placeholder : "Enter your text here" }}).then(function(res) {var chk = deviceCheck(); console.log(JSON.stringify(res)); console.log(chk+"체크체크"); var typeValue = res.value; var findHttp = "http";var regExp = /\d{2}-\d{3}/;var regExp_2 = /\d{2}-\d{3,4}-\d{3,4}/;var regExp_3 = /\d{4}-\d{4}/;console.log(regExp.test(typeValue));console.log(regExp_2.test(typeValue));console.log(regExp_3.test(typeValue)); if(typeValue.indexOf(findHttp) != -1){ var link = res.value; location.href = link; }else if(regExp.test(typeValue) == true){ location.href = "tel :" + typeValue;}else if(regExp_2.test(typeValue) == true){location.href = "tel :" + typeValue; }else if(regExp_3.test(typeValue) == true){ location.href = "tel :" + typeValue; }else{ axios.get("/sentence/",{params : {inputValue : res.value ,deviceType : chk ,pushType : res.type } } ).then( function(response){var arrList = response.data;var len = arrList.length; for(var i = 0; i < len; i++) {if(arrList[i].type == "multiple") {botui.message.output(arrList[i]).then( function(res) {input(); initSlider(); initQuickSlider();})}else if(arrList[i].type == "single"){ botui.message.output(arrList[i]).then( function(res) { input(); initQuickSlider(); })}else{botui.message.output(arrList[i]).then( function(res) {input();})}}})}});};'
        document.body.appendChild(script);
      }


      function deviceCheck() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.innerHTML = 'function deviceCheck() {var pc_device = "win16|win32|win64|mac|macintel";var deviceType = ""; var this_device = navigator.platform;if (this_device) {if (pc_device.indexOf(navigator.platform.toLowerCase()) < 0) {if (/Android/i.test(navigator.userAgent)) {deviceType = \'android\';} else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {deviceType = \'ios\';} else {deviceType = \'mobile\';}} else {deviceType = \'pc\';}}console.log("버튼 눌러서 일단 실행 됨 " + deviceType); return deviceType;};'
        document.body.appendChild(script);
      }
      
     
      
      if (_options.debug) {
        _interface._botApp = _botApp; // current Vue instance
      }

      return _interface;
    });

    return BotUI;

  }));