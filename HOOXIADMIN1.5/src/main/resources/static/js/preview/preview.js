function fnPreview(){
	
	/*List = ['BC','SI','ST'] 같은 리스트형 데이터*/
	var resTypeList = fnResTypeList();
	
	/*응답 타입마다 카드를 몇개나 가지고 있는지 List=['5','6','1']*/
	var caroselCount = fnCaroselCount(resTypeList);
	
	
	console.log(resTypeList);
	/*응답 타입 값으로 순환*/
	resTypeList.forEach(function(e,index){
		switch(e){
			case "ST":
				fnTextPreview(index);
				break;
			case "SI":
				fnImagePreview(index);
				break;
			case "BC":
				fnCardPreview(caroselCount,index);			
				break;
			case "LC":
				fnListPreview(index);
				break;
			default:
				break;
		}
	})
	fnQuickReply();
	/*slick js 실행*/
	create_initSlider();
	
	/*단일 말풍선의 경우 화살표 제거*/
	slick_remove_arrow(caroselCount);

}

/*응답 타입 1,2,3이 가지고있는 카드형 개수 구하는 함수*/
function fnCaroselCount(resTypeList){
	/*응답 타입을 몇개나 사용하는지?*/
	var resLength = resTypeList.length;
	var caroselCount;
	var countList = [];
	for(var i=1; i<resLength+1; i++){
		caroselCount = "#bcCarouselList_"+i
		caroselCount = $(caroselCount).children().length;
		countList.push(caroselCount);
	}
	return countList;
}

/*응답 타입을 몇개 사용하는지 구하는 함수*/
function fnResTypeList(){
	var resTypeList = [];
	caroselResponseHtml();
	for (var i=0; i<3; i++){
		var optionSelect = "#resType_"+i+" "+"option:selected";
		 
		var resType = $(optionSelect).val();
		if(resType!=""){
			resTypeList.push(resType);
			
			/* 슬릭 사용을 위한 응답 타입별 섹션 생성*/
			previewResponse();
		}
			
			
	}
	return resTypeList;
}

/*caroselCount는 ['1','2','4'] 형태의 캐로셀 카드형 개수, index는 응답타입의 인덱스 값*/
function fnCardPreview(caroselCount,index){
	/*타입마다 카드형이 몇개 인지 담겨있는 리스트에서 값 꺼내는 부분*/
	caroselCount = caroselCount[index];
	
	for(var j=1; j<=caroselCount; j++){
		previewCardAppend(index);
		previewCardImgSet(index,j);
		previewCardDesSet(index,j);
		previewCardButtonSet(index,j);
	}
}

function fnTextPreview(index){
	previewSimpleTextAppend(index);
}

function previewSimpleTextAppend(i){
		var simpleText = "textarea[name=simpleTextVO\\["+i+"\\]\\[text\\]]"
		simpleText = $(simpleText).val()
		
		var responseHtml = 					'<div name=\"previewSimpleText\">';
			responseHtml += 						'<div name=\"individualSimpleText\" class=\"botui-message-content-single\">';
			responseHtml += 							'<span name=\"simpleTextResponse\">';
			responseHtml += 								"<div style=\"width:102%; white-space: break-spaces;\" name=\"simpleTextDesc\">"+simpleText+"</div>";
			responseHtml += 							'</span>';
			responseHtml += 						'</div>';
			responseHtml += 					'</div>';
		
		//미리보기에 카드 추가
		$('div[name=responseBundle]').eq(i).append(responseHtml);
		
	}


function fnImagePreview(index){
	previewSimpleImageAppend(index);
}

function previewSimpleImageAppend(i){
		
		var simpleImage = "#r"+(i+1)+"si_url";
		console.log(i+"i값은")
		simpleImage = $(simpleImage).val();
		console.log(simpleImage);
		var responseHtml = 						'<div name=\"previewSimpleImage\">';
			responseHtml += 						'<div name=\"individualSimpleImage\" class=\"botui-message-content-simple-image\">';
			responseHtml += 							'<span name=\"simpleImageResponse\">';
			responseHtml += 								'<img name=\"simpleImg\" src=\"\" class=\"botui-message-content-simple-img\">';
			responseHtml += 							'</span>';
			responseHtml += 						'</div>';
			responseHtml += 					'</div>';
			
			//미리보기에 카드 추가
		$('div[name=responseBundle]').eq(i).append(responseHtml);
		
		$('div[name=responseBundle]').eq(i).find('img[name=simpleImg]').attr('src',simpleImage);
		
}


/*리스트형 답변 미리포기 세팅*/
function fnListPreview(index){

	var listCount = "#lcListPage_"+(index+1);
	listCount = $(listCount).children().length;
	console.log(listCount+"qq");
	
	
	/*리스트 카드형 미리보기 제목 세팅*/
	listCardTitleSet(index);
	
	for(var i=0; i<listCount; i++){
		console.log(i+"i값");
		console.log(index+"index값");
		listCardResponseSet(index);
		listCardImgSet(index,i);
		listCardDescSet(index,i);
	}
	listCardButtonSet(index,listCount);
}

/*리스트카드형 제목 세팅*/
function listCardTitleSet(i){
	var responseHtml = '<div name=\"previewListCardTitle\">'
		responseHtml += 	'<div name=\"listCardTitle\" class=\"botui-message-content-single\">'
		responseHtml += 		'<span name=\"listCardResponse\">'
		responseHtml += 			'<div style=\"position:relative; top:6px;\" name=\"listCardTitleDesc\">'
		responseHtml += 			'</div>'
		responseHtml += 		'</span>'
		responseHtml += 	'</div>'
		responseHtml += '</div>'
		
		$('div[name=responseBundle]').eq(i).append(responseHtml);
		
	var listCardTitle= "input[name=listCardVO\\["+i+"\\]\\[headerVO\\]\\[title\\]]";
	listCardTitle = $(listCardTitle).val();
	$('div[name=responseBundle]').eq(i).find('div[name=listCardTitleDesc]').text(listCardTitle);
		
}

/*리스트카드형 말풍선 구조 세팅*/
function listCardResponseSet(i){
	var responseHtml = '<div name=\"previewListCard\">'
	responseHtml += 	'<div style=\"float:left;\" name=\"individualListCard\" class=\"botui-message-content-list\">'
	responseHtml += 		'<span name=\"listResponse\">'
	responseHtml += 			'<div style=\"\" name=\"listCardForm\">'
	responseHtml += 				'<div>'
	responseHtml +=				 		'<div style=\"float:left; width:55%; position:relative; top:17px;\">'
	responseHtml +=				 			'<div style=\"width:100%;\">'
	responseHtml +=						 		'<div name=\"listCardMainDesc\" style=\"\">'
	responseHtml +=							 		'축구장,농구장,탁구장,야구장 등 (총 15개)'
	responseHtml +=						 		'</div>'
	responseHtml +=					 		'</div>'
	responseHtml +=					 		'<div style=\"width:100%;\">'
	responseHtml +=						 		'<div name=\"listCardDesc\" style=\"\">'
	responseHtml +=							 		'내용16자'
	responseHtml +=						 		'</div>'
	responseHtml +=					 		'</div>'
	responseHtml += 					'</div>'
	responseHtml +=				 		'<div style=\"float:left; width:30%;\">'
	responseHtml +=					 		'<img name=\"listCardImg\" style=\"width:78%; height:63px; position:relative; left:73%; top:7px;\" src=\"/manager/img/manage.png\"/>'
	responseHtml +=				 		'</div>'
	responseHtml +=			 		'</div>'
	responseHtml +=		 		'</div>'
	responseHtml += 		'</span>'
	responseHtml += 	'</div>'
	responseHtml += '</div>'
	
	$('div[name=responseBundle]').eq(i).append(responseHtml);
	$('div[name=responseBundle]').eq(i).css('display','inline-block');
}

/*리스트카드 미리보기 이미지 세팅하는 부분으로, i는 응답타입의 인덱스값, j는 리스트 목록중 몇번째인지 값*/
function listCardImgSet(i,j){
	var listCardImg = "input[name=listCardVO\\["+i+"\\]\\[itemsVO\\]\\["+j+"\\]\\[imageUrl\\]]";
	listCardImg = $(listCardImg).val();
	
	$('div[name=responseBundle]').eq(i).find('img[name=listCardImg]').eq(j).attr('src',listCardImg);
}

function listCardDescSet(i,j){
	var listCardMainDesc = "input[name=listCardVO\\["+i+"\\]\\[itemsVO\\]\\["+j+"\\]\\[title\\]]";
	listCardMainDesc = $(listCardMainDesc).val();
	$('div[name=responseBundle]').eq(i).find('div[name=listCardMainDesc]').eq(j).text(listCardMainDesc);
	
	var listCardDesc = "input[name=listCardVO\\["+i+"\\]\\[itemsVO\\]\\["+j+"\\]\\[description\\]]";
	listCardDesc = $(listCardDesc).val();
	$('div[name=responseBundle]').eq(i).find('div[name=listCardDesc]').eq(j).text(listCardDesc);
	$('div[name=responseBundle]').eq(i).find('div[name=listCardDesc]').eq(j).css('color','#C5C5C3');
}


/* 버튼 내용 세팅해주는 부분 i는 응답타입의 index값, j는 카드형의 순서값, k는 리스트목록 전체개수 */
function listCardButtonSet(i,k){
	/*버튼의 개수*/
	var buttonCount = "#lcBtnList_"+i;
	buttonCount = $(buttonCount).children().length;
	console.log(buttonCount+"버튼 개수");
	console.log(k+"리스트 목록 개수");
	var responseHtml = 			'<div class=\"listButtons\">';
	responseHtml += 			'</div>';
	
	$('div[name=responseBundle]').eq(i).find('div[name=previewListCard]').eq(k-1).find('span[name=listResponse]').append(responseHtml);
	
	for(var k=0; k<buttonCount; k++){
		var buttonLabel = "input[name=listCardVO\\["+i+"\\]\\[buttonVO\\]\\["+k+"\\]\\[label\\]]";
		buttonLabel = $(buttonLabel).val();
		console.log(buttonLabel);
		var buttonHtml = "<button name=\"ButtonLabel\" class=\"botui-output-buttons-button\">"+buttonLabel+"</button>"
		$('div[name=responseBundle').eq(i).find('.listButtons').append(buttonHtml);
		
		if(buttonCount==2){
			$('div[name=responseBundle').eq(i).find('.listButtons').children().attr('class','botui-output-buttons-list-button');
			
		}
		
	}
}





function fnQuickReply(){
	previewQuickReply();
}
/*######################## 슬릭관련 함수 모음 시작 ########################*/
/*슬릭 js를 장착해주는 함수*/
function create_initSlider() {
	$(".createRegular").not('.slick-initialized').slick({
			dots : false
			,arrows:true
			,infinite : false
			,slidesToShow : 0.94
			,slidesToScroll : 1
		})
		
	$(".createQuick").not('.slick-initialized').slick({
					dots : false
					,arrows:true
					,infinite : false
					,slidesToShow : 1.45
					,slidesToScroll : 1
		})	
};
	
/*슬릭 js를 제거해주는 함수*/
function remove_initSlider(){
	$(".createRegular").slick("unslick");
	$(".createQuick").slick("unslick");
}

function slick_remove_arrow(caroselCount){
	caroselCount.forEach(function(e,index){
		if(e==1){
			$('div[name=responseBundle]').eq(index).find('.slick-next.slick-arrow').hide();
			$('div[name=responseBundle]').eq(index).find('.slick-prev.slick-arrow.slick-disabled').hide();
		}
	})

}
/*########################슬릭 관련 함수 모음 끝########################*/

/*기본적인 미리보기 구조 생성*/
function caroselResponseHtml(){
	$('.preview').empty();
	$('.response-create-design-form').remove();
	
	var responseHtml = '<div id=\"hello-world\" class=\"create-botui-app-container\">';
		responseHtml += '<div class=\"botui botui-container\">';
		responseHtml += 	'<div id=\"message-container\" class=\"botui-messages-container\">';
		responseHtml += 		'<div name=\"botui-create-message-response\" class=\"botui-message\">';
		responseHtml += 			'<div id=\"previewRes\">';
		responseHtml += 				'<div class=\"profil profile agent\">';
		responseHtml += 					'<img class=\"agent\" src=\"/manager/img/manage.png\">';
		responseHtml += 				'</div>';
		responseHtml += 				'<div class=\"botui-message-name text\">';
		responseHtml += 					'<span>서울톡</span><span id=\"botui-time\"><a></a></span>';
		responseHtml += 				'</div>';
		responseHtml += 			'</div>';
		responseHtml += 		'</div>';
		responseHtml += 		'<div name=\"botui-create-message-response\" class=\"botui-message\">';
		responseHtml += 			'<div style=\"\">';
		responseHtml += 				'<div class=\"botui-quick-buttons\">';
		responseHtml += 					'<section id=\"quickReplies\" class=\"createQuick\"></section>';
		responseHtml += 				'</div>';
		responseHtml += 			'</div>';
		responseHtml += 		'</div>';
		responseHtml += 	'</div>';
		responseHtml += 	'<div class=\"botui-actions-container\">';
		responseHtml += 	'</div>';
		responseHtml += '</div>';
		responseHtml += '</div>';
	
	
	
	$('.preview').append(responseHtml);
	/*create_initSlider();*/
}



/*카드형 추가하는 부분*/
function previewCardAppend(i){
		var responseHtml = 					'<div name=\"previewCreateCaroselCard\">';
			responseHtml += 						'<div name=\"caroselIndividualCard\" class=\"botui-message-content-multiple\">';
			responseHtml += 							'<span name=\"caroselResponse\">';
			responseHtml += 								'<img name=\"caroselIndividualImg\" src=\"\" class=\"botui-message-content-image\">';
			responseHtml += 								'<div style=\"width:102%; white-space: break-spaces;\" name="caroselTitle"></div>';
			responseHtml += 								'<div style=\"width:102%; white-space: break-spaces;\" name="caroselDesc"></div>';
			responseHtml += 								'<div class=\"caroselButtons\">';
			responseHtml += 								'</div>';
			responseHtml += 							'</span>';
			responseHtml += 						'</div>';
			responseHtml += 					'</div>';
		
		//미리보기에 카드 추가
		$('.createRegular').eq(i).append(responseHtml);
		/*create_initSlider();*/
}

/*slick js를 위한 섹션 생성하는 함수*/
function previewResponse(){
	var responseHtml = 				'<div name=\"responseBundle\">';
		responseHtml += 				'<section class=\"createRegular\">';
		responseHtml += 				'</section>';
		responseHtml += 			'</div>';
	$('#previewRes').append(responseHtml);
}

/*i는 응답타입의 index값, j는 카드형의 순서값 */
function previewCardImgSet(i,j){
	var image = "#r"+(i+1)+"bc"+j+"_url";
		image = $(image).val();
	
	var buttonCount = "#bcCsBtnList_"+i+"_"+(j-1);
	buttonCount = $(buttonCount).children().length;
	console.log(buttonCount);
	
	
		/*이미지가 없는 경우 예외처리*/
		if(image==""){
			$('.createRegular').eq(i).find("img[name=caroselIndividualImg]").eq(j-1).hide();
			/*$('.createRegular').eq(i).find('div[name=caroselIndividualCard]').eq(j-1).css('height','295px');*/
			/*switch(buttonCount){
					case 1:
						$('.createRegular').eq(i).find('.caroselButtons').eq(j-1).css('margin-top','98%');
						break;
					case 2:
						$('.createRegular').eq(i).find('.caroselButtons').eq(j-1).css('margin-top','81%');
						break;
					case 3:
						$('.createRegular').eq(i).find('.caroselButtons').eq(j-1).css('margin-top','63%');
						break;
				}*/
			
		}else{
			$('.createRegular').eq(i).find("img[name=caroselIndividualImg]").eq(j-1).attr('src',image);
		}
}

/*상세 내용 세팅해주는 부분 i는 응답타입의 index값, j는 카드형의 순서값 */
function previewCardDesSet(i,j){

	var title = "input[name=basicCardVO\\["+i+"\\]\\["+(j-1)+"\\]\\[title\\]]"
	title = $(title).val();
	console.log(title+"gg");
	console.log(i+"i값은");			
	var desc = "textarea[name=basicCardVO\\["+i+"\\]\\["+(j-1)+"\\]\\[description\\]]"
	console.log(desc);		
	desc = $(desc).val();
	if(title==""){
		
	}else{
		
		$('.createRegular').eq(i).find("div[name=caroselDesc]").eq(j-1).css('color','#C5C5C3');
	}
	
	$('.createRegular').eq(i).find("div[name=caroselTitle]").eq(j-1).text(title);
	$('.createRegular').eq(i).find("div[name=caroselDesc]").eq(j-1).text(desc);
}

/* 카드형 버튼 내용 세팅해주는 부분 i는 응답타입의 index값, j는 카드형의 순서값 */
function previewCardButtonSet(i,j){
	/*버튼의 개수*/
	var buttonCount = "#bcCsBtnList_"+i+"_"+(j-1);
	buttonCount = $(buttonCount).children().length;
	for(var k=0; k<buttonCount; k++){
		var buttonLabel = "input[name=basicCardVO\\["+i+"\\]\\["+(j-1)+"\\]\\[buttonVO\\]\\["+k+"\\]\\[label\\]]";
		buttonLabel = $(buttonLabel).val();
		var buttonHtml = "<button name=\"ButtonLabel\" class=\"botui-output-buttons-button\">"+buttonLabel+"</button>"
		$('.createRegular').eq(i).find('.caroselButtons').eq(j-1).append(buttonHtml);
		
		
	}
}

/*퀵 리플라이 세팅하는 부분*/
function previewQuickReply(){
	var quickReplyCount = $('#qrPageList').children().length;
	var quickValLengthList = [];
	for(var i=0; i<quickReplyCount; i++){
		var quickReplyLabel = "input[name=quickRepliesVO\\["+i+"\\]\\[label\\]]"
		quickReplyLabel = $(quickReplyLabel).val();
		quickValLengthList.push(quickReplyLabel.length);
		var responseHtml = '<button style=\"white-space: nowrap;\" name=\"createQuickReply\" type=\"button\" class=\"botui-actions-buttons-button\">'+quickReplyLabel+'</button>'
		$('#quickReplies').append(responseHtml);
	}
	return quickValLengthList;
	
}