//값 변경시 유효성 확인
function fnTextvalidRemove(i){
	//text값 변경해주기
	var byteChk = $("input[name='byteChk[basicCard]["+i+"]']");
	
	var idx = $(".btn.btn-white.active.bcCsCnt"+(i+1));
	
	if(idx.length == 0){
		idx = 0;
	}else{
		idx = $(".btn.btn-white.active.bcCsCnt"+(i+1))[0].outerText-1;
	}
	var responseList = $('#bcCarouselList_'+(i+1))[0].children.length;						//캐러셀 체크
	var urlChk = $("input[name='basicCardVO["+i+"]["+idx+"][imageUrlLink]']").val().length;		//이미지 url 체크
	var titleChk = $("input[name='basicCardVO["+i+"]["+idx+"][title]").val().length;			//제목 체크
	var txt = $("textarea[name='basicCardVO["+i+"]["+idx+"][description]']");
	
	var byteType = byteChk.eq(idx);
	
	//캐러셀 응답일 경우
	if(responseList >1){
		//이미지가 없는 경우
		if(urlChk == 0){
			if(txt.val().length>BCCLEN){
				txt.css('border-color','red');
				txt.val(txt.val().substring(0,BCCLEN));	
			}else{
				txt.css('border-color','inherit');
			}
			byteType.val(txt.val().length+"/"+BCCLEN);
		}else{					//이미지가 있는 경우
			if(titleChk == 0){	//제목이 없는경우
				if(txt.val().length>BCICLEN){
					txt.css('border-color','red');
					txt.val(txt.val().substring(0,BCICLEN));	
				}else{
					txt.css('border-color','inherit');
				}
				byteType.val(txt.val().length+"/"+BCICLEN);
			}else{				//제목이 있는 경우
				if(txt.val().length>BCICTLEN){
					txt.css('border-color','red');
					txt.val(txt.val().substring(0,BCICTLEN));	
				}else{
					txt.css('border-color','inherit');
				}
				byteType.val(txt.val().length+"/"+BCICTLEN);	
			}
		}
	}else{		//단일 응답인 경우
		if(urlChk == 0){		//이미지가 없는 경우
			if(txt.val().length>BCLEN){
				txt.css('border-color','red');
				txt.val(txt.val().substring(0,BCLEN));	
			}else{
				txt.css('border-color','inherit');
			}
			byteChk.val(txt.val().length+"/"+BCLEN);
		}else{					//이미지 있는 경우
			if(txt.val().length>BCILEN){
				txt.css('border-color','red');
				txt.val(txt.val().substring(0,BCILEN));	
			}else{
				txt.css('border-color','inherit');
			}
			byteChk.val(txt.val().length+"/"+BCILEN);
		}
	}
}

//응답 유형 선택시 유효성 확인
function fnTextvalidChk(cnt,type){
	var caseRespons;
	var len;
	switch (type) {
	case "ST"://Simple Text
		//일반 텍스트 응답으로 웹에서는 단일 응답만 가능
		var byteChk = $("input[name='byteChk[simpleText]']");
		caseRespons = $("#content_st_"+cnt);
		len = $("textarea[name='simpleTextVO["+cnt+"][text]']").val().length;
		byteChk.val(len+"/"+STLEN);
		break;
	case "SI"://Simple Image
		//이미지 URL 없을시 altText 부분 고정
		var byteChk = $("input[name='byteChk[simpleImage]']");
		caseRespons = $("#content_si_"+cnt);
		len = $("textarea[name='simpleImageVO["+cnt+"][altText]']").val().length;
		byteChk.val(len+"/"+STLEN);
		break;
	case "BC"://Basic Card
		fnTextvalidRemove(cnt);
		break;
	case "CC"://Commerce Card
		//사용안함
		caseRespons = $("#content_cc_"+cnt);
		break;
	case "LC"://List Card
		caseRespons = $("#content_lc_"+cnt);
		var listTitle = $("input[name='listCardVO["+cnt+"][itemsVO][0][title]']");			//제목 길이 제한
		var listDes = $("input[name='listCardVO["+cnt+"][itemsVO][0][description]']");		//제목 길이 제한
		if(listDes.val().length == 0){
			listTitle.attr("maxlength",LCLEN);
		}else{
			listTitle.attr("maxlength",LCTLEN);
			listTitle.val(listTitle.val().substring(0,LCTLEN));
		}
		break;
	}
}

//리스트 카드 전용 유효성 확인
function listCardVaild(i){
	var list = $(".btn.btn-white.active.lcCnt"+i);
	var idx = 0;
	if(idx.length != 0){
		idx = list[0].outerText-1;
	}
	var listTitle = $("input[name='listCardVO["+i+"][itemsVO]["+idx+"][title]']");
	var listDescription = $("input[name='listCardVO["+i+"][itemsVO]["+idx+"][description]']");
	
	if(listDescription[0].value.length == 0){
		listTitle[0].maxLength = 35;
	}else{
	var txt = listTitle.eq(0);
		if(txt.val().length>30){
			txt.css('border-color','red');
			txt.val(txt.val().substring(0,30));	
		}else{
			txt.css('border-color','inherit');
		}
		listTitle[0].maxLength = 30;
	}
}

//버튼 유효성 체크
function fnChangedBtn(id,i,j,k,type){
	var sel;
	var link;
	
	if(id == "bc"){
		sel = $("select[name='basicCardVO["+i+"]["+j+"][buttonVO]["+k+"][action]'] option:selected");
		link = $("input[name ='basicCardVO["+i+"]["+j+"][buttonVO]["+k+"][link]']");
	}else if(id == "lc"){
		sel = $("select[name='listCardVO["+i+"][buttonVO]["+k+"][action]'] option:selected");
		link = $("input[name ='listCardVO["+i+"][buttonVO]["+k+"][link]']");
	}
	
	if(type == 0){
		link.eq(0).val("");
	}
	
	switch (sel.val()) {
	case "bl":
		link.eq(0).attr("maxlength",-1);
		break;
	case "ul":
		link.eq(0).attr("maxlength",-1);
		break;
	case "tl":
		link.eq(0).attr("maxlength",20);
		break;
	case "ms":
		link.eq(0).attr("maxlength",14);
		break;
	case "al":
		link.eq(0).attr("maxlength",-1);
		break;
	}
}
