/* manage | common */

/**
 * 테이블의 소팅기능 세팅
 */
function wn_setSortTable() {
	var body = $("body");
	var sortFunc = null;
	$("table[sortFunc][sortLoaded!=true]", body).each(function(){
		sortFunc = $(this).attr("sortFunc");
		try {
			sortFunc = eval(sortFunc);
			var tableObj = $(this);
			tableObj.find("th[sortKey]").each(function(){
				var thisObj = $(this);
				if (!thisObj.attr("sortDrct")) {
					thisObj.removeClass();
					thisObj.addClass("sorting");
				} else {
					thisObj.removeClass();
					if (thisObj.attr("sortDrct") == "ASC") {
						thisObj.addClass("sorting_asc");
					} else {
						thisObj.addClass("sorting_desc");
					}
				}
				thisObj.click(function(){
					var drct = "ASC";
					
					if (thisObj.hasClass("sorting")) {
						drct = "DESC";
						thisObj.removeClass();
						thisObj.addClass("sorting_desc");
					} else if(thisObj.hasClass("sorting_asc")) {
						drct = "DESC";
						thisObj.removeClass();
						thisObj.addClass("sorting_desc");
					} else {
						drct = "ASC";
						thisObj.removeClass();
						thisObj.addClass("sorting_asc");
					}
					
					sortFunc.call(this, $(this).attr("sortKey"), drct);
					thisObj.attr("sortDrct", drct);
				});
			});
		} catch(e) {
			alert("테이블 정렬을 위한 ["+sortFunc+"] 가 정의되지 않았습니다.");
		}
		$(this).attr("sortLoaded", true);
	});
}

/**
 * 모달팝업 호출
 * @param jsonObj
 * @param paramObj
 */
var wn_modalUserCallBackFunctionNm
function wn_modalPop(jsonObj, paramObj) {
	
	var divObj = $("#cmmModalDiv");
	if (divObj.length == 0) {
		alert("팝업을 사용할 수 없습니다. LAYOUT 을 확인하세요.");
		return;
	}
	
	// default option setting
	var opts = $.extend({
		title: "manage 기본팝업",
		desc: "manage 기본팝업 설명입니다.",
		width: "70px",
		actUrl: "/",
		cbFunc:wnBlankFunction
	}, jsonObj);
	
	wn_modalUserCallBackFunctionNm = opts.cbFunc;
	
	$("#cmmModalTitle").text(opts.title);
	$("#cmmModalDesc").text(opts.desc);
	$("#cmmModalSubDiv").css("width", opts.width);

	if (typeof paramObj == "object") {
		paramObj = $(paramObj).serialize();
	}
	
	$(divObj).modal("show");
	
	$.ajax({
		url: opts.actUrl,
		type:'POST',
		cache: false,
		dataType: "html",
		data: paramObj,
		async: false,
        success: function(data) {
        	$("#popupViewDiv").html(data);
        	if (wn_modalUserCallBackFunctionNm != null) {
        		wn_modalUserCallBackFunctionNm.call(this);
        	}
		},
		error: function(e) {
			console.log(e);
		}
 	});
	
}

//Modal Close
function wn_modalClose() {
	$("#cmmModalDiv").modal("hide");
}

/**
 * 모달팝업 호출
 * @param jsonObj
 * @param paramObj
 */
var wn_modalUserCallBackFunctionNm2
function wn_modalPop2(jsonObj, paramObj) {
	
	var divObj = $("#cmmModalDiv2");
	if (divObj.length == 0) {
		alert("팝업을 사용할 수 없습니다. LAYOUT 을 확인하세요.");
		return;
	}
	
	// default option setting
	var opts = $.extend({
		title: "manage 기본팝업",
		desc: "manage 기본팝업 설명입니다.",
		width: "70px",
		actUrl: "/",
		cbFunc:wnBlankFunction
	}, jsonObj);
	
	wn_modalUserCallBackFunctionNm2 = opts.cbFunc;
	
	$("#cmmModalTitle2").text(opts.title);
	$("#cmmModalDesc2").text(opts.desc);
	$("#cmmModalSubDiv2").css("width", opts.width);
	
	if (typeof paramObj == "object") {
		paramObj = $(paramObj).serialize();
	}
	
	$(divObj).modal("show");
	
	$.ajax({
		url: opts.actUrl,
		type:'POST',
		cache: false,
		dataType: "html",
		data: paramObj,
		async: false,
		success: function(data) {
			$("#popupViewDiv2").html(data);
			if (wn_modalUserCallBackFunctionNm2 != null) {
				wn_modalUserCallBackFunctionNm2.call(this);
			}
		},
		error: function(e) {
			console.log(e);
		}
	});
	
}

//Modal Close
function wn_modalClose2() {
	fnTextvalidRemove(0);
	fnTextvalidRemove(1);
	fnTextvalidRemove(2);
	$("#cmmModalDiv2").modal("hide");
}

//Blank Function 
function wnBlankFunction() {	
}

String.prototype.endsWith = function(str){
	if (str) {
		if (this.length < str.length) { return false; }
		return this.lastIndexOf(str) + str.length == this.length;
	}
}

Map = function(){
	this.map = new Object();
};   
Map.prototype = {   
    put : function(key, value){   
        this.map[key] = value;
    },   
    get : function(key){   
        return this.map[key];
    },
    containsKey : function(key){    
     return key in this.map;
    },
    containsValue : function(value){    
     for(var prop in this.map){
      if(this.map[prop] == value) return true;
     }
     return false;
    },
    isEmpty : function(key){    
     return (this.size() == 0);
    },
    clear : function(){   
     for(var prop in this.map){
      delete this.map[prop];
     }
    },
    remove : function(key){    
     delete this.map[key];
    },
    keys : function(){   
        var keys = new Array();   
        for(var prop in this.map){   
            keys.push(prop);
        }   
        return keys;
    },
    values : function(){   
     var values = new Array();   
        for(var prop in this.map){   
         values.push(this.map[prop]);
        }   
        return values;
    },
    size : function(){
      var count = 0;
      for (var prop in this.map) {
        count++;
      }
      return count;
    }
};
