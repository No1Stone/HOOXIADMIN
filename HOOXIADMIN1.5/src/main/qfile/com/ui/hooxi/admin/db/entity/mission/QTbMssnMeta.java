package com.ui.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnMeta is a Querydsl query type for TbMssnMeta
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnMeta extends EntityPathBase<TbMssnMeta> {

    private static final long serialVersionUID = -2026708686L;

    public static final QTbMssnMeta tbMssnMeta = new QTbMssnMeta("tbMssnMeta");

    public final NumberPath<Long> mssnId = createNumber("mssnId", Long.class);

    public final StringPath prpsCd = createString("prpsCd");

    public QTbMssnMeta(String variable) {
        super(TbMssnMeta.class, forVariable(variable));
    }

    public QTbMssnMeta(Path<? extends TbMssnMeta> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnMeta(PathMetadata metadata) {
        super(TbMssnMeta.class, metadata);
    }

}

