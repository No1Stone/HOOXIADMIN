package com.ui.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnRecomPlace is a Querydsl query type for TbMssnRecomPlace
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnRecomPlace extends EntityPathBase<TbMssnRecomPlace> {

    private static final long serialVersionUID = 191516838L;

    public static final QTbMssnRecomPlace tbMssnRecomPlace = new QTbMssnRecomPlace("tbMssnRecomPlace");

    public final StringPath placeCd = createString("placeCd");

    public final NumberPath<Long> recomId = createNumber("recomId", Long.class);

    public QTbMssnRecomPlace(String variable) {
        super(TbMssnRecomPlace.class, forVariable(variable));
    }

    public QTbMssnRecomPlace(Path<? extends TbMssnRecomPlace> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnRecomPlace(PathMetadata metadata) {
        super(TbMssnRecomPlace.class, metadata);
    }

}

