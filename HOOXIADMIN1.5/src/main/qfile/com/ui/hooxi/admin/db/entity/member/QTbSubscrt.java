package com.ui.hooxi.admin.db.entity.member;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbSubscrt is a Querydsl query type for TbSubscrt
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbSubscrt extends EntityPathBase<TbSubscrt> {

    private static final long serialVersionUID = 325567436L;

    public static final QTbSubscrt tbSubscrt = new QTbSubscrt("tbSubscrt");

    public final NumberPath<Long> alarmYn = createNumber("alarmYn", Long.class);

    public final NumberPath<Long> mmbtId = createNumber("mmbtId", Long.class);

    public final NumberPath<Long> mmbtshpId = createNumber("mmbtshpId", Long.class);

    public final NumberPath<Long> subscrtDttn = createNumber("subscrtDttn", Long.class);

    public QTbSubscrt(String variable) {
        super(TbSubscrt.class, forVariable(variable));
    }

    public QTbSubscrt(Path<? extends TbSubscrt> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbSubscrt(PathMetadata metadata) {
        super(TbSubscrt.class, metadata);
    }

}

