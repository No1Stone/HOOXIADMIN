package com.ui.hooxi.admin.db.entity.story;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbLike is a Querydsl query type for TbLike
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbLike extends EntityPathBase<TbLike> {

    private static final long serialVersionUID = -1593681382L;

    public static final QTbLike tbLike = new QTbLike("tbLike");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final NumberPath<Long> storyId = createNumber("storyId", Long.class);

    public final StringPath useYn = createString("useYn");

    public QTbLike(String variable) {
        super(TbLike.class, forVariable(variable));
    }

    public QTbLike(Path<? extends TbLike> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbLike(PathMetadata metadata) {
        super(TbLike.class, metadata);
    }

}

