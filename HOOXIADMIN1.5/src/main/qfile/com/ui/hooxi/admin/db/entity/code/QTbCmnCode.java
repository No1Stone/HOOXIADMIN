package com.ui.hooxi.admin.db.entity.code;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbCmnCode is a Querydsl query type for TbCmnCode
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbCmnCode extends EntityPathBase<TbCmnCode> {

    private static final long serialVersionUID = -1363061538L;

    public static final QTbCmnCode tbCmnCode = new QTbCmnCode("tbCmnCode");

    public final StringPath cd = createString("cd");

    public final StringPath cdNm = createString("cdNm");

    public final StringPath cdVal = createString("cdVal");

    public final StringPath grpCd = createString("grpCd");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final NumberPath<Integer> ordr = createNumber("ordr", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath useYn = createString("useYn");

    public QTbCmnCode(String variable) {
        super(TbCmnCode.class, forVariable(variable));
    }

    public QTbCmnCode(Path<? extends TbCmnCode> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbCmnCode(PathMetadata metadata) {
        super(TbCmnCode.class, metadata);
    }

}

