package com.ui.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbCateCompany is a Querydsl query type for TbCateCompany
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbCateCompany extends EntityPathBase<TbCateCompany> {

    private static final long serialVersionUID = 686092706L;

    public static final QTbCateCompany tbCateCompany = new QTbCateCompany("tbCateCompany");

    public final NumberPath<Long> companyId = createNumber("companyId", Long.class);

    public final StringPath contents = createString("contents");

    public final StringPath imgUrl = createString("imgUrl");

    public final DateTimePath<java.time.LocalDateTime> modDttm = createDateTime("modDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDttm = createDateTime("regDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath title = createString("title");

    public QTbCateCompany(String variable) {
        super(TbCateCompany.class, forVariable(variable));
    }

    public QTbCateCompany(Path<? extends TbCateCompany> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbCateCompany(PathMetadata metadata) {
        super(TbCateCompany.class, metadata);
    }

}

