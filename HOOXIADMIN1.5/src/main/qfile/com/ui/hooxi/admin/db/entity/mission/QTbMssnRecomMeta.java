package com.ui.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnRecomMeta is a Querydsl query type for TbMssnRecomMeta
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnRecomMeta extends EntityPathBase<TbMssnRecomMeta> {

    private static final long serialVersionUID = -271012218L;

    public static final QTbMssnRecomMeta tbMssnRecomMeta = new QTbMssnRecomMeta("tbMssnRecomMeta");

    public final StringPath prpsCd = createString("prpsCd");

    public final NumberPath<Long> recomId = createNumber("recomId", Long.class);

    public QTbMssnRecomMeta(String variable) {
        super(TbMssnRecomMeta.class, forVariable(variable));
    }

    public QTbMssnRecomMeta(Path<? extends TbMssnRecomMeta> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnRecomMeta(PathMetadata metadata) {
        super(TbMssnRecomMeta.class, metadata);
    }

}

