package com.ui.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnRecomContents is a Querydsl query type for TbMssnRecomContents
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnRecomContents extends EntityPathBase<TbMssnRecomContents> {

    private static final long serialVersionUID = -664614117L;

    public static final QTbMssnRecomContents tbMssnRecomContents = new QTbMssnRecomContents("tbMssnRecomContents");

    public final StringPath langCd = createString("langCd");

    public final StringPath recomContents = createString("recomContents");

    public final NumberPath<Long> recomId = createNumber("recomId", Long.class);

    public final StringPath recomTitle = createString("recomTitle");

    public QTbMssnRecomContents(String variable) {
        super(TbMssnRecomContents.class, forVariable(variable));
    }

    public QTbMssnRecomContents(Path<? extends TbMssnRecomContents> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnRecomContents(PathMetadata metadata) {
        super(TbMssnRecomContents.class, metadata);
    }

}

