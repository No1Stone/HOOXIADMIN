package com.ui.hooxi.admin.db.entity.user;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMmbr is a Querydsl query type for TbMmbr
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMmbr extends EntityPathBase<TbMmbr> {

    private static final long serialVersionUID = -267010587L;

    public static final QTbMmbr tbMmbr = new QTbMmbr("tbMmbr");

    public final StringPath actClausYn = createString("actClausYn");

    public final StringPath ci = createString("ci");

    public final StringPath cntrCd = createString("cntrCd");

    public final StringPath email = createString("email");

    public final StringPath kidYn = createString("kidYn");

    public final DateTimePath<java.time.LocalDateTime> lastLoginDttm = createDateTime("lastLoginDttm", java.time.LocalDateTime.class);

    public final StringPath lastLoginTp = createString("lastLoginTp");

    public final StringPath mainLang = createString("mainLang");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final StringPath mmbrNm = createString("mmbrNm");

    public final StringPath mmbrStatusCd = createString("mmbrStatusCd");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final StringPath mpno = createString("mpno");

    public final StringPath nickname = createString("nickname");

    public final StringPath oauthId = createString("oauthId");

    public final StringPath oauthTpCd = createString("oauthTpCd");

    public final StringPath pinNum = createString("pinNum");

    public final StringPath profileImg = createString("profileImg");

    public final StringPath pushToken = createString("pushToken");

    public final StringPath pwd = createString("pwd");

    public final DateTimePath<java.time.LocalDateTime> pwModDttm = createDateTime("pwModDttm", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath subLang = createString("subLang");

    public final DateTimePath<java.time.LocalDateTime> wtdDttm = createDateTime("wtdDttm", java.time.LocalDateTime.class);

    public QTbMmbr(String variable) {
        super(TbMmbr.class, forVariable(variable));
    }

    public QTbMmbr(Path<? extends TbMmbr> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMmbr(PathMetadata metadata) {
        super(TbMmbr.class, metadata);
    }

}

