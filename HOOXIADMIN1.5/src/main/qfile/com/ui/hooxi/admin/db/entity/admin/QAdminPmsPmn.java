package com.ui.hooxi.admin.db.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminPmsPmn is a Querydsl query type for AdminPmsPmn
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QAdminPmsPmn extends EntityPathBase<AdminPmsPmn> {

    private static final long serialVersionUID = 1091474895L;

    public static final QAdminPmsPmn adminPmsPmn = new QAdminPmsPmn("adminPmsPmn");

    public final NumberPath<Long> adminPmsId = createNumber("adminPmsId", Long.class);

    public final StringPath adminPmsNm = createString("adminPmsNm");

    public final NumberPath<Long> adminPmsPmnId = createNumber("adminPmsPmnId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDttm = createDateTime("modDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDttm = createDateTime("regDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public QAdminPmsPmn(String variable) {
        super(AdminPmsPmn.class, forVariable(variable));
    }

    public QAdminPmsPmn(Path<? extends AdminPmsPmn> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminPmsPmn(PathMetadata metadata) {
        super(AdminPmsPmn.class, metadata);
    }

}

