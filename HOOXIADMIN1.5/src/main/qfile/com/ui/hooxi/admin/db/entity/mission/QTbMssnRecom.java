package com.ui.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnRecom is a Querydsl query type for TbMssnRecom
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnRecom extends EntityPathBase<TbMssnRecom> {

    private static final long serialVersionUID = 1601141985L;

    public static final QTbMssnRecom tbMssnRecom = new QTbMssnRecom("tbMssnRecom");

    public final StringPath imgUrl = createString("imgUrl");

    public final DateTimePath<java.time.LocalDateTime> modDttm = createDateTime("modDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final NumberPath<Long> recomId = createNumber("recomId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDttm = createDateTime("regDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath useYn = createString("useYn");

    public QTbMssnRecom(String variable) {
        super(TbMssnRecom.class, forVariable(variable));
    }

    public QTbMssnRecom(Path<? extends TbMssnRecom> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnRecom(PathMetadata metadata) {
        super(TbMssnRecom.class, metadata);
    }

}

