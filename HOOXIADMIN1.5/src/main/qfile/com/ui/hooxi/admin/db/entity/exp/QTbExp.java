package com.ui.hooxi.admin.db.entity.exp;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbExp is a Querydsl query type for TbExp
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbExp extends EntityPathBase<TbExp> {

    private static final long serialVersionUID = 782661474L;

    public static final QTbExp tbExp = new QTbExp("tbExp");

    public final NumberPath<Long> exp = createNumber("exp", Long.class);

    public final DateTimePath<java.time.LocalDateTime> expAcmltDt = createDateTime("expAcmltDt", java.time.LocalDateTime.class);

    public final StringPath expAcmltDtlTpCd = createString("expAcmltDtlTpCd");

    public final NumberPath<Long> expAcmltId = createNumber("expAcmltId", Long.class);

    public final StringPath expAcmltTpCd = createString("expAcmltTpCd");

    public final NumberPath<Long> expId = createNumber("expId", Long.class);

    public final StringPath expStateCd = createString("expStateCd");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public QTbExp(String variable) {
        super(TbExp.class, forVariable(variable));
    }

    public QTbExp(Path<? extends TbExp> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbExp(PathMetadata metadata) {
        super(TbExp.class, metadata);
    }

}

