package com.ui.hooxi.admin.db.entity.user;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbLoginHist is a Querydsl query type for TbLoginHist
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbLoginHist extends EntityPathBase<TbLoginHist> {

    private static final long serialVersionUID = 445647702L;

    public static final QTbLoginHist tbLoginHist = new QTbLoginHist("tbLoginHist");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.time.Instant> loginDttm = createDateTime("loginDttm", java.time.Instant.class);

    public final StringPath loginRsltCd = createString("loginRsltCd");

    public final StringPath loginTp = createString("loginTp");

    public QTbLoginHist(String variable) {
        super(TbLoginHist.class, forVariable(variable));
    }

    public QTbLoginHist(Path<? extends TbLoginHist> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbLoginHist(PathMetadata metadata) {
        super(TbLoginHist.class, metadata);
    }

}

