package com.ui.hooxi.admin.db.entity.exp;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMmbrExp is a Querydsl query type for TbMmbrExp
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMmbrExp extends EntityPathBase<TbMmbrExp> {

    private static final long serialVersionUID = -1848369966L;

    public static final QTbMmbrExp tbMmbrExp = new QTbMmbrExp("tbMmbrExp");

    public final NumberPath<Long> exp = createNumber("exp", Long.class);

    public final DateTimePath<java.time.LocalDateTime> expAcmltDt = createDateTime("expAcmltDt", java.time.LocalDateTime.class);

    public final StringPath lvlCd = createString("lvlCd");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final NumberPath<Long> nextNcssryExp = createNumber("nextNcssryExp", Long.class);

    public final StringPath title = createString("title");

    public QTbMmbrExp(String variable) {
        super(TbMmbrExp.class, forVariable(variable));
    }

    public QTbMmbrExp(Path<? extends TbMmbrExp> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMmbrExp(PathMetadata metadata) {
        super(TbMmbrExp.class, metadata);
    }

}

