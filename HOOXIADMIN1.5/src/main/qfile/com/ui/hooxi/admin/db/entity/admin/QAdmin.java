package com.ui.hooxi.admin.db.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdmin is a Querydsl query type for Admin
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QAdmin extends EntityPathBase<Admin> {

    private static final long serialVersionUID = -267181932L;

    public static final QAdmin admin = new QAdmin("admin");

    public final StringPath adminEmail = createString("adminEmail");

    public final NumberPath<Long> adminId = createNumber("adminId", Long.class);

    public final StringPath adminName = createString("adminName");

    public final NumberPath<Long> adminPmsId = createNumber("adminPmsId", Long.class);

    public final StringPath adminPwd = createString("adminPwd");

    public final DateTimePath<java.time.LocalDateTime> regDttm = createDateTime("regDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath useYn = createString("useYn");

    public QAdmin(String variable) {
        super(Admin.class, forVariable(variable));
    }

    public QAdmin(Path<? extends Admin> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdmin(PathMetadata metadata) {
        super(Admin.class, metadata);
    }

}

