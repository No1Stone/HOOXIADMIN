package com.ui.hooxi.admin.db.entity.member;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMmbrshp is a Querydsl query type for TbMmbrshp
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMmbrshp extends EntityPathBase<TbMmbrshp> {

    private static final long serialVersionUID = -933535291L;

    public static final QTbMmbrshp tbMmbrshp = new QTbMmbrshp("tbMmbrshp");

    public final StringPath afltn = createString("afltn");

    public final StringPath bgImg = createString("bgImg");

    public final DateTimePath<java.time.LocalDateTime> fnlModDttm = createDateTime("fnlModDttm", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> frstRgstDttm = createDateTime("frstRgstDttm", java.time.LocalDateTime.class);

    public final StringPath gradCd = createString("gradCd");

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final NumberPath<Long> mmbrshpId = createNumber("mmbrshpId", Long.class);

    public final StringPath mmbrshpNum = createString("mmbrshpNum");

    public final StringPath mmbrshpStateCd = createString("mmbrshpStateCd");

    public final StringPath nickname = createString("nickname");

    public final StringPath profileImg = createString("profileImg");

    public final StringPath rjctRsn = createString("rjctRsn");

    public final StringPath stateMsg = createString("stateMsg");

    public QTbMmbrshp(String variable) {
        super(TbMmbrshp.class, forVariable(variable));
    }

    public QTbMmbrshp(Path<? extends TbMmbrshp> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMmbrshp(PathMetadata metadata) {
        super(TbMmbrshp.class, metadata);
    }

}

