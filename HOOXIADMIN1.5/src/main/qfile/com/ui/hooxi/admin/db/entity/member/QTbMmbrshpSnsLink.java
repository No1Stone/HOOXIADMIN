package com.ui.hooxi.admin.db.entity.member;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMmbrshpSnsLink is a Querydsl query type for TbMmbrshpSnsLink
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMmbrshpSnsLink extends EntityPathBase<TbMmbrshpSnsLink> {

    private static final long serialVersionUID = 1449133069L;

    public static final QTbMmbrshpSnsLink tbMmbrshpSnsLink = new QTbMmbrshpSnsLink("tbMmbrshpSnsLink");

    public final NumberPath<Long> mmbrshpId = createNumber("mmbrshpId", Long.class);

    public final StringPath snsCd = createString("snsCd");

    public final StringPath snsLink = createString("snsLink");

    public final StringPath snsLinkId = createString("snsLinkId");

    public final NumberPath<Integer> sort = createNumber("sort", Integer.class);

    public final StringPath useYn = createString("useYn");

    public QTbMmbrshpSnsLink(String variable) {
        super(TbMmbrshpSnsLink.class, forVariable(variable));
    }

    public QTbMmbrshpSnsLink(Path<? extends TbMmbrshpSnsLink> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMmbrshpSnsLink(PathMetadata metadata) {
        super(TbMmbrshpSnsLink.class, metadata);
    }

}

