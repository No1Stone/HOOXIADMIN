package com.ui.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnPlace is a Querydsl query type for TbMssnPlace
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnPlace extends EntityPathBase<TbMssnPlace> {

    private static final long serialVersionUID = 1599501178L;

    public static final QTbMssnPlace tbMssnPlace = new QTbMssnPlace("tbMssnPlace");

    public final NumberPath<Long> mssnId = createNumber("mssnId", Long.class);

    public final StringPath placeCd = createString("placeCd");

    public QTbMssnPlace(String variable) {
        super(TbMssnPlace.class, forVariable(variable));
    }

    public QTbMssnPlace(Path<? extends TbMssnPlace> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnPlace(PathMetadata metadata) {
        super(TbMssnPlace.class, metadata);
    }

}

