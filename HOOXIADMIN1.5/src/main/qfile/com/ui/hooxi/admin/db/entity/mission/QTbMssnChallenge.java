package com.ui.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnChallenge is a Querydsl query type for TbMssnChallenge
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnChallenge extends EntityPathBase<TbMssnChallenge> {

    private static final long serialVersionUID = 143369558L;

    public static final QTbMssnChallenge tbMssnChallenge = new QTbMssnChallenge("tbMssnChallenge");

    public final StringPath actArea = createString("actArea");

    public final DateTimePath<java.time.LocalDateTime> actDt = createDateTime("actDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> challengeId = createNumber("challengeId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> endDt = createDateTime("endDt", java.time.LocalDateTime.class);

    public final StringPath link = createString("link");

    public final StringPath method = createString("method");

    public final NumberPath<Long> mssnId = createNumber("mssnId", Long.class);

    public final StringPath particptnTpCd = createString("particptnTpCd");

    public final StringPath rangeYn = createString("rangeYn");

    public final DateTimePath<java.time.LocalDateTime> startDt = createDateTime("startDt", java.time.LocalDateTime.class);

    public final StringPath useYn = createString("useYn");

    public QTbMssnChallenge(String variable) {
        super(TbMssnChallenge.class, forVariable(variable));
    }

    public QTbMssnChallenge(Path<? extends TbMssnChallenge> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnChallenge(PathMetadata metadata) {
        super(TbMssnChallenge.class, metadata);
    }

}

