package com.ui.hooxi.admin.db.entity.point;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QPointDtl is a Querydsl query type for PointDtl
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QPointDtl extends EntityPathBase<PointDtl> {

    private static final long serialVersionUID = 376624550L;

    public static final QPointDtl pointDtl = new QPointDtl("pointDtl");

    public final NumberPath<Long> dtlCnclPointId = createNumber("dtlCnclPointId", Long.class);

    public final NumberPath<Long> dtlOrgPointId = createNumber("dtlOrgPointId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> expireDt = createDateTime("expireDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final NumberPath<Long> point = createNumber("point", Long.class);

    public final NumberPath<Long> pointDtlId = createNumber("pointDtlId", Long.class);

    public final NumberPath<Long> pointId = createNumber("pointId", Long.class);

    public final EnumPath<com.ui.hooxi.admin.db.entity.point.type.PointStatCd> pointStateCd = createEnum("pointStateCd", com.ui.hooxi.admin.db.entity.point.type.PointStatCd.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> trstnDt = createDateTime("trstnDt", java.time.LocalDateTime.class);

    public final EnumPath<com.ui.hooxi.admin.db.entity.point.type.TrstnDtlTpCd> trstnDtlTpCd = createEnum("trstnDtlTpCd", com.ui.hooxi.admin.db.entity.point.type.TrstnDtlTpCd.class);

    public final NumberPath<Long> trstnId = createNumber("trstnId", Long.class);

    public final EnumPath<com.ui.hooxi.admin.db.entity.point.type.TrstnTpCd> trstnTpCd = createEnum("trstnTpCd", com.ui.hooxi.admin.db.entity.point.type.TrstnTpCd.class);

    public QPointDtl(String variable) {
        super(PointDtl.class, forVariable(variable));
    }

    public QPointDtl(Path<? extends PointDtl> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPointDtl(PathMetadata metadata) {
        super(PointDtl.class, metadata);
    }

}

