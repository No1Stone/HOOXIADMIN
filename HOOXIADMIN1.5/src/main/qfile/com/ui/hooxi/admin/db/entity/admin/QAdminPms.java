package com.ui.hooxi.admin.db.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminPms is a Querydsl query type for AdminPms
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QAdminPms extends EntityPathBase<AdminPms> {

    private static final long serialVersionUID = -1042456350L;

    public static final QAdminPms adminPms = new QAdminPms("adminPms");

    public final NumberPath<Long> adminPmsId = createNumber("adminPmsId", Long.class);

    public final StringPath adminPmsNm = createString("adminPmsNm");

    public final DateTimePath<java.time.LocalDateTime> modDttm = createDateTime("modDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDttm = createDateTime("regDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public QAdminPms(String variable) {
        super(AdminPms.class, forVariable(variable));
    }

    public QAdminPms(Path<? extends AdminPms> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminPms(PathMetadata metadata) {
        super(AdminPms.class, metadata);
    }

}

