package com.ui.hooxi.admin.db.entity.mission;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMssnCate is a Querydsl query type for TbMssnCate
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMssnCate extends EntityPathBase<TbMssnCate> {

    private static final long serialVersionUID = -2027010436L;

    public static final QTbMssnCate tbMssnCate = new QTbMssnCate("tbMssnCate");

    public final NumberPath<Long> cateId = createNumber("cateId", Long.class);

    public final NumberPath<Long> companyId = createNumber("companyId", Long.class);

    public final StringPath imgUrl = createString("imgUrl");

    public final DateTimePath<java.time.LocalDateTime> modDttm = createDateTime("modDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDttm = createDateTime("regDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public final StringPath tpcTpCd = createString("tpcTpCd");

    public final StringPath useYn = createString("useYn");

    public QTbMssnCate(String variable) {
        super(TbMssnCate.class, forVariable(variable));
    }

    public QTbMssnCate(Path<? extends TbMssnCate> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMssnCate(PathMetadata metadata) {
        super(TbMssnCate.class, metadata);
    }

}

