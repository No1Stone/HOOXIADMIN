package com.ui.hooxi.admin.db.entity.admin;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdminPmsCmn is a Querydsl query type for AdminPmsCmn
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QAdminPmsCmn extends EntityPathBase<AdminPmsCmn> {

    private static final long serialVersionUID = 1091462402L;

    public static final QAdminPmsCmn adminPmsCmn = new QAdminPmsCmn("adminPmsCmn");

    public final StringPath adminCmnC = createString("adminCmnC");

    public final StringPath adminCmnD = createString("adminCmnD");

    public final StringPath adminCmnNm = createString("adminCmnNm");

    public final StringPath adminCmnR = createString("adminCmnR");

    public final StringPath adminCmnU = createString("adminCmnU");

    public final NumberPath<Long> adminPmsCmnId = createNumber("adminPmsCmnId", Long.class);

    public final NumberPath<Long> adminPmsId = createNumber("adminPmsId", Long.class);

    public final NumberPath<Long> adminPmsPmnId = createNumber("adminPmsPmnId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDttm = createDateTime("modDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> modId = createNumber("modId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDttm = createDateTime("regDttm", java.time.LocalDateTime.class);

    public final NumberPath<Long> regId = createNumber("regId", Long.class);

    public QAdminPmsCmn(String variable) {
        super(AdminPmsCmn.class, forVariable(variable));
    }

    public QAdminPmsCmn(Path<? extends AdminPmsCmn> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminPmsCmn(PathMetadata metadata) {
        super(AdminPmsCmn.class, metadata);
    }

}

