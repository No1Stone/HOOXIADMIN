package com.ui.hooxi.admin.db.entity.user.id;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbClausAgrId is a Querydsl query type for TbClausAgrId
 */
@Generated("com.querydsl.codegen.DefaultEmbeddableSerializer")
public class QTbClausAgrId extends BeanPath<TbClausAgrId> {

    private static final long serialVersionUID = -1691000341L;

    public static final QTbClausAgrId tbClausAgrId = new QTbClausAgrId("tbClausAgrId");

    public final NumberPath<Long> clausId = createNumber("clausId", Long.class);

    public final NumberPath<Long> mmbrId = createNumber("mmbrId", Long.class);

    public QTbClausAgrId(String variable) {
        super(TbClausAgrId.class, forVariable(variable));
    }

    public QTbClausAgrId(Path<? extends TbClausAgrId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbClausAgrId(PathMetadata metadata) {
        super(TbClausAgrId.class, metadata);
    }

}

