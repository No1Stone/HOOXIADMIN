package com.ui.hooxi.admin.db.entity.member;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;


/**
 * QTbMmbrshpArticle is a Querydsl query type for TbMmbrshpArticle
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTbMmbrshpArticle extends EntityPathBase<TbMmbrshpArticle> {

    private static final long serialVersionUID = -1524733071L;

    public static final QTbMmbrshpArticle tbMmbrshpArticle = new QTbMmbrshpArticle("tbMmbrshpArticle");

    public final StringPath link = createString("link");

    public final NumberPath<Long> mmbrshpId = createNumber("mmbrshpId", Long.class);

    public final NumberPath<Integer> sort = createNumber("sort", Integer.class);

    public final StringPath useYn = createString("useYn");

    public QTbMmbrshpArticle(String variable) {
        super(TbMmbrshpArticle.class, forVariable(variable));
    }

    public QTbMmbrshpArticle(Path<? extends TbMmbrshpArticle> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTbMmbrshpArticle(PathMetadata metadata) {
        super(TbMmbrshpArticle.class, metadata);
    }

}

