package com.ui.hooxi.admin.db.repository.exp.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.ui.hooxi.admin.db.entity.exp.QTbAdminExp;
import com.ui.hooxi.admin.db.entity.exp.QTbExp;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;

@Aspect
@Slf4j
@RequiredArgsConstructor
public class TbExpRepositoryDslImpl implements TbExpRepositoryDsl {
    private final JPAQueryFactory queryFactory;

    private QTbExp qTbExp = QTbExp.tbExp;
    private QTbAdminExp qTbAdminExp = QTbAdminExp.tbAdminExp;

    public Long MmbrExpSum(Long mmbrId) {
        Long result = 0L;
        result = queryFactory.select(qTbExp.exp.sum()).from(qTbExp).where(qTbExp.mmbrId.eq(mmbrId)).fetchFirst();
        return result;
    }

    @Override
    public String AdminExpLevelFind(Long exp) {
        String result = "";

        result = queryFactory.select(qTbAdminExp.lvlCd)
                .from(qTbAdminExp)
                .where(qTbAdminExp.ncssryExp.lt(exp))
                .orderBy(qTbAdminExp.ncssryExp.desc())
                .fetchFirst();

        return result;
    }

    @Override
    public Long AdminExpNextLevelFind(Long exp) {
        Long result = 0L;

        result = queryFactory.select(qTbAdminExp.ncssryExp)
                .from(qTbAdminExp)
                .where(qTbAdminExp.ncssryExp.gt(exp))
                .orderBy(qTbAdminExp.ncssryExp.asc())
                .fetchFirst();

        return result;
    }


}
