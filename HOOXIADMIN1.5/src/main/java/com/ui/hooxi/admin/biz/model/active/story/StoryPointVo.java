package com.ui.hooxi.admin.biz.model.active.story;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoryPointVo {

    private Long storyId;
    private Long point;
    private Long exp;

}
