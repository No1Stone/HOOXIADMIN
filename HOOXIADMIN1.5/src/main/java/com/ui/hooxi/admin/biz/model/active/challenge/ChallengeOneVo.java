package com.ui.hooxi.admin.biz.model.active.challenge;

import com.ui.hooxi.admin.config.type.YnType;
import com.ui.hooxi.admin.db.entity.mission.TbMssnCate;
import com.ui.hooxi.admin.db.entity.mission.TbMssnCateMeta;
import com.ui.hooxi.admin.db.entity.mission.TbMssnCatePlace;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChallengeOneVo {

    private String companyName;
    private Long companyId;

    private Long cateId;
    private Long cateTitle;
    private String tpcTpCd;
    private String useYn;
    private String imgUrl;
    private Long regId;
    private LocalDateTime regDttm;
    private Long modId;
    private LocalDateTime modDttm;

    private List<String> place;
    private List<String> prps;

    private String addHi;
    private List<Long> addOp;

    public TbMssnCate ofSave(Long seq){
        TbMssnCate tmc = new TbMssnCate();
        if(this.cateId ==null){
            tmc.setTpcTpCd("TPC003");
            tmc.setUseYn(YnType.Y.getName());
            tmc.setImgUrl(this.imgUrl);
            tmc.setRegId(seq);
            tmc.setRegDttm(LocalDateTime.now());
            tmc.setModId(seq);
            tmc.setModDttm(LocalDateTime.now());
            tmc.setCompanyId(this.companyId);
        }
        else {
            tmc.setCateId(this.cateId);
            tmc.setTpcTpCd("TPC003");
            tmc.setUseYn(YnType.Y.getName());
            tmc.setImgUrl(this.imgUrl);
            tmc.setRegId(this.regId);
            tmc.setRegDttm(this.regDttm);
            tmc.setModId(seq);
            tmc.setModDttm(LocalDateTime.now());
            tmc.setCompanyId(this.companyId);
        }
        return tmc;
    }
    public TbMssnCate ofSave(Long seq, String img){
        TbMssnCate tmc = new TbMssnCate();
        if(this.cateId ==null){
            tmc.setTpcTpCd("TPC003");
            tmc.setUseYn(YnType.Y.getName());
            tmc.setImgUrl(img);
            tmc.setRegId(seq);
            tmc.setRegDttm(LocalDateTime.now());
            tmc.setModId(seq);
            tmc.setModDttm(LocalDateTime.now());
            tmc.setCompanyId(this.companyId);
        }
        else {
            tmc.setCateId(this.cateId);
            tmc.setTpcTpCd("TPC003");
            tmc.setUseYn(YnType.Y.getName());
            tmc.setImgUrl(img);
            tmc.setRegId(this.regId);
            tmc.setRegDttm(this.regDttm);
            tmc.setModId(seq);
            tmc.setModDttm(LocalDateTime.now());
            tmc.setCompanyId(this.companyId);
        }
        return tmc;
    }

    public List<TbMssnCatePlace> ofPlace(Long seq) {
        List<TbMssnCatePlace> tmcp = new ArrayList<>();
        for(String e: this.place){
            tmcp.add(TbMssnCatePlace.builder().cateId(seq).placeCd(e).build());
        }
        return tmcp;
    }

    public List<TbMssnCateMeta> ofPrps(Long seq) {
        List<TbMssnCateMeta> tmcp = new ArrayList<>();
        for(String e: this.prps){
            tmcp.add(TbMssnCateMeta.builder().cateId(seq).prpsCd(e).build());
        }
        return tmcp;
    }



}
