package com.ui.hooxi.admin.db.repository.story;

import com.ui.hooxi.admin.db.entity.story.TbLike;
import com.ui.hooxi.admin.db.entity.story.id.TbLikeId;
import com.ui.hooxi.admin.db.repository.story.dsl.TbStoryRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbLikeRepository extends JpaRepository<TbLike, TbLikeId> , TbStoryRepositoryDsl {
}
