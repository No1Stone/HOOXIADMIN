package com.ui.hooxi.admin.config.util;

public class WSURLBuilder {
    private String schema = "";
    private String baseURL = "";
    private String api = "";
    private String param = "";

    public static WSURLBuilder builder() {
        return new WSURLBuilder();
    }

    public WSURLBuilder schema(String schema) {
        this.schema = schema;
        return this;
    }

    public WSURLBuilder baseURL(String baseURL) {
        this.baseURL = baseURL;
        return this;
    }

    public WSURLBuilder api(String api) {
        this.api = api;
        return this;
    }

    public WSURLBuilder param(String param, String arg) {
        String con = "" ;
        if (this.param.length() > 0) {
            con = "&" ;
        } else {
            con = "?" ;
        }
        this.param += con + param + "=" + arg;
        return this;
    }

    public String build() {
        System.out.println(this.schema+this.baseURL+this.api+this.param);
        return this.schema+this.baseURL+this.api+this.param;
    }


}
