package com.ui.hooxi.admin.biz.model.admin;


import com.ui.hooxi.admin.db.entity.admin.AdminPmsPmn;
import org.apache.tomcat.jni.Local;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDateTime;

public class AdminPmsPmnReqTestModel {

    private Long adminPmsId;
    private String adminPmsNm;
    private String startDt;
    private String endDt;

    public AdminPmsPmnReqTestModel(){}

    public AdminPmsPmnReqTestModel(Long adminPmsId, String adminPmsNm, String startDt, String endDt) {
        this.adminPmsId = adminPmsId;
        this.adminPmsNm = adminPmsNm;
        this.startDt = startDt;
        this.endDt = endDt;
    }

    public Long getAdminPmsId() {
        return adminPmsId;
    }

    public void setAdminPmsId(Long adminPmsId) {
        this.adminPmsId = adminPmsId;
    }

    public String getAdminPmsNm() {
        return adminPmsNm;
    }

    public void setAdminPmsNm(String adminPmsNm) {
        this.adminPmsNm = adminPmsNm;
    }

    public String getStartDt() {
        return startDt;
    }

    public void setStartDt(String startDt) {
        this.startDt = startDt;
    }

    public String getEndDt() {
        return endDt;
    }

    public void setEndDt(String endDt) {
        this.endDt = endDt;
    }

    public AdminPmsPmn ofAdminPmsPmn(){
        AdminPmsPmn app = new AdminPmsPmn();
        app.setAdminPmsId(3L);
        app.setAdminPmsNm(this.adminPmsNm);
        app.setRegId(3L);
        app.setRegDttm(LocalDateTime.now());
        app.setModId(3L);
        app.setModDttm(LocalDateTime.now());
        return app;
    }
}
