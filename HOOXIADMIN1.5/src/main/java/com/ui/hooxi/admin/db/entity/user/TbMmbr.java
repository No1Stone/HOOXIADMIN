package com.ui.hooxi.admin.db.entity.user;

import com.ui.hooxi.admin.config.type.CmnCodeType;
import com.ui.hooxi.admin.config.type.CmnConverter;
import com.ui.hooxi.admin.db.entity.code.id.CmnCodeId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mmbr")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbMmbr {
    @Id
    @Column(name = "mmbr_id", nullable = true)
    private Long mmbrId;
    @Column(name = "mmbr_status_cd", nullable = true)
    private String mmbrStatusCd;
    @Column(name = "cntr_cd", nullable = true)
    private String cntrCd;
    @Column(name = "email", nullable = true)
    private String email;
    @Column(name = "pwd", nullable = true)
    private String pwd;
    @Column(name = "ci", nullable = true)
    private String ci;
    @Column(name = "kid_yn", nullable = true)
    private String kidYn;
    @Column(name = "mpno", nullable = true)
    private String mpno;
    @Column(name = "profile_img", nullable = true)
    private String profileImg;
    @Column(name = "push_token", nullable = true)
    private String pushToken;
    @Column(name = "pin_num", nullable = true)
    private String pinNum;
    @Column(name = "mmbr_nm", nullable = true)
    private String mmbrNm;
    @Column(name = "nickname", nullable = true)
    private String nickname;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "pw_mod_dttm", nullable = true)
    private LocalDateTime pwModDttm;
    @Column(name = "last_login_dttm", nullable = true)
    private LocalDateTime lastLoginDttm;
    @Column(name = "last_login_tp", nullable = true)
    private String lastLoginTp;
    @Column(name = "wtd_dttm", nullable = true)
    private LocalDateTime wtdDttm;//탈퇴
    @Column(name = "oauth_id", nullable = true)
    private String oauthId;
    @Column(name = "oauth_tp_cd", nullable = true)
    private String oauthTpCd;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "main_lang", nullable = true)
    private String mainLang;
    @Column(name = "sub_lang", nullable = true)
    private String subLang;
    @Column(name = "act_claus_yn", nullable = true)
    private String actClausYn;

    @Builder

    public TbMmbr(Long mmbrId, String mmbrStatusCd, String cntrCd, String email, String pwd, String ci, String kidYn,
                  String mpno, String profileImg, String pushToken, String pinNum, String mmbrNm, String nickname,
                  LocalDateTime regDt, LocalDateTime pwModDttm, LocalDateTime lastLoginDttm, String lastLoginTp, LocalDateTime wtdDttm,
                  String oauthId, String oauthTpCd, LocalDateTime modDt, String mainLang, String subLang, String actClausYn) {
        this.mmbrId = mmbrId;
        this.mmbrStatusCd = mmbrStatusCd;
        this.cntrCd = cntrCd;
        this.email = email;
        this.pwd = pwd;
        this.ci = ci;
        this.kidYn = kidYn;
        this.mpno = mpno;
        this.profileImg = profileImg;
        this.pushToken = pushToken;
        this.pinNum = pinNum;
        this.mmbrNm = mmbrNm;
        this.nickname = nickname;
        this.regDt = regDt;
        this.pwModDttm = pwModDttm;
        this.lastLoginDttm = lastLoginDttm;
        this.lastLoginTp = lastLoginTp;
        this.wtdDttm = wtdDttm;
        this.oauthId = oauthId;
        this.oauthTpCd = oauthTpCd;
        this.modDt = modDt;
        this.mainLang = mainLang;
        this.subLang = subLang;
        this.actClausYn = actClausYn;
    }
}
