package com.ui.hooxi.admin.db.repository.admin.dsl;

import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.admin.AdminListReq;
import com.ui.hooxi.admin.biz.model.admin.AdminListRes;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerListReq;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerListRes;
import com.ui.hooxi.admin.db.entity.admin.Admin;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsPmn;
import org.springframework.transaction.annotation.Transactional;

public interface AdminRepositoryDsl {

    void test();

    @Transactional(readOnly = true)
    QueryResults<AdminListRes> AdminListSelect(AdminListReq dto);

    @Transactional(readOnly = true)
    QueryResults<AdminPerListRes> AdminPerSelectList(AdminPerListReq dto);


}
