package com.ui.hooxi.admin.biz.model;

public enum UserStatusType {

    NOMAL("정상"),
    DORMANCY("휴면"),
    SECESSION("탈퇴");

    private String val;
    public static final UserStatusType[] LIST = { NOMAL, DORMANCY, SECESSION};

    UserStatusType(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }

    public String getName() {
        return name();
    }


}
