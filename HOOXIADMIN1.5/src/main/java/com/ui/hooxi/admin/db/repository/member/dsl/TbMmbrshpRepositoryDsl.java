package com.ui.hooxi.admin.db.repository.member.dsl;


import com.querydsl.core.QueryResults;
import org.springframework.transaction.annotation.Transactional;

public interface TbMmbrshpRepositoryDsl {

    @Transactional(readOnly = true)
    QueryResults UserListSelect();

}
