package com.ui.hooxi.admin.biz.model.active.cate;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CateListReq {


    private int page;
    private int size;

    private String searchName;
    private String searchNameVal;
    private String metaType;
    private String startDt;
    private String endDt;


}
