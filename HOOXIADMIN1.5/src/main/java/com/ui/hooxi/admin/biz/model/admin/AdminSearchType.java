package com.ui.hooxi.admin.biz.model.admin;

public enum AdminSearchType {

    EMAIL("이메일"),
    NAME("이름"),
    DATE("일자");

    private String val;

    public static final AdminSearchType[] LIST = {NAME, EMAIL, DATE};

    AdminSearchType(String val) {
        this.val = val;
    }

    public String getVal() {
        return this.val;
    }

    public String getName() {
        return name();
    }

}
