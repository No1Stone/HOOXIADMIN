package com.ui.hooxi.admin.config.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;

import java.util.Locale;

public class MessageAccessor {

    @Autowired
    private MessageSourceAccessor messageSourceAccessor;

    /**
     * KEY에 해당하는 메세지 반환
     * @param key
     * @return
     */
    public String getMessage(String key) {
        return messageSourceAccessor.getMessage(key, Locale.getDefault());
    }

    /**
     * KEY에 해당하는 메세지 반환
     * @param key
     * @param objs
     * @return
     */
    public String getMessage(String key, Object[] objs) {
        return messageSourceAccessor.getMessage(key, objs, Locale.getDefault());
    }

}
