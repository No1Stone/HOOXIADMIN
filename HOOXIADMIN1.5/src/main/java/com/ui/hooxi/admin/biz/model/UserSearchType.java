package com.ui.hooxi.admin.biz.model;

public enum UserSearchType {

    ALL("-검색유형선택-"),
    NAME("이름"),
    NICKNAME("닉네임"),
    PHONENUMBER("핸드폰번호"),
    EMAIL("이메일"),
    LANGUAGE("언어"),
    STATUS("상태"),
    DATE("일자")
    ;

    private String val;

    public static final UserSearchType[] LIST = {ALL, NICKNAME, EMAIL, LANGUAGE, STATUS, DATE};

    UserSearchType(String val) {
        this.val = val;
    }

    public String getVal() {
        return this.val;
    }

    public String getName() {
        return name();
    }

}
