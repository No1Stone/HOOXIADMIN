package com.ui.hooxi.admin.db.repository.company.dsl;


import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.ui.hooxi.admin.biz.model.active.challenge.ChallengeComSelectVo;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerListRes;
import com.ui.hooxi.admin.biz.model.user.CompanyListReq;
import com.ui.hooxi.admin.biz.model.user.CompanyListRes;
import com.ui.hooxi.admin.biz.model.user.search.CompanySearch;
import com.ui.hooxi.admin.db.entity.admin.QAdmin;
import com.ui.hooxi.admin.db.entity.admin.QAdminPms;
import com.ui.hooxi.admin.db.entity.company.QTbCompany;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Aspect
@Slf4j
@RequiredArgsConstructor
public class TbCompanyRepositoryDslImpl implements TbCompanyRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QTbCompany qTbCompany = QTbCompany.tbCompany;
    private QAdmin qAdmin = QAdmin.admin;

    @Override
    public QueryResults<CompanyListRes> CompannyListSelectList(CompanyListReq dto) {
        if (dto.getSize() == 0) {
            dto.setSize(10);
        }

        LocalDateTime startDateString = null;
        LocalDateTime endDateString = null;
        log.info("=====DSL INIT=====");
        if (dto.getSearchName() != null && dto.getSearchName().equals("DATE")) {
            startDateString = LocalDate.parse(dto.getStartDt()).atStartOfDay();
            endDateString = LocalDate.parse(dto.getEndDt()).atStartOfDay().plusDays(1l);
            log.info("sdaaa = {}", startDateString);
            log.info("edaaa = {}", endDateString);
        }


        QueryResults<CompanyListRes> result =
                queryFactory.select(
                                Projections.fields(CompanyListRes.class,
                                        qTbCompany.companyId,
                                        qTbCompany.companyName,
                                        qTbCompany.contents,
                                        qTbCompany.imgUrl,
                                        qTbCompany.regId,
                                        qTbCompany.regDttm,
                                        qTbCompany.modId,
                                        qTbCompany.modDttm,
                                        qAdmin.adminName
                                )
                        )
                        .from(
                                qTbCompany
                        )
                        .where(CompannyListSelectListEq(dto, startDateString, endDateString))
                        .leftJoin(qAdmin).on(qAdmin.adminId.eq(qTbCompany.regId))
                        .orderBy(qTbCompany.regDttm.desc())
                        .offset(dto.getPage() * dto.getSize())
                        .limit(dto.getSize())
                        .fetchResults();
        return result;
    }

    private BooleanExpression CompannyListSelectListEq(CompanyListReq dto, LocalDateTime sd, LocalDateTime ed){

        if(dto.getSearchName() == null){
            return  null;
        }

        else if(dto.getSearchName().equals("")){
            return null;
        }
        else if(dto.getSearchName().isBlank()){
            return null;
        }

        else if(dto.getSearchName().equals(CompanySearch.NAME.getName())){
            return qTbCompany.companyName.contains(dto.getSearchNameVal());
        }
        else if(dto.getSearchName().equals(CompanySearch.DATE.getName())){
            return qTbCompany.regDttm.between(sd, ed);
        }

        else{
            return null;
        }

    }

    @Override
    public List<ChallengeComSelectVo> ChalComSelect(ChallengeComSelectVo dto) {

        List<ChallengeComSelectVo> result =
                queryFactory.select(
                                Projections.fields(ChallengeComSelectVo.class,
                                        qTbCompany.companyName,
                                        qTbCompany.companyId
                                )
                        )
                        .from(
                                qTbCompany
                        )
                        .where(ChalComName(dto))
                        .orderBy(qTbCompany.regDttm.desc())
                        .fetchResults().getResults();
        return result;
    }

    public BooleanExpression ChalComName(ChallengeComSelectVo dto) {
        if (dto.getCompanyName() == null || dto.getCompanyName().equals("") || dto.getCompanyName().isBlank()) {
            return null;
        } else {
            return qTbCompany.companyName.contains(dto.getCompanyName());
        }
    }

}
