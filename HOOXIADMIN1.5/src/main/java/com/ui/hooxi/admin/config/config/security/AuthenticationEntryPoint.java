package com.ui.hooxi.admin.config.config.security;

import com.ui.hooxi.admin.db.entity.admin.Admin;
import com.ui.hooxi.admin.db.repository.admin.AdminRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class AuthenticationEntryPoint implements AuthenticationSuccessHandler {

    private final AdminRepository adminRepository;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {

        HttpSession session = request.getSession();
        Long sessionAdminId = Long.parseLong(authentication.getName());
        Admin admin = adminRepository.findById(sessionAdminId).get();
        String sessionAdminName = admin.getAdminName();

        System.out.println(">>>>>>>>>>>>>>>>> sesseionId : " + sessionAdminId);
        System.out.println(">>>>>>>>>>>>>>>>> sesseionName : " + sessionAdminName);

        session.setAttribute("sesseionId", sessionAdminId);
        session.setAttribute("sesseionName", sessionAdminName);

        AuthenticationSuccessHandler.super.onAuthenticationSuccess(request, response, chain, authentication);

    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

    }
}
