package com.ui.hooxi.admin.biz.model.active.subaction;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubActionRes {

    private Long mssnSubId;
    private String subTitle;
    private String subCtnt;
    private LocalDateTime regDttm;

}
