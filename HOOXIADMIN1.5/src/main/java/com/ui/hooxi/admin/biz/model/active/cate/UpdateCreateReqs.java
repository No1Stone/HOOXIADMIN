package com.ui.hooxi.admin.biz.model.active.cate;

import com.ui.hooxi.admin.config.type.YnType;
import com.ui.hooxi.admin.db.entity.mission.*;
import lombok.*;
import org.apache.tomcat.jni.Local;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateCreateReqs {

    private Long cateId;
    private String tpcTpCd;
    private String useYn;
    private String imgUrl;
    private Long regId;
    private LocalDateTime regDttm;
    private Long modId;
    private LocalDateTime modDttm;
    private Long companyId;

    private List<String> place;
    private List<String> prps;


    public TbMssnCate ofSave(Long seq, String tpcTpCd){
        TbMssnCate tmc = new TbMssnCate();
        //TPC002 액션
        //TPC003 챌린지

        if(this.cateId==null){
            tmc.setTpcTpCd(tpcTpCd);
            tmc.setUseYn(YnType.Y.getName());
            tmc.setImgUrl(this.imgUrl);
            tmc.setRegId(seq);
            tmc.setRegDttm(LocalDateTime.now());
            tmc.setModId(seq);
            tmc.setModDttm(LocalDateTime.now());
            tmc.setCompanyId(this.companyId);
        }
        else {
            tmc.setCateId(this.cateId);
            tmc.setTpcTpCd(this.tpcTpCd);
            tmc.setUseYn(this.useYn);
            tmc.setImgUrl(this.imgUrl);
            tmc.setRegId(this.regId);
            tmc.setRegDttm(this.regDttm);
            tmc.setModId(seq);
            tmc.setModDttm(LocalDateTime.now());
            tmc.setCompanyId(this.companyId);
        }
        return tmc;
    }

    public TbMssnCate ofSave(Long seq, String img, String tpcTpCd){
        TbMssnCate tmc = new TbMssnCate();
        //TPC002 액션
        //TPC003 챌린지

        if(this.cateId==null){
            tmc.setTpcTpCd(tpcTpCd);
            tmc.setUseYn(YnType.Y.getName());
            tmc.setImgUrl(img);
            tmc.setRegId(seq);
            tmc.setRegDttm(LocalDateTime.now());
            tmc.setModId(seq);
            tmc.setModDttm(LocalDateTime.now());
            tmc.setCompanyId(this.companyId);
        }
        else {
            tmc.setCateId(this.cateId);
            tmc.setTpcTpCd(this.tpcTpCd);
            tmc.setUseYn(this.useYn);
            tmc.setImgUrl(img);
            tmc.setRegId(this.regId);
            tmc.setRegDttm(this.regDttm);
            tmc.setModId(seq);
            tmc.setModDttm(LocalDateTime.now());
            tmc.setCompanyId(this.companyId);
        }
        return tmc;
    }

    public List<TbMssnCatePlace> ofPlace(Long seq){
        List<TbMssnCatePlace> tmp = new ArrayList<>();
        this.place.stream().forEach(e -> tmp.add(TbMssnCatePlace.builder().cateId(seq).placeCd(e).build()));
        return tmp;
    }

    public List<TbMssnCateMeta> ofPrps(Long seq){
        List<TbMssnCateMeta> tmp = new ArrayList<>();
        this.prps.stream().forEach(e -> tmp.add(TbMssnCateMeta.builder().cateId(seq).prpsCd(e).build()));
        return tmp;
    }



}
