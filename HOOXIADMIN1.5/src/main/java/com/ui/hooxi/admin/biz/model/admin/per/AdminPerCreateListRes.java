package com.ui.hooxi.admin.biz.model.admin.per;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminPerCreateListRes {

    private Long pmenuSeq;
    private boolean checkPer;

}
