package com.ui.hooxi.admin.biz.model.admin.per;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminPerCreateReq {

    private String adminPmsNm;
    private String pmenuVal;

}
