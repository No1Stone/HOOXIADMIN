package com.ui.hooxi.admin.biz.model.active.cate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CateLangContentsReq {
    private Long cateId;
    private String langCodes;
}
