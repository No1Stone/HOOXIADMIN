package com.ui.hooxi.admin.db.entity.exp.convert;

import com.ui.hooxi.admin.config.util.AbstractLegacyEnumAttributeConverter;
import com.ui.hooxi.admin.db.entity.exp.type.ExpStateCd;

import javax.persistence.Converter;

@Converter
public class ExpStateCdConvert extends AbstractLegacyEnumAttributeConverter<ExpStateCd> {
	public static final String ENUM_NAME = "경험치상태코드";

	public ExpStateCdConvert() {
		super(ExpStateCd.class,true, ENUM_NAME);
	}
}
