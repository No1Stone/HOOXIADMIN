package com.ui.hooxi.admin.db.repository.member;

import com.ui.hooxi.admin.db.entity.member.TbMmbrshpArticle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMmbrshpArticleRepository extends JpaRepository<TbMmbrshpArticle, Long> {
}