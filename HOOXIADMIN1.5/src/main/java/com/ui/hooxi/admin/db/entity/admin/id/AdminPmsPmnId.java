package com.ui.hooxi.admin.db.entity.admin.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class AdminPmsPmnId implements Serializable {
    private Long adminPmsPmnId;
    private Long adminPmsId;
}
