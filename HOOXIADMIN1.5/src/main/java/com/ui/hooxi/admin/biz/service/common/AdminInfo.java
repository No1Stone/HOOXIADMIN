package com.ui.hooxi.admin.biz.service.common;


import lombok.Builder;

public class AdminInfo {

    private Long adminId;
    private String adminEmail;
    private String adminName;
    private Long adminPmsId;

    public AdminInfo(){}
    @Builder
    public AdminInfo(Long adminId, String adminEmail, String adminName, Long adminPmsId) {
        this.adminId = adminId;
        this.adminEmail = adminEmail;
        this.adminName = adminName;
        this.adminPmsId = adminPmsId;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public Long getAdminPmsId() {
        return adminPmsId;
    }

    public void setAdminPmsId(Long adminPmsId) {
        this.adminPmsId = adminPmsId;
    }
}
