package com.ui.hooxi.admin.db.entity.mission;

import com.ui.hooxi.admin.db.entity.mission.id.TbMssnId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mssn")
@Entity
@Getter
@Setter
@NoArgsConstructor
//@IdClass(TbMssnId.class)
public class TbMssn {

    /*
    select nextval('시퀀스');
    select setval('시퀀스',(select max(admin_id) from 타겟_테이블));
     */

    @Id
    @GeneratedValue(generator = "tb_mssn_id_seq")
    @Column(name = "mssn_id", nullable = true)
    private Long mssnId;
    @Column(name = "cate_id", nullable = true)
    private Long cateId;
    @Column(name = "tpc_tp_cd", nullable = true)
    private String tpcTpCd;
    @Column(name = "img_url", nullable = true)
    private String imgUrl;
    @Column(name = "exp", nullable = true)
    private int exp;
    @Column(name = "point", nullable = true)
    private int point;
    @Column(name = "hit", nullable = true)
    private Long hit;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "link", nullable = true)
    private String linkUrl;

    @Builder
    public TbMssn(Long mssnId, Long cateId, String tpcTpCd, String imgUrl, int exp, int point, Long hit, String useYn, Long regId, LocalDateTime regDt, Long modId, LocalDateTime modDt, String linkUrl) {
        this.mssnId = mssnId;
        this.cateId = cateId;
        this.tpcTpCd = tpcTpCd;
        this.imgUrl = imgUrl;
        this.exp = exp;
        this.point = point;
        this.hit = hit;
        this.useYn = useYn;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
        this.linkUrl = linkUrl;
    }
}
