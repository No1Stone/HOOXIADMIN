package com.ui.hooxi.admin.db.entity.member.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class TbMmbrshpId implements Serializable {
    private Long mmbrshpId;
    private Long mmbrId;
}
