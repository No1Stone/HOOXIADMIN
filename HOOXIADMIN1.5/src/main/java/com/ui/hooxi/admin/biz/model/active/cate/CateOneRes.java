package com.ui.hooxi.admin.biz.model.active.cate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;


@AllArgsConstructor
@Getter
@Setter
public class CateOneRes {

    private Long cateId;
    private String prpsCd;
    private String cateTitle;
    private String imgUrl;
    private Long regId;
    private LocalDateTime regDttm;
    private Long  modId;
    private LocalDateTime modDttm;
        private List<String> plcaces;
    public CateOneRes(){}

}

