package com.ui.hooxi.admin.config.type;

import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Component
@Converter
public class CmnConverter implements AttributeConverter<CmnCodeType, String> {
    @Override
    public String convertToDatabaseColumn(CmnCodeType attribute) {
        return attribute.getName();
    }

    @Override
    public CmnCodeType convertToEntityAttribute(String dbData) {
        return CmnCodeType.ofCode(dbData);
    }

}
