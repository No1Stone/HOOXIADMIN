package com.ui.hooxi.admin.biz.model.active.cate;

import com.ui.hooxi.admin.db.entity.mission.TbMssnCateContents;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CateContentsReq {
    private Long cateId;
    private String langCodes;
    private String cateTitle;
//    private String cateTitleDtl;
    private String cateContents;

    public TbMssnCateContents ofSave() {
        TbMssnCateContents tmcc = new TbMssnCateContents();
        tmcc.setCateId(this.cateId);
        tmcc.setLangCd(this.langCodes);
        tmcc.setCateTitle(this.cateTitle);
//        tmcc.setCateTitleDtl(this.cateTitleDtl);
        tmcc.setCateContents(this.cateContents);
        return tmcc;
    }

}
