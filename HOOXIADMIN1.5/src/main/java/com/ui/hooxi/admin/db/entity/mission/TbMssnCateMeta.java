package com.ui.hooxi.admin.db.entity.mission;

import com.ui.hooxi.admin.db.entity.mission.id.TbMssnCateContentsId;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnCateMetaId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_cate_meta")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnCateMetaId.class)
public class TbMssnCateMeta {

    @Id
    @Column(name = "cate_id", nullable = true)
    private Long cateId;
    @Id
    @Column(name = "prps_cd", nullable = true)
    private String prpsCd;

    @Builder
    public TbMssnCateMeta(Long cateId, String prpsCd) {
        this.cateId = cateId;
        this.prpsCd = prpsCd;
    }

}
