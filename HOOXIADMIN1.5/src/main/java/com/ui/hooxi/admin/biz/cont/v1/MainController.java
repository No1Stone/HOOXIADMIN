package com.ui.hooxi.admin.biz.cont.v1;

import com.ui.hooxi.admin.config.type.PMenu;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping(path = "/dash")
public class MainController {

    private final Logger logger = LoggerFactory.getLogger(MainController.class);
    private final MainTestService mainTestService;

    @ModelAttribute("PMenu")
    public List<PMenu> PMENUType() {
        return Arrays.asList(PMenu.ALL);
    }

    @GetMapping(path = {"/", "/index"})
    public ModelAndView MainPage() {
        logger.info("init========================");
        return new ModelAndView("/index");
    }

    @GetMapping(path = "/test1")
    public ModelAndView test1() {
        logger.info("init========================");
        return new ModelAndView("/test1");
    }

    @GetMapping(path = "/test2")
    public void test2() {
        logger.info("init========================");
    }









}
