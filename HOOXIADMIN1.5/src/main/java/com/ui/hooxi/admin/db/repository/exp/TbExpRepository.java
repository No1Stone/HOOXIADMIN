package com.ui.hooxi.admin.db.repository.exp;

import com.ui.hooxi.admin.db.entity.exp.TbExp;
import com.ui.hooxi.admin.db.entity.exp.id.TbExpId;
import com.ui.hooxi.admin.db.entity.point.type.PointStatCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnTpCd;
import com.ui.hooxi.admin.db.repository.exp.dsl.TbExpRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbExpRepository extends JpaRepository<TbExp, TbExpId> , TbExpRepositoryDsl {

    boolean existsByExpAcmltIdAndExpStateCdAndExpAcmltTpCdAndExpAcmltDtlTpCd(Long seq, String expCode, String expAcmlt, String expAcmltDtl);


}
