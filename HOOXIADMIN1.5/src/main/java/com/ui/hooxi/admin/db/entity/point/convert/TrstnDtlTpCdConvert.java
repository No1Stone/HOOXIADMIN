package com.ui.hooxi.admin.db.entity.point.convert;

import com.ui.hooxi.admin.config.util.AbstractLegacyEnumAttributeConverter;
import com.ui.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;

import javax.persistence.Converter;

@Converter
public class TrstnDtlTpCdConvert extends AbstractLegacyEnumAttributeConverter<TrstnDtlTpCd> {
	public static final String ENUM_NAME = "거래상세유형코드";

	public TrstnDtlTpCdConvert() {
		super(TrstnDtlTpCd.class,true, ENUM_NAME);
	}
}
