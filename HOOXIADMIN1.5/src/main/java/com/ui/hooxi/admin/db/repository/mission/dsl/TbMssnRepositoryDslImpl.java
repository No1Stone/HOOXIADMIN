package com.ui.hooxi.admin.db.repository.mission.dsl;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.sql.JPASQLQuery;
import com.querydsl.sql.SQLQueryFactory;
import com.ui.hooxi.admin.biz.model.active.action.*;
import com.ui.hooxi.admin.biz.model.active.cate.CateListReq;
import com.ui.hooxi.admin.biz.model.active.cate.CateListRes;
import com.ui.hooxi.admin.biz.model.active.cate.CateOneRes;
import com.ui.hooxi.admin.biz.model.active.cate.UpdateCreateReqs;
import com.ui.hooxi.admin.biz.model.active.challenge.*;
import com.ui.hooxi.admin.biz.model.active.challengeAction.ChallengeActionSearchType;
import com.ui.hooxi.admin.biz.model.active.challengeAction.ChallengeActionViewVo;
import com.ui.hooxi.admin.biz.model.active.story.CateSearchType;
import com.ui.hooxi.admin.biz.model.active.subaction.SubActionReq;
import com.ui.hooxi.admin.biz.model.active.subaction.SubActionRes;
import com.ui.hooxi.admin.db.entity.admin.QAdmin;
import com.ui.hooxi.admin.db.entity.company.QTbCompany;
import com.ui.hooxi.admin.db.entity.mission.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Local;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Aspect
@Slf4j
@RequiredArgsConstructor
public class TbMssnRepositoryDslImpl implements TbMssnRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private final JPASQLQuery jpasqlQuery;
    private final SQLQueryFactory sqlQueryFactory;

    private QTbMssn qTbMssn = QTbMssn.tbMssn;
    private QTbMssnContents qTbMssnContent = QTbMssnContents.tbMssnContents;
    private QTbMssnMeta qTbMssnMeta = QTbMssnMeta.tbMssnMeta;
    private QTbMssnPlace qTbMssnPlace = QTbMssnPlace.tbMssnPlace;

    private QTbMssnCate qTbMssnCate = QTbMssnCate.tbMssnCate;
    private QTbMssnCateContents qTbMssnCateContents = QTbMssnCateContents.tbMssnCateContents;
    private QTbMssnCateMeta qTbMssnCateMeta = QTbMssnCateMeta.tbMssnCateMeta;
    private QTbMssnCatePlace qTbMssnCatePlace = QTbMssnCatePlace.tbMssnCatePlace;

    private QTbMssnRecom qTbMssnRecom = QTbMssnRecom.tbMssnRecom;
    private QTbMssnRecomContents qTbMssnRecomContents = QTbMssnRecomContents.tbMssnRecomContents;
    private QTbMssnRecomMeta qTbMssnRecomMeta = QTbMssnRecomMeta.tbMssnRecomMeta;
    private QTbMssnRecomPlace qTbMssnRecomPlace = QTbMssnRecomPlace.tbMssnRecomPlace;

    private QTbCompany qTbCompany = QTbCompany.tbCompany;
    private QTbMssnChallenge qTbMssnChallenge = QTbMssnChallenge.tbMssnChallenge;

    private QAdmin qAdmin = QAdmin.admin;

    @Override
    public QueryResults<ActionListRes> ActionListRes(ActionListReq dto, String topic) {
        QueryResults<ActionListRes> result = null;
        if (dto.getSize() == 0) {
            dto.setSize(10);
        }

        LocalDateTime startDateString = null;
        LocalDateTime endDateString = null;
        log.info("=====DSL INIT=====");
        if (dto.getSearchName() != null && dto.getSearchName().equals("DATE")) {
            startDateString = LocalDate.parse(dto.getStartDt()).atStartOfDay();
            endDateString = LocalDate.parse(dto.getEndDt()).atStartOfDay().plusDays(1l);
            log.info("sdaaa = {}", startDateString);
            log.info("edaaa = {}", endDateString);
        }

        result = queryFactory.select(Projections.fields(
                                ActionListRes.class,
                                qTbMssn.mssnId,
                                qTbMssn.tpcTpCd,
                                qTbMssn.imgUrl,
                                qTbMssnContent.mssnTitle,
                                qTbMssnContent.mssnEffect,
                                qTbMssn.exp,
                                qTbMssn.point,
                                qTbMssn.hit,
                                qTbMssn.useYn,
                                qTbMssn.regId,
                                qTbMssn.regDt,
                                qTbMssn.modId,
                                qTbMssn.modDt,
                                qAdmin.adminName
                        )
                )
                .from(qTbMssn)
                .where(qTbMssnContent.langCd.eq("ko"),
                        qTbMssn.tpcTpCd.eq(topic),
                        ActionListResEq(dto, startDateString, endDateString)
                )
                .leftJoin(qTbMssnContent).on(qTbMssnContent.mssnId.eq(qTbMssn.mssnId))
                .leftJoin(qAdmin).on(qAdmin.adminId.eq(qTbMssn.regId))
                .orderBy(qTbMssn.mssnId.desc())
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .fetchResults();
        return result;
    }

    private BooleanExpression ActionListResEq(ActionListReq dto, LocalDateTime sd, LocalDateTime ed) {

        if (dto.getSearchName() == null) {
            return null;
        } else if (dto.getSearchName().equals("")) {
            return null;
        } else if (dto.getSearchName().equals(ActionSearchType.NAME.getName())) {
            return qTbMssnContent.mssnTitle.contains(dto.getSearchNameVal());
        } else if (dto.getSearchName().equals(ChallengeActionSearchType.CHALLENGENAME.getName())) {
            return qTbMssnContent.mssnTitle.contains(dto.getSearchNameVal());
        } else if (dto.getSearchName().equals(ActionSearchType.DATE.getName())) {
            return qTbMssn.regDt.between(sd, ed);
        } else {
            return null;
        }


    }

    @Override
    public List<ActionCateSelectRes> ActionCateTitleSelectList(ActionCateSelectReq dto, String topic) {
        List<ActionCateSelectRes> result = null;
        result = queryFactory.select(Projections.fields(
                                ActionCateSelectRes.class,
                                qTbMssnCate.cateId,
                                qTbMssnCateContents.cateTitle
                        )
                ).distinct()
                .from(qTbMssnCate)
                .where(qTbMssnCateContents.langCd.eq("ko"),
                        qTbMssnCate.tpcTpCd.eq(topic),
                        ActionCateTitleSelectListPlaceeq(dto),
                        ActionCateTitleSelectListPrpseq(dto),
                        ActionCateTitleSelectListTitle(dto),
                        ActionCateTitleSelectListCateId(dto)
                )
                .leftJoin(qTbMssnCateContents).on(qTbMssnCateContents.cateId.eq(qTbMssnCate.cateId))
                .leftJoin(qTbMssnCateMeta).on(qTbMssnCateMeta.cateId.eq(qTbMssnCate.cateId))
                .leftJoin(qTbMssnCatePlace).on(qTbMssnCatePlace.cateId.eq(qTbMssnCate.cateId))
                .orderBy(qTbMssnCate.cateId.desc())
                .fetchResults().getResults();
        return result;
    }

    @Override
    public Optional<ActionViewVo> ActionOneViewSelect(Long seq) {
        ActionViewVo result = null;

        result = queryFactory.select(Projections.fields(
                                ActionViewVo.class,
                                qTbMssn.mssnId,
                                qTbMssn.tpcTpCd,
                                qTbMssn.imgUrl,
                                qTbMssn.exp,
                                qTbMssn.point,
                                qTbMssn.hit,
                                qTbMssn.useYn,
                                qTbMssn.regId,
                                qTbMssn.regDt,
                                qTbMssn.modId,
                                qTbMssn.modDt,
                                qTbMssn.cateId,
                                qTbMssn.linkUrl

                        )
                )
                .from(qTbMssn)
                .where(
                        qTbMssn.mssnId.eq(seq)
//                        qTbMssnContent.langCd.eq("ko")
                )
                .leftJoin(qTbMssnContent).on(qTbMssnContent.mssnId.eq(qTbMssn.mssnId))
                .fetchFirst()
        ;

        return Optional.of(result);
    }

    @Override
    public Optional<ChallengeOneVo> ChalOneViewSelect(Long seq) {

        ChallengeOneVo result = null;
        result = queryFactory.select(Projections.fields(
                                ChallengeOneVo.class,
                                qTbCompany.companyName,
                                qTbCompany.companyId,

                                qTbMssnCate.cateId,
                                qTbMssnCate.tpcTpCd,
                                qTbMssnCate.useYn,
                                qTbMssnCate.imgUrl,
                                qTbMssnCate.regId,
                                qTbMssnCate.regDttm,
                                qTbMssnCate.modId,
                                qTbMssnCate.modDttm
                        )
                )
                .from(qTbMssnCate)
                .where(
                        qTbMssnCate.cateId.eq(seq)
                )
                .leftJoin(qTbCompany).on(qTbCompany.companyId.eq(qTbMssnCate.companyId))
                .fetchFirst()
        ;
        return Optional.of(result);
    }

    @Override
    public List<ActionSelectListVo> ChalActionSelectList(ActionSelectListVo dto) {
        List<ActionSelectListVo> result = null;

        result = queryFactory.select(Projections.fields(
                                ActionSelectListVo.class,
                                qTbMssn.mssnId,
                                qTbMssnContent.mssnTitle
                        )
                )
                .from(qTbMssn)
                .where(qTbMssnContent.langCd.eq("ko"), ChalActionNameDynamic(dto))
                .leftJoin(qTbMssnContent).on(qTbMssnContent.mssnId.eq(qTbMssn.mssnId))
                .orderBy(qTbMssn.regDt.desc())
                .fetchResults().getResults();
        return result;
    }

    private BooleanExpression ChalActionNameDynamic(ActionSelectListVo dto) {
        if (dto.getMssnTitle() == null || StringUtils.containsWhitespace(dto.getMssnTitle()) || dto.getMssnTitle().isBlank()) {
            return null;
        } else {
            return qTbMssnContent.mssnTitle.contains(dto.getMssnTitle());
        }
    }

    public BooleanExpression ActionCateTitleSelectListCateId(ActionCateSelectReq dto) {
        if (dto.getCateId() == null) {
            return null;
        }
        if (dto.getCateId() > 0L) {
            return qTbMssnCatePlace.cateId.eq(dto.getCateId());
        } else {
            return null;
        }
    }

    public BooleanExpression ActionCateTitleSelectListPlaceeq(ActionCateSelectReq dto) {
        if (dto.getPlace() == null) {
            return null;
        }
        if (dto.getPlace().size() > 0 && dto.getPlace() != null) {
            return qTbMssnCatePlace.placeCd.in(dto.getPlace());
        } else {
            return null;
        }
    }

    public BooleanExpression ActionCateTitleSelectListPrpseq(ActionCateSelectReq dto) {
        if (dto.getPrps() == null) {
            return null;
        }
        if (dto.getPrps().size() > 0 && dto.getPrps() != null) {
            return qTbMssnCateMeta.prpsCd.in(dto.getPrps());
        } else {
            return null;
        }
    }

    public BooleanExpression ActionCateTitleSelectListTitle(ActionCateSelectReq dto) {
        if (dto.getCateTitle() == null || dto.getCateTitle().isBlank() || dto.getCateTitle().isEmpty()) {
            return null;
        } else {
            return qTbMssnCateContents.cateTitle.contains(dto.getCateTitle());
        }
    }


    @Override
    public QueryResults<ChallengeListRes> ChallengeListSelect(ChallengeListReq dto) {
        QueryResults<ChallengeListRes> result = null;
        if (dto.getSize() == 0) {
            dto.setSize(10);
        }

        LocalDateTime startDateString = null;
        LocalDateTime endDateString = null;
        log.info("=====DSL INIT=====");
        if (dto.getSearchName() != null && dto.getSearchName().equals("DATE")) {
            startDateString = LocalDate.parse(dto.getStartDt()).atStartOfDay();
            endDateString = LocalDate.parse(dto.getEndDt()).atStartOfDay().plusDays(1l);
            log.info("sdaaa = {}", startDateString);
            log.info("edaaa = {}", endDateString);
        }

        result = queryFactory.select(Projections.fields(
                                ChallengeListRes.class,
                                qTbMssnCate.cateId,
                                qTbMssnCate.tpcTpCd,
                                qTbMssnCate.useYn,
                                qTbMssnCate.imgUrl,
                                qTbMssnCate.regId,
                                qTbMssnCate.regDttm,
                                qTbMssnCate.modId,
                                qTbMssnCate.modDttm,

                                qTbMssnCateMeta.prpsCd,
                                qTbMssnCateContents.cateTitle,

                                qTbCompany.companyId,
                                qTbCompany.companyName,
                                qTbCompany.contents,
                                qTbCompany.imgUrl,
                                qAdmin.adminName
                        )
                )
                .from(qTbMssnCate)
                .where(qTbMssnCateContents.langCd.eq("ko"),
                        qTbMssnCate.tpcTpCd.eq("TPC003"),
                        ChallengeListSelectEq(dto, startDateString, endDateString)
                )
                .leftJoin(qTbMssnCateContents).on(qTbMssnCateContents.cateId.eq(qTbMssnCate.cateId))
                .leftJoin(qTbMssnCateMeta).on(qTbMssnCateMeta.cateId.eq(qTbMssnCate.cateId))
                .leftJoin(qTbCompany).on(qTbCompany.companyId.eq(qTbMssnCate.companyId))
                .leftJoin(qAdmin).on(qAdmin.adminId.eq(qTbMssnCate.regId))
                .orderBy(qTbMssnCate.regDttm.desc())
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .fetchResults();

        return result;
    }

    private BooleanExpression ChallengeListSelectEq(ChallengeListReq dto, LocalDateTime sd, LocalDateTime ed) {

        if (dto.getSearchName() == null) {
            return null;
        } else if (dto.getSearchName().equals("")) {
            return null;
        } else if (dto.getSearchName().equals(ChallengeSearchType.CHALLENGENAME.getName())) {
            return qTbMssnCateContents.cateTitle.contains(dto.getSearchNameVal());
        } else if (dto.getSearchName().equals(ChallengeSearchType.COMPANYNAME.getName())) {
            return qTbCompany.companyName.contains(dto.getSearchNameVal());
        } else if (dto.getSearchName().equals(ChallengeSearchType.DATE.getName())) {
            return qTbMssnCate.regDttm.between(sd, ed);
        } else {
            return null;
        }

    }

    @Override
    public QueryResults<CateListRes> CateListRes(CateListReq dto) {

        QueryResults<CateListRes> result = null;
        if (dto.getSize() == 0) {
            dto.setSize(10);
        }

        LocalDateTime startDateString = null;
        LocalDateTime endDateString = null;
        log.info("=====DSL INIT=====");
        if (dto.getSearchName() != null && dto.getSearchName().equals("DATE")) {
            startDateString = LocalDate.parse(dto.getStartDt()).atStartOfDay();
            endDateString = LocalDate.parse(dto.getEndDt()).atStartOfDay().plusDays(1l);
            log.info("sdaaa = {}", startDateString);
            log.info("edaaa = {}", endDateString);
        }

        result = queryFactory.select(Projections.fields(
                                CateListRes.class,
                                qTbMssnCate.cateId,
                                qTbMssnCateMeta.prpsCd,
                                qTbMssnCateContents.cateTitle,
                                qTbMssnCate.regId,
                                qTbMssnCate.regDttm,
                                qTbMssnCate.modId,
                                qTbMssnCate.modDttm,
                                qAdmin.adminName
                        )
                )
                .from(qTbMssnCate)
                .where(qTbMssnCateContents.langCd.eq("ko"),
                        qTbMssnCate.tpcTpCd.eq("TPC002"),
                        CateListResEq(dto, startDateString, endDateString)
                )
                .leftJoin(qTbMssnCateContents).on(qTbMssnCateContents.cateId.eq(qTbMssnCate.cateId))
                .leftJoin(qTbMssnCateMeta).on(qTbMssnCateMeta.cateId.eq(qTbMssnCate.cateId))
                .leftJoin(qAdmin).on(qAdmin.adminId.eq(qTbMssnCate.regId))
                .orderBy(qTbMssnCate.regDttm.desc())
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .fetchResults();

        return result;
    }

    private BooleanExpression CateListResEq(CateListReq dto, LocalDateTime sd, LocalDateTime ed) {


        if (dto.getSearchName() == null) {
            return null;
        } else if (dto.getSearchName().equals("")) {
            return null;
        } else if (dto.getSearchName().equals(CateSearchType.NAME.getName())) {
            return qTbMssnCateContents.cateTitle.contains(dto.getSearchNameVal());
        } else if (dto.getSearchName().equals(CateSearchType.META.getName())) {
            return qTbMssnCateMeta.prpsCd.eq(dto.getMetaType());
        } else if (dto.getSearchName().equals(CateSearchType.DATE.getName())) {
            return qTbMssnCate.regDttm.between(sd, ed);
        } else {
            return null;
        }

    }

    @Override
    public Optional<UpdateCreateReqs> CateOneRes(Long seq) {

        UpdateCreateReqs result = null;


        result = queryFactory.select(Projections.fields(
                                UpdateCreateReqs.class,
                                qTbMssnCate.cateId,
                                qTbMssnCateMeta.prpsCd,
                                qTbMssnCate.imgUrl,
                                qTbMssnCate.regId,
                                qTbMssnCate.regDttm,
                                qTbMssnCate.modId,
                                qTbMssnCate.modDttm
                        )
                )
                .from(qTbMssnCate)
                .where(qTbMssnCate.cateId.eq(seq))
                .leftJoin(qTbMssnCateMeta).on(qTbMssnCateMeta.cateId.eq(qTbMssnCate.cateId))
                .fetchOne()
        ;

        return Optional.of(result);
    }

    @Override
    public ChallengeActionViewVo ChallengeActionOneViewDsl(Long seq) {

        ChallengeActionViewVo result = null;

        result = queryFactory.select(Projections.fields(
                                ChallengeActionViewVo.class,
                                qTbMssn.mssnId,
                                qTbMssn.tpcTpCd,
                                qTbMssn.imgUrl,
                                qTbMssn.exp,
                                qTbMssn.point,
                                qTbMssn.hit,
                                qTbMssn.useYn,
                                qTbMssn.regId,
                                qTbMssn.regDt,
                                qTbMssn.modId,
                                qTbMssn.modDt,
                                qTbMssn.cateId,
                                qTbMssn.linkUrl,

                                qTbMssnChallenge.challengeId,
                                qTbMssnChallenge.rangeYn,
                                qTbMssnChallenge.particptnTpCd,
                                qTbMssnChallenge.startDt,
                                qTbMssnChallenge.endDt,
                                qTbMssnChallenge.actDt,
                                qTbMssnChallenge.actArea,
                                qTbMssnChallenge.method

                        )
                )
                .from(qTbMssn)
                .where(
                        qTbMssn.mssnId.eq(seq)
                )
                .leftJoin(qTbMssnContent).on(qTbMssnContent.mssnId.eq(qTbMssn.mssnId))
                .leftJoin(qTbMssnChallenge).on(qTbMssnChallenge.mssnId.eq(qTbMssn.mssnId))
                .fetchFirst()
        ;


        return result;
    }


}
