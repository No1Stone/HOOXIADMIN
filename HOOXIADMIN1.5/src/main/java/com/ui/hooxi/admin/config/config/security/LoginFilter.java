package com.ui.hooxi.admin.config.config.security;

import com.google.gson.Gson;
import com.ui.hooxi.admin.biz.service.common.AdminInfoBean;
import com.ui.hooxi.admin.biz.service.login.CustomUserDetailsService;
import com.ui.hooxi.admin.config.type.PMenu;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class LoginFilter extends OncePerRequestFilter {

    private final AdminInfoBean adminInfoBean;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String reqUrl = request.getRequestURI().toString();
        if (reqUrl.startsWith("/admin/login")
                || reqUrl.startsWith("/admin/img")
                || reqUrl.startsWith("/admin/js")
                || reqUrl.startsWith("/admin/css")
                || reqUrl.startsWith("/admin/fontawesome")
                || reqUrl.startsWith("/admin/test")) {

            filterChain.doFilter(request, response);
        } else {
            log.info("필터 요청유알엘 - {}", request.getRequestURI().toString());
            Long info = null;

            if (request.getSession().getAttribute("seq") == null) {
//                  response.encodeRedirectURL("/admin/login/info");
                response.sendRedirect("/admin/login/info");
                response.setStatus(401);
            } else {
                info = Long.parseLong(request.getSession().getAttribute("seq").toString());
                Long pmsId = adminInfoBean.getSeqToAdmin().get(info).getAdminPmsId();
                log.info("pms id = {}", new Gson().toJson(adminInfoBean.getSeqToPmn(pmsId)));

                List<Long> pmnSeq = new ArrayList<>();
                adminInfoBean.getSeqToPmn(pmsId).stream().forEach(e -> pmnSeq.add(e.getAdminPmsPmnId()));
                // 로그인할때 퍼미션seq List<Long> 리스트를 넣어 놓는 로직으로 변경예정
                if (reqUrl.startsWith("/admin/user")) {
                    if (pmnSeq.contains(1L)) {
                        log.info("유저관리 O");
                    } else {
                        log.info("유저관리 X");
                        response.setStatus(401);
//                        response.sendRedirect("/admin/user/info/listView");
                    }
                }

                if (reqUrl.startsWith("/admin/active")) {
                    if (pmnSeq.contains(2L)) {
                        log.info("활동관리 O");
                    } else {
                        log.info("활동관리 X");
                        response.setStatus(401);
//                        response.sendRedirect("/admin/user/info/listView");
                    }
                }

                if (reqUrl.startsWith("/admin/manage")) {
                    if (pmnSeq.contains(3L)) {
                        log.info("어드민관리 O");
                    } else {
                        log.info("어드민관리 X");
                        response.setStatus(401);
//                        response.sendRedirect("/admin/user/info/listView");
                    }
                }

                filterChain.doFilter(request, response);
            }
        }
    }


}
