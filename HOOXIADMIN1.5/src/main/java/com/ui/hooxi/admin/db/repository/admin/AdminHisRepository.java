package com.ui.hooxi.admin.db.repository.admin;

import com.ui.hooxi.admin.db.entity.admin.AdminHis;
import com.ui.hooxi.admin.db.entity.admin.id.AdminHisId;
import com.ui.hooxi.admin.db.repository.admin.dsl.AdminRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminHisRepository extends JpaRepository<AdminHis, AdminHisId>  {

}
