package com.ui.hooxi.admin.db.entity.mission;

import com.ui.hooxi.admin.db.entity.mission.id.TbMssnRecomContentsId;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnRecomMetaId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_recom_meta")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnRecomMetaId.class)
public class TbMssnRecomMeta {

    @Id
    @Column(name = "recom_id", nullable = true)
    private Long recomId;
    @Id
    @Column(name = "prps_cd", nullable = true)
    private String prpsCd;

    @Builder
    public TbMssnRecomMeta(Long recomId, String prpsCd) {
        this.recomId = recomId;
        this.prpsCd = prpsCd;
    }


}
