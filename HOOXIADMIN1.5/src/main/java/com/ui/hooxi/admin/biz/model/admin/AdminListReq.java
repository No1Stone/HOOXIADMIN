package com.ui.hooxi.admin.biz.model.admin;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminListReq {


    private int page;
    private int size;

    private String searchName;
    private String searchNameVal;

    private String startDt;
    private String endDt;


}
