package com.ui.hooxi.admin.db.repository.admin;

import com.ui.hooxi.admin.db.entity.admin.AdminPmsPmn;
import com.ui.hooxi.admin.db.entity.admin.id.AdminPmsPmnId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdminPmsPmnRepository extends JpaRepository<AdminPmsPmn, AdminPmsPmnId> {

    List<AdminPmsPmn> findByAdminPmsId(Long pmsSeq);

}
