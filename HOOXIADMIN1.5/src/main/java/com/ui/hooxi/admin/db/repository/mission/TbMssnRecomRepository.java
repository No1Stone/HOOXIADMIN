package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnRecom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMssnRecomRepository extends JpaRepository<TbMssnRecom, Long> {
}
