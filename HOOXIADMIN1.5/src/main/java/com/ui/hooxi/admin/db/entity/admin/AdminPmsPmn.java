package com.ui.hooxi.admin.db.entity.admin;


import com.ui.hooxi.admin.db.entity.admin.id.AdminPmsPmnId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_admin_pms_pmn")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(AdminPmsPmnId.class)
public class AdminPmsPmn {

    @Id
    @Column(name = "admin_pms_pmn_id", nullable = true)
    private Long adminPmsPmnId;
    @Id
    @Column(name = "admin_pms_id", nullable = true)
    private Long adminPmsId;
    @Column(name = "admin_pms_nm", nullable = true)
    private String adminPmsNm;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dttm", nullable = true)
    private LocalDateTime regDttm;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dttm", nullable = true)
    private LocalDateTime modDttm;

    @Builder
    public AdminPmsPmn(Long adminPmsPmnId, Long adminPmsId, String adminPmsNm, Long regId, LocalDateTime regDttm, Long modId, LocalDateTime modDttm) {
        this.adminPmsPmnId = adminPmsPmnId;
        this.adminPmsId = adminPmsId;
        this.adminPmsNm = adminPmsNm;
        this.regId = regId;
        this.regDttm = regDttm;
        this.modId = modId;
        this.modDttm = modDttm;
    }
}
