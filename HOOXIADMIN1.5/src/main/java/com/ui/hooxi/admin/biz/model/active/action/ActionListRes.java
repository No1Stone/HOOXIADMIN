package com.ui.hooxi.admin.biz.model.active.action;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActionListRes {

    private Long mssnId;
    private String tpcTpCd;
    private String imgUrl;
    private String mssnTitle;
    private String mssnEffect;
    private int exp;
    private int point;
    private Long hit;
    private String useYn;
    private Long regId;
    private LocalDateTime regDt;
    private Long modId;
    private LocalDateTime modDt;
    private String adminName;

}
