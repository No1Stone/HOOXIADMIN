package com.ui.hooxi.admin.biz.model.admin;


import com.ui.hooxi.admin.config.type.YnType;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsCmn;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsPmn;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDateTime;

public class AdminPmsCmnReqTestModel {

    private Long adminPmsPmnId;
    private String adminCmnNm;
    private String adminCmnC;
    private String adminCmnR;
    private String adminCmnU;
    private String adminCmnD;

    public AdminPmsCmnReqTestModel(){}

    public AdminPmsCmnReqTestModel(Long adminPmsPmnId, String adminCmnNm, String adminCmnC, String adminCmnR, String adminCmnU, String adminCmnD) {
        this.adminPmsPmnId = adminPmsPmnId;
        this.adminCmnNm = adminCmnNm;
        this.adminCmnC = adminCmnC;
        this.adminCmnR = adminCmnR;
        this.adminCmnU = adminCmnU;
        this.adminCmnD = adminCmnD;
    }

    public Long getAdminPmsPmnId() {
        return adminPmsPmnId;
    }

    public void setAdminPmsPmnId(Long adminPmsPmnId) {
        this.adminPmsPmnId = adminPmsPmnId;
    }

    public String getAdminCmnNm() {
        return adminCmnNm;
    }

    public void setAdminCmnNm(String adminCmnNm) {
        this.adminCmnNm = adminCmnNm;
    }

    public String getAdminCmnC() {
        return adminCmnC;
    }

    public void setAdminCmnC(String adminCmnC) {
        this.adminCmnC = adminCmnC;
    }

    public String getAdminCmnR() {
        return adminCmnR;
    }

    public void setAdminCmnR(String adminCmnR) {
        this.adminCmnR = adminCmnR;
    }

    public String getAdminCmnU() {
        return adminCmnU;
    }

    public void setAdminCmnU(String adminCmnU) {
        this.adminCmnU = adminCmnU;
    }

    public String getAdminCmnD() {
        return adminCmnD;
    }

    public void setAdminCmnD(String adminCmnD) {
        this.adminCmnD = adminCmnD;
    }

    public AdminPmsCmn ofAdminPmsCmn(){

        AdminPmsCmn apc = new AdminPmsCmn();
        apc.setAdminPmsCmnId(1L);
        apc.setAdminPmsPmnId(this.adminPmsPmnId);
        apc.setAdminPmsId(1L);
        apc.setAdminCmnNm(this.adminCmnNm);
        apc.setAdminCmnC(this.adminCmnC);
        apc.setAdminCmnR(this.adminCmnR);
        apc.setAdminCmnU(this.adminCmnU);
        apc.setAdminCmnD(this.adminCmnD);
        apc.setModId(3L);
        apc.setRegId(3L);
        apc.setModDttm(LocalDateTime.now());
        apc.setRegDttm(LocalDateTime.now());
        return apc;
    }
}
