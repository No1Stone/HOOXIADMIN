package com.ui.hooxi.admin.biz.service.active;

import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.active.story.StoryListReq;
import com.ui.hooxi.admin.biz.model.active.story.StoryListRes;
import com.ui.hooxi.admin.biz.model.active.story.StoryOneRes;
import com.ui.hooxi.admin.biz.model.active.story.StoryPointVo;
import com.ui.hooxi.admin.biz.service.active.pack.PointApplication;
import com.ui.hooxi.admin.biz.service.active.pack.PointService;
import com.ui.hooxi.admin.biz.service.login.RegGetService;
import com.ui.hooxi.admin.db.entity.exp.TbExp;
import com.ui.hooxi.admin.db.entity.exp.TbMmbrExp;
import com.ui.hooxi.admin.db.entity.point.Point;
import com.ui.hooxi.admin.db.entity.point.PointDtl;
import com.ui.hooxi.admin.db.entity.point.model.PointRequest;
import com.ui.hooxi.admin.db.entity.point.type.PointStatCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnTpCd;
import com.ui.hooxi.admin.db.entity.story.TbStory;
import com.ui.hooxi.admin.db.repository.exp.TbAdminExpRepository;
import com.ui.hooxi.admin.db.repository.exp.TbExpRepository;
import com.ui.hooxi.admin.db.repository.exp.TbMmbrExpRepository;
import com.ui.hooxi.admin.db.repository.point.TbPointDtlRepository;
import com.ui.hooxi.admin.db.repository.point.TbPointRepository;
import com.ui.hooxi.admin.db.repository.story.TbStoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Slf4j
public class StoryService {

    private final TbStoryRepository tbStoryRepository;
    private final PointService pointService;
    private final TbPointRepository pointRepository;
    private final TbPointDtlRepository pointDtlRepository;
    private final TbExpRepository tbExpRepository;
    private final PointApplication pointApplication;
    private final RegGetService regGetService;
    private final TbAdminExpRepository tbAdminExpRepository;
    private final TbMmbrExpRepository tbMmbrExpRepository;

    public QueryResults<StoryListRes> storyListResQueryResults(StoryListReq dto) {
        dto.setSize(10);
        QueryResults<StoryListRes> result = tbStoryRepository.StoryListResListSelect(dto);
        return result;
    }


    public StoryOneRes StorySelectOneService(Long seq) {
        var result = tbStoryRepository.StoryOneResListSelect(seq).orElse(null);
        return result;
    }

    public void StoryStopUpdate(Long seq) {
        tbStoryRepository.UpdateStoryStatus("SRS004", seq);
    }

    public void StoryNormalUpdate(Long seq) {
        tbStoryRepository.UpdateStoryStatus("SRS001", seq);
    }

    public void StoryRejectUpdate(Long seq) {
        tbStoryRepository.UpdateStoryStatus("SRS003", seq);
    }

    public StoryPointVo ModalPointSelect(Long seq) {
        StoryPointVo result = tbStoryRepository.StoryPointExpSelect(seq);

        if (result == null) {
            return result = new StoryPointVo();
        } else {
            return result;
        }
    }

    public void PointSave() {


    }

    @Transactional
    public String StoryPointExpSaveService(StoryPointVo dto) {

        String result = "fail";
        int count = 0;

        if (pointRepository.existsByTrstnIdAndPointStateCdAndTrstnTpCdAndTrstnDtlTpCd(
                dto.getStoryId(),
                PointStatCd.ACCUMULATE,
                TrstnTpCd.STORY,
                TrstnDtlTpCd.STORY_CREATE
        )) {
            log.info("포인트 저장할수없음, 회수처리 후 재저장");
            return result = "포인트 저장할수없음, 회수처리 후 재저장해야합니다.";
        } else {
            log.info("포인트 저장가능");
            count++;
        }

//        if (tbExpRepository.existsByExpAcmltIdAndExpStateCdAndExpAcmltTpCdAndExpAcmltDtlTpCd(
//                dto.getStoryId(),
//                "ESC001",//경험치코드 획득
//                TrstnTpCd.STORY.getLegacyCode(),
//                TrstnDtlTpCd.STORY_CREATE.getLegacyCode()
//
//        )) {
//            log.info("경험치 저장할수없음, 경험치 회수 후 재저장해야합니다");
//            return result = "경험치 저장할수없음, 경험치 회수 후 재저장해야합니다";
//        } else {
//            log.info("경험치 부여 가능");
//            count++;
//        }

        if (count == 1) {

            if (dto.getPoint() == null) {
                return result = "포인트는 0일수 없습니다.";
            }
//            else if (dto.getExp() == null) {
//                return result = "경험치는 0일수 없습니다.";
//            }
            else if (dto.getPoint() == 0) {
                return result = "포인트는 0일수 없습니다.";
            }
//            else if (dto.getExp() == 0) {
//                return result = "경험치는 0일수 없습니다.";
//            }
            else {
                tbStoryRepository.UpdateStoryStatus("SRS001", dto.getStoryId());
                //포인트 적립 로직
                PointRequest pointSaveRequest = new PointRequest();
                TbStory storyResult = tbStoryRepository.findById(dto.getStoryId()).get();
                pointSaveRequest.setMmbrId(storyResult.getMmbrId());
                pointSaveRequest.setTrstnId(dto.getStoryId());
                pointSaveRequest.setPoint(Long.parseLong(String.valueOf(dto.getPoint())));
                pointSaveRequest.setTrstnTpCd(TrstnTpCd.STORY);
                pointSaveRequest.setTrstnDtlTpCd(TrstnDtlTpCd.STORY_CREATE);

                Point savePoint = pointRepository.save(pointSaveRequest.toSavaEntity(storyResult.getMmbrId(), regGetService.getRegId()));
//                savePoint.saveOrgPointId();
                PointDtl savePointDtl = pointDtlRepository.save(savePoint.toSavePointDtlEntity(regGetService.getRegId()));
//                savePointDtl.saveUpdate();
                // 포인트 적립 로직
                Point pointSelect = pointRepository.findByTrstnIdAndPointStateCdAndTrstnTpCdAndTrstnDtlTpCd(
                        dto.getStoryId(),
                        PointStatCd.WAIT_ACCUMULATE,
                        TrstnTpCd.STORY,
                        TrstnDtlTpCd.STORY_CREATE
                ).get();
                pointSelect.updateStoryPointState();
                PointDtl pointDtl = pointDtlRepository.findByTrstnIdAndPointStateCdAndTrstnTpCdAndTrstnDtlTpCd(
                        dto.getStoryId(),
                        PointStatCd.WAIT_ACCUMULATE,
                        TrstnTpCd.STORY,
                        TrstnDtlTpCd.STORY_CREATE
                ).get();
                pointDtl.updateStoryPointState();


                // 경험치 적립 로직시작

                //경험치를 저장한다.
//                TbExp expSava = tbExpRepository.save(
//                        new TbExp().toSave(
//                                storyResult.getMmbrId(),
//                                dto.getStoryId(),
//                                dto.getExp(),
//                                regGetService.getRegId()
//                        )
//                );
//                //경험치 합계를 구한다.
//                Long expSum = tbExpRepository.MmbrExpSum(78L);
//
//                //어드민 exp에서 칭호를 검색한다.
//                //현재레벨
//                String adminFindLevel = tbExpRepository.AdminExpLevelFind(expSum);
//                //다음레벨까지 남은 경험치
//                Long adminNectLovel = tbExpRepository.AdminExpNextLevelFind(expSum) - expSum;
//                //유저 칭호를 변경한다.
//
//                TbMmbrExp tme = new TbMmbrExp();
//                tbMmbrExpRepository.save(
//                        tme.ofSave(storyResult.getMmbrId(), adminFindLevel, expSum, adminNectLovel)
//                );
                // 경험치 적립 로직 끝
                return result = "성공";
            }
        } else {
            log.info("로직 실패");
            return result = "실패";
        }

    }

    public void expSumTest() {
        Long result = tbExpRepository.MmbrExpSum(78L);
        log.info("expSumTest - {}", result);
    }

    public void adminExpFindTest() {
        String adminFindLevel = tbExpRepository.AdminExpLevelFind(1200L);
        Long adminNectLovel = tbExpRepository.AdminExpNextLevelFind(1200L);
        log.info("adminFindLevel - {}", adminFindLevel);
        log.info("adminNectLovel - {}", adminNectLovel);
    }

    public void pointgetTest() {
        StoryPointVo result = tbStoryRepository.StoryPointExpSelect(1L);

        log.info("point EXP result = {}", new Gson().toJson(result));
    }

    public void ModiStoryContentsService(StoryOneRes dto) {
        log.info("StoryContents Modi - {}",new Gson().toJson(dto));

        tbStoryRepository.UpdateStoryContents(dto.getStoryCntn(), dto.getStoryId());

    }
}
