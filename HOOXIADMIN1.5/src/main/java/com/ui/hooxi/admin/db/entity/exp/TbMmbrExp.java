package com.ui.hooxi.admin.db.entity.exp;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "tb_mmbr_exp")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbMmbrExp {

    @Id
    @Column(name = "mmbr_id", nullable = true)
    private Long mmbrId;
    @Column(name = "lvl_cd", nullable = true)
    private String lvlCd;
    @Column(name = "title", nullable = true)
    private String title;
    @Column(name = "exp", nullable = true)
    private Long exp;
    @Column(name = "next_ncssry_exp", nullable = true)
    private Long nextNcssryExp;
    @Column(name = "exp_acmlt_dt", nullable = true)
    private LocalDateTime expAcmltDt;

    @Builder
    public TbMmbrExp(Long mmbrId, String lvlCd, String title, Long exp, Long nextNcssryExp, LocalDateTime expAcmltDt) {
        this.mmbrId = mmbrId;
        this.lvlCd = lvlCd;
        this.title = title;
        this.exp = exp;
        this.nextNcssryExp = nextNcssryExp;
        this.expAcmltDt = expAcmltDt;
    }

    public TbMmbrExp ofSave(Long mmbrId, String lvlCd, Long exp, Long nextExp){
        TbMmbrExp tme = new TbMmbrExp();
        tme.setMmbrId(mmbrId);
        tme.setLvlCd(lvlCd);
        tme.setExp(exp);
        tme.setNextNcssryExp(nextExp);
        tme.setExpAcmltDt(LocalDateTime.now());
        return tme;
    }

}
