package com.ui.hooxi.admin.biz.cont.v1;

import com.google.gson.Gson;
import com.ui.hooxi.admin.biz.model.AdminSearch;
import com.ui.hooxi.admin.biz.model.UserSearchType;
import com.ui.hooxi.admin.biz.model.UserStatusType;
import com.ui.hooxi.admin.biz.model.admin.AdminCreateReq;
import com.ui.hooxi.admin.biz.model.admin.AdminListReq;
import com.ui.hooxi.admin.biz.model.admin.AdminOneviewRes;
import com.ui.hooxi.admin.biz.model.admin.AdminSearchType;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerCreateListRes;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerCreateReq;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerCreateRes;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerListReq;
import com.ui.hooxi.admin.biz.restcont.TestService;
import com.ui.hooxi.admin.biz.service.admin.AdminService;
import com.ui.hooxi.admin.config.type.CMenu;
import com.ui.hooxi.admin.config.type.PMenu;
import com.ui.hooxi.admin.config.util.ThymeLeafPaging;
import com.ui.hooxi.admin.db.entity.admin.AdminPms;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.descriptor.web.ContextHandler;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@CrossOrigin("*")
@RequiredArgsConstructor
@Controller
@RequestMapping(path = "/manage")
@Slf4j
public class ManagementController {

    private final AdminService adminService;
    private final TestService testService;
    private final HttpServletRequest httpServletRequest;

    @ModelAttribute("PMenu")
    public List<PMenu> PMENUType() {
        return Arrays.asList(PMenu.ALL);
    }

    @ModelAttribute("CMenu")
    public List<CMenu> CMENUType() {
        return Arrays.asList(CMenu.ALL);
    }


    @ModelAttribute("UserSearchType")
    public List<UserSearchType> UserSearchType() {
        return Arrays.asList(UserSearchType.LIST);
    }

    @ModelAttribute("UserStatusType")
    public List<UserStatusType> UserStatusType() {
        return Arrays.asList(UserStatusType.LIST);
    }

    @ModelAttribute("AdminSearchType")
    public List<AdminSearchType> AdminSearchType() {
        return Arrays.asList(AdminSearchType.LIST);
    }


    @GetMapping(path = "/admin/listView")
    public ModelAndView List(
            Authentication auth,
            Principal principal,
            @AuthenticationPrincipal User user
//            SecurityContext sc
    ) {
        var aa = adminService.test7(new AdminListReq());
        log.info("result - {}", new Gson().toJson(aa));

        String id = (String) httpServletRequest.getSession().getAttribute("seq");
        SecurityContext contex = (SecurityContext) httpServletRequest.getSession().getAttribute("SPRING_SECURITY_CONTEXT");


//        log.info("auth -{}", auth.getName());
//        log.info("auth -{}", auth.getDetails());
//        log.info("auth -{}", auth.getPrincipal());
        ModelAndView mv = new ModelAndView("layout/manage/admin/listView")
                .addObject("adminList", aa)
                .addObject("page", new ThymeLeafPaging(aa));
        return mv;
    }

    @ResponseBody
    @PostMapping(path = "/admin/listSelect")
    public ModelAndView SearchBinding(Model model, AdminListReq dto) {
//        QueryResults<Admin>
        log.info("databind - {}", new Gson().toJson(dto));
        var aa = adminService.test7(dto);
        log.info("result - {}", new Gson().toJson(aa));
//        ModelAndView mv = new ModelAndView("layout/manage/admin/searchList :: #listArea")
        var page = new ThymeLeafPaging(aa);
        log.info("page - {}", new Gson().toJson(page));

        ModelAndView mv = new ModelAndView("layout/manage/admin/searchList")
                .addObject("adminList", aa)
                .addObject("page", new ThymeLeafPaging(aa))
//                .addObject("adminCreateReq", new AdminCreateReq())
//                .addObject("adminPmsList", adminService.PermisionList())
                ;

        return mv;
        //return mv;
//        model.addAttribute("adminList", aa);
//        return "layout/manage/admin/searchList";
//        return "layout/manage/admin/listView :: #listArea";
    }

    @GetMapping(path = "/admin/oneView")
    public ModelAndView OneVireController(@RequestParam("seq") Long seq) {
        AdminOneviewRes result = adminService.AdminSelectOneView(seq);
        log.info("result - {}", new Gson().toJson(result));
        ModelAndView mv = new ModelAndView("layout/manage/admin/oneView")
                .addObject("adminOneView", result);
        return mv;
    }

    @GetMapping(path = "/admin/insert")
    public ModelAndView AdminInsert() {
        var aa = adminService.test7(new AdminListReq());
        log.info("result - {}", new Gson().toJson(aa));
        ModelAndView mv = new ModelAndView("layout/manage/admin/insert")
                .addObject("adminCreateReq", new AdminCreateReq());
        return mv;
    }

    @PostMapping(path = "/admin/createReq")
    public ModelAndView AdminCreateReq(AdminCreateReq dto) {
        log.info("Admin Create Dto - {}", new Gson().toJson(dto));

        adminService.adminCreateService(dto);

        var aa = adminService.test7(new AdminListReq());
        ModelAndView mv = new ModelAndView("layout/manage/admin/searchList")
                .addObject("adminList", aa)
                .addObject("page", new ThymeLeafPaging(aa))
                .addObject("adminCreateReq", new AdminCreateReq());
        return mv;
    }

    @GetMapping(path = "/admin/selectModal")
    public ModelAndView AdminSelectCreateModal() {
        ModelAndView mv = new ModelAndView("layout/manage/admin/modal")
                .addObject("adminCreateReq", new AdminCreateReq())
                .addObject("adminPmsList", adminService.PermisionList())
//                .addObject("adminPmsList",new ArrayList<AdminPms>())

                ;
        return mv;
    }

    @GetMapping(path = "/per/listView")
    public ModelAndView Permission(AdminPerListReq dto) {


        ModelAndView mv = new ModelAndView("layout/manage/per/listView")
                .addObject("adminPerList", new AdminPerListReq());
        return mv;
    }


    @ResponseBody
    @PostMapping(path = "/per/listSelect")
    public ModelAndView AdminPermissionSearchBinding(AdminPerListReq dto) {
        var result = adminService.PermissionListSelect(dto);
        ModelAndView mv = new ModelAndView("layout/manage/per/searchList")
                .addObject("adminPerList", result)
                .addObject("page", new ThymeLeafPaging(result));
    return mv;
    }

    @GetMapping(path = "/per/selectModal")
    public ModelAndView AdminPerSelectCreateModal(@RequestParam(name = "seq")Long seq) {
        ModelAndView mv = new ModelAndView("layout/manage/per/modal")
//                .addObject("adminPerCreateReq", new AdminPerCreateReq())
                .addObject("adminPerCreateReq", adminService.PermissionModal(seq))
//                .addObject("adminPmsList",new ArrayList<AdminPms>())

                ;
        return mv;
    }


    @PostMapping(path = "/per/insert")
    public ModelAndView AdminPerInsert(AdminPerCreateRes dto) {

        log.info("modal insert -{}", new Gson().toJson(dto));

        adminService.PermissionSaveService(dto);

        var result = adminService.PermissionListSelect(new AdminPerListReq() );
        ModelAndView mv = new ModelAndView("layout/manage/per/searchList")
                .addObject("adminPerList", result)
                .addObject("page", new ThymeLeafPaging(result));
        return mv;
    }

    @GetMapping(path = "/per/oneView")
    public ModelAndView AdminPerOneViewController(@RequestParam(name = "seq")Long seq) {
        ModelAndView mv = new ModelAndView("layout/manage/per/oneView")
//                .addObject("adminPerCreateReq", new AdminPerCreateReq())
                .addObject("adminPerCreateReq", adminService.PermissionModal(seq))
//                .addObject("adminPmsList",new ArrayList<AdminPms>())

                ;
        return mv;
    }
}

