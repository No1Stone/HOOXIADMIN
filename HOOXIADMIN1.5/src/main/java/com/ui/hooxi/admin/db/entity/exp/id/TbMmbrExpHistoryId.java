package com.ui.hooxi.admin.db.entity.exp.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class TbMmbrExpHistoryId implements Serializable {

    private Long historyId;
    private Long mmbrId;

}
