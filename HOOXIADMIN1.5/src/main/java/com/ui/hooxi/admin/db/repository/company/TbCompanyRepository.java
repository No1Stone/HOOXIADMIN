package com.ui.hooxi.admin.db.repository.company;

import com.ui.hooxi.admin.db.entity.company.TbCompany;
import com.ui.hooxi.admin.db.repository.company.dsl.TbCompanyRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TbCompanyRepository extends JpaRepository<TbCompany, Long> , TbCompanyRepositoryDsl {



}
