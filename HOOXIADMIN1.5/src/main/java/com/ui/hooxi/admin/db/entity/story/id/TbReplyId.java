package com.ui.hooxi.admin.db.entity.story.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbReplyId implements Serializable {
    private Long replyId;
    private Long storyId;
}
