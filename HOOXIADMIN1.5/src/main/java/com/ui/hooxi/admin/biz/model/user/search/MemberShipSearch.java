package com.ui.hooxi.admin.biz.model.user.search;

public enum MemberShipSearch {

    ALL("전체"),
    NICKNAME("닉네임"),
    EMAIL("이메일"),
    GRADE("등급"),
    MMBRSHPSTATECD("승인상태"),
    DATE("신청일");
    private String val;

    MemberShipSearch(String val) {
        this.val = val;
    }

    public static final MemberShipSearch[] LIST = {ALL, NICKNAME, EMAIL, GRADE, MMBRSHPSTATECD, DATE};

    public String getVal() {
        return val;
    }

    public String getName() {
        return name();
    }


}
