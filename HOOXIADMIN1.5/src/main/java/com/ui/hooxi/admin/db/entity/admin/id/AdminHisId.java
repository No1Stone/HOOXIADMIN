package com.ui.hooxi.admin.db.entity.admin.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class AdminHisId implements Serializable {
    private Long adminId;
    private LocalDateTime adminLoginDttm;
}
