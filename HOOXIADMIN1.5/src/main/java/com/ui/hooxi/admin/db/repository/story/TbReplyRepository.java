package com.ui.hooxi.admin.db.repository.story;

import com.ui.hooxi.admin.db.entity.story.TbReply;
import com.ui.hooxi.admin.db.entity.story.id.TbReplyId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbReplyRepository extends JpaRepository<TbReply, TbReplyId> {
}
