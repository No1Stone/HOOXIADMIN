package com.ui.hooxi.admin.db.repository.admin;

import com.ui.hooxi.admin.db.entity.admin.AdminPmsCmn;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsPmn;
import com.ui.hooxi.admin.db.entity.admin.id.AdminPmsCmnId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdminPmsCmnRepository extends JpaRepository<AdminPmsCmn, AdminPmsCmnId> {


    List<AdminPmsCmn> findByAdminPmsId(Long pmsSeq);

}
