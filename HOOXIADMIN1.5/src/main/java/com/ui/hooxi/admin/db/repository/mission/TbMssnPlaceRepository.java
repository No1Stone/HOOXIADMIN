package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnPlace;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnPlaceId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TbMssnPlaceRepository extends JpaRepository<TbMssnPlace, TbMssnPlaceId> {

    List<TbMssnPlace> findByMssnId(Long seq);

}
