package com.ui.hooxi.admin.db.entity.story.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class TbLikeId implements Serializable {
    private Long storyId;
    private Long mmbrId;
}
