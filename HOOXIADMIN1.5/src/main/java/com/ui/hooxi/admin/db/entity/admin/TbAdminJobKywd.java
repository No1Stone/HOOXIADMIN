package com.ui.hooxi.admin.db.entity.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tb_admin_job_kywd")
@Entity
public class TbAdminJobKywd {
    @Id
    @Column(name = "kywd_id", nullable = false)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}