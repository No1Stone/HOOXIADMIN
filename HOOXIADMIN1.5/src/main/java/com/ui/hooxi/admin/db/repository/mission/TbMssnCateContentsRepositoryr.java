package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnCateContents;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnCateContentsId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TbMssnCateContentsRepositoryr extends JpaRepository<TbMssnCateContents, TbMssnCateContentsId> {

    List<TbMssnCateContents> findByCateId(Long seq);

    Optional<TbMssnCateContents> findByCateIdAndLangCd(Long seq, String lang);

    boolean existsByCateIdAndLangCd(Long seq, String lang);


}
