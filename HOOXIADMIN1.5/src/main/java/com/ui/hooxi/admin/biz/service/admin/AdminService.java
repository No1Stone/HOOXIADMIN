package com.ui.hooxi.admin.biz.service.admin;

import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.admin.*;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerCreateReq;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerCreateRes;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerListReq;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerListRes;
import com.ui.hooxi.admin.biz.service.common.AdminInfoBean;
import com.ui.hooxi.admin.biz.service.login.RegGetService;
import com.ui.hooxi.admin.config.type.LoginStatus;
import com.ui.hooxi.admin.config.type.PMenu;
import com.ui.hooxi.admin.db.entity.admin.*;
import com.ui.hooxi.admin.db.repository.admin.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AdminService {

    private final AdminRepository adminRepository;
    private final AdminHisRepository adminHisRepository;
    private final AdminPmsRepository adminPmsRepository;
    private final AdminPmsPmnRepository adminPmsPmnRepository;
    private final AdminPmsCmnRepository adminPmsCmnRepository;
    private final PasswordEncoder passwordEncoder;
    private final RegGetService regGetService;
    private final AdminInfoBean adminInfoBean;

    public void test1() {

    }

    public void test2(AdminJoinReqTestModel dto) {
        Admin adminResult = adminRepository.save(dto.ofAdmin(passwordEncoder));
        adminHisRepository.save(AdminHis
                .builder()
                .adminId(adminResult.getAdminId())
                .adminLoginDttm(LocalDateTime.now())
                .adminLoginRsltCd(LoginStatus.LOGIN_LGT.getVal())
                .build());
    }

    public AdminHis test3() {
        return adminHisRepository.save(AdminHis
                .builder()
                .adminId(3L)
                .adminLoginDttm(LocalDateTime.now())
                .adminLoginRsltCd(LoginStatus.LOGIN_LGT.getVal())
                .build());
    }

    public AdminPms test4(AdminPmsTestModel dto) {
        return adminPmsRepository.save(dto.ofAdminPms());
    }

    public AdminPmsPmn test5(AdminPmsPmnReqTestModel dto) {
        return adminPmsPmnRepository.save(dto.ofAdminPmsPmn());
    }


    public AdminPmsCmn test6(AdminPmsCmnReqTestModel dto) {
        return adminPmsCmnRepository.save(dto.ofAdminPmsCmn());
    }

    public QueryResults<AdminListRes> test7(AdminListReq dto) {
        AdminListReq alr = new AdminListReq();
        log.info("dto value = {}", new Gson().toJson(dto));
//        dto.setPage(0);
        if (dto.getSize() == 0) {
            dto.setSize(10);
        }
//        adminRepository.AdminListSelect(alr).getResults().stream().map(e -> e.getAdminEmail());

        adminInfoBean.ReSetCode();
        return adminRepository.AdminListSelect(dto);
    }

    public List<AdminPms> PermisionList() {
        List<AdminPms> aps = adminPmsRepository.findAll();
        return aps;
    }

    public void adminCreateService(AdminCreateReq dto) {
        Admin result = adminRepository.save(dto.ofAdmin(passwordEncoder, regGetService.getRegId()));
        log.info("SaveResult - {}", result);
    }

    public QueryResults<AdminPerListRes> PermissionListSelect(AdminPerListReq dto) {

        var result = adminRepository.AdminPerSelectList(dto);

        return result;
    }

    @Transactional
    public void PermissionSaveService(AdminPerCreateRes dto) {
        log.info("==init==");
        Long regId = regGetService.getRegId();
        log.info("==regId=={}", regId);
        AdminPms ap = adminPmsRepository.save(dto.ofPermission(regId));
        log.info("==ap==",new Gson().toJson(ap.getAdminPmsId()));

        dto.ofPmn(regId, ap.getAdminPmsId()).stream().forEach(e -> log.info("ofPmn - {}",new Gson().toJson(e)));
        dto.ofPmn(regId, ap.getAdminPmsId()).stream().forEach(e -> adminPmsPmnRepository.save(e));
        log.info("==ofPmn==");

        dto.ofCmn(regId, ap.getAdminPmsId()).stream().forEach(e -> log.info("ofCmn - {}",new Gson().toJson(e)));
        dto.ofCmn(regId, ap.getAdminPmsId()).stream().forEach(e -> adminPmsCmnRepository.save(e));
        log.info("==ofCmn==");
    }

    public AdminOneviewRes AdminSelectOneView(Long seq) {
        AdminOneviewRes result = new AdminOneviewRes();

        Admin admin = adminRepository.findById(seq).get();

        result.setAdminId(admin.getAdminId());
        result.setAdminEmail(admin.getAdminEmail());
        result.setAdminName(admin.getAdminName());
        result.setAdminPwd(admin.getAdminPwd());
        result.setAdminPmsId(admin.getAdminPmsId());
        result.setUseYn(admin.getUseYn());
        result.setRegId(admin.getRegId());
        result.setRegDttm(admin.getRegDttm());

        return result;
    }

    public AdminPerCreateRes PermissionModal(Long seq) {

        log.info("permission seq = {}", seq);

        if(seq.equals(0L)){
            log.info("null = {}", seq);
            return new AdminPerCreateRes();
        }
        else {
            log.info("!null = {}", seq);

            AdminPerCreateRes apcr = new AdminPerCreateRes();
            List<String> resultString = new ArrayList<>();
            List<AdminPmsPmn> resultPms = adminPmsPmnRepository.findByAdminPmsId(seq);

            for(AdminPmsPmn e : resultPms){
                if(e.getAdminPmsPmnId().equals(1L)){
                    resultString.add(PMenu.USER_LIST.getName());
                }
                if(e.getAdminPmsPmnId().equals(2L)){
                    resultString.add(PMenu.ACTIVE_LIST.getName());
                }
                if(e.getAdminPmsPmnId().equals(3L)){
                    resultString.add(PMenu.ADMIN_LIST.getName());
                }
            }
            apcr.setAdminPmsNm(resultPms.get(0).getAdminPmsNm());
            apcr.setPmenuVal(resultString);
            log.info(" apcr result - {} ", new Gson().toJson(apcr));
            return apcr;
        }
    }
}
