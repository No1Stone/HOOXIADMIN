package com.ui.hooxi.admin.biz.model.active.challenge;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActionSelectListVo {

    private Long mssnId;
    private String mssnTitle;

}
