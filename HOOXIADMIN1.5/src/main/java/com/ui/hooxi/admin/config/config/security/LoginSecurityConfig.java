package com.ui.hooxi.admin.config.config.security;

import com.ui.hooxi.admin.biz.service.login.CustomUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@RequiredArgsConstructor
public class LoginSecurityConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private final CustomUserDetailsService customUserDetailsService;

    @Override
    public void configure(HttpSecurity http) {
//        LoginFilter customFilter = new LoginFilter(customUserDetailsService);
//        http.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
