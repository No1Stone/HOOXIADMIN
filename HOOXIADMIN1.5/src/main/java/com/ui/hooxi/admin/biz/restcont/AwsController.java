package com.ui.hooxi.admin.biz.restcont;

import com.ui.hooxi.admin.biz.service.util.S3AWSService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1/aws")
@RequiredArgsConstructor
@Slf4j
@CrossOrigin("*")
public class AwsController {

    private final S3AWSService s3AWSService;

    @GetMapping(path = "/presign")
    public String PresignURLController(){
       return s3AWSService.CreatePresignUrl("","");
    }

}
