package com.ui.hooxi.admin.config.util;


import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.db.entity.admin.Admin;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter

public class ThymeLeafPaging {

    private Long page;
    private Long size;
    private Long start;
    private Long end;
    private Long backPage;
    private Long nextPage;
    private Long startNum;
    private Long lastNum;
    private Long total;
    private Long offset;

    public ThymeLeafPaging() {
    }
    ;

    public ThymeLeafPaging(Long page, Long size, Long start, Long end, Long backPage, Long nextPage, Long startNum, Long lastNum, Long total, Long offset) {
        this.page = page;
        this.size = size;
        this.start = start;
        this.end = end;
        this.backPage = backPage;
        this.nextPage = nextPage;
        this.startNum = startNum;
        this.lastNum = lastNum;
        this.total = total;
        this.offset = offset;
    }

    public ThymeLeafPaging(QueryResults qr) {
        this.page = qr.getOffset() / qr.getLimit();
        this.size = qr.getLimit();
        this.start = 0L;
        var end = qr.getTotal() % qr.getLimit() == 0 ? qr.getTotal() / qr.getLimit() -1 : qr.getTotal() / qr.getLimit();
        this.end = end;
        this.backPage = qr.getOffset() / qr.getLimit() < 10 ? 0 : qr.getOffset() / 100L * 10l -10L;
        this.nextPage = page + 10L > end ? end : page + 10L;
        this.startNum = qr.getOffset() / 100L * 10l;
        this.lastNum = startNum + 9L > end ? end : startNum + 9L;
        this.total = qr.getTotal();
        this.offset = qr.getOffset();
    }

}
