package com.ui.hooxi.admin.db.repository.admin;

import com.ui.hooxi.admin.db.entity.admin.AdminPms;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdminPmsRepository extends JpaRepository<AdminPms, Long> {

}
