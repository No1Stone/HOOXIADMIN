package com.ui.hooxi.admin.db.repository.member.dsl;

import com.querydsl.core.QueryResults;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;

@Aspect
@RequiredArgsConstructor
@Slf4j
public class TbMmbrshpRepositoryDslImpl implements TbMmbrshpRepositoryDsl{

    private final JPAQueryFactory queryFactory;

    @Override
    public QueryResults UserListSelect() {

        return null;
    }
}
