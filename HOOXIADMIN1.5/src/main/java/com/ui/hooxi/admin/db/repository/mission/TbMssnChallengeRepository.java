package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnChallenge;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnChallengeId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMssnChallengeRepository extends JpaRepository<TbMssnChallenge, TbMssnChallengeId> {


}
