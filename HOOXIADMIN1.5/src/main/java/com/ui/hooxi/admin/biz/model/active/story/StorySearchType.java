package com.ui.hooxi.admin.biz.model.active.story;

public enum StorySearchType {

    ALL("-검색유형선택-"),
    STORYSTATUS("상태"),
    CONTENTS("내용"),
    FEEDTYPE("피드타입"),
    PRIVATETYPE("공개범위"),
    DATE("일자");

    private String val;

    public static final StorySearchType[] LIST = {CONTENTS, STORYSTATUS, FEEDTYPE, PRIVATETYPE, DATE};

    StorySearchType(String val) {
        this.val = val;
    }

    public String getVal() {
        return this.val;
    }

    public String getName() {
        return name();
    }

}
