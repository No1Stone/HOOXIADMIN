package com.ui.hooxi.admin.biz.service.common;

import com.ui.hooxi.admin.db.repository.code.TbCmnCodeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CodeService {

    private final TbCmnCodeRepository tbCmnCodeRepository;


    private final CodeBean codeBean;

    public Object test0() {
        return codeBean.getCodeList();
    }

    public Object test1(String name) {
        return codeBean.getCodeList(name);
    }

    public Object test2() {
        return codeBean.getNameToCode();
    }

    public Object test3(String name) {
        log.info("ma - {}",name);
        return codeBean.getNameToCode(name);
    }

    public void test4() {
        codeBean.ReSetCode();
    }

    public void test5() {
        String result = CodeSeqUp("mrs");
        log.info("result -= {}", result);
    }


    public String CodeSeqUp(String groupName) {
        String val = groupName.toUpperCase();
        var re = tbCmnCodeRepository.findByCdContainsOrderByCdDesc(val);
        var repl = re.get(0).getCd().replaceAll(val, "");
        return val + String.format("%03d", Integer.parseInt(repl) + 1);
    }


}
