package com.ui.hooxi.admin.db.repository.member;

import com.ui.hooxi.admin.db.entity.member.TbMmbrshp;
import com.ui.hooxi.admin.db.entity.member.TbMmbrshpJob;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMmbrshpJobRepository extends JpaRepository<TbMmbrshpJob, TbMmbrshp> {
}