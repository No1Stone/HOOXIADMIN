package com.ui.hooxi.admin.biz.model.admin;

import com.ui.hooxi.admin.config.type.YnType;
import com.ui.hooxi.admin.db.entity.admin.Admin;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Column;
import java.time.LocalDateTime;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminCreateReq {
    private String adminEmail;
    private String adminName;
    private String adminPwd;
    private Long adminPmsId;

    public Admin ofAdmin(PasswordEncoder pass, Long regId) {
        Admin a = new Admin();
        a.setAdminEmail(this.adminEmail);
        a.setAdminName(this.adminName);
        a.setAdminPwd(pass.encode(this.adminPwd));
        a.setAdminPmsId(this.adminPmsId);
        a.setUseYn(YnType.Y.getVal());
        a.setRegId(regId);
        a.setRegDttm(LocalDateTime.now());
        return a;
    }


}
