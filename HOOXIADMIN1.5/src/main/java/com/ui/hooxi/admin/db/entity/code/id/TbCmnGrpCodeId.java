package com.ui.hooxi.admin.db.entity.code.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class TbCmnGrpCodeId implements Serializable {

    private Long id;
    private String grpCdNm;

}
