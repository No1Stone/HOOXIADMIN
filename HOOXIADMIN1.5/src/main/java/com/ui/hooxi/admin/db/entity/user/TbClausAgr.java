package com.ui.hooxi.admin.db.entity.user;

import com.ui.hooxi.admin.db.entity.user.id.TbClausAgrId;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.Instant;

@Table(name = "tb_claus_agr")
@Entity
public class TbClausAgr {
    @EmbeddedId
    private TbClausAgrId id;

    @Column(name = "claus_agr_yn", length = 1)
    private String clausAgrYn;

    @Column(name = "frst_rgst_dttm")
    private Instant frstRgstDttm;

    @Column(name = "fnl_mod_dttm")
    private Instant fnlModDttm;

    public Instant getFnlModDttm() {
        return fnlModDttm;
    }

    public void setFnlModDttm(Instant fnlModDttm) {
        this.fnlModDttm = fnlModDttm;
    }

    public Instant getFrstRgstDttm() {
        return frstRgstDttm;
    }

    public void setFrstRgstDttm(Instant frstRgstDttm) {
        this.frstRgstDttm = frstRgstDttm;
    }

    public String getClausAgrYn() {
        return clausAgrYn;
    }

    public void setClausAgrYn(String clausAgrYn) {
        this.clausAgrYn = clausAgrYn;
    }

    public TbClausAgrId getId() {
        return id;
    }

    public void setId(TbClausAgrId id) {
        this.id = id;
    }
}