package com.ui.hooxi.admin.biz.model.active.subaction;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.format.DateTimeFormatter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubActionReq {

    private int page;
    private int size;

    private String searchName;
    private String searchNameVal;
    private String searchStatus;
    private String searchStatusVal;
    private String cntCode;
    private String startDt;
    private String endDt;
    private Long userSeq;


    private DateTimeFormatter dt = DateTimeFormatter.ofPattern("yyyy-MM-dd");


}
