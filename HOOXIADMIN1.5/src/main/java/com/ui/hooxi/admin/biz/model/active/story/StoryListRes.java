package com.ui.hooxi.admin.biz.model.active.story;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
public class StoryListRes {

    private Long storyId;
    private Long mmbrId;
    private Long actTlId;
    private String storyStatCd;
    private String storyCntn;
    private String feedTpCd;
    private String privateTpCd;
    private Long comentCount;
    private Long viewCnt;
    private String locat;

    private LocalDateTime regDt;

    public StoryListRes(Long storyId, Long mmbrId, Long actTlId, String storyStatCd, String storyCntn, String feedTpCd, String privateTpCd, Long comentCount, Long viewCnt, String locat, LocalDateTime regDt) {
        this.storyId = storyId;
        this.mmbrId = mmbrId;
        this.actTlId = actTlId;
        this.storyStatCd = storyStatCd;
        this.storyCntn = replace(storyCntn);
        this.feedTpCd = feedTpCd;
        this.privateTpCd = privateTpCd;
        this.comentCount = comentCount;
        this.viewCnt = viewCnt;
        this.locat = locat;
        this.regDt = regDt;
    }


    public Long getStoryId() {
        return storyId;
    }

    public void setStoryId(Long storyId) {
        this.storyId = storyId;
    }

    public Long getMmbrId() {
        return mmbrId;
    }

    public void setMmbrId(Long mmbrId) {
        this.mmbrId = mmbrId;
    }

    public Long getActTlId() {
        return actTlId;
    }

    public void setActTlId(Long actTlId) {
        this.actTlId = actTlId;
    }

    public String getStoryStatCd() {
        return storyStatCd;
    }

    public void setStoryStatCd(String storyStatCd) {
        this.storyStatCd = storyStatCd;
    }

    public String getStoryCntn() {
        return replace(this.storyCntn);
    }

    public void setStoryCntn(String storyCntn) {
        this.storyCntn = replace(storyCntn);
    }

    public String getFeedTpCd() {
        return feedTpCd;
    }

    public void setFeedTpCd(String feedTpCd) {
        this.feedTpCd = feedTpCd;
    }

    public String getPrivateTpCd() {
        return privateTpCd;
    }

    public void setPrivateTpCd(String privateTpCd) {
        this.privateTpCd = privateTpCd;
    }

    public Long getComentCount() {
        return comentCount;
    }

    public void setComentCount(Long comentCount) {
        this.comentCount = comentCount;
    }

    public Long getViewCnt() {
        return viewCnt;
    }

    public void setViewCnt(Long viewCnt) {
        this.viewCnt = viewCnt;
    }

    public String getLocat() {
        return locat;
    }

    public void setLocat(String locat) {
        this.locat = locat;
    }

    public LocalDateTime getRegDt() {
        return regDt;
    }

    public void setRegDt(LocalDateTime regDt) {
        this.regDt = regDt;
    }

    private String replace(String data) {
        String result = data;
        result = result.replaceAll("\\<.*?>","");
        return result;
    }
}
