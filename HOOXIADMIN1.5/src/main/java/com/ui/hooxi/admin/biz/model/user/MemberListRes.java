package com.ui.hooxi.admin.biz.model.user;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MemberListRes {
    private Long mmbrId;
    private String mmbrNm;
    private String nickname;
    private String email;
    private String gradCd;
    private Long story;
    private Long like;//hit
    private String mmbrshpStateCd;
    private LocalDateTime frstRgstDttm;
}
