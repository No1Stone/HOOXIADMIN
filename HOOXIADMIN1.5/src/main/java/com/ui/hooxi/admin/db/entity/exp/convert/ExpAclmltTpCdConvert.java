package com.ui.hooxi.admin.db.entity.exp.convert;

import com.ui.hooxi.admin.config.util.AbstractLegacyEnumAttributeConverter;
import com.ui.hooxi.admin.db.entity.exp.type.ExpAcmltTpCd;

import javax.persistence.Converter;

@Converter
public class ExpAclmltTpCdConvert extends AbstractLegacyEnumAttributeConverter<ExpAcmltTpCd> {
	public static final String ENUM_NAME = "경험치적립유형코드";

	public ExpAclmltTpCdConvert() {
		super(ExpAcmltTpCd.class,true, ENUM_NAME);
	}
}
