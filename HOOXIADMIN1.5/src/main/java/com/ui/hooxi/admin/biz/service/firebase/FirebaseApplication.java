package com.ui.hooxi.admin.biz.service.firebase;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.ui.hooxi.admin.biz.service.firebase.model.FcmMessage;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FirebaseApplication {

    private final Logger logger = LoggerFactory.getLogger(FirebaseApplication.class);
    private final FirebaseMessaging firebaseMessaging;

    public String sendNotification(FcmMessage fcmMessage, String token) throws FirebaseMessagingException {

        Notification notification = Notification
                .builder()
                .setTitle("title")
                .setBody("body")
                .build();

        Message message = Message
                .builder()
                .setToken(token)
                .setNotification(notification)
//                .putAllData(note.getData())
                .build();

        return firebaseMessaging.send(message);
    }
}
