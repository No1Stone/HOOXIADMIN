package com.ui.hooxi.admin.biz.cont.v1;

import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.user.CompanyListReq;
import com.ui.hooxi.admin.biz.model.user.CompanyOneVo;
import com.ui.hooxi.admin.biz.model.user.UserListReq;
import com.ui.hooxi.admin.biz.model.UserSearchType;
import com.ui.hooxi.admin.biz.model.UserStatusType;
import com.ui.hooxi.admin.biz.model.user.UserListRes;
import com.ui.hooxi.admin.biz.model.user.search.CompanySearch;
import com.ui.hooxi.admin.biz.model.user.search.MemberShipSearch;
import com.ui.hooxi.admin.biz.service.common.AdminInfo;
import com.ui.hooxi.admin.biz.service.common.AdminInfoBean;
import com.ui.hooxi.admin.biz.service.common.CodeBean;
import com.ui.hooxi.admin.biz.service.user.UserService;
import com.ui.hooxi.admin.config.type.PMenu;
import com.ui.hooxi.admin.config.util.ThymeLeafPaging;
import com.ui.hooxi.admin.db.entity.code.TbCmnCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
@RequestMapping(path = "/user")
@Slf4j
public class UserController {

    private final HttpServletRequest httpServletRequest;
    private final CodeBean codeBean;
    private final AdminInfoBean adminInfoBean;

    @ModelAttribute("PMenu")
    public List<PMenu> PMENUType() {
        return Arrays.asList(PMenu.ALL);
    }

    @ModelAttribute("codes")
    public Map<String, String> CodeBeanList() {
        log.info("codes  - {}",codeBean.getNameToString());
        return codeBean.getNameToString();
    }

    @ModelAttribute("adminIdMap")
    public Map<Long, String> adminId(){
        return adminInfoBean.getSeqToString();
    }
    @ModelAttribute("adminIdsMap")
    public Map<String, String> adminIds(){
        return adminInfoBean.getStringToString();
    }

    @ModelAttribute("UserSearchType")
    public List<UserSearchType> UserSearchType() {
        return Arrays.asList(UserSearchType.LIST);
    }

    @ModelAttribute("MemberShipSearchType")
    public List<MemberShipSearch> MemberShipSearchType() {
        return Arrays.asList(MemberShipSearch.LIST);
    }
    @ModelAttribute("CompanySearchType")
    public List<CompanySearch> CompanySearchType() {
        return Arrays.asList(CompanySearch.LIST);
    }

    @ModelAttribute("MembershipStatusType")
    public Map<String, String > MembershipStatusType() {
        return codeBean.getNameToString("MSS");
    }

    @ModelAttribute("MembershipGradeType")
    public Map<String, String > MembershipGradeType() {
        return codeBean.getNameToString("GDT");
    }

    @ModelAttribute("UserStatusType")
    public Map<String, String > UserStatusType() {
        return codeBean.getNameToString("MRS");
    }
    @ModelAttribute("LangStatusType")
    public Map<String, String > LangStatusType() {
        return codeBean.getNameToStringGRP("LANG_TP_CD");
    }

    private final UserService userService;

    @GetMapping(path = "/info/listView")
    public ModelAndView List(UserListReq dto) {
        log.info("dto - {}", new Gson().toJson(dto));

        Long id = Long.parseLong(httpServletRequest.getSession().getAttribute("seq").toString());
        log.info("userinfo id ==== {} ",id);

        ModelAndView mv = new ModelAndView("layout/user/user/listView")
                .addObject("userSelectForm", new UserListReq())
//                .addObject("userList", userService.UserSelectList(dto))
        ;
        return mv;
    }


//    @ResponseBody
    @PostMapping(path = "/info/listSelect")
    public ModelAndView SearchBinding(UserListReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));
        QueryResults<UserListRes> result = userService.UserSelectList(dto);
        ModelAndView mv = new ModelAndView("layout/user/user/searchList")
                .addObject("userList", result)
                .addObject("page", new ThymeLeafPaging(result));
        return mv;
    }



    @GetMapping(path = "/info/oneView")
    public ModelAndView OneView(@RequestParam(value = "seq") Long seq) {
        log.info("dto - {}", new Gson().toJson(seq));
        ModelAndView mv = new ModelAndView("layout/user/user/oneView")
                .addObject("userForm", userService.UserSelectOneService(seq));
        return mv;
    }

    @GetMapping(path = "/info/stop")
    @ResponseBody
    public String UserStopUpdate(@RequestParam(value = "seq") Long seq) {
        log.info("seq = {}", seq);

        userService.UserStopUpdateService(seq);

        return "stopsuccess";
    }

    @GetMapping(path = "/info/normal")
    @ResponseBody
    public String UserNormalUpdate(@RequestParam(value = "seq") Long seq) {
        log.info("seq = {}", seq);
        userService.UserNormalUpdateService(seq);
        return "normalsuccess";
    }


    @GetMapping(path = "/mem/listView")
    public ModelAndView MemList(UserListReq dto) {
        log.info("dto - {}", new Gson().toJson(dto));
        ModelAndView mv = new ModelAndView("layout/user/member/listView")
                .addObject("userForm", new UserListReq());

//                .addObject("userList", userService.MembershipSelectList(dto));
        return mv;
    }

    @ResponseBody
    @PostMapping(path = "/mem/listSelect")
    public ModelAndView MemSearchBinding(UserListReq dto) {
//        QueryResults<Admin>
        log.info("databind - {}", new Gson().toJson(dto));
        var aa = userService.MembershipSelectList(dto);
//        log.info("result - {}", new Gson().toJson(aa));
        ModelAndView mv = new ModelAndView("layout/user/member/searchList")
                .addObject("userList", userService.MembershipSelectList(dto))
                .addObject("page", new ThymeLeafPaging(aa));
        return mv;
    }

    @GetMapping(path = "/mem/oneView")
    public ModelAndView MemberOneView(@RequestParam(value = "seq") Long seq) {
        log.info("dto - {}", new Gson().toJson(seq));
        ModelAndView mv = new ModelAndView("layout/user/member/oneView")
                .addObject("selectForm", userService.MemberSelectOneService(seq));
        return mv;
    }

    @GetMapping(path = "/mem/stop")
    @ResponseBody
    public String MemberUserStopUpdate(@RequestParam(value = "seq") Long seq) {
        log.info("seq = {}", seq);
        userService.MemberShipeStopUpdate(seq);
        return "stopsuccess";
    }
    @GetMapping(path = "/mem/normal")
    @ResponseBody
    public String MemberUserNormalUpdate(@RequestParam(value = "seq") Long seq) {
        log.info("seq = {}", seq);
        userService.MemberShipeNormalUpdate(seq);
        return "stopsuccess";
    }
    @GetMapping(path = "/mem/reject")
    @ResponseBody
    public String MemberUserRejectUpdate(@RequestParam(value = "seq") Long seq) {
        log.info("seq = {}", seq);
        userService.MemberShipeRejectUpdate(seq);
        return "stopsuccess";
    }

    @GetMapping(path = "/com/listView")
    public ModelAndView ComListController(CompanyListReq dto) {
        log.info("dto - {}", new Gson().toJson(dto));
        ModelAndView mv = new ModelAndView("layout/user/com/listView")
                .addObject("comForm", new CompanyListReq())
                ;
        return mv;
    }

    @ResponseBody
    @PostMapping(path = "/com/listSelect")
    public ModelAndView ComSearchBinding(CompanyListReq dto) {
//        QueryResults<Admin>
        log.info("databind - {}", new Gson().toJson(dto));
        var aa = userService.CompanyListSelectService(dto);
//        log.info("result - {}", new Gson().toJson(aa));
        ModelAndView mv = new ModelAndView("layout/user/com/searchList")
                .addObject("comList", userService.CompanyListSelectService(dto))
                .addObject("page", new ThymeLeafPaging(aa));
        return mv;
    }

    @GetMapping(path = "/com/oneView")
    public ModelAndView CompanyOneViewController(@RequestParam(value = "seq") Long seq) {
        log.info("dto - {}", new Gson().toJson(seq));
        ModelAndView mv = new ModelAndView("layout/user/com/oneView")
                .addObject("selectForm", userService.CompanyOneView(seq));
        return mv;
    }

    @GetMapping(path = "/com/create")
    public ModelAndView CompanyCreateController() {
        log.info("create init");
        ModelAndView mv = new ModelAndView("layout/user/com/oneView")
                .addObject("selectForm", new CompanyOneVo());
        return mv;
    }

    @PostMapping(path = "/com/save")
    @ResponseBody
    public String CompanySaveController(@RequestParam(name = "file") MultipartFile[] file, CompanyOneVo dto) {
        log.info("Save init");
        userService.CompanySaveService(file, dto);
        return "/admin/user/com/listView";
    }
}
