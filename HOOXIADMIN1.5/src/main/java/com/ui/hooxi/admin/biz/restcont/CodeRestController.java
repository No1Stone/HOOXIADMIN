package com.ui.hooxi.admin.biz.restcont;

import com.ui.hooxi.admin.biz.service.common.CodeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v1/code")
@RequiredArgsConstructor
@Slf4j
@CrossOrigin("*")
public class CodeRestController {

    private final CodeService codeService;

    @GetMapping(path = "/test0")
    public Object test0() {
        return codeService.test0();
    }

    @GetMapping(path = {"/test1/","/test1"})
    public Object test1as() {
        return codeService.test0();
    }

    @GetMapping(path = "/test1/{name}")
    public Object test1(@PathVariable(name = "name") String name) {
        return codeService.test1(name);
    }

    @GetMapping(path = {"/test2/", "/test2"})
    public Object test2as() {
        return codeService.test2();
    }

    @GetMapping(path = "/test2/{name}")
    public Object test2(@PathVariable(name = "name") String name) {
        return codeService.test3(name);
    }

    @GetMapping(path = "/test4")
    public void test4() {
        codeService.test4();
    }


}
