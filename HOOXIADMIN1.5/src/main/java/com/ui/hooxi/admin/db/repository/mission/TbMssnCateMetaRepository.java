package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnCateMeta;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnCateMetaId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TbMssnCateMetaRepository extends JpaRepository<TbMssnCateMeta, TbMssnCateMetaId> {

    List<TbMssnCateMeta> findByCateId(Long seq);



    boolean existsByCateId(Long cateId);

    @Modifying
    @Transactional
    @Query(
            value = "DELETE From tb_mssn_cate_meta where cate_id = :cateId "
            ,nativeQuery = true
    )
    void DeletePlace(@Param("cateId")Long cateId);


}
