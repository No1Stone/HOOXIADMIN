package com.ui.hooxi.admin.biz.service.common;

import com.ui.hooxi.admin.config.type.YnType;
import com.ui.hooxi.admin.db.entity.admin.AdminPms;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsPmn;
import com.ui.hooxi.admin.db.entity.code.TbCmnCode;
import com.ui.hooxi.admin.db.repository.admin.AdminPmsCmnRepository;
import com.ui.hooxi.admin.db.repository.admin.AdminPmsPmnRepository;
import com.ui.hooxi.admin.db.repository.admin.AdminPmsRepository;
import com.ui.hooxi.admin.db.repository.admin.AdminRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class AdminInfoBean {

    private final AdminRepository adminRepository;
    private final AdminPmsRepository adminPmsRepository;
    private final AdminPmsCmnRepository adminPmsCmnRepository;
    private final AdminPmsPmnRepository adminPmsPmnRepository;

    private List<AdminInfo> adminList = null;
    private Map<Long, AdminInfo> seqToAdmin = null;
    private Map<Long, String> seqToString = null;
    private Map<String, String> stringToString = null;
    private List<AdminPmsPmn> pmns = null;

    @PostConstruct
    private void PostSetAdmin() {
        ReSetCode();
    }

    private void ListReset() {
        this.adminList = new ArrayList<>();
    }

    private void MapReset() {
        this.seqToAdmin = new HashMap<>();
    }

    private void MapToStringReset() {
        this.seqToAdmin = new HashMap<>();
    }

    private void StringToStringReset() {
        this.seqToAdmin = new HashMap<>();
    }

    private void PmnReset() {
        this.pmns = new ArrayList<>();
    }

    public List<AdminInfo> getCodeList() {
        return this.adminList;
    }

    public List<AdminInfo> getCodeList(Long seq) {
        return this.adminList.stream().filter(e -> e.getAdminId().equals(seq))
                .collect(Collectors.toList());
    }

    public Map<Long, AdminInfo> getSeqToAdmin() {
        return this.seqToAdmin;
    }

    public List<AdminPmsPmn> getSeqToPmn() {
        return this.pmns;
    }

    public List<AdminPmsPmn> getSeqToPmn(Long seq) {
        return this.pmns.stream().filter(e -> e.getAdminPmsId().equals(seq)).collect(Collectors.toList());
    }

    public Map<Long, AdminInfo> getSeqToAdmin(Long seq) {
        return this.adminList.stream().filter(admin -> admin.getAdminId().equals(seq))
                .collect(Collectors.toMap(AdminInfo::getAdminId, admin -> admin));
    }

    public Map<Long, String> getSeqToString() {
        return this.seqToString;
    }

    public Map<String, String> getStringToString() {
        return stringToString;
    }

    @Transactional(readOnly = true)
    public void ReSetCode() {
        ListReset();
        MapReset();
        PmnReset();
        MapToStringReset();
        StringToStringReset();
        this.adminList = adminRepository.findByUseYn(YnType.Y.getName())
                .stream().map(e -> AdminInfo.builder()
                        .adminId(e.getAdminId())
                        .adminEmail(e.getAdminEmail())
                        .adminName(e.getAdminName())
                        .adminPmsId(e.getAdminPmsId())
                        .build()
                ).collect(Collectors.toList());

        this.seqToAdmin = adminRepository.findByUseYn(YnType.Y.getName())
                .stream().map(e -> AdminInfo.builder()
                        .adminId(e.getAdminId())
                        .adminEmail(e.getAdminEmail())
                        .adminName(e.getAdminName())
                        .adminPmsId(e.getAdminPmsId())
                        .build()
                ).collect(Collectors.toMap(AdminInfo::getAdminId, admin -> admin));


        this.pmns = adminPmsPmnRepository.findAll();

        this.seqToString = adminRepository.findByUseYn(YnType.Y.getName())
                .stream().map(e -> AdminInfo.builder()
                        .adminId(e.getAdminId())
                        .adminEmail(e.getAdminEmail())
                        .adminName(e.getAdminName())
                        .adminPmsId(e.getAdminPmsId())
                        .build()
                ).collect(Collectors.toMap(AdminInfo::getAdminId, admin -> admin.getAdminName()));

        this.stringToString = adminRepository.findByUseYn(YnType.Y.getName())
                .stream().map(e -> AdminInfo.builder()
                        .adminId(e.getAdminId())
                        .adminEmail(e.getAdminEmail())
                        .adminName(e.getAdminName())
                        .adminPmsId(e.getAdminPmsId())
                        .build()
                ).collect(Collectors.toMap(AdminInfo -> AdminInfo.getAdminId().toString(), admin -> admin.getAdminName()));
    }
}



