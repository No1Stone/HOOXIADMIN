package com.ui.hooxi.admin.db.entity.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tb_admin_srvy")
@Entity
public class TbAdminSrvy {
    @Id
    @Column(name = "srvy_id", nullable = false)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}