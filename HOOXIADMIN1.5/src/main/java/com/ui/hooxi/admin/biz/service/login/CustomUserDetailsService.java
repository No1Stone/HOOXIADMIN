package com.ui.hooxi.admin.biz.service.login;

import com.google.gson.Gson;
import com.ui.hooxi.admin.config.type.CMenu;
import com.ui.hooxi.admin.config.type.PMenu;
import com.ui.hooxi.admin.config.type.YnType;
import com.ui.hooxi.admin.db.entity.admin.Admin;
import com.ui.hooxi.admin.db.entity.admin.AdminPms;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsCmn;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsPmn;
import com.ui.hooxi.admin.db.repository.admin.AdminPmsCmnRepository;
import com.ui.hooxi.admin.db.repository.admin.AdminPmsPmnRepository;
import com.ui.hooxi.admin.db.repository.admin.AdminPmsRepository;
import com.ui.hooxi.admin.db.repository.admin.AdminRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomUserDetailsService implements UserDetailsService {

    private final AdminRepository adminRepository;
    private final AdminPmsRepository adminPmsRepository;
    private final AdminPmsPmnRepository adminPmsPmnRepository;
    private final AdminPmsCmnRepository adminPmsCmnRepository;
//    private final AuthenticationManager authenticationManager;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        var a = adminRepository.findByAdminEmail(email)
                .map(this::createUserDetail)
                .orElseThrow(() -> new UsernameNotFoundException(email + " -> 데이터베이스에서 찾을 수 없습니다."));

        log.info("Detatil - {}", new Gson().toJson(a));

        return a;
    }



    private UserDetails createUserDetail(Admin admin) {

        log.info("===============USER DETAIL INIT===============");

        List<Long> app = adminPmsPmnRepository.findByAdminPmsId(admin.getAdminPmsId())
                .stream().map(e -> e.getAdminPmsPmnId())
                .collect(Collectors.toList());
        List<AdminPmsCmn> apc = adminPmsCmnRepository.findByAdminPmsId(admin.getAdminPmsId());



        List<String> authList = new ArrayList<>();


        for(Long e : app){
            if(e == 1l){
                authList.add(PMenu.USER_LIST.getAuthName());
            }
            if(e == 2l){
                authList.add(PMenu.ACTIVE_LIST.getAuthName());
            }
            if(e == 3l){
                authList.add(PMenu.ADMIN_LIST.getAuthName());
            }

        }


        for(AdminPmsCmn e : apc){
            if(e.getAdminPmsCmnId() == 10l){
                if(e.getAdminCmnC().equals(YnType.Y.getVal())) {
                    log.info("== 10l ==");
                    authList.add(CMenu.USER_R.getAuthName());
                }
                if(e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    log.info("== 10l ==");
                    authList.add(CMenu.USER_R.getAuthName());
                }
            }

            if(e.getAdminPmsCmnId() == 20l) {
                if(e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    log.info("== 20l ==");
                    authList.add(CMenu.MEMBERSHIP_R.getAuthName());
                }
            }
            if(e.getAdminPmsCmnId() == 30l) {
                if(e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    log.info("== 30l ==");
                    authList.add(CMenu.STORY_R.getAuthName());
                }
            }
            if(e.getAdminPmsCmnId() == 40l) {
                if(e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    log.info("== 40l ==");
                    authList.add(CMenu.ACTION_R.getAuthName());
                }
            }
            if(e.getAdminPmsCmnId() == 50l) {
                if(e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    authList.add(CMenu.CHALLENGE_R.getAuthName());
                }
            }
            if(e.getAdminPmsCmnId() == 60l) {
                if(e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    authList.add(CMenu.COMUNITY_R.getAuthName());
                }
            }
            if(e.getAdminPmsCmnId() == 70l) {
                if(e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    authList.add(CMenu.ADMINLIST_R.getAuthName());
                }
            }
            if(e.getAdminPmsCmnId() == 80l) {
                if(e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    authList.add(CMenu.PERMISSION_R.getAuthName());
                }
            }

        }



        log.info("app - {}", new Gson().toJson(app));
        log.info("apc - {}", new Gson().toJson(apc));
        log.info("authList - {}", new Gson().toJson(authList));



        var permission=  authList.stream()
//               .map(e -> e.getAdminCmnNm())
                .map(SimpleGrantedAuthority::new).collect(Collectors.toList());



        return new User(String.valueOf(admin.getAdminId()),
                admin.getAdminPwd(), permission)
                ;
    }

}

