package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnCate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMssnCateRepository extends JpaRepository<TbMssnCate, Long> {
}
