package com.ui.hooxi.admin.biz.model.admin;

import com.ui.hooxi.admin.db.entity.admin.AdminPms;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;


public class AdminPmsTestModel {

    private String adminPmsNm;


    public AdminPmsTestModel(){}
    public AdminPmsTestModel(String adminPmsNm) {
        this.adminPmsNm = adminPmsNm;
    }

    public String getAdminPmsNm() {
        return adminPmsNm;
    }

    public void setAdminPmsNm(String adminPmsNm) {
        this.adminPmsNm = adminPmsNm;
    }

    public AdminPms ofAdminPms(){
        AdminPms ap = new AdminPms();
        ap.setAdminPmsNm(this.adminPmsNm);
        ap.setRegDttm(LocalDateTime.now());
        ap.setRegId(3L);
        ap.setModDttm(LocalDateTime.now());
        ap.setModId(3L);
        return ap;
    }

}
