package com.ui.hooxi.admin.biz.service.user;

import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.user.*;
import com.ui.hooxi.admin.biz.service.common.CodeBean;
import com.ui.hooxi.admin.biz.service.login.RegGetService;
import com.ui.hooxi.admin.biz.service.util.S3AWSService;
import com.ui.hooxi.admin.db.entity.company.TbCompany;
import com.ui.hooxi.admin.db.entity.member.TbMmbrshp;
import com.ui.hooxi.admin.db.repository.company.TbCompanyRepository;
import com.ui.hooxi.admin.db.repository.member.TbMmbrshpRepository;
import com.ui.hooxi.admin.db.repository.user.TbMmbrRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final TbMmbrRepository tbMmbrRepository;
    private final TbMmbrshpRepository tbMmbrshpRepository;
    private final TbCompanyRepository tbCompanyRepository;
    private final S3AWSService s3AWSService;
    private final RegGetService regGetService;
    private final CodeBean codeBean;

    public void UserListSelectService(UserListReq userListReq) {
    }

    public QueryResults<UserListRes> UserSelectList(UserListReq dto) {
        log.info("=====================");
        if (dto.getSize() == 0) {
            dto.setSize(10);
        }
        QueryResults<UserListRes> result = tbMmbrRepository.userListResListSelect(dto);
        log.info(" - - - {}", new Gson().toJson(result));
        return result;
    }

    public QueryResults<MemberListRes> MembershipSelectList(UserListReq dto) {
        log.info("=====================");
        if (dto.getSize() == 0) {
            dto.setSize(10);
        }
        var aa = tbMmbrRepository.MembershipListResListSelect(dto);
        log.info(" - - - {}", new Gson().toJson(aa));
        return aa;
    }
    @Transactional
    public UserOneRes UserSelectOneService(Long seq) {
        UserOneRes result = tbMmbrRepository.UserOneRes(seq);
        return result;
    }
    @Transactional
    public void UserStopUpdateService(Long seq) {
        tbMmbrRepository.UpdateMemberStatus("MRS002", seq);
    }
    @Transactional
    public void UserNormalUpdateService(Long seq) {
        tbMmbrRepository.UpdateMemberStatus("MRS001", seq);
    }

    @Transactional
    public MemberOneRes MemberSelectOneService(Long seq) {

        MemberOneRes result = tbMmbrRepository.MemberShipOneSelect(seq);

        return result;
    }
    @Transactional
    public void MemberShipeStopUpdate(Long seq) {
        tbMmbrshpRepository.UpdateMemberShipStatus("MSS004", seq);
    }

    @Transactional
    public void MemberShipeNormalUpdate(Long seq) {
        tbMmbrshpRepository.UpdateMemberShipStatus("MSS001", seq);
        tbMmbrshpRepository.UpdateMemberGradStatus("GDT002", seq);
    }

    @Transactional
    public void MemberShipeRejectUpdate(Long seq) {
        tbMmbrshpRepository.UpdateMemberShipStatus("MSS003", seq);
    }

    public QueryResults<CompanyListRes> CompanyListSelectService(CompanyListReq dto) {
        QueryResults<CompanyListRes> result = tbCompanyRepository.CompannyListSelectList(dto);
        return result;
    }
    @Transactional
    public CompanyOneVo CompanyOneView(Long seq) {
        CompanyOneVo result = new CompanyOneVo();

        TbCompany tc = tbCompanyRepository.getById(seq);
        result.setCompanyId(tc.getCompanyId());
        result.setCompanyName(tc.getCompanyName());
        result.setContents(tc.getContents());
        result.setImgUrl(tc.getImgUrl());
        result.setRegId(tc.getRegId());
        result.setRegDttm(tc.getRegDttm());
        result.setModId(tc.getModId());
        result.setModDttm(tc.getModDttm());

        return result;
    }
    @Transactional
    public void CompanySaveService(MultipartFile[] file, CompanyOneVo dto) {
        TbCompany result = null;
        if (file[0].getName() != null && !file[0].getOriginalFilename().equals("")) {
            try {
                List<String> imgString = s3AWSService.ObjectSaveService(file);
                log.info("file name -= {}", imgString.get(0));
                result = tbCompanyRepository.save(dto.ofSave(regGetService.getRegId(), imgString.get(0)));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            result = tbCompanyRepository.save(dto.ofSave(regGetService.getRegId()));
        }
    }
}
