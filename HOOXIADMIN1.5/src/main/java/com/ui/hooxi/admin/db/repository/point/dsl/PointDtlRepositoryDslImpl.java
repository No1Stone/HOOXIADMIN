package com.ui.hooxi.admin.db.repository.point.dsl;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.ui.hooxi.admin.db.entity.point.QPoint;
import com.ui.hooxi.admin.db.entity.point.QPointDtl;
import com.ui.hooxi.admin.db.entity.point.model.PointDtlAmtDTO;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.List;

@Aspect
public class PointDtlRepositoryDslImpl implements PointDtlRepositoryDsl {


    private EntityManager em;
    private JPAQueryFactory queryFactory;

    private final Logger logger = LoggerFactory.getLogger(PointDtlRepositoryDslImpl.class);

    private QPointDtl qPointDtl = QPointDtl.pointDtl;
    private QPoint qPoint = QPoint.point1;

    private PointDtlRepositoryDslImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    /**
     * Point Dtl List
     * @param mmbrId
     * @return
     */
    public List<PointDtlAmtDTO> pointDtlList(Long mmbrId) {
        Path<LocalDateTime> expireDt = Expressions.datePath(LocalDateTime.class, "expireDt");
        List<PointDtlAmtDTO> query = queryFactory
                .select(
                        Projections.fields(PointDtlAmtDTO.class,
                                qPointDtl.dtlOrgPointId,
                                qPointDtl.point.sum().as("pointAmt"),
                                qPointDtl.expireDt.min().as(expireDt),
                                qPoint.point.max().as("orgPointAmt")
                        )
                )
                .from(qPointDtl).innerJoin(qPoint).on(qPointDtl.pointId.eq(qPoint.pointId))
                .where(
                        qPointDtl.mmbrId.eq(mmbrId),
                        qPointDtl.expireDt.gt(LocalDateTime.now())
                ).groupBy(qPointDtl.dtlOrgPointId)
                .having(qPointDtl.point.sum().gt(0L))
                .orderBy(
                        ((ComparableExpressionBase<LocalDateTime>)expireDt).asc(),
                        qPointDtl.dtlOrgPointId.asc()
                )
                .fetch();
        return query;
    }

    @Override
    public PointDtlAmtDTO pointDtlList(Long pointDtlId, Long mmbrId) {
        Path<LocalDateTime> expireDt = Expressions.datePath(LocalDateTime.class, "expireDt");
        PointDtlAmtDTO query = queryFactory
                .select(
                        Projections.fields(PointDtlAmtDTO.class,
                                qPointDtl.dtlOrgPointId,
                                qPointDtl.point.sum().as("pointAmt"),
                                qPointDtl.expireDt.min().as(expireDt),
                                qPointDtl.point.max().as("orgPointAmt")
                        )
                )
                .from(qPointDtl)
                .where(
                        qPointDtl.mmbrId.eq(mmbrId),
                        qPointDtl.dtlOrgPointId.eq(pointDtlId),
                        qPointDtl.expireDt.gt(LocalDateTime.now())
                ).groupBy(qPointDtl.dtlOrgPointId)
                .having(qPointDtl.point.sum().gt(0L))
                .orderBy(
                        ((ComparableExpressionBase<LocalDateTime>)expireDt).asc(),
                        qPointDtl.dtlOrgPointId.asc()
                )
                .fetchOne();
        return query;
    }


}
