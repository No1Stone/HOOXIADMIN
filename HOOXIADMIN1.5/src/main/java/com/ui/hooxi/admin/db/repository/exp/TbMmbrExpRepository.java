package com.ui.hooxi.admin.db.repository.exp;

import com.ui.hooxi.admin.db.entity.exp.TbMmbrExp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMmbrExpRepository extends JpaRepository<TbMmbrExp, Long> {
}
