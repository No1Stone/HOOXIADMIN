package com.ui.hooxi.admin.biz.model;


import com.ui.hooxi.admin.db.entity.admin.Admin;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

public class LoginReq {

    private String userId;
    private String passwd;

    public LoginReq() {
    }

    public LoginReq(String userId, String passwd) {
        this.userId = userId;
        this.passwd = passwd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public boolean ofValid(PasswordEncoder passwordEncoder, String dbPass) {
        return passwordEncoder.matches(this.passwd, dbPass);
    }

//    public UsernamePasswordAuthenticationToken toAuthentication() {
//        return new UsernamePasswordAuthenticationToken(userId, passwd);
//    }

    public Admin ofSave(PasswordEncoder passwordEncoder) {
        Admin a = new Admin();

        a.setAdminPwd(passwordEncoder.encode(this.passwd));
        return a;
    }

}
