package com.ui.hooxi.admin.db.entity.mission;

import com.ui.hooxi.admin.db.entity.mission.id.TbMssnMetaId;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnPlaceId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_place")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnPlaceId.class)
public class TbMssnPlace {

    @Id
    @Column(name = "mssn_id", nullable = true)
    private Long mssnId;
    @Id
    @Column(name = "place_cd", nullable = true)
    private String placeCd;

    @Builder
    public TbMssnPlace(Long mssnId, String placeCd) {
        this.mssnId = mssnId;
        this.placeCd = placeCd;
    }

}
