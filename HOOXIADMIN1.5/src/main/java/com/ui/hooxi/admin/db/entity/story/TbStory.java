package com.ui.hooxi.admin.db.entity.story;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "tb_story")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbStory {
    @Id
    @Column(name = "story_id", nullable = true)
    private Long storyId;
    @Column(name = "mmbr_id", nullable = true)
    private Long mmbrId;
    @Column(name = "act_tl_id", nullable = true)
    private Long actTlId;
    @Column(name = "story_stat_cd", nullable = true)
    private String storyStatCd;
    @Column(name = "story_tp_cd", nullable = true)
    private String storyTpCd;
    @Column(name = "feed_tp_cd", nullable = true)
    private String feedTpCd;
    @Column(name = "story_cntn", nullable = true)
    private String storyCntn;
    @Column(name = "link_url", nullable = true)
    private String linkUrl;
    @Column(name = "main_img_url", nullable = true)
    private String mainImgUrl;
    @Column(name = "reply_yn", nullable = true)
    private String replyYn;
    @Column(name = "views_yn", nullable = true)
    private String viewsYn;
    @Column(name = "like_yn", nullable = true)
    private String likeYn;
    @Column(name = "private_tp_cd", nullable = true)
    private String privateTpCd;
    @Column(name = "locat", nullable = true)
    private String locat;
    @Column(name = "view_cnt", nullable = true)
    private Long viewCnt;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder

    public TbStory(Long storyId, Long mmbrId, Long actTlId, String storyStatCd, String storyTpCd, String feedTpCd, String storyCntn, String linkUrl, String mainImgUrl, String replyYn, String viewsYn, String likeYn, String privateTpCd, String locat, Long viewCnt, Long regId, LocalDateTime regDt, Long modId, LocalDateTime modDt) {
        this.storyId = storyId;
        this.mmbrId = mmbrId;
        this.actTlId = actTlId;
        this.storyStatCd = storyStatCd;
        this.storyTpCd = storyTpCd;
        this.feedTpCd = feedTpCd;
        this.storyCntn = storyCntn;
        this.linkUrl = linkUrl;
        this.mainImgUrl = mainImgUrl;
        this.replyYn = replyYn;
        this.viewsYn = viewsYn;
        this.likeYn = likeYn;
        this.privateTpCd = privateTpCd;
        this.locat = locat;
        this.viewCnt = viewCnt;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
    }
}
