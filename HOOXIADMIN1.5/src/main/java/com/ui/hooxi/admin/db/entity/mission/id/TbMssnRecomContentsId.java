package com.ui.hooxi.admin.db.entity.mission.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnRecomContentsId implements Serializable {

    private Long recomId;
    private String langCd;

}
