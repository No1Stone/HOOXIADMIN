package com.ui.hooxi.admin.biz.model.active.challenge;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChallengeListRes {

    private Long cateId;
    private String tpcTpCd;
    private String useYn;
    private String cateimgUrl;
    private Long regId;
    private LocalDateTime regDttm;
    private Long modId;
    private LocalDateTime modDttm;

    private String prpsCd;
    private String cateTitle;

    private Long companyId;
    private String companyName;
    private String contents;
    private String comimgUrl;
    private String adminName;



}
