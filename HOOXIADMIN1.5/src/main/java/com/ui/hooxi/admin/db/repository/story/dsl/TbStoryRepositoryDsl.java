package com.ui.hooxi.admin.db.repository.story.dsl;

import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.active.story.StoryListReq;
import com.ui.hooxi.admin.biz.model.active.story.StoryListRes;
import com.ui.hooxi.admin.biz.model.active.story.StoryOneRes;
import com.ui.hooxi.admin.biz.model.active.story.StoryPointVo;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface TbStoryRepositoryDsl {

    @Transactional(readOnly = true)
    QueryResults<StoryListRes> StoryListResListSelect(StoryListReq dto);

    @Transactional(readOnly = true)
    Optional<StoryOneRes> StoryOneResListSelect(Long seq);

    @Transactional(readOnly = true)
    StoryPointVo StoryModalPointSelect(Long seq);

    @Transactional(readOnly = true)
    StoryPointVo StoryPointExpSelect(Long seq);


}
