package com.ui.hooxi.admin.db.entity.mission;

import com.ui.hooxi.admin.db.entity.mission.id.TbMssnRecomMetaId;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnRecomPlaceId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_recom_place")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnRecomPlaceId.class)
public class TbMssnRecomPlace {
    @Id
    @Column(name = "recom_id", nullable = true)
    private Long recomId;
    @Id
    @Column(name = "place_cd", nullable = true)
    private String placeCd;

    @Builder
    public TbMssnRecomPlace(Long recomId, String placeCd) {
        this.recomId = recomId;
        this.placeCd = placeCd;
    }

}
