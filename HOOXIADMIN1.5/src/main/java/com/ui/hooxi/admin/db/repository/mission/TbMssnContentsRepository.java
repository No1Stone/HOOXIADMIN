package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnContents;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnContentsId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TbMssnContentsRepository extends JpaRepository<TbMssnContents, TbMssnContentsId> {

    List<TbMssnContents> findByMssnId(Long seq);

    boolean existsByMssnIdAndLangCd(Long seq, String language);

    Optional<TbMssnContents> findByMssnIdAndLangCd(Long seq, String language);
}
