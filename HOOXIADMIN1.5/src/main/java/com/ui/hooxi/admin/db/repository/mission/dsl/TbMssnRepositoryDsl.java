package com.ui.hooxi.admin.db.repository.mission.dsl;

import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.active.action.*;
import com.ui.hooxi.admin.biz.model.active.cate.CateListReq;
import com.ui.hooxi.admin.biz.model.active.cate.CateListRes;
import com.ui.hooxi.admin.biz.model.active.cate.CateOneRes;
import com.ui.hooxi.admin.biz.model.active.cate.UpdateCreateReqs;
import com.ui.hooxi.admin.biz.model.active.challenge.ActionSelectListVo;
import com.ui.hooxi.admin.biz.model.active.challenge.ChallengeListReq;
import com.ui.hooxi.admin.biz.model.active.challenge.ChallengeListRes;
import com.ui.hooxi.admin.biz.model.active.challenge.ChallengeOneVo;
import com.ui.hooxi.admin.biz.model.active.challengeAction.ChallengeActionViewVo;
import com.ui.hooxi.admin.biz.model.active.subaction.SubActionReq;
import com.ui.hooxi.admin.biz.model.active.subaction.SubActionRes;
import com.ui.hooxi.admin.biz.model.user.MemberOneRes;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface TbMssnRepositoryDsl {

    @Transactional(readOnly = true)
    QueryResults<ActionListRes> ActionListRes(ActionListReq dto, String topic);

    @Transactional(readOnly = true)
    List<ActionCateSelectRes> ActionCateTitleSelectList(ActionCateSelectReq dto, String topic);

    @Transactional(readOnly = true)
    Optional<ActionViewVo> ActionOneViewSelect(Long seq);

    @Transactional(readOnly = true)
    Optional<ChallengeOneVo> ChalOneViewSelect(Long seq);

    @Transactional(readOnly = true)
    List<ActionSelectListVo> ChalActionSelectList(ActionSelectListVo dto);


    @Transactional(readOnly = true)
    QueryResults<ChallengeListRes> ChallengeListSelect(ChallengeListReq dto);

      @Transactional(readOnly = true)
    QueryResults<CateListRes> CateListRes(CateListReq dto);

    @Transactional(readOnly = true)
    Optional<UpdateCreateReqs> CateOneRes(Long seq);

    @Transactional(readOnly = true)
    ChallengeActionViewVo ChallengeActionOneViewDsl(Long seq);



}
