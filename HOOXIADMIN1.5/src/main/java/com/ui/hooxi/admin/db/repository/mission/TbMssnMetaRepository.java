package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnMeta;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnMetaId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TbMssnMetaRepository extends JpaRepository<TbMssnMeta, TbMssnMetaId> {

    List<TbMssnMeta> findByMssnId(Long seq);

}
