package com.ui.hooxi.admin.db.repository.user.dsl;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.ui.hooxi.admin.biz.model.UserSearchType;
import com.ui.hooxi.admin.biz.model.user.*;
import com.ui.hooxi.admin.biz.model.user.search.MemberShipSearch;
import com.ui.hooxi.admin.db.entity.member.QTbMmbrshp;
import com.ui.hooxi.admin.db.entity.member.QTbSubscrt;
import com.ui.hooxi.admin.db.entity.point.QPoint;
import com.ui.hooxi.admin.db.entity.point.QPointDtl;
import com.ui.hooxi.admin.db.entity.story.QTbLike;
import com.ui.hooxi.admin.db.entity.story.QTbStory;
import com.ui.hooxi.admin.db.entity.user.QTbClausAgr;
import com.ui.hooxi.admin.db.entity.user.QTbClausHist;
import com.ui.hooxi.admin.db.entity.user.QTbLoginHist;
import com.ui.hooxi.admin.db.entity.user.QTbMmbr;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Aspect
@Slf4j
@RequiredArgsConstructor
public class TbMmbrReposiroryDslImpl implements TbMmbrReposiroryDsl {
    private final JPAQueryFactory queryFactory;

    private final QTbMmbr qTbMmbr = QTbMmbr.tbMmbr;
    private final QTbMmbrshp qTbMmbrshp = QTbMmbrshp.tbMmbrshp;
    private final QTbStory qTbStory = QTbStory.tbStory;
    private final QTbLike qTbLike = QTbLike.tbLike;
    private final QTbSubscrt qTbSubscrt = QTbSubscrt.tbSubscrt;

    private final QTbLoginHist qTbLoginHist = QTbLoginHist.tbLoginHist;
    private final QTbClausHist qTbClausHist = QTbClausHist.tbClausHist;
    private final QTbClausAgr qTbClausAgr = QTbClausAgr.tbClausAgr;
    private final QPoint qPoint = QPoint.point1;
    private final QPointDtl qPointDtl = QPointDtl.pointDtl;

    @Override
    public QueryResults<UserListRes> userListResListSelect(UserListReq dto) {

//        DateTimeFormatter dt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime startDateString = null;
        LocalDateTime endDateString = null;
        log.info("=====DSL INIT=====");
        if (dto.getSearchName() != null && dto.getSearchName().equals("DATE")) {
            startDateString = LocalDate.parse(dto.getStartDt()).atStartOfDay();
            endDateString = LocalDate.parse(dto.getEndDt()).atStartOfDay().plusDays(1l);
            log.info("sdaaa = {}", startDateString);
            log.info("edaaa = {}", endDateString);
        }


        var result = queryFactory.select(
                        Projections.fields(UserListRes.class,
                                qTbMmbr.mmbrId,
                                qTbMmbr.mmbrNm,
                                qTbMmbr.nickname,
                                qTbMmbr.email,
                                qTbMmbr.oauthTpCd,
                                ExpressionUtils.as(JPAExpressions.select(qPoint.point.sum()).from(qPoint).where(qPoint.mmbrId.eq(qTbMmbr.mmbrId)), "point"),
                                qTbMmbr.cntrCd,
                                qTbMmbr.regDt
                        )
                ).from(qTbMmbr)
                .where(userListResListSelecteq(dto),
                        userListResListSelectDateeq(dto, startDateString, endDateString))
                .orderBy(qTbMmbr.regDt.desc())
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .fetchResults();
        return result;
    }

    private BooleanExpression userListResListSelecteq(UserListReq dto) {
        if (dto.getSearchName() == null) {
            return null;
        } else if (dto.getSearchName().equals("")) {
            return null;
        } else if (dto.getSearchName().equals(UserSearchType.ALL.getName())) {
            return qTbMmbr.nickname.contains(dto.getSearchString())
                    .or(qTbMmbr.email.contains(dto.getSearchString()));
        } else if (dto.getSearchName().equals(UserSearchType.EMAIL.getName())) {
            return qTbMmbr.email.contains(dto.getSearchString());
        } else if (dto.getSearchName().equals(UserSearchType.NICKNAME.getName())) {
            return qTbMmbr.nickname.contains(dto.getSearchString());
        } else if (dto.getSearchName().equals(UserSearchType.LANGUAGE.getName())) {
            return qTbMmbr.cntrCd.contains(dto.getLangCd());
        } else if (dto.getSearchName().equals(UserSearchType.STATUS.getName())) {
            return qTbMmbr.mmbrStatusCd.eq(dto.getSearchStatus());
        } else {
            return null;
        }
    }

    private BooleanExpression userListResListSelectDateeq(UserListReq dto, LocalDateTime sd, LocalDateTime ed) {
        if (dto.getSearchName() != null && !dto.getSearchName().equals("DATE")) {
            return null;
        }
        if (dto.getSearchName() != null && dto.getSearchName().equals(UserSearchType.DATE.getName())) {
            return qTbMmbr.regDt.between(sd, ed);
        } else {
            return null;
        }
    }

    @Override
    public QueryResults<MemberListRes> MembershipListResListSelect(UserListReq dto) {

        DateTimeFormatter dt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime startDateString = null;
        LocalDateTime endDateString = null;
        log.info("=====Membership DSL INIT=====");
        if (dto.getStartDt() != null && !dto.getStartDt().equals("")) {
            startDateString = LocalDate.parse(dto.getStartDt()).atStartOfDay();
            endDateString = LocalDate.parse(dto.getEndDt()).atStartOfDay().plusDays(1l);
            log.info("sdaaa = {}", startDateString);
            log.info("edaaa = {}", endDateString);
        }


        var result = queryFactory.select(
                        Projections.fields(MemberListRes.class,
                                qTbMmbr.mmbrId,
                                qTbMmbr.mmbrNm,
                                qTbMmbr.nickname,
                                qTbMmbr.email,
                                //신청등급
                                qTbMmbrshp.gradCd,
                                //스토리작성갯수
                                ExpressionUtils.as(JPAExpressions.select(qTbStory.mmbrId.count()).from(qTbStory).where(qTbStory.mmbrId.eq(qTbMmbr.mmbrId)), "story"),
//                              JPAExpressions.select(qTbStory.mmbrId.count()).from(qTbStory).where(qTbStory.mmbrId.eq(qTbMmbr.mmbrId)),
                                //좋아요 히트
                                ExpressionUtils.as(JPAExpressions.select(qTbLike.mmbrId.count()).from(qTbLike).where(qTbLike.mmbrId.eq(qTbMmbr.mmbrId)), "like"),
                                //승인상태
                                qTbMmbrshp.mmbrshpStateCd,
                                //싱청일
                                qTbMmbrshp.frstRgstDttm
                        )
                ).from(qTbMmbr)
                .innerJoin(qTbMmbrshp).on(qTbMmbr.mmbrId.eq(qTbMmbrshp.mmbrId))
//                .join(qTbStory).on(qTbMmbr.mmbrId.eq(qTbStory.mmbrId))
                .where(MembershipListResListSelectEQ(dto, startDateString, endDateString))
                .orderBy(qTbMmbr.regDt.desc())
                .offset(dto.getPage() * dto.getSize())
                .limit(dto.getSize())
                .fetchResults();
        return result;
    }

    public BooleanExpression MembershipListResListSelectEQ(UserListReq dto,LocalDateTime sd, LocalDateTime ed) {

        if (dto.getSearchName() == null) {
            return null;
        } else if (dto.getSearchName().equals("")) {
            return null;
        } else if (dto.getSearchName().equals(MemberShipSearch.ALL.getName())) {
            if(dto.getSearchString()==null){
                return null;
            }
            return qTbMmbr.nickname.contains(dto.getSearchString()).or(qTbMmbr.email.contains(dto.getSearchString()));
        } else if (dto.getSearchName().equals(MemberShipSearch.NICKNAME.getName())) {
            if(dto.getSearchString()==null){
                return null;
            }
            return qTbMmbr.nickname.contains(dto.getSearchString());
        } else if (dto.getSearchName().equals(MemberShipSearch.EMAIL.getName())) {
            if(dto.getSearchString()==null){
                return null;
            }
            return qTbMmbr.email.contains(dto.getSearchString());
        } else if (dto.getSearchName().equals(MemberShipSearch.GRADE.getName())) {
            return qTbMmbrshp.gradCd.eq(dto.getSearchGrade());
        } else if (dto.getSearchName().equals(MemberShipSearch.MMBRSHPSTATECD.getName())) {
            return qTbMmbrshp.mmbrshpStateCd.eq(dto.getSearchStatus());
        }else if (dto.getSearchName().equals(MemberShipSearch.DATE.getName())) {
            return qTbMmbrshp.frstRgstDttm.between(sd, ed);
        }
        else {
            return null;
        }
    }


    @Override
    public UserOneRes UserOneRes(Long seq) {
        var result = queryFactory.select(
                        Projections.fields(UserOneRes.class,
                                qTbMmbr.mmbrId,
                                qTbMmbr.mmbrStatusCd,
                                qTbMmbr.cntrCd,
                                qTbMmbr.email,
                                qTbMmbr.kidYn,
                                qTbMmbr.mpno,
                                qTbMmbr.profileImg,
                                qTbMmbr.mmbrNm,
                                qTbMmbr.nickname,
                                ExpressionUtils.as(JPAExpressions.select(qTbMmbrshp.mmbrId.count()).from(qTbMmbrshp).where(qTbMmbrshp.mmbrId.eq(qTbMmbr.mmbrId)), "mmbrshp"),
                                qTbMmbr.regDt,
                                qTbMmbr.lastLoginDttm,
                                qTbMmbr.wtdDttm,
                                qTbMmbr.oauthTpCd,
                                qTbMmbr.mainLang,
                                qTbMmbr.subLang,
                                qTbMmbr.actClausYn
                        )
                ).from(qTbMmbr)
                .where(qTbMmbr.mmbrId.eq(seq))
                .fetchOne();
        return result;
    }

    @Override
    public MemberOneRes MemberShipOneSelect(Long seq) {
        MemberOneRes result = new MemberOneRes();

        result =
                queryFactory.select(
                                Projections.fields(MemberOneRes.class,
                                        qTbMmbr.mmbrId,
                                        qTbMmbr.mmbrStatusCd,
                                        qTbMmbr.cntrCd,
                                        qTbMmbr.email,
                                        qTbMmbr.kidYn,
                                        qTbMmbr.mpno,
                                        qTbMmbr.profileImg,
                                        qTbMmbr.mmbrNm,
                                        qTbMmbr.nickname,
                                        ExpressionUtils.as(JPAExpressions.select(qTbMmbrshp.mmbrId.count()).from(qTbMmbrshp).where(qTbMmbrshp.mmbrId.eq(qTbMmbr.mmbrId)), "mmbrshp"),
                                        qTbMmbr.regDt,
                                        qTbMmbr.lastLoginDttm,
                                        qTbMmbr.wtdDttm,
                                        qTbMmbr.oauthTpCd,
                                        qTbMmbr.mainLang,
                                        qTbMmbr.subLang,
                                        qTbMmbr.actClausYn,

                                        qTbMmbrshp.mmbrshpId,
                                        qTbMmbrshp.afltn,
                                        qTbMmbrshp.stateMsg,
                                        qTbMmbrshp.mmbrshpStateCd,
                                        qTbMmbrshp.gradCd,
                                        qTbMmbrshp.frstRgstDttm,
                                        qTbMmbrshp.bgImg,
                                        qTbMmbrshp.nickname.as("memNickname"),
                                        qTbMmbrshp.profileImg.as("memProfileImg"),
                                        qTbMmbrshp.mmbrshpNum,
                                        qTbMmbrshp.rjctRsn,
                                        qTbMmbrshp.fnlModDttm,
                                        ExpressionUtils.as(JPAExpressions.select(qTbSubscrt.mmbtshpId.count())
                                                .from(qTbSubscrt).where(qTbSubscrt.mmbtshpId.eq(qTbMmbrshp.mmbrshpId)), "subscripCount")
                                )
                        ).from(qTbMmbr)
                        .innerJoin(qTbMmbrshp).on(qTbMmbrshp.mmbrId.eq(qTbMmbr.mmbrId))
                        .where(qTbMmbr.mmbrId.eq(seq))
                        .fetchOne();

        return result;
    }


}
