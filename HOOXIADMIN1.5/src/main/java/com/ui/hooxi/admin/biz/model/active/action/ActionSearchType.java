package com.ui.hooxi.admin.biz.model.active.action;

public enum ActionSearchType {


    NAME("액션 이름"),
    DATE("일자");

    private String val;

    public static final ActionSearchType[] LIST = {NAME, DATE};

    ActionSearchType(String val) {
        this.val = val;
    }

    public String getVal() {
        return this.val;
    }

    public String getName() {
        return name();
    }

}
