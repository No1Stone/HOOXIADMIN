package com.ui.hooxi.admin.db.repository.admin.dsl;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.ui.hooxi.admin.biz.model.active.action.ActionSearchType;
import com.ui.hooxi.admin.biz.model.active.challengeAction.ChallengeActionSearchType;
import com.ui.hooxi.admin.biz.model.active.story.StoryListRes;
import com.ui.hooxi.admin.biz.model.admin.AdminListReq;
import com.ui.hooxi.admin.biz.model.admin.AdminListRes;
import com.ui.hooxi.admin.biz.model.admin.AdminSearchType;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerListReq;
import com.ui.hooxi.admin.biz.model.admin.per.AdminPerListRes;
import com.ui.hooxi.admin.db.entity.admin.Admin;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsPmn;
import com.ui.hooxi.admin.db.entity.admin.QAdmin;
import com.ui.hooxi.admin.db.entity.admin.QAdminPms;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Aspect
@Slf4j
@RequiredArgsConstructor
public class AdminRepositoryDslImpl implements AdminRepositoryDsl {
    private final JPAQueryFactory queryFactory;

    private QAdmin qAdmin = QAdmin.admin;
    private QAdminPms qAdminPms = QAdminPms.adminPms;


    @Override
    public void test() {

    }

    @Override
    public QueryResults<AdminListRes> AdminListSelect(AdminListReq dto) {

        if (dto.getSize() == 0) {
            dto.setSize(10);
        }

        LocalDateTime startDateString = null;
        LocalDateTime endDateString = null;
        log.info("=====DSL INIT=====");
        if (dto.getSearchName() != null && dto.getSearchName().equals("DATE")) {
            startDateString = LocalDate.parse(dto.getStartDt()).atStartOfDay();
            endDateString = LocalDate.parse(dto.getEndDt()).atStartOfDay().plusDays(1l);
            log.info("sdaaa = {}", startDateString);
            log.info("edaaa = {}", endDateString);
        }

        if (!StringUtils.isEmptyOrWhitespace(dto.getStartDt()) && !StringUtils.isEmptyOrWhitespace(dto.getEndDt())) {
            startDateString = LocalDate.parse(dto.getStartDt()).atStartOfDay();
            endDateString = LocalDate.parse(dto.getEndDt()).atStartOfDay().plusDays(1l);
        }

        var result = queryFactory.select(
                Projections.fields(AdminListRes.class,
                        qAdmin.adminId,
                        qAdmin.adminEmail,
                        qAdmin.adminName,
                        qAdmin.adminPwd,
                        qAdmin.adminPmsId,
                        qAdmin.useYn,
                        qAdmin.regId,
                        qAdmin.regDttm,
                        qAdminPms.adminPmsNm
                        )
                ).from(qAdmin)
                .where(AdminListSelectEq(dto, startDateString, endDateString))
                .leftJoin(qAdminPms).on(qAdminPms.adminPmsId.eq(qAdmin.adminPmsId))
                .limit(dto.getSize())
                .offset(dto.getSize() * dto.getPage())
                .orderBy(qAdmin.regDttm.desc())
                .fetchResults();
        return result;
    }

    private BooleanExpression AdminListSelectEq(AdminListReq dto, LocalDateTime sd, LocalDateTime ed) {

        if (dto.getSearchName() == null) {
            return null;
        } else if (dto.getSearchName().equals("")) {
            return null;
        } else if (dto.getSearchName().equals(AdminSearchType.NAME.getName())) {
            return qAdmin.adminName.contains(dto.getSearchNameVal());
        } else if (dto.getSearchName().equals(AdminSearchType.EMAIL.getName())) {
            return qAdmin.adminEmail.contains(dto.getSearchNameVal());
        } else if (dto.getSearchName().equals(AdminSearchType.DATE.getName())) {
            return qAdmin.regDttm.between(sd, ed);
        } else {
            return null;
        }


    }

    @Override
    public QueryResults<AdminPerListRes> AdminPerSelectList(AdminPerListReq dto) {

        if (dto.getSize() == 0) {
            dto.setSize(10);
        }

        QueryResults<AdminPerListRes> result =
                queryFactory.select(
                                Projections.fields(AdminPerListRes.class,
                                        qAdminPms.adminPmsId,
                                        qAdminPms.adminPmsNm,
                                        qAdminPms.regId,
                                        qAdminPms.regDttm
                                )
                        )
                        .from(
                                qAdminPms
                        )
                        .where()
                        .orderBy(qAdminPms.adminPmsId.desc())
                        .offset(dto.getPage() * dto.getSize())
                        .limit(dto.getSize())
                        .fetchResults();

        return result;
    }

    private BooleanExpression eqEmail(String email) {
        return StringUtils.isEmptyOrWhitespace(email) ? null : qAdmin.adminEmail.lower().contains(email.toLowerCase());
    }

    private BooleanExpression eqName(String name) {
        return StringUtils.isEmptyOrWhitespace(name) ? null : qAdmin.adminName.lower().contains(name.toLowerCase());
    }

    private BooleanExpression eqPermission(Long seq) {
        return seq == null ? null : qAdmin.adminPmsId.eq(seq);
    }

}
