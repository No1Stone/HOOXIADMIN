package com.ui.hooxi.admin.db.repository.exp.dsl;

import com.ui.hooxi.admin.db.entity.exp.TbExp;
import org.springframework.transaction.annotation.Transactional;

public interface TbExpRepositoryDsl {

    @Transactional(readOnly = true)
    Long MmbrExpSum(Long mmbrId);

    @Transactional(readOnly = true)
    String AdminExpLevelFind(Long exp);

    @Transactional(readOnly = true)
    Long AdminExpNextLevelFind(Long exp);

}
