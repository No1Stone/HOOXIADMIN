package com.ui.hooxi.admin.biz.service.common;

import com.ui.hooxi.admin.config.type.YnType;
import com.ui.hooxi.admin.db.entity.code.TbCmnCode;
import com.ui.hooxi.admin.db.repository.code.TbCmnCodeRepository;
import com.ui.hooxi.admin.db.repository.code.TbCmnGrpCodeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class CodeBean {

    private final TbCmnCodeRepository tbCmnCodeRepository;

    private List<TbCmnCode> codeList = null;
    private Map<String, TbCmnCode> nameToCode = null;
    private Map<String, String> nameToString = null;

    @PostConstruct
    private void PostSetCode(){
        ReSetCode();
    }
    private void ListReset(){
        this.codeList = new ArrayList<>();
    }
    private void MapReset(){
        this.nameToCode = new HashMap<>();
    }
    private void MapStringReset(){
        this.nameToString = new HashMap<>();
    }

    public List<TbCmnCode> getCodeList(){
        return this.codeList;
    }
    public List<TbCmnCode> getCodeList(String name){
        return this.codeList.stream().filter(e -> e.getCd().startsWith(name))
                .collect(Collectors.toList());
    }

    public Map<String, TbCmnCode> getNameToCode() {
        return this.nameToCode;
    }
    public Map<String, String> getNameToString() {
        return this.nameToString;
    }
    public Map<String, String> getNameToString(String group) {
        return this.codeList.stream().filter(tbCmnCode -> tbCmnCode.getCd().contains(group) && tbCmnCode.getUseYn().equals(YnType.Y.getName()))
                .collect(Collectors.toMap(TbCmnCode::getCd, TbCmnCode::getCdNm));
    }

    public Map<String, String> getNameToStringGRP(String group) {
        return this.codeList.stream().filter(tbCmnCode -> tbCmnCode.getGrpCd().contains(group))
                .collect(Collectors.toMap(TbCmnCode::getCd, TbCmnCode::getCdNm));
    }

    public Map<String, TbCmnCode> getNameToCode(String group) {
        return this.codeList.stream().filter(tbCmnCode -> tbCmnCode.getCd().contains(group))
                .collect(Collectors.toMap(TbCmnCode::getCd, tbCmnCode -> tbCmnCode));
    }

    @Transactional(readOnly = true)
    public void ReSetCode(){
        ListReset();
        MapReset();
        MapStringReset();
        this.codeList = tbCmnCodeRepository.findByUseYn(YnType.Y.getName());
        this.nameToCode = tbCmnCodeRepository.findByUseYn(YnType.Y.getName()).stream().collect(Collectors.toMap(TbCmnCode::getCd, tbCmnCode -> tbCmnCode));
        this.nameToString = tbCmnCodeRepository.findByUseYn(YnType.Y.getName()).stream().collect(Collectors.toMap(TbCmnCode::getCd, TbCmnCode::getCdNm));
    }
}
