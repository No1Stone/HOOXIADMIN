package com.ui.hooxi.admin.db.entity.point;

import com.ui.hooxi.admin.db.entity.point.convert.PointStatCdConvert;
import com.ui.hooxi.admin.db.entity.point.convert.TrstnDtlTpCdConvert;
import com.ui.hooxi.admin.db.entity.point.convert.TrstnTpCdConvert;
import com.ui.hooxi.admin.db.entity.point.id.PointDtlId;
import com.ui.hooxi.admin.db.entity.point.type.PointStatCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnTpCd;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@IdClass(PointDtlId.class)
@Table(name = "tb_point_dtl")
public class PointDtl {

    @Id // 포인트 상세 ID
    @GeneratedValue(generator="tb_point_dtl_piont_dtl_id_seq")
    @Column(name = "point_dtl_id", nullable = false)
    private Long pointDtlId;
    @Id // 포인트 ID
    @Column(name = "point_id", nullable = false)
    private Long pointId;
    @Id
    @Column(name = "mmbr_id", nullable = false)
    private Long mmbrId;

    @Column(name = "point_state_cd", nullable = false, length = 6)
    @Convert(converter = PointStatCdConvert.class)
    private PointStatCd pointStateCd;

    @Column(name = "trstn_id", nullable = false)
    private Long trstnId;

    @Column(name = "trstn_tp_cd", nullable = false, length = 6)
    @Convert(converter = TrstnTpCdConvert.class)
    private TrstnTpCd trstnTpCd;

    @Column(name = "trstn_dtl_tp_cd", nullable = false, length = 6)
    @Convert(converter = TrstnDtlTpCdConvert.class)
    private TrstnDtlTpCd trstnDtlTpCd;

    @Column(name = "point", nullable = false)
    private Long point;

    // 포인트 '상세' 원본 아이디 -> pointDtlId인데 적립할 때 생성되는 pointDtlId를 저장하면 된다.
    @Column(name = "dtl_org_point_id")
    private Long dtlOrgPointId;

    // 포인트 취소 아이디
    @Column(name = "dtl_cncl_point_id")
    private Long dtlCnclPointId;

    @Column(name = "trstn_dt", nullable = false)
    private LocalDateTime trstnDt;

    @Column(name = "expire_dt", nullable = false)
    private LocalDateTime expireDt;

    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    public PointDtl(Long pointDtlId, Long pointId, Long mmbrId, PointStatCd pointStateCd, Long trstnId, TrstnTpCd trstnTpCd, TrstnDtlTpCd trstnDtlTpCd, Long point, Long dtlOrgPointId, Long dtlCnclPointId, LocalDateTime trstnDt, LocalDateTime expireDt, Long regId, LocalDateTime regDt, Long modId, LocalDateTime modDt) {
        this.pointDtlId = pointDtlId;
        this.pointId = pointId;
        this.mmbrId = mmbrId;
        this.pointStateCd = pointStateCd;
        this.trstnId = trstnId;
        this.trstnTpCd = trstnTpCd;
        this.trstnDtlTpCd = trstnDtlTpCd;
        this.point = point;
        this.dtlOrgPointId = dtlOrgPointId;
        this.dtlCnclPointId = dtlCnclPointId;
        this.trstnDt = trstnDt;
        this.expireDt = expireDt;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
    }

    /**
     *
     */
    public void saveUpdate() {
        this.dtlOrgPointId = this.pointDtlId;
        this.dtlCnclPointId = this.pointDtlId;
    }

    public void useIdUpdate() {
        this.dtlCnclPointId = this.pointDtlId;
    }

    // point사용 및 point 취소 후 만료시간 update
    public void updateExpireDt() {

    }

    public void updateStoryPointState() {
        this.pointStateCd = PointStatCd.ACCUMULATE;
        this.trstnDt = LocalDateTime.now();
    }
}
