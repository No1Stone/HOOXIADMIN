package com.ui.hooxi.admin.db.repository.point;

import com.ui.hooxi.admin.db.entity.point.Point;
import com.ui.hooxi.admin.db.entity.point.id.PointId;
import com.ui.hooxi.admin.db.entity.point.type.PointStatCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnTpCd;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TbPointRepository extends JpaRepository<Point, PointId> {
    /**
     * 포인트 내역
     * @param mmbrId
     * @param pageable
     * @return
     */
    Page<Point> findPointsByMmbrIdOrderByTrstnDtDescPointIdDesc(Long mmbrId, Pageable pageable);

    Optional<Point> findByTrstnIdAndTrstnTpCdAndPointStateCd(Long seq, TrstnTpCd ttc, PointStatCd psc);
    boolean existsByTrstnIdAndPointStateCdAndTrstnTpCdAndTrstnDtlTpCd(Long seq, PointStatCd psc, TrstnTpCd ttc, TrstnDtlTpCd dtd);
    Optional<Point>  findByTrstnIdAndPointStateCdAndTrstnTpCdAndTrstnDtlTpCd(Long seq, PointStatCd psc, TrstnTpCd ttc, TrstnDtlTpCd dtd);


}
