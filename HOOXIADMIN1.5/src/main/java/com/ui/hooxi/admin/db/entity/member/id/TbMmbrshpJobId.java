package com.ui.hooxi.admin.db.entity.member.id;

import com.ui.hooxi.admin.db.entity.admin.TbAdminJobKywd;
import com.ui.hooxi.admin.db.entity.member.TbMmbrshp;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class TbMmbrshpJobId implements Serializable {
    private Long mmbrshpId;
    private Long kywd_id;
    private String useTpCd;

}
