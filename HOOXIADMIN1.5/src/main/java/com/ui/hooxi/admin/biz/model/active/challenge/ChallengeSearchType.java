package com.ui.hooxi.admin.biz.model.active.challenge;

public enum ChallengeSearchType {

    COMPANYNAME("회사 이름"),
    CHALLENGENAME("챌린지 이름"),
    DATE("일자");

    private String val;

    public static final ChallengeSearchType[] LIST = {COMPANYNAME, CHALLENGENAME, DATE};

    ChallengeSearchType(String val) {
        this.val = val;
    }

    public String getVal() {
        return this.val;
    }

    public String getName() {
        return name();
    }

}
