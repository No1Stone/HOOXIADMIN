package com.ui.hooxi.admin.db.repository.story.dsl;


import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.ui.hooxi.admin.biz.model.active.story.*;
import com.ui.hooxi.admin.db.entity.exp.QTbExp;
import com.ui.hooxi.admin.db.entity.point.QPoint;
import com.ui.hooxi.admin.db.entity.point.type.TrstnTpCd;
import com.ui.hooxi.admin.db.entity.story.QTbReply;
import com.ui.hooxi.admin.db.entity.story.QTbStory;
import com.ui.hooxi.admin.db.entity.user.QTbMmbr;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@Aspect
@Slf4j
@RequiredArgsConstructor
public class TbStoryRepositoryDslImpl implements TbStoryRepositoryDsl {

    private final JPAQueryFactory queryFactory;
    private QTbStory qTbStory = QTbStory.tbStory;
    private QTbReply qTbReply = QTbReply.tbReply;
    private QTbMmbr qTbMmbr = QTbMmbr.tbMmbr;
    private QPoint qqpoint = QPoint.point1;
    private QTbExp qTbExp = QTbExp.tbExp;

    @Override
    public QueryResults<StoryListRes> StoryListResListSelect(StoryListReq dto) {

        LocalDateTime startDateString = null;
        LocalDateTime endDateString = null;
        log.info("=====DSL INIT=====");
        if (dto.getSearchName() != null && dto.getSearchName().equals("DATE")) {
            startDateString = LocalDate.parse(dto.getStartDt()).atStartOfDay();
            endDateString = LocalDate.parse(dto.getEndDt()).atStartOfDay().plusDays(1l);
            log.info("sdaaa = {}", startDateString);
            log.info("edaaa = {}", endDateString);
        }

        QueryResults<StoryListRes> result =
                queryFactory.select(
                                Projections.fields(StoryListRes.class,
                                        qTbStory.storyId,
                                        qTbStory.mmbrId,
                                        qTbStory.actTlId,
                                        qTbStory.storyStatCd,
                                        qTbStory.storyCntn,
                                        qTbStory.feedTpCd,
                                        qTbStory.privateTpCd,
                                        ExpressionUtils.as(JPAExpressions.select(qTbReply.mmbrId.count()).from(qTbReply).where(qTbReply.mmbrId.eq(qTbStory.mmbrId)), "comentCount"),
                                        qTbStory.viewCnt,
                                        qTbStory.locat,
                                        qTbStory.regDt
                                )
                        )
                        .from(
                                qTbStory
                        )
                        .where(StoryListResListSelectEq(dto, startDateString, endDateString),
                                qTbStory.storyTpCd.eq(dto.getStoryTpCd()))
                        .orderBy(qTbStory.storyId.desc())
                        .offset(dto.getPage() * dto.getSize())
                        .limit(dto.getSize())
                        .fetchResults();
//        SubQueryExpression<Tuple> aa0 = queryFactory.query().select(qTbStory.mmbrId, qTbStory.storyId).from(qTbStory);
//        SubQueryExpression<Tuple> aa1 = queryFactory.query().select(qTbStory.mmbrId, qTbStory.storyId).from(qTbStory);

        queryFactory.selectFrom(qTbStory);


        return result;
    }


    private BooleanExpression StoryListResListSelectEq(StoryListReq dto, LocalDateTime sd, LocalDateTime ed){

        if(dto.getSearchName() == null){
            return null;
        }
        else if (dto.getSearchName().equals("")){
            return null;
        }
        else if (dto.getSearchName().isEmpty()){
            return null;
        }
        else if (dto.getSearchName().equals(StorySearchType.STORYSTATUS.getName())){
            return qTbStory.storyStatCd.eq(dto.getStoryStatusSearch());
        }
        else if (dto.getSearchName().equals(StorySearchType.CONTENTS.getName())){
            return qTbStory.storyCntn.contains(dto.getSearchNameVal());
        }
        else if (dto.getSearchName().equals(StorySearchType.FEEDTYPE.getName())){
            return qTbStory.feedTpCd.eq(dto.getFeedTypeSearch());
        }
        else if (dto.getSearchName().equals(StorySearchType.PRIVATETYPE.getName())){
            return qTbStory.privateTpCd.eq(dto.getPrivateTypeSearch());
        }
        else if (dto.getSearchName().equals(StorySearchType.DATE.getName())){
            return qTbStory.regDt.between(sd,ed);
        }
        else {
            return null;
        }

    }

    @Override
    public Optional<StoryOneRes> StoryOneResListSelect(Long seq) {

        StoryOneRes result =
                queryFactory.select(
                                Projections.fields(StoryOneRes.class,
                                        qTbStory.storyId,
                                        qTbMmbr.nickname,
                                        qTbStory.actTlId,
                                        qTbStory.storyStatCd,
                                        qTbStory.storyTpCd,
                                        qTbStory.feedTpCd,
                                        qTbStory.storyCntn,
                                        qTbStory.linkUrl,
                                        qTbStory.mainImgUrl,
                                        qTbStory.replyYn,
                                        qTbStory.viewsYn,
                                        qTbStory.likeYn,
                                        qTbStory.privateTpCd,
                                        qTbStory.locat,
                                        qTbStory.viewCnt,
                                        qTbMmbr.nickname.as("regId"),
                                        qTbStory.regDt,
                                        qTbMmbr.nickname.as("modId"),
                                        qTbStory.modDt,
                                        ExpressionUtils.as(JPAExpressions.select(qTbReply.mmbrId.count()).from(qTbReply).where(qTbReply.mmbrId.eq(qTbStory.mmbrId)), "comentCount")
                                )
                        )
                        .from(
                                qTbStory
                        )
                        .where(qTbStory.storyId.eq(seq))
                        .leftJoin(qTbMmbr).on(qTbMmbr.mmbrId.eq(qTbStory.mmbrId))
                        .fetchOne();


        return Optional.of(result);
    }

    @Override
    public StoryPointVo StoryModalPointSelect(Long seq) {

        StoryPointVo result = null;

        result =
                queryFactory.select(
                                Projections.fields(StoryPointVo.class,
                                        qTbStory.storyId
                                )
                        )
                        .from(
                                qTbStory
                        )
                        .where(qTbStory.storyId.eq(seq))
                        .leftJoin(qTbMmbr).on(qTbMmbr.mmbrId.eq(qTbStory.mmbrId))
                        .fetchOne();

        return result;
    }

    @Override
    public StoryPointVo StoryPointExpSelect(Long seq) {
        StoryPointVo result = null;


        result = queryFactory.select(
                        Projections.fields(StoryPointVo.class,
                                qTbStory.storyId,

                                ExpressionUtils.as(JPAExpressions
                                        .select(qqpoint.point.sum())
                                        .from(qqpoint)
                                        .where(
                                                qqpoint.trstnTpCd.eq(TrstnTpCd.STORY),
                                                qqpoint.trstnId.eq(seq)
                                        ), "point"),

                                ExpressionUtils.as(JPAExpressions
                                        .select(qTbExp.exp.sum())
                                        .from(qTbExp)
                                        .where(
                                                qTbExp.expAcmltTpCd.eq(TrstnTpCd.STORY.getLegacyCode()),
                                                qTbExp.expAcmltId.eq(seq)
                                        ), "exp")
                        )
                ).from(qTbStory)
                .where(qTbStory.storyId.eq(seq))
                .fetchFirst();


        return result;
    }


}
