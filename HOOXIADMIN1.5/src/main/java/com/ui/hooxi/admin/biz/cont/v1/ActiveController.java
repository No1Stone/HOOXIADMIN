package com.ui.hooxi.admin.biz.cont.v1;


import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.UserSearchType;
import com.ui.hooxi.admin.biz.model.UserStatusType;
import com.ui.hooxi.admin.biz.model.active.action.*;
import com.ui.hooxi.admin.biz.model.active.cate.CateContentsReq;
import com.ui.hooxi.admin.biz.model.active.cate.CateListReq;
import com.ui.hooxi.admin.biz.model.active.cate.UpdateCreateReqs;
import com.ui.hooxi.admin.biz.model.active.challenge.*;
import com.ui.hooxi.admin.biz.model.active.challengeAction.ChallengeActionSearchType;
import com.ui.hooxi.admin.biz.model.active.challengeAction.ChallengeActionViewVo;
import com.ui.hooxi.admin.biz.model.active.recommendaction.RecommendActionListReq;
import com.ui.hooxi.admin.biz.model.active.recommendaction.RecommendActionListRes;
import com.ui.hooxi.admin.biz.model.active.story.*;
import com.ui.hooxi.admin.biz.model.active.subaction.SubActionReq;
import com.ui.hooxi.admin.biz.model.admin.AdminSearchType;
import com.ui.hooxi.admin.biz.service.active.*;
import com.ui.hooxi.admin.biz.service.common.CodeBean;
import com.ui.hooxi.admin.config.type.LangType;
import com.ui.hooxi.admin.config.type.PMenu;
import com.ui.hooxi.admin.config.util.ThymeLeafPaging;
import com.ui.hooxi.admin.db.entity.code.TbCmnCode;
import com.ui.hooxi.admin.db.entity.mission.TbMssn;
import com.ui.hooxi.admin.db.entity.mission.TbMssnCate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
@RequestMapping(path = "/active")
@Slf4j
public class ActiveController {

    private final StoryService storyService;
    private final ActionService actionService;
    private final SubActionService subActionService;
    private final RecommendActionService recommendActionService;
    private final ChallengeService challengeService;
    private final ComunityService comunityService;

    private final CodeBean codeBean;

    @ModelAttribute("PMenu")
    public List<PMenu> PMENUType() {
        return Arrays.asList(PMenu.ALL);
    }

    @ModelAttribute("UserSearchType")
    public List<UserSearchType> UserSearchType() {
        return Arrays.asList(UserSearchType.LIST);
    }

    @ModelAttribute("UserStatusType")
    public List<UserStatusType> UserStatusType() {
        return Arrays.asList(UserStatusType.LIST);
    }

    @ModelAttribute("StorySearchType")
    public List<StorySearchType> StorySearchType() {
        return Arrays.asList(StorySearchType.LIST);
    }

    @ModelAttribute("StoryStatusSearchType")
    public List<TbCmnCode> StoryStatusSearchType() {
        return codeBean.getCodeList("SRS");
    }

    @ModelAttribute("StoryTypeSearchType")
    public List<TbCmnCode> StoryTypeSearchType() {
        return codeBean.getCodeList("SRT");
    }

    @ModelAttribute("FeedTypeSearchType")
    public List<TbCmnCode> FeedTypeSearchType() {
        return codeBean.getCodeList("FDT");
    }

    @ModelAttribute("PrivateTypeSearchType")
    public List<TbCmnCode> PrivateTypeSearchType() {
        return codeBean.getCodeList("PTT");
    }

    @ModelAttribute("CateSearchType")
    public List<CateSearchType> CateSearchType() {
        return Arrays.asList(CateSearchType.LIST);
    }

    @ModelAttribute("MetaType")
    public List<TbCmnCode> MetaType() {
        return codeBean.getCodeList("PPT");
    }


    @ModelAttribute("ActionSearchType")
    public List<ActionSearchType> ActionSearchType() {
        return Arrays.asList(ActionSearchType.LIST);
    }


    @ModelAttribute("ChallengeSearchType")
    public List<ChallengeSearchType> ChallengeSearchType() {
        return Arrays.asList(ChallengeSearchType.LIST);
    }


    @ModelAttribute("ChallengeActionSearchType")
    public List<ChallengeActionSearchType> ChallengeActionSearchType() {
        return Arrays.asList(ChallengeActionSearchType.LIST);
    }



    @ModelAttribute("codes")
    public Map<String, String> CodeBeanList() {
        log.info("codes  - {}",codeBean.getNameToString());
        return codeBean.getNameToString();
    }
//    @ModelAttribute("CNTCode")
//    public List<TbCmnCode> CountryCode() {
//        return codeBean.getCodeList("CNT");
//    }

    @ModelAttribute("placeCode")
    public List<TbCmnCode> PlaceType() {
        return codeBean.getCodeList("PLT");
    }

    @ModelAttribute("purposeCode")
    public List<TbCmnCode> PurposeType() {
        return codeBean.getCodeList("PPT");
    }

    @ModelAttribute("langCode")
    public List<LangType> LangType() {
        return Arrays.asList(LangType.ALL);
    }


    @GetMapping(path = "/story/listView")
    public ModelAndView StoryListViewController(StoryListReq dto) {
        log.info("===== effect======");

//        QueryResults<StoryListRes> result = storyService.storyListResQueryResults(dto);
        dto.setStoryTpCd("SRT001");
        ModelAndView mv = new ModelAndView("layout/active/story/listView")
                .addObject("storyReq", new SubActionReq())
//                .addObject(result);
;
        return mv;
    }

    @ResponseBody
    @PostMapping(path = "/story/listSelect")
    public ModelAndView StorySearchBinding(StoryListReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));
        dto.setStoryTpCd("SRT001");
        QueryResults<StoryListRes> result = storyService.storyListResQueryResults(dto);
        ModelAndView mv = new ModelAndView("layout/active/story/searchList")
                .addObject("storyList", result)
                .addObject("page", new ThymeLeafPaging(result));
        log.info("result =- {}", new Gson().toJson(result));
        return mv;
    }

    @GetMapping(path = "/story/oneView")
    public ModelAndView StoryOneView(@RequestParam(value = "seq") Long seq) {
        log.info("dto - {}", new Gson().toJson(seq));
        ModelAndView mv = new ModelAndView("layout/active/story/oneView")
                .addObject("selectForm", storyService.StorySelectOneService(seq));
        return mv;
    }

    @ResponseBody
    @PostMapping(path = "/storyAc/listSelect")
    public ModelAndView StoryAcSearchBinding(StoryListReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));
        dto.setStoryTpCd("SRT002");
        QueryResults<StoryListRes> result = storyService.storyListResQueryResults(dto);
        ModelAndView mv = new ModelAndView("layout/active/story/searchList")
                .addObject("storyList", result)
                .addObject("page", new ThymeLeafPaging(result));
        log.info("result =- {}", new Gson().toJson(result));
        return mv;
    }
    @ResponseBody
    @PostMapping(path = "/storyCm/listSelect")
    public ModelAndView StoryCmSearchBinding(StoryListReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));
        dto.setStoryTpCd("SRT003");
        QueryResults<StoryListRes> result = storyService.storyListResQueryResults(dto);
        ModelAndView mv = new ModelAndView("layout/active/story/searchList")
                .addObject("storyList", result)
                .addObject("page", new ThymeLeafPaging(result));
        log.info("result =- {}", new Gson().toJson(result));
        return mv;
    }
    @ResponseBody
    @PostMapping(path = "/storyCh/listSelect")
    public ModelAndView StoryChSearchBinding(StoryListReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));
        dto.setStoryTpCd("SRT004");
        QueryResults<StoryListRes> result = storyService.storyListResQueryResults(dto);
        ModelAndView mv = new ModelAndView("layout/active/story/searchList")
                .addObject("storyList", result)
                .addObject("page", new ThymeLeafPaging(result));
        log.info("result =- {}", new Gson().toJson(result));
        return mv;
    }

    @GetMapping(path = "/storyAc/listView")
    public ModelAndView StoryAcListViewController(StoryListReq dto) {
        log.info("===== effect======");

//        QueryResults<StoryListRes> result = storyService.storyListResQueryResults(dto);
        dto.setStoryTpCd("SRT002");
        ModelAndView mv = new ModelAndView("layout/active/story/listViewAc")
                .addObject("storyReq", new SubActionReq())
//                .addObject(result);
                ;
        return mv;
    }
    @GetMapping(path = "/storyCm/listView")
    public ModelAndView StoryCmListViewController(StoryListReq dto) {
        log.info("===== effect======");

//        QueryResults<StoryListRes> result = storyService.storyListResQueryResults(dto);
        dto.setStoryTpCd("SRT003");
        ModelAndView mv = new ModelAndView("layout/active/story/listViewCm")
                .addObject("storyReq", new SubActionReq())
//                .addObject(result);
                ;
        return mv;
    }
    @GetMapping(path = "/storyCh/listView")
    public ModelAndView StoryChListViewController(StoryListReq dto) {
        log.info("===== effect======");

//        QueryResults<StoryListRes> result = storyService.storyListResQueryResults(dto);
        dto.setStoryTpCd("SRT004");
        ModelAndView mv = new ModelAndView("layout/active/story/listViewCh")
                .addObject("storyReq", new SubActionReq())
//                .addObject(result);
                ;
        return mv;
    }




    @PostMapping(path = "/story/modiStory")
    @ResponseBody
    public String ModiStoryContentsController(StoryOneRes dto){
        storyService.ModiStoryContentsService(dto);
        return "/admin/active/story/oneView?seq="+dto.getStoryId();
    }

    @GetMapping(path = "/story/stop")
    @ResponseBody
    public String MemberUserStopUpdate(@RequestParam(value = "seq") Long seq) {
        log.info("seq = {}", seq);
        storyService.StoryStopUpdate(seq);
        return "stopsuccess";
    }

    @GetMapping(path = "/story/normal")
    @ResponseBody
    public String MemberUserNormalUpdate(@RequestParam(value = "seq") Long seq) {
        log.info("seq = {}", seq);
        storyService.StoryNormalUpdate(seq);
        return "normalsuccess";
    }

    @GetMapping(path = "/story/reject")
    @ResponseBody
    public String MemberUserRejectUpdate(@RequestParam(value = "seq") Long seq) {
        log.info("seq = {}", seq);
        storyService.StoryRejectUpdate(seq);
        return "rejectsuccess";
    }

    @ResponseBody
    @PostMapping(path = "/story/selectModal")
    public ModelAndView StorySelectModalBinding(@RequestParam("storyId")Long seq) {
        log.info("selectModal seq - {}",seq);
        StoryPointVo result = storyService.ModalPointSelect(seq);

        ModelAndView mv = new ModelAndView("layout/active/story/modal")
                .addObject("pointVo",  result)
                ;
        return mv;
    }

    @PostMapping(path = "/story/pointExpSave")
    @ResponseBody
    public String StoryPointExpSaveModalBinding(StoryPointVo dto) {

    log.info("story pointSave - {}", new Gson().toJson(dto));

       return storyService.StoryPointExpSaveService(dto);

    }

    @GetMapping(path = "/cate/listView")
    public ModelAndView CateListViewController(CateListReq dto) {
        log.info("===== effect======");

        ModelAndView mv = new ModelAndView("layout/active/cate/listView")
                .addObject("cateReq", new ActionListReq());
        return mv;
    }

    @ResponseBody
    @PostMapping(path = "/cate/listSelect")
    public ModelAndView CateSearchBinding(CateListReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));
        var aa = actionService.CateListService(dto);
        ModelAndView mv = new ModelAndView("layout/active/cate/searchList")
                .addObject("cateList", aa)
                .addObject("page", new ThymeLeafPaging(aa));
        return mv;
    }


    @GetMapping(path = "/cate/oneView")
    public ModelAndView CateOneView(@RequestParam(value = "seq") Long seq) {
        log.info("cateone seq - {}", seq);
        ModelAndView mv = new ModelAndView("layout/active/cate/oneView")
                .addObject("cateData", actionService.CateOneDetailService(seq))
                .addObject("cateLangConReq", new CateContentsReq())
//                .addObject("selectForm", storyService.StorySelectOneService(seq))
                ;
        return mv;
    }

    @GetMapping(path = "/cate/langSelect")
    public ModelAndView CateConSelectController(@RequestParam("seq") Long seq, @RequestParam("lang") String language) {
        log.info("dto check = {}", seq);
        log.info("dto check = {}", language);
        ModelAndView mv = new ModelAndView("layout/active/cate/oneviewmodal")
                .addObject("newContents", new CateContentsReq())
                .addObject("cateLangConRes", actionService.CateLangContentsSelectService(seq, language));
        return mv;
    }


    @PostMapping(path = "/cate/update")
    public ModelAndView CateOneUpdateView(UpdateCreateReqs dto) {
        log.info("updateForm seq - {}", new Gson().toJson(dto));

        ModelAndView mv = new ModelAndView("layout/active/cate/listView")
                .addObject("cateReq", new ActionListReq());
        return mv;
    }

    @GetMapping(path = "/cate/selectModal")
    public ModelAndView AdminPerSelectCreateModal() {
        ModelAndView mv = new ModelAndView("layout/active/cate/modal")
                .addObject("createOne", new CateContentsReq())
//                .addObject("cateContentsReq", new CateContentsReq())
                ;
        return mv;
    }

    @GetMapping(path = "/cate/create")
    public ModelAndView CateSaveController() {
        UpdateCreateReqs baseVo = new UpdateCreateReqs();
        baseVo.setPrps(new ArrayList<>());
        baseVo.setPlace(new ArrayList<>());
        ModelAndView mv = new ModelAndView("layout/active/cate/oneView")
                .addObject("createOne", new UpdateCreateReqs())
                .addObject("cateData", baseVo)
                .addObject("cateLangConReq", new CateContentsReq());
        return mv;
    }

    @PostMapping(path = "/cate/contentsUpdateReq")
    @ResponseBody
    public String CateContentsUpdateController(CateContentsReq dto) {
        log.info("cateContentsUpdate - {}", new Gson().toJson(dto));

        actionService.CateContentSaveService(dto);

        return "success";
    }

    //모달이입력되는곳
    @PostMapping(path = "/cate/createReq")
    public ModelAndView CateCreateReq(@RequestParam(name = "file", required = false) MultipartFile[] file, UpdateCreateReqs dto) {
        log.info("cate Create Dto - {}", new Gson().toJson(dto));
        actionService.CateModalSaveService(file, dto);

        var aa = actionService.CateListService(new CateListReq());
        ModelAndView mv = new ModelAndView("layout/active/cate/searchList")
                .addObject("cateList", aa)
                .addObject("page", new ThymeLeafPaging(aa));
        return mv;
    }


    @GetMapping(path = "/action/listView")
    public ModelAndView ActionListViewController(ActionListReq dto) {
        log.info("===== effect======");
//        QueryResults<ActionListRes> result = actionService.ActionListService(dto);
        ModelAndView mv = new ModelAndView("layout/active/action/listView")
                .addObject("actionReq", new ActionListReq());
        return mv;
    }


    @ResponseBody
    @PostMapping(path = "/action/listSelect")
    public ModelAndView ActionSearchBinding(ActionListReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));
        var aa = actionService.ActionListService(dto);
        ModelAndView mv = new ModelAndView("layout/active/action/searchList")
                .addObject("actionList", aa)
                .addObject("page", new ThymeLeafPaging(aa));
        return mv;
    }


    @GetMapping(path = "/action/oneView")
    public ModelAndView ActionViewController(@RequestParam(value = "seq") Long seq) {
        log.info("cateone seq - {}", seq);

        ActionViewVo oneViewResult = actionService.ActionOneViewService(seq);

        ModelAndView mv = new ModelAndView("layout/active/action/oneView")
                .addObject("createOne", new UpdateCreateReqs())
                .addObject("cateLangConReq", new CateContentsReq())
                .addObject("actionOneView", oneViewResult);
        return mv;
    }



    @PostMapping(path = "/action/save")
    @ResponseBody
    public String ActionSaveConteoller(@RequestParam(name = "file", required = false) MultipartFile[] file, ActionViewVo dto) {
        log.info("action save - {}", new Gson().toJson(dto));
//
        TbMssn result = actionService.ActionSaveService(file, dto);

        return "/admin/active/action/oneView?seq=" + result.getMssnId();
    }

    @PostMapping(path = "/cate/save")
    @ResponseBody
    public String ActionCateSaveConteoller(@RequestParam(name = "file", required = false) MultipartFile[] file, UpdateCreateReqs dto) {
        log.info("action cate save - {}", new Gson().toJson(dto));
        TbMssnCate result = actionService.ActionCateSaveService(file, dto);
        return "/admin/active/cate/oneView?seq=" + result.getCateId();
    }


    @PostMapping(path = "/action/cateSelect")
    public ModelAndView ActionCateSelectConteoller(ActionCateSelectReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));

        List<ActionCateSelectRes> cateTitleSelect = actionService.ActionCateTitleSelect(dto);

        ModelAndView mv = new ModelAndView("layout/active/action/selecCateList")
                .addObject("cateTitleList", cateTitleSelect)
                .addObject("cateLangConReq", new CateContentsReq());
        return mv;
    }


    @GetMapping(path = "/action/langSelect")
    public ModelAndView actionConSelectController(@RequestParam("seq") Long seq, @RequestParam("lang") String language) {
        log.info("dto check = {}", seq);
        log.info("dto check = {}", language);
        ModelAndView mv = new ModelAndView("layout/active/action/oneviewmodal")
                .addObject("newContents", new CateContentsReq())
                .addObject("LangConRes", actionService.LangContentsSelectService(seq, language));
        return mv;
    }


    @PostMapping(path = "/action/contentsUpdateReq")
    @ResponseBody
    public String ContentsUpdateController(ContentsReq dto) {
        log.info("cateContentsUpdate - {}", new Gson().toJson(dto));
        actionService.ContentSaveService(dto);
        return "success";
    }

    @GetMapping(path = "/action/create")
    public ModelAndView ContentsCreateController() {
//        log.info("cateContentsUpdate - {}", new Gson().toJson(dto));

//        actionService.ContentSaveService(dto);
        ModelAndView mv = new ModelAndView("layout/active/action/oneView")
                .addObject("createOne", new UpdateCreateReqs())
                .addObject("cateLangConReq", new CateContentsReq())
                .addObject("actionOneView", new ActionViewVo());

        return mv;
    }


    @GetMapping(path = "/subaction/listView")
    public ModelAndView SubActionListViewController(SubActionReq dto) {
        log.info("===== effect======");

//        QueryResults<SubActionRes> result = actionService.SelectSubActionListService(dto);
        ModelAndView mv = new ModelAndView("layout/active/subaction/listView")
                .addObject("effectReq", new SubActionReq())
//                .addObject(result)
                ;
        return mv;
    }

    @ResponseBody
    @PostMapping(path = "/subaction/listSelect")
    public ModelAndView SubActionSearchBinding(SubActionReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));
//        var aa = actionService.SelectSubActionListService(dto);
        ModelAndView mv = new ModelAndView("layout/active/subaction/searchList")
//                .addObject("userList", aa)
//                .addObject("page", new ThymeLeafPaging(aa))
                ;
        return mv;
    }

    @GetMapping(path = "/recommen/listView")
    public ModelAndView RecommenActionListViewController(RecommendActionListReq dto) {
        log.info("===== effect======");
//
//        QueryResults<SubActionRes> result = actionService.SelectSubActionListService(dto);

        ModelAndView mv = new ModelAndView("layout/active/recommended/listView")
                .addObject("recommendReq", new RecommendActionListRes())
//                .addObject(result)
                ;
        return mv;
    }

    @ResponseBody
    @PostMapping(path = "/recommen/listSelect")
    public ModelAndView RecommenActionSearchBinding(ChallengeListReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));
//        var aa = challengeService.ChallengeListService(dto);
        ModelAndView mv = new ModelAndView("layout/active/recommended/searchList")
//                .addObject("userList", aa)
//                .addObject("page", new ThymeLeafPaging(aa))
                ;
        return mv;
    }


    @GetMapping(path = "/challenge/listView")
    public ModelAndView ChallengeListViewController(ChallengeListReq dto) {
        log.info("===== effect======");

        ModelAndView mv = new ModelAndView("layout/active/challenge/listView")
                .addObject("challengeReq", new ChallengeListReq());
        return mv;
    }

    @ResponseBody
    @PostMapping(path = "/challenge/listSelect")
    public ModelAndView ChallengeSearchBinding(ChallengeListReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));

        var result = actionService.ChallengeListSelectService(dto);

        ModelAndView mv = new ModelAndView("layout/active/challenge/searchList")
                .addObject("chalList", actionService.ChallengeListSelectService(dto))
                .addObject("page", new ThymeLeafPaging(result));
        ;
        return mv;
    }

    @GetMapping(path = "/challenge/oneView")
    public ModelAndView ChallengeOneViewController(@RequestParam(value = "seq") Long seq) {

        log.info("ChalOneView - {}", seq);

        ChallengeOneVo result = actionService.ChalSelectOneView(seq);

        UpdateCreateReqs result2 = new UpdateCreateReqs();

        result2.setPlace(new ArrayList<>());
        result2.setPrps(new ArrayList<>());
        result2 = actionService.CateOneDetailService(seq);

        ModelAndView mv = new ModelAndView("layout/active/challenge/oneView")
                .addObject("chalOneSel", result2) //디테일 페이지 전체 VO
                .addObject("chalComSel", new ChallengeComSelectVo()) //회사검색용
                .addObject("mssnSel")//미션 검색용
                .addObject("cateLangConReq", new CateContentsReq())//언어검색및 입력
                .addObject("actionListSelect", new ActionSelectListVo())
                ;
        return mv;
    }

    @GetMapping(path = "/challenge/create")
    public ModelAndView ChallengeCreateController() {
        UpdateCreateReqs result2 = new UpdateCreateReqs();
        result2.setPlace(new ArrayList<>());
        result2.setPrps(new ArrayList<>());

        ModelAndView mv = new ModelAndView("layout/active/challenge/oneView")
                .addObject("chalOneSel", result2)
                .addObject("createOne", new ChallengeOneVo())
                .addObject("chalComSel", new ChallengeComSelectVo());
        return mv;
    }

    @PostMapping(path = "/challenge/comSelect")
    public ModelAndView ChallengeComSelectController(ChallengeComSelectVo dto) {
        List<ChallengeComSelectVo> result = new ArrayList<>();
        result = actionService.ChalComSelectService(dto);
        ModelAndView mv = new ModelAndView("layout/active/challenge/selectComList")
                .addObject("comSelect", result)
                .addObject("cateLangConReq", new CateContentsReq());
        return mv;
    }

    @PostMapping(path = "/challenge/save")
    @ResponseBody
    public String ChalCateSaveConteoller(@RequestParam(name = "file", required = false) MultipartFile[] file, ChallengeOneVo dto) {
        log.info("action cate save - {}", new Gson().toJson(dto));
        TbMssnCate result = actionService.ChalCreateService(file, dto);
            return "/admin/active/challenge/oneView?seq=" + result.getCateId();
    }

    @PostMapping(path = "/challenge/actionSelectList")
    @ResponseBody
    public ModelAndView ChalActionSelectListConteoller(ActionSelectListVo dto) {
        log.info("chal action List Vo - {}", new Gson().toJson(dto));
//        TbMssnCate result = actionService.ActionCateSaveService(file, dto);

        List<ActionSelectListVo> result = actionService.ChalActionSelectListService(dto);

        ModelAndView mv = new ModelAndView("layout/active/challenge/selectActionList")
                .addObject("actionSelect", result)
                ;
        return mv;
    }

    @GetMapping(path = "/challengeAction/listView")
    public ModelAndView ChallengeActionSelectListController(){
        ModelAndView mv = new ModelAndView("layout/active/challengeAction/listView")
                .addObject("actionReq", new ActionListReq());
                ;
        return mv;
    }

    @ResponseBody
    @PostMapping(path = "/challengeAction/listSelect")
    public ModelAndView ChallengeActionSearchBinding(ActionListReq dto) {
        log.info("databind - {}", new Gson().toJson(dto));
        var aa = actionService.ChallengeActionListService(dto);
        ModelAndView mv = new ModelAndView("layout/active/challengeAction/searchList")
                .addObject("actionList", aa)
                .addObject("page", new ThymeLeafPaging(aa));
        return mv;
    }


    @GetMapping(path = "/challengeAction/oneView")
    public ModelAndView ChallengeActionViewController(@RequestParam(value = "seq") Long seq) {
        log.info("challengeAction seq - {}", seq);

//        ActionViewVo oneViewResult = actionService.ActionOneViewService(seq);

        ChallengeActionViewVo chalAct = actionService.ChalActionOneViewService(seq);

        ModelAndView mv = new ModelAndView("layout/active/challengeAction/oneView")
                .addObject("chalAct", chalAct);
        return mv;
    }

    @PostMapping(path = "/challengeAction/chalCateSelector")
    public ModelAndView ChalCateSelectorController(ActionCateSelectReq dto) {

        List<ActionCateSelectRes> cateTitleSelect = actionService.ChalActionCateTitleSelect(dto);

        ModelAndView mv = new ModelAndView("layout/active/challengeAction/cateSelector")
                .addObject("cateTitleList", cateTitleSelect)
                .addObject("cateLangConReq", new CateContentsReq());
        return mv;

    }

    @PostMapping(path = "/challengeAction/save")
    @ResponseBody
    public String ChallengeActionSaveConteoller(@RequestParam(name = "file", required = false) MultipartFile[] file, ChallengeActionViewVo dto) {
        log.info("ChallengeActionSaveConteoller save - {}", new Gson().toJson(dto));
//
        TbMssn result = actionService.ChallengeActionSaveService(file, dto);

        return "/admin/active/challengeAction/oneView?seq=" + result.getMssnId();
//        return "/admin/active/action/oneView?seq=" + 4L;
    }

    @GetMapping(path = "/challengeAction/create")
    public ModelAndView challengeActionCreateController() {
//        log.info("cateContentsUpdate - {}", new Gson().toJson(dto));

//        actionService.ContentSaveService(dto);
        ModelAndView mv = new ModelAndView("layout/active/challengeAction/oneView")
                .addObject("chalAct", new ChallengeActionViewVo())

                ;

        return mv;
    }


}
