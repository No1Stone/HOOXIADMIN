package com.ui.hooxi.admin.db.repository.company.dsl;


import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.active.challenge.ChallengeComSelectVo;
import com.ui.hooxi.admin.biz.model.user.CompanyListReq;
import com.ui.hooxi.admin.biz.model.user.CompanyListRes;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TbCompanyRepositoryDsl {
    @Transactional(readOnly = true)
    QueryResults<CompanyListRes> CompannyListSelectList(CompanyListReq dto);

    @Transactional(readOnly = true)
    List<ChallengeComSelectVo> ChalComSelect(ChallengeComSelectVo dto);


}
