package com.ui.hooxi.admin.db.repository.user;

import com.ui.hooxi.admin.db.entity.user.TbClausAgr;
import com.ui.hooxi.admin.db.entity.user.id.TbClausAgrId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbClausAgrRepository extends JpaRepository<TbClausAgr, TbClausAgrId> {
}