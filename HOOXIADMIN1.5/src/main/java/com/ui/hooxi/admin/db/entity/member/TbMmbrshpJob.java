package com.ui.hooxi.admin.db.entity.member;

import com.ui.hooxi.admin.db.entity.admin.TbAdminJobKywd;
import com.ui.hooxi.admin.db.entity.member.id.TbMmbrshpJobId;

import javax.persistence.*;

@Table(name = "tb_mmbrshp_job")
@Entity
@IdClass(TbMmbrshpJobId.class)
public class TbMmbrshpJob {
    @Id
    @Column(name = "mmbrshp_id", nullable = false)
    private Long mmbrshpId;
    @Id
    @Column(name = "kywd_id", nullable = false)
    private Long kywd_id;
    @Id
    @Column(name = "use_tp_cd", nullable = false)
    private String useTpCd;
    @Column(name = "mmbrshp_job_id", nullable = false)
    private Long mmbrshpJobId;

}