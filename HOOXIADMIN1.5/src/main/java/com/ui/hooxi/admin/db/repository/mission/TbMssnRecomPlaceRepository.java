package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnRecomPlace;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnRecomPlaceId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMssnRecomPlaceRepository extends JpaRepository<TbMssnRecomPlace, TbMssnRecomPlaceId> {
}
