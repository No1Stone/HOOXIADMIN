package com.ui.hooxi.admin.biz.service.active.pack;

import com.ui.hooxi.admin.biz.service.login.RegGetService;
import com.ui.hooxi.admin.db.entity.point.Point;
import com.ui.hooxi.admin.db.entity.point.PointDtl;
import com.ui.hooxi.admin.db.entity.point.id.PointId;
import com.ui.hooxi.admin.db.entity.point.model.PointDtlAmtDTO;
import com.ui.hooxi.admin.db.entity.point.model.PointListResponse;
import com.ui.hooxi.admin.db.entity.point.model.PointRequest;
import com.ui.hooxi.admin.db.entity.point.type.PointStatCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnTpCd;
import com.ui.hooxi.admin.db.repository.point.TbPointDtlRepository;
import com.ui.hooxi.admin.db.repository.point.TbPointRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PointApplication {

    private final ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(PointApplication.class);
    private final TbPointDtlRepository pointDtlRepository;
    private final TbPointRepository pointRepository;
    private final RegGetService regGetService;

    public Point findPointByPrimaryKey(PointId key) {
        return pointRepository.findById(key)
                .orElseThrow(() -> {throw new IllegalArgumentException("NOT FOUND point");});
    }

    public List<PointDtl> findPointDtlsByMmbrIdAndPointId(Long mmbrId, Long pointId) {
        return pointDtlRepository.findPointDtlsByMmbrIdAndPointIdOrderByTrstnDtAscPointDtlIdAsc(mmbrId, pointId);
    }

    /**
     * 포인트 남은 금액 혹은 가용 가능한 금액
     * @param mmbrId
     * @return
     */
    public Long getRemainingPoint(Long mmbrId) {
        return pointDtlRepository.sumPointByMmbrId(mmbrId);
    }


    /**
     * 포인트 내역 리스트
     * @param mmbrId
     * @param pageable
     * @return
     */
    public PageImpl<PointListResponse> getPointList(Long mmbrId, Pageable pageable) {
        Page<Point> pointPage = pointRepository.findPointsByMmbrIdOrderByTrstnDtDescPointIdDesc(mmbrId, pageable);
        List<PointListResponse> list = pointPage.getContent().stream().map(e -> modelMapper.map(e, PointListResponse.class)).collect(Collectors.toList());
        return new PageImpl<PointListResponse>(list, pageable, pointPage.getTotalElements());
    }

    /**
     * 사용가능 포인트 체크
     * @param mmbrId
     * @param point
     * @return
     */
    public Long getRemainingPoint(Long mmbrId, Long point) {
        Long resultPoint = pointDtlRepository.sumPointByMmbrId(mmbrId);
        if (resultPoint < point) {
            throw new IllegalArgumentException("잔액이 부족합니다.");
        }
        return resultPoint;
    }


    /**
     * pointId To pointDtlId
     * @param mmbrId
     * @param pointId
     * @param legacyCode
     * @return
     */
    public Long getPointDtlIdByKeyAndLegacyCode(Long mmbrId, Long pointId, String legacyCode) {
        Long pointDtlId = pointDtlRepository.getPointDtlId(mmbrId, pointId, PointStatCd.ACCUMULATE.getLegacyCode());
        if (pointDtlId.equals(null) || pointDtlId.equals(0L) ) {
            throw new IllegalArgumentException("해당 정보가 없습니다.");
        }
        return pointDtlId;
    }


    /**
     * 적립 취소 전 적립 된 포인트 사용 여부 체크
     * @param mmbrId
     * @param pointDtlId
     * @return
     */
    public PointDtlAmtDTO getPointDtlAmt(Long mmbrId, Long pointDtlId) {
        PointDtlAmtDTO result = pointDtlRepository.pointDtlList(pointDtlId, mmbrId);
        if ( result == null ) {
            throw new IllegalArgumentException("잔액이 부족합니다.");
        }

        // 적립 취소 불가능 케이스

        // case2. 이미 사용, 사용했지만 적립취소가 가능? -> 해당 경우는 만료기간 때문에 이슈가 발생할 수도 있음,
        // 이벤트로 인해 생성 된 포인트의 경우 만료기간이 짧을 경우 해당 포인트가 먼저 사용 될 경우 복잡해짐
        if ( result.getOrgPointAmt() - result.getPointAmt() > 0 ) {
            throw new IllegalArgumentException("이미 사용한 포인트");
        }
        return result;
    }


    /**
     * 기본 포인트 저장
     * @param mmbrId
     * @param pointSaveRequest
     */
    public void saveBasePoint(Long mmbrId, PointRequest pointSaveRequest) {
        Point savePoint = pointRepository.save(pointSaveRequest.toSavaEntity(mmbrId, regGetService.getRegId()));
        savePoint.saveOrgPointId();
        PointDtl savePointDtl = pointDtlRepository.save(savePoint.toSavePointDtlEntity(regGetService.getRegId()));
        savePointDtl.saveUpdate();
    }

    /**
     * 기본 포인트 저장
     * @param mmbrId
     * @param trstnId
     */

    public void savePointProcess(Long mmbrId, Long trstnId, TrstnTpCd trstnTpCd, TrstnDtlTpCd trstnDtlTpCd) {

        PointRequest pointSaveRequest = PointRequest.builder()
                .mmbrId(mmbrId)
                .trstnId(trstnId)
                .trstnTpCd(trstnTpCd)
                .trstnDtlTpCd(trstnDtlTpCd)
                .build();

        // 보상테이블 검색
//        pointSaveRequest.updatePoint(100L);
        this.saveBasePoint(pointSaveRequest.getMmbrId(), pointSaveRequest);
    }


    /**
     * 기본 포인트 저장
     * @param pointSaveRequest
     */
//    public void savePointProcess(PointRequest pointSaveRequest) {
//        Point savePoint = pointRepository.save(pointSaveRequest.toSavaEntity(pointSaveRequest.getMmbrId()));
//        savePoint.saveOrgPointId();
//        PointDtl savePointDtl = pointDtlRepository.save(savePoint.toSavePointDtlEntity());
//        savePointDtl.saveUpdate();
//    }

    /**
     * 포인트 명세 사용
     * @param mmbrId
     * @param usePoint
     */
    public Point useBasePoint(Long mmbrId, PointRequest usePoint) {
        Point useResult = pointRepository.save(usePoint.toUseEntity(mmbrId));
        useResult.saveOrgPointId();
        return useResult;
    }


    /**
     * 포인트 상세 내역 조회 후 사용포인트 차감
     * @param usePoint
     * @param pointDto
     */
    public void useBasePointDtl(Long usePoint, Point pointDto) {
        List<PointDtlAmtDTO> pointDtlList = pointDtlRepository.pointDtlList(pointDto.getMmbrId());

        boolean breakPoint = false;
        Long amt = usePoint;

        for (PointDtlAmtDTO dto : pointDtlList) {
            Long minusPoint = 0L;
            if (dto.getPointAmt() >= amt) {
                minusPoint = amt;
                breakPoint = true;
            } else {
                amt = usePoint - dto.getPointAmt();
                minusPoint = dto.getPointAmt();
            }

            PointDtl result = pointDtlRepository.save(pointDto.toUsePointDtlEntity(dto.getDtlOrgPointId(),(minusPoint * -1),dto.getExpireDt()));
            result.useIdUpdate();

            // 차감 포인트 없으면 탈출
            if (breakPoint) break;
        }
    }


    /**
     * 포인트 적립 취소
     * @param point
     * @param pointAmt
     */
    public void cancelSavePoint(Point point, PointDtlAmtDTO pointAmt) {
        Point cancelPoint = pointRepository.save(point.toCancelPointEntity(PointStatCd.CANCEL, regGetService.getRegId()));
        PointDtl cancelPointDtl = pointDtlRepository.save(cancelPoint.toCancelSavePointDtlEntity(pointAmt.getDtlOrgPointId(), pointAmt.getExpireDt()));
        cancelPointDtl.useIdUpdate();
    }


    /**
     * 사용취소 명세
     * @param cancelPoint
     * @return
     */
    public Point cancelUseBasePoint(Point cancelPoint) {
        Point useResult = pointRepository.save(cancelPoint.toCancelPointEntity(PointStatCd.USE_CANCEL, regGetService.getRegId()));
        return useResult;
    }

    /**
     * 포인트 사용취소 상세
     * @param pointDto
     */
    public void cancelUseBasePointDtl(Long targetPointId, Point pointDto) {
        List<PointDtl> pointUseList = this.findPointDtlsByMmbrIdAndPointId(pointDto.getMmbrId(), targetPointId);

        for (PointDtl dto : pointUseList) {
            PointDtl cancelPoint = pointDtlRepository.save(pointDto.toCancelUsePointDtlEntity(dto.getDtlOrgPointId(), dto.getPoint(), dto.getExpireDt()));
            cancelPoint.useIdUpdate();
        }
    }
}
