package com.ui.hooxi.admin.db.repository.exp;

import com.ui.hooxi.admin.db.entity.exp.TbAdminExp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbAdminExpRepository extends JpaRepository<TbAdminExp, Long> {
}
