package com.ui.hooxi.admin.db.repository.code;

import com.ui.hooxi.admin.db.entity.code.TbCmnCode;
import com.ui.hooxi.admin.db.entity.code.id.CmnCodeId;
import com.ui.hooxi.admin.db.entity.code.id.TbCmnGrpCodeId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TbCmnCodeRepository extends JpaRepository<TbCmnCode, CmnCodeId> {

    List<TbCmnCode> findByUseYn(String use);

    List<TbCmnCode> findByCdContainsOrderByCdDesc(String cdContain);
}
