package com.ui.hooxi.admin.db.entity.code;

import com.ui.hooxi.admin.db.entity.code.id.TbCmnGrpCodeId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;

@Table(name = "tb_cmn_grp_code")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbCmnGrpCodeId.class)
public class TbCmnGrpCode {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "ordr", nullable = false)
    private Integer ordr;

    @Id
    @Column(name = "grp_cd_nm", nullable = false, length = 50)
    private String grpCdNm;

    @Column(name = "grp_cd_val", nullable = false, length = 3)
    private String grpCdVal;

    @Column(name = "parent_grp_cd", length = 20)
    private String parentGrpCd;

    @Column(name = "grp_cd_desc", length = 100)
    private String grpCdDesc;

    @Column(name = "use_yn", length = 1)
    private String useYn;

    @Column(name = "reg_id")
    private Long regId;

    @Column(name = "reg_dt")
    private LocalDateTime regDt;

    @Column(name = "mod_id")
    private Long modId;

    @Column(name = "mod_dt")
    private LocalDateTime modDt;

    @Builder
    public TbCmnGrpCode(Long id, Integer ordr, String grpCdNm, String grpCdVal, String parentGrpCd, String grpCdDesc, String useYn, Long regId, LocalDateTime regDt, Long modId, LocalDateTime modDt) {
        this.id = id;
        this.ordr = ordr;
        this.grpCdNm = grpCdNm;
        this.grpCdVal = grpCdVal;
        this.parentGrpCd = parentGrpCd;
        this.grpCdDesc = grpCdDesc;
        this.useYn = useYn;
        this.regId = regId;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
    }
}