package com.ui.hooxi.admin.db.repository.exp;

import com.ui.hooxi.admin.db.entity.exp.TbMmbrExpHistory;
import com.ui.hooxi.admin.db.entity.exp.id.TbMmbrExpHistoryId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMmbrExpHistoryRepository extends JpaRepository<TbMmbrExpHistory, TbMmbrExpHistoryId> {
}
