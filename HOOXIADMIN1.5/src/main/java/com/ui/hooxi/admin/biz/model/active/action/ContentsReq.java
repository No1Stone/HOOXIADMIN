package com.ui.hooxi.admin.biz.model.active.action;

import com.ui.hooxi.admin.db.entity.mission.TbMssnCateContents;
import com.ui.hooxi.admin.db.entity.mission.TbMssnContents;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContentsReq {

    private Long mssnId;
    private String langCd;
    private String mssnTitle;
    private String mssnTitleDtl;
    private String mssnEffect;
    private String mssnEtc;

    public TbMssnContents ofSave(){
        TbMssnContents tmc = new TbMssnContents();

        if(this.mssnId != null){
            tmc.setMssnId(this.mssnId);
        }
        tmc.setLangCd(this.langCd);
        tmc.setMssnTitle(this.mssnTitle);
        tmc.setMssnTitleDtl(this.mssnTitleDtl);
        tmc.setMssnEffect(this.mssnEffect);
        tmc.setMssnEtc(this.mssnEtc);

        return tmc;
    }


}
