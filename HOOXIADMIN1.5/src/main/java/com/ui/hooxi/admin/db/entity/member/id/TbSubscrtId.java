package com.ui.hooxi.admin.db.entity.member.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbSubscrtId implements Serializable {
    private Long mmbtId;
    private Long mmbtshpId;
}
