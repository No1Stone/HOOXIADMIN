package com.ui.hooxi.admin.db.repository.user;

import com.ui.hooxi.admin.db.entity.user.TbLoginHist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbLoginHistRepository extends JpaRepository<TbLoginHist, Long> {
}