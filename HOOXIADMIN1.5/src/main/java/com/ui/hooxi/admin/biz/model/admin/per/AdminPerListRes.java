package com.ui.hooxi.admin.biz.model.admin.per;


import javax.persistence.Column;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AdminPerListRes {

    //퍼미션 아이디
    private Long adminPmsId;
    //퍼미션이름
    private String adminPmsNm;

    private Long regId;
    //퍼미션 생성일
    private LocalDateTime regDttm;


    public AdminPerListRes(){}
    public AdminPerListRes(Long adminPmsId, String adminPmsNm, Long regId, LocalDateTime regDttm) {
        this.adminPmsId = adminPmsId;
        this.adminPmsNm = adminPmsNm;
        this.regId = regId;
        this.regDttm = regDttm;
    }

    public Long getAdminPmsId() {
        return adminPmsId;
    }

    public void setAdminPmsId(Long adminPmsId) {
        this.adminPmsId = adminPmsId;
    }

    public String getAdminPmsNm() {
        return adminPmsNm;
    }

    public void setAdminPmsNm(String adminPmsNm) {
        this.adminPmsNm = adminPmsNm;
    }

    public Long getRegId() {
        return regId;
    }

    public void setRegId(Long regId) {
        this.regId = regId;
    }

    public LocalDateTime getRegDttm() {
        return regDttm;
    }
    public String getRegDttms() {
        DateTimeFormatter dt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return regDttm.format(dt);
    }

    public void setRegDttm(LocalDateTime regDttm) {
        this.regDttm = regDttm;
    }
}
