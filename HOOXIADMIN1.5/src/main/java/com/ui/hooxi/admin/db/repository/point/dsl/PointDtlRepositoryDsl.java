package com.ui.hooxi.admin.db.repository.point.dsl;

import com.ui.hooxi.admin.db.entity.point.model.PointDtlAmtDTO;

import java.util.List;

public interface PointDtlRepositoryDsl {

    List<PointDtlAmtDTO> pointDtlList(Long mmbrId);
    PointDtlAmtDTO pointDtlList(Long pointDtlId, Long mmbrId);
}
