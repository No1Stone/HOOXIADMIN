package com.ui.hooxi.admin.db.repository.user;

import com.ui.hooxi.admin.db.entity.user.TbMmbr;
import com.ui.hooxi.admin.db.repository.user.dsl.TbMmbrReposiroryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface TbMmbrRepository extends JpaRepository<TbMmbr, Long>, TbMmbrReposiroryDsl {

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_mmbr set " +
                    "mmbr_status_cd = :mmbrStatusCd " +
                    "where " +
                    "mmbr_id = :mmbrId "
            , nativeQuery = true
    )
    int UpdateMemberStatus(
            @Param("mmbrStatusCd") String mmbrStatusCd,
            @Param("mmbrId") Long mmbrId);


}