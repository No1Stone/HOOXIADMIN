package com.ui.hooxi.admin.db.repository.member;

import com.ui.hooxi.admin.db.entity.member.TbMmbrshp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface TbMmbrshpRepository extends JpaRepository<TbMmbrshp, Long> {

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_mmbrshp set " +
                    "mmbrshp_state_cd = :mmbrshpStateCd " +
                    "where " +
                    "mmbr_id = :mmbrId "
            , nativeQuery = true
    )
    int UpdateMemberShipStatus(
            @Param("mmbrshpStateCd") String mmbrshpStateCd,
            @Param("mmbrId") Long mmbrId);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE tb_mmbrshp set " +
                    "grad_cd = :gradCd " +
                    "where " +
                    "mmbr_id = :mmbrId "
            , nativeQuery = true
    )
    int UpdateMemberGradStatus(
            @Param("gradCd") String gradCd,
            @Param("mmbrId") Long mmbrId);



}