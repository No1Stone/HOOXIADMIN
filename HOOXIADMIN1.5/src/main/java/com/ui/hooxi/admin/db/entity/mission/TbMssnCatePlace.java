package com.ui.hooxi.admin.db.entity.mission;


import com.ui.hooxi.admin.db.entity.mission.id.TbMssnCatePlaceId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_cate_place")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnCatePlaceId.class)
public class TbMssnCatePlace {
    @Id
    @Column(name = "cate_id", nullable = true)
    private Long cateId;
    @Id
    @Column(name = "place_cd", nullable = true)
    private String placeCd;

    @Builder
    public TbMssnCatePlace(Long cateId, String placeCd) {
        this.cateId = cateId;
        this.placeCd = placeCd;
    }


}
