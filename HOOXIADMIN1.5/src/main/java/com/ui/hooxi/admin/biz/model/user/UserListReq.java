package com.ui.hooxi.admin.biz.model.user;

import com.ui.hooxi.admin.biz.model.UserSearchType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserListReq {

    private int page;
    private int size;

    private String searchName;
    private String searchString;
    private String searchStatus;
    private String searchStatusVal;
    private String searchGrade;
    private String langCd;
    private String startDt;
    private String endDt;

    private Long userSeq;


}
