package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnRecomMeta;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnRecomMetaId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMssnRecomMetaRepository extends JpaRepository<TbMssnRecomMeta, TbMssnRecomMetaId> {
}
