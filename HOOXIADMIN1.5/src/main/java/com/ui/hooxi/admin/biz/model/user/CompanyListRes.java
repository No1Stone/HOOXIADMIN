package com.ui.hooxi.admin.biz.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyListRes {

    private Long companyId;
    private String companyName;
    private String contents;
    private String imgUrl;
    private Long regId;
    private LocalDateTime regDttm;
    private Long modId;
    private LocalDateTime modDttm;
    private String adminName;
}
