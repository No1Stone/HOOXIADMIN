package com.ui.hooxi.admin.biz.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyListReq {
    private int page;
    private int size;

    private String searchName;
    private String searchNameVal;
    private String searchStatus;
    private String searchStatusVal;

    private String startDt;
    private String endDt;

    private Long userSeq;


}
