package com.ui.hooxi.admin.biz.restcont;

import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.active.story.StoryListReq;
import com.ui.hooxi.admin.biz.model.active.story.StoryListRes;
import com.ui.hooxi.admin.biz.model.active.story.StoryPointVo;
import com.ui.hooxi.admin.biz.model.admin.AdminListReq;
import com.ui.hooxi.admin.biz.model.user.UserListReq;
import com.ui.hooxi.admin.biz.service.active.StoryService;
import com.ui.hooxi.admin.biz.service.common.AdminInfoBean;
import com.ui.hooxi.admin.biz.service.util.AwsSesService;
import com.ui.hooxi.admin.config.type.CMenu;
import com.ui.hooxi.admin.config.type.PMenu;
import com.ui.hooxi.admin.config.type.YnType;
import com.ui.hooxi.admin.config.util.ThymeLeafPaging;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsCmn;
import com.ui.hooxi.admin.db.entity.company.TbCompany;
import com.ui.hooxi.admin.db.repository.admin.AdminPmsCmnRepository;
import com.ui.hooxi.admin.db.repository.admin.AdminPmsPmnRepository;
import com.ui.hooxi.admin.db.repository.admin.AdminPmsRepository;
import com.ui.hooxi.admin.db.repository.admin.AdminRepository;
import com.ui.hooxi.admin.db.repository.company.TbCompanyRepository;
import com.ui.hooxi.admin.db.repository.story.TbStoryRepository;
import com.ui.hooxi.admin.db.repository.user.TbMmbrRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.management.Notification;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TestService {

    private final AdminRepository adminRepository;
    private final AdminPmsRepository adminPmsRepository;
    private final AdminPmsPmnRepository adminPmsPmnRepository;
    private final AdminPmsCmnRepository adminPmsCmnRepository;
    private final TbMmbrRepository tbMmbrRepository;
    private final TbStoryRepository tbStoryRepository;
    private final TbCompanyRepository tbCompanyRepository;
    private final AwsSesService awsSesService;
    private final AdminInfoBean adminInfoBean;
    private final StoryService storyService;
    private final HttpServletRequest httpServletRequest;
    private final HttpServletResponse httpServletResponse;
//    private final TbMssnRepository tbMssnRepository;

//    private final MmbrRepository mmbrRepository;

//    public List<TbMmbr> test1() {
//        return mmbrRepository.findAll();
//    }

    public void test9() {

        List<Long> app = adminPmsPmnRepository.findByAdminPmsId(1l)
                .stream().map(e -> e.getAdminPmsPmnId())
                .collect(Collectors.toList());

        List<AdminPmsCmn> apc = adminPmsCmnRepository.findByAdminPmsId(1l);

        List<String> authList = new ArrayList<>();
        for (Long e : app) {
            if (e == 1l) {
                authList.add(PMenu.USER_LIST.getAuthName());
            }
            if (e == 2l) {
                authList.add(PMenu.ACTIVE_LIST.getAuthName());
            }
            if (e == 3l) {
                authList.add(PMenu.ADMIN_LIST.getAuthName());
            }
        }


        for (AdminPmsCmn e : apc) {
            if (e.getAdminPmsCmnId() == 10l) {
                if (e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    log.info("== 10l ==");
                    authList.add(CMenu.USER_R.getAuthName());
                }
            }

            if (e.getAdminPmsCmnId() == 20l) {
                if (e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    log.info("== 20l ==");
                    authList.add(CMenu.MEMBERSHIP_R.getAuthName());
                }
            }
            if (e.getAdminPmsCmnId() == 30l) {
                if (e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    log.info("== 30l ==");
                    authList.add(CMenu.STORY_R.getAuthName());
                }
            }
            if (e.getAdminPmsCmnId() == 40l) {
                if (e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    log.info("== 40l ==");
                    authList.add(CMenu.ACTION_R.getAuthName());
                }
            }
            if (e.getAdminPmsCmnId() == 50l) {
                if (e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    authList.add(CMenu.CHALLENGE_R.getAuthName());
                }
            }
            if (e.getAdminPmsCmnId() == 60l) {
                if (e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    authList.add(CMenu.COMUNITY_R.getAuthName());
                }
            }
            if (e.getAdminPmsCmnId() == 70l) {
                if (e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    authList.add(CMenu.ADMINLIST_R.getAuthName());
                }
            }
            if (e.getAdminPmsCmnId() == 80l) {
                if (e.getAdminCmnR().equals(YnType.Y.getVal())) {
                    authList.add(CMenu.PERMISSION_R.getAuthName());
                }
            }

        }

        var aa = authList.stream()
//               .map(e -> e.getAdminCmnNm())
                .map(SimpleGrantedAuthority::new).collect(Collectors.toList());

        log.info("app - {}", new Gson().toJson(app));
        log.info("apc - {}", new Gson().toJson(apc));
        log.info("authList - {}", new Gson().toJson(authList));
        log.info("aa - {}", new Gson().toJson(aa));

    }

    public void MailSendTest() {

        //implementation 'javax.mail:mail:1.4.7'

        //보내는 계정 아이디 비밀번호
        String email = "";
        String password = "";

        //        구글 465
        //         네이버 587

        Properties pr = new Properties();
        pr.put("mail.smtp.host", "smtp.naver.com");
        pr.put("mail.smtp.port", 587);
        pr.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(pr, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, password);
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));            //수신자메일주소
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("jangws1003.ui@gmail.com"));
            message.setSubject("test");
            message.setText("test111");

            // Subject
            try {
                message.setSubject("제목을 입력하세요"); //메일 제목을 입력
            } catch (MessagingException ex) {
                ex.printStackTrace();
            }

            // Text
            try {
                message.setText("내용을 입력하세요");    //메일 내용을 입력
            } catch (MessagingException ex) {
                ex.printStackTrace();
            }

            // send the message
            try {
                Transport.send(message); ////전송
            } catch (MessagingException ex) {
                ex.printStackTrace();
            }
            System.out.println("message sent successfully...");
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }


    public void filetest(HttpServletResponse response, String pa) throws Exception {
        String path = " C:\\WorkSpace\\Hooxi\\HOOXIADMIN1.5\\src\\main\\resources\\static\\img\\" + pa; // 경로에 접근할 때 역슬래시('\') 사용
//            String path = " C:\\Users\\leejo\\Desktop\\바탕화면\\"+pa; // 경로에 접근할 때 역슬래시('\') 사용

        log.info("pa - {}", path);
        response.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
//            response.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//            response.setHeader("Content-Disposition", "attachment;filename=" + file.getName()); // 다운로드 되거나 로컬에 저장되는 용도로 쓰이는지를 알려주는 헤더
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Disposition", "attachment; fileName=\"" + URLEncoder.encode(pa, "UTF-8") + "\";");
        response.setContentType("application/download; utf-8");
        log.info("Response set");
        FileInputStream fileInputStream = new FileInputStream(path); // 파일 읽어오기
        log.info("file input");
        OutputStream out = response.getOutputStream();
        log.info("//////////// - {}");
        int read = 0;
        byte[] buffer = new byte[1024];
        try {
            while ((read = fileInputStream.read(buffer)) != -1) { // 1024바이트씩 계속 읽으면서 outputStream에 저장, -1이 나오면 더이상 읽을 파일이 없음
                log.info("while");
                out.write(buffer, 0, read);
            }

        } catch (Exception e) {
            log.info("error = {}", e.getMessage());
            throw new Exception("download error");
        }
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }

    public Object test15() {
        var aa = new UserListReq();
        aa.setSize(10);
        var bbb = tbMmbrRepository.MembershipListResListSelect(aa);
        return bbb;
    }

    public Object test16(Long seq) {
        log.info("===================");
        var aa = tbMmbrRepository.UserOneRes(seq);
        log.info("result = {}", new Gson().toJson(aa));
        return aa;
    }

    public QueryResults<StoryListRes> test17(StoryListReq dto) {

        dto.setSize(10);

        QueryResults<StoryListRes> result = tbStoryRepository.StoryListResListSelect(dto);

        return result;
    }
//
//    public Object test18() {
//        return tbMssnRepository.ActionListRes(new ActionListReq());
//    }
//
//    public Object test19() {
//        return tbMssnRepository.selecttest1();
//    }
//
//    public Object test20() {
//        return tbMssnRepository.union();
//    }

    public Object test22() {

        AdminListReq dto = new AdminListReq();

        dto.setSize(10);

        var select = adminRepository.AdminListSelect(dto);
        ThymeLeafPaging tp = new ThymeLeafPaging(select);

        return select;
    }

    public Object test23() {
        var result = tbMmbrRepository.MemberShipOneSelect(2L);
        log.info("result - {}", result);
        return result;
    }

    public Object test24() {
        return tbStoryRepository.StoryOneResListSelect(39L);
    }

    @Transactional
    public void test26() {
        tbCompanyRepository.save(TbCompany.builder()
                .companyName("testCompanyName1")
                .contents("testCompanyContents1")
                .imgUrl("https://hooxi-s3.s3.ap-northeast-2.amazonaws.com/dev/980ba0c3-5e00-4a3f-9110-02118e21d14d.jpg")
                .regId(1L)
                .regDttm(LocalDateTime.now())
                .modId(1L)
                .modDttm(LocalDateTime.now())
                .build());
        tbCompanyRepository.save(TbCompany.builder()
                .companyName("testCompanyName2")
                .contents("testCompanyContents2")
                .imgUrl("https://hooxi-s3.s3.ap-northeast-2.amazonaws.com/dev/980ba0c3-5e00-4a3f-9110-02118e21d14d.jpg")
                .regId(1L)
                .regDttm(LocalDateTime.now())
                .modId(1L)
                .modDttm(LocalDateTime.now())
                .build());
        tbCompanyRepository.save(TbCompany.builder()
                .companyName("testCompanyName3")
                .contents("testCompanyContents3")
                .imgUrl("https://hooxi-s3.s3.ap-northeast-2.amazonaws.com/dev/980ba0c3-5e00-4a3f-9110-02118e21d14d.jpg")
                .regId(1L)
                .regDttm(LocalDateTime.now())
                .modId(1L)
                .modDttm(LocalDateTime.now())
                .build());
        tbCompanyRepository.save(TbCompany.builder()
                .companyName("testCompanyName4")
                .contents("testCompanyContents4")
                .imgUrl("https://hooxi-s3.s3.ap-northeast-2.amazonaws.com/dev/980ba0c3-5e00-4a3f-9110-02118e21d14d.jpg")
                .regId(1L)
                .regDttm(LocalDateTime.now())
                .modId(1L)
                .modDttm(LocalDateTime.now())
                .build());
    }

    public void test27() {
        String[] recep = {"jangws1003@naver.com", "smn1j@naver.com"};
        awsSesService.ArrayfromSendMail(recep, "jangws1003.ui@gmail.com", "testtext", "testSubject");
    }


    public void test28(Long seq) {
        String result = adminInfoBean.getSeqToString().get(seq);
        log.info("result28 - {}", result);
    }

    public void test29(String seq) {
        String result = adminInfoBean.getStringToString().get(seq);
        log.info("result 29 - {}", result);
    }


    public void test30(StoryPointVo dto) {
        log.info("test30 - {}", new Gson().toJson(dto));
        storyService.StoryPointExpSaveService(dto);

    }

    private Map<Long, List<String>> data = new HashMap<>();

    @Async
    public void test34(Long seq, String te) throws IOException {
        log.info("request - {}", httpServletRequest.getRequestURI());
        httpServletResponse.setContentType("text/event-stream");    // Header에 Content Type을 Event Stream으로 설정
        httpServletResponse.setCharacterEncoding("UTF-8");        // Header에 encoding을 UTF-8로 설정
        PrintWriter writer = httpServletResponse.getWriter();

        if (data.get(seq) != null) {
            for (String e : data.get(seq)) {
                writer.print(e);
                writer.print("\n");
            }
        }
    }

    public void test35(Long seq, String te) throws IOException {
        List<String> ori = new ArrayList<>();
        if (data.get(seq) != null) {
            ori = data.get(seq);
            log.info("data map - {}", new Gson().toJson(ori));
        }
        ori.add(te);
        log.info("data list - {}", new Gson().toJson(ori));
        data.put(seq, ori);
        log.info("data all - {}", new Gson().toJson(ori));
    }


    private Map<Long, SseEmitter> emiterMap = new HashMap<>();

    public SseEmitter SseEmitter(Long seq) {
        SseEmitter sseEmitter = new SseEmitter(1000L*60L*60L);
        log.info("sse info - {}", new Gson().toJson(sseEmitter));
        emiterMap.put(seq, sseEmitter);
        return sseEmitter;
    }

    public void sendToSseEmitter(Long seq, String data) {
        data = "aaaasssss";
        SseEmitter emitter = emiterMap.get(seq);
        try {
            emitter.send(
                    SseEmitter
                            .event()
                            .id(seq.toString())
                            .name("sse")
                            .data(data)
            )
            ;
        } catch (IOException exception) {
            throw new RuntimeException("연결 오류!");
        }
    }
}
