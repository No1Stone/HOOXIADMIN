package com.ui.hooxi.admin.config.type;


public enum CMenu {

    USER_C("유저", "/user/info/insert", 10l, "USER_C"),
    USER_R("유저", "/user/info/listView", 11l, "USER_R"),
    USER_R1("유저", "/user/info/oneView", 12l, "USER_R1"),
    USER_U("유저", "/user/info/update", 13l, "USER_U"),
    USER_D("유저", "/user/info/delete", 14l, "USER_D"),

    MEMBERSHIP_C("멤버쉽", "/user/mem/insert", 20l, "MEMBERSHIP_C"),
    MEMBERSHIP_R("멤버쉽", "/user/mem/listView", 21l, "MEMBERSHIP_R"),
    MEMBERSHIP_R1("멤버쉽", "/user/mem/oneView", 22l, "MEMBERSHIP_R1"),
    MEMBERSHIP_U("멤버쉽", "/user/mem/update", 23l, "MEMBERSHIP_U"),
    MEMBERSHIP_D("멤버쉽", "/user/mem/delete", 24l, "MEMBERSHIP_D"),

    STORY_C("스토리", "/active/story/insert", 30l, "STORY_C"),
    STORY_R("스토리(멤버쉽)", "/active/story/listView", 31l, "STORY_R"),
    STORY_R1("스토리", "/active/story/oneView", 32l, "STORY_R1"),
    STORY_U("스토리", "/active/story/update", 33l, "STORY_U"),
    STORY_D("스토리", "/active/story/delete", 34l, "STORY_D"),

    ACTION_C("액션", "/active/action/insert", 40l, "ACTION_C"),
    ACTION_R("액션", "/active/action/listView", 41l, "ACTION_R"),
    ACTION_R1("액션", "/active/action/oneView", 42l, "ACTION_R1"),
    ACTION_U("액션", "/active/action/update", 43l, "ACTION_U"),
    ACTION_D("액션", "/active/action/delete", 44l, "ACTION_D"),

    SUBACT_C("서브액션", "/active/subaction/insert", 50l, "SUBACT_C"),
    SUBACT_R("서브액션", "/active/subaction/listView", 51l, "SUBACT_R"),
    SUBACT_R1("서브액션", "/active/subaction/oneView", 52l, "SUBACT_R1"),
    SUBACT_U("서브액션", "/active/subaction/update", 53l, "SUBACT_U"),
    SUBACT_D("서브액션", "/active/subaction/delete", 54l, "SUBACT_D"),


    CATE_C("액션 카테고리", "/active/cate/insert", 110l, "CATE_C"),
    CATE_R("액션 카테고리", "/active/cate/listView", 111l, "CATE_R"),
    CATE_R1("액션 카테고리", "/active/cate/oneView", 112l, "CATE_R1"),
    CATE_U("액션 카테고리", "/active/cate/update", 113l, "CATE_U"),
    CATE_D("액션 카테고리", "/active/cate/delete", 114l, "CATE_D"),


    RECOMMENDED_C("추천액션", "/active/recommen/insert", 100l, "RECOMMENDED_C"),
    RECOMMENDED_R("추천액션", "/active/recommen/listView", 101l, "RECOMMENDED_R"),
    RECOMMENDED_R1("추천액션", "/active/recommen/oneView", 102l, "RECOMMENDED_R1"),
    RECOMMENDED_U("추천액션", "/active/recommen/update", 103l, "RECOMMENDED_U"),
    RECOMMENDED_D("추천액션", "/active/recommen/delete", 104l, "RECOMMENDED_D"),

    CHALLENGE_C("챌린지 카테고리", "/active/challenge/insert", 60l, "CHALLENGE_C"),
    CHALLENGE_R("챌린지 카테고리", "/active/challenge/listView", 61l, "CHALLENGE_R"),
    CHALLENGE_R1("챌린지 카테고리", "/active/challenge/oneView", 62l, "CHALLENGE_R1"),
    CHALLENGE_U("챌린지 카테고리", "/active/challenge/update", 63l, "CHALLENGE_U"),
    CHALLENGE_D("챌린지 카테고리", "/active/challenge/delete", 64l, "CHALLENGE_D"),

    COMUNITY_C("커뮤니티", "/active/community/insert", 70l, "COMUNITY_C"),
    COMUNITY_R("커뮤니티", "/active/community/listView", 71l, "COMUNITY_R"),
    COMUNITY_R1("커뮤니티", "/active/community/oneView", 72l, "COMUNITY_R1"),
    COMUNITY_U("커뮤니티", "/active/community/update", 73l, "COMUNITY_U"),
    COMUNITY_D("커뮤니티", "/active/community/delete", 74l, "COMUNITY_D"),

    ADMINLIST_C("관리자", "/manage/admin/insert", 80l, "ADMINLIST_C"),
    ADMINLIST_R("관리자", "/manage/admin/listView", 81l, "ADMINLIST_R"),
    ADMINLIST_R1("관리자", "/manage/admin/oneView", 82l, "ADMINLIST_R1"),
    ADMINLIST_U("관리자", "/manage/admin/update", 83l, "ADMINLIST_U"),
    ADMINLIST_D("관리자", "/manage/admin/delete", 84l, "ADMINLIST_D"),


    PERMISSION_C("관리자권한", "/manage/per/insert", 90l, "PERMISSION_C"),
    PERMISSION_R("관리자권한", "/manage/per/listView", 91l, "PERMISSION_R"),
    PERMISSION_R1("관리자권한", "/manage/per/oneView", 92l, "PERMISSION_R1"),
    PERMISSION_U("관리자권한", "/manage/per/update", 93l, "PERMISSION_U"),
    PERMISSION_D("관리자권한", "/manage/per/delete", 94l, "PERMISSION_D"),

    COMPANY_C("회사관리", "/user/com/insert", 120l, "COMPANY_C"),
    COMPANY_R("회사관리", "/user/com/listView", 121l, "COMPANY_R"),
    COMPANY_R1("회사관리", "/user/com/oneView", 122l, "COMPANY_R1"),
    COMPANY_U("회사관리", "/user/com/update", 123l, "COMPANY_U"),
    COMPANY_D("회사관리", "/user/com/delete", 124l, "COMPANY_D"),

    CHALLENGE_ACTION_C("챌린지", "/active/challengeAction/insert", 130l, "CHALLENGE_ACTION_C"),
    CHALLENGE_ACTION_R("챌린지", "/active/challengeAction/listView", 131l, "CHALLENGE_ACTION_R"),
    CHALLENGE_ACTION_R1("챌린지", "/active/challengeAction/oneView", 132l, "CHALLENGE_ACTION_R1"),
    CHALLENGE_ACTION_U("챌린지", "/active/challengeAction/update", 133l, "CHALLENGE_ACTION_U"),
    CHALLENGE_ACTION_D("챌린지", "/active/challengeAction/delete", 134l, "CHALLENGE_ACTION_D"),

    STORYAC_C("행동인증", "/active/storyAc/insert", 140l, "STORYAC_C"),
    STORYAC_R("행동인증", "/active/storyAc/listView", 141l, "STORYAC_R"),
    STORYAC_R1("행동인증", "/active/storyAc/oneView", 142l, "STORYAC_R1"),
    STORYAC_U("행동인증", "/active/storyAc/update", 143l, "STORYAC_U"),
    STORYAC_D("행동인증", "/active/storyAc/delete", 144l, "STORYAC_D"),

    STORYCM_C("스토리", "/active/storyCm/insert", 150l, "STORYCM_C"),
    STORYCM_R("스토리(커뮤니티)", "/active/storyCm/listView", 151l, "STORYCM_R"),
    STORYCM_R1("스토리", "/active/storyCm/oneView", 152l, "STORYCM_R1"),
    STORYCM_U("스토리", "/active/storyCm/update", 153l, "STORYCM_U"),
    STORYCM_D("스토리", "/active/storyCm/delete", 154l, "STORYCM_D"),

    STORYCH_C("챌린지인증", "/active/storyCh/insert", 160l, "STORYCH_C"),
    STORYCH_R("챌린지인증", "/active/storyCh/listView", 161l, "STORYCH_R"),
    STORYCH_R1("챌린지인증", "/active/storyCh/oneView", 162l, "STORYCH_R1"),
    STORYCH_U("챌린지인증", "/active/storyCh/update", 163l, "STORYCH_U"),
    STORYCH_D("챌린지인증", "/active/storyCh/delete", 164l, "STORYCH_D"),

    //last number seq 160

    ;

    private String val;
    private String url;
    private Long seq;
    private String authName;

    public static final CMenu[] ALL = {
            USER_C, USER_R, USER_U, USER_D,
            MEMBERSHIP_C, MEMBERSHIP_R, MEMBERSHIP_U, MEMBERSHIP_D,
            STORY_C, STORY_R, STORY_U, STORY_D,
            ACTION_C, ACTION_R, ACTION_U, ACTION_D,
            SUBACT_C, SUBACT_R, SUBACT_R1, SUBACT_U, SUBACT_D,
            RECOMMENDED_C, RECOMMENDED_R, RECOMMENDED_R1, RECOMMENDED_U, RECOMMENDED_D,
            CHALLENGE_C, CHALLENGE_R, CHALLENGE_U, CHALLENGE_D,
            COMUNITY_C, COMUNITY_R, COMUNITY_U, COMUNITY_D,
            ADMINLIST_C, ADMINLIST_R, ADMINLIST_U, ADMINLIST_D,
            PERMISSION_C, PERMISSION_R, PERMISSION_U, PERMISSION_D,
            COMPANY_C, COMPANY_R, COMPANY_U, COMPANY_D
    };

    CMenu(String val, String url, Long seq, String authName) {
        this.val = val;
        this.url = url;
        this.seq = seq;
        this.authName = authName;
    }

    public String getVal() {
        return val;
    }

    public String getURL() {
        return url;
    }

    public String getName() {
        return name();
    }

    public Long getSeq() {
        return seq;
    }

    public String getAuthName() {
        return authName;
    }

}
