package com.ui.hooxi.admin.biz.model.active.challengeAction;

public enum ChallengeActionSearchType {

    CHALLENGENAME("챌린지 이름"),
    DATE("일자");

    private String val;

    public static final ChallengeActionSearchType[] LIST = {CHALLENGENAME, DATE};

    ChallengeActionSearchType(String val) {
        this.val = val;
    }

    public String getVal() {
        return this.val;
    }

    public String getName() {
        return name();
    }

}
