package com.ui.hooxi.admin.biz.model.user;

import com.ui.hooxi.admin.db.entity.company.TbCompany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyOneVo {
    //나중에 어차피 내역이 추가되니 req랑 분리
    private Long companyId;
    private String companyName;
    private String contents;
    private String imgUrl;
    private Long regId;
    private LocalDateTime regDttm;
    private Long modId;
    private LocalDateTime modDttm;

    public TbCompany ofSave(Long seq){
        TbCompany tc = new TbCompany();

        if(this.companyId == null){
            tc.setCompanyName(this.companyName);
            tc.setContents(this.contents);
            tc.setImgUrl(this.imgUrl);
            tc.setRegId(seq);
            tc.setRegDttm(LocalDateTime.now());
            tc.setModId(seq);
            tc.setModDttm(LocalDateTime.now());
        }
        else {
            tc.setCompanyId(this.companyId);
            tc.setCompanyName(this.companyName);
            tc.setContents(this.contents);
            tc.setImgUrl(this.imgUrl);
            tc.setRegId(this.regId);
            tc.setRegDttm(this.regDttm);
            tc.setModId(seq);
            tc.setModDttm(LocalDateTime.now());
        }
        return tc;
    }

    public TbCompany ofSave(Long seq, String img){
        TbCompany tc = new TbCompany();

        if(this.companyId == null){
            tc.setCompanyName(this.companyName);
            tc.setContents(this.contents);
            tc.setImgUrl(img);
            tc.setRegId(seq);
            tc.setRegDttm(LocalDateTime.now());
            tc.setModId(seq);
            tc.setModDttm(LocalDateTime.now());
        }
        else {
            tc.setCompanyId(this.companyId);
            tc.setCompanyName(this.companyName);
            tc.setContents(this.contents);
            tc.setImgUrl(img);
            tc.setRegId(this.regId);
            tc.setRegDttm(this.regDttm);
            tc.setModId(seq);
            tc.setModDttm(LocalDateTime.now());
        }
        return tc;
    }

}
