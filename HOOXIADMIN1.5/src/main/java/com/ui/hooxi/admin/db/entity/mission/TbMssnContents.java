package com.ui.hooxi.admin.db.entity.mission;

import com.ui.hooxi.admin.db.entity.mission.id.TbMssnCatePlaceId;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnContentsId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_contents")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnContentsId.class)
public class TbMssnContents {

    @Id
    @Column(name = "mssn_id", nullable = true)
    private Long mssnId;
    @Id
    @Column(name = "lang_cd", nullable = true)
    private String langCd;
    @Column(name = "mssn_title", nullable = true)
    private String mssnTitle;
    @Column(name = "mssn_title_dtl", nullable = true)
    private String mssnTitleDtl;
    @Column(name = "mssn_effect", nullable = true)
    private String mssnEffect;
    @Column(name = "mssn_etc", nullable = true)
    private String mssnEtc;

    @Builder
    public TbMssnContents(Long mssnId, String langCd, String mssnTitle, String mssnTitleDtl, String mssnEffect, String mssnEtc) {
        this.mssnId = mssnId;
        this.langCd = langCd;
        this.mssnTitle = mssnTitle;
        this.mssnTitleDtl = mssnTitleDtl;
        this.mssnEffect = mssnEffect;
        this.mssnEtc = mssnEtc;
    }
}
