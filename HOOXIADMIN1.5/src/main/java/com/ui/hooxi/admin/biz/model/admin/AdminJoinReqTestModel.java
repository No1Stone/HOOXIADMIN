package com.ui.hooxi.admin.biz.model.admin;

import com.ui.hooxi.admin.config.type.YnType;
import com.ui.hooxi.admin.db.entity.admin.Admin;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;

public class AdminJoinReqTestModel {

    private String adminEmail;
    private String adminName;
    private String adminPwd;
    private Long adminPmsId;


    public AdminJoinReqTestModel(String adminEmail, String adminName, String adminPwd, Long adminPmsId) {
        this.adminEmail = adminEmail;
        this.adminName = adminName;
        this.adminPwd = adminPwd;
        this.adminPmsId = adminPmsId;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getAdminPwd() {
        return adminPwd;
    }

    public void setAdminPwd(String adminPwd) {
        this.adminPwd = adminPwd;
    }

    public Long getAdminPmsId() {
        return adminPmsId;
    }

    public void setAdminPmsId(Long adminPmsId) {
        this.adminPmsId = adminPmsId;
    }

    public Admin ofAdmin(PasswordEncoder pe){
        Admin ad = new Admin();
        ad.setAdminEmail(this.adminEmail);
        ad.setAdminName(this.adminName);
        ad.setAdminPwd(pe.encode(this.adminPwd));
        ad.setAdminPmsId(this.adminPmsId);
        ad.setUseYn(YnType.Y.getVal());
        ad.setRegId(1L);
        ad.setRegDttm(LocalDateTime.now());
    return ad;
    }



}
