package com.ui.hooxi.admin.biz.model;

public enum AdminSearch {

    ALL("-검색유형선택-"),
    NAME("이름"),
    EMAIL("이메일"),
    DATE("일자");

    private String des;
    public static final AdminSearch[] LIST = {ALL, NAME, EMAIL, DATE};

    AdminSearch(String des) {
        this.des = des;
    }

    public String getDes() {
        return this.des;
    }

    public String getName() {
        return name();
    }

}
