package com.ui.hooxi.admin.config.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    private final Logger logger = LoggerFactory.getLogger(MvcConfig.class);

    @Value("${domain.file}")
    private String filePath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        Path path = Paths.get(filePath).toAbsolutePath().normalize();

        String patsString = path.toString();

        logger.info("path = {}",path);
        logger.info("pathString = {}",patsString);
        registry
//                .addResourceHandler("/img/menu/**")
                .addResourceHandler("/img/**")
//                .addResourceLocations("file:///C:/WorkSpace/Bucheon/town/src/main/resources/static/img/trash/");
                .addResourceLocations("file:///"+patsString+"/");

    }

}