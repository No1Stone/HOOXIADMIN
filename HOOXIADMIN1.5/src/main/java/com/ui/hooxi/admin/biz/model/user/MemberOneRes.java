package com.ui.hooxi.admin.biz.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MemberOneRes {
    //=========유저
    private Long mmbrId;
    private String mmbrStatusCd;     //상태코드
    private String cntrCd;           //국가코드
    private String email;            //이메일
    private String kidYn;            //어린이여부
    private String mpno;             //핸드폰번호
    private String profileImg;       //이미지
    private String mmbrNm;           //이름
    private String nickname;         //닉네임
    private Long mmbrshp;            //멤버쉽유무
    private LocalDateTime regDt;     //가입일
    private LocalDateTime lastLoginDttm;  //마지막접속일
    private LocalDateTime wtdDttm;        //탈퇴일
    private String oauthTpCd;             //가입경로
    private String mainLang;       //주언어
    private String subLang;        //보조언어
    private String actClausYn;     //액션서약
    //=========멤버쉽
    private Long mmbrshpId;    //멤버쉽 아이디
    private String afltn;     //소속명
    private String stateMsg;       //상태메세지
    private String mmbrshpStateCd;     //멤버쉽 상태코드
    private String gradCd;                //멤버쉽종류 환경리더, 웰컴 등등
    private LocalDateTime frstRgstDttm;   //최초등록일
    private String bgImg;                 //배경이미지
    private String memNickname;            // 멤버십활동명
    private String memProfileImg;           //멤버쉽 이미지
    private String mmbrshpNum;              //멤버쉽 카드 번호
    private String rjctRsn;                 //거절사유
    private LocalDateTime fnlModDttm;       //수정일
    private Long subscripCount; //구독자수
    //=========리스트
    private List<Object> jobs;
    private List<Object> article ;  //멤버쉽 이 등록한 URL 리스트
    private List<Object> snsList;  //멤버쉽이 등록한 sns 링크
    private List<Object> connectInfos; //멤버쉽이 등록한 전화번호들


}
