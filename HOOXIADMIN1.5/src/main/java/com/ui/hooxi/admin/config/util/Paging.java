package com.ui.hooxi.admin.config.util;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Paging {

    private int size;
    private int page;
    private Long totalSize;
    private Long totalPage;
    private Object content;

}
