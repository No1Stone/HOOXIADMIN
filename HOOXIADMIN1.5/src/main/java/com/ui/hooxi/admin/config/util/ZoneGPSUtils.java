package com.ui.hooxi.admin.config.util;

public class ZoneGPSUtils {
	// Big Zone ID 시작
	static final long BIG_ZONE_ID_REF = 50000000000L;
	// Medium Zone ID 시작
	static final long MED_ZONE_ID_REF = 20000000000L;
	// SMALL Zone ID 시작
	static final long SML_ZONE_ID_REF = 10000000000L;
	// x zone multiplier
	static final long X_ZONE_ID_MULTIPLIER = 100000L;

	// Big Zone Meter
	public static final int BIG_ZONE = 500;
	// Medium Zone Meter
	public static final int MED_ZONE = 250;
	// SMALL Zone Meter
	public static final int SML_ZONE = 10;

	// 1m 단위 그리드 크기 15km
	static final int GRID_SIZE = 150000;

	// 그리드 시작점
	static final double GRID_START_X = 126.272548d;
	static final double GRID_START_Y = 38.104881d;

	// 그리드 유닛
	static final double GRID_UNIT_X = 0.0000112197d;
	static final double GRID_UNIT_Y = 0.0000091d;

	// 그리드 종료점
	static final double GRID_END_X = GRID_START_X + GRID_SIZE * GRID_UNIT_X;
	static final double GRID_END_Y = GRID_START_Y + GRID_SIZE * GRID_UNIT_Y;

	public static long getZoneId(double x, double y, int zoneMeter) {
		long yZoneId = (long) ((GRID_START_Y - y) / (GRID_UNIT_Y * zoneMeter) + 1);
		long xZoneId = X_ZONE_ID_MULTIPLIER * ((long) ((x - GRID_START_X) / (GRID_UNIT_X * zoneMeter)) + 1);
		long zoneIdRef = zoneIdRefByZoneMeter(zoneMeter);
		return zoneIdRef + xZoneId + yZoneId;
	}

	private static long zoneIdRefByZoneMeter(int zoneMeter) {
		if (zoneMeter == BIG_ZONE) {
			return BIG_ZONE_ID_REF;
		} else if (zoneMeter == MED_ZONE) {
			return MED_ZONE_ID_REF;
		} else {
			return SML_ZONE_ID_REF;
		}
	}

//	public static Bbox getXyByZoneId(long zoneId) {
//		Bbox bbox = new Bbox();
//		long zoneIdRef = getZoneIdRef(zoneId);
//		int zoneMeter = getZoneMeter(zoneId);
//		long gridOffsetX = (long) ((zoneId - zoneIdRef) / X_ZONE_ID_MULTIPLIER);
//		long gridOffsetY = (long) ((zoneId - zoneIdRef) % X_ZONE_ID_MULTIPLIER);
//		System.out.println(gridOffsetX + " - " + gridOffsetY);
//		double x = GRID_START_X + (GRID_UNIT_X * zoneMeter) * gridOffsetX;
//		double y = GRID_START_Y - (GRID_UNIT_Y * zoneMeter) * gridOffsetY;
//		bbox.setMaxX(x);
//		bbox.setMaxY(y + (GRID_UNIT_Y * zoneMeter));
//		bbox.setMinX(x - (GRID_UNIT_X * zoneMeter));
//		bbox.setMinY(y);
//		return bbox;
//	}

	private static long getZoneIdRef(long zoneId) {
		if (zoneId > BIG_ZONE_ID_REF) {
			return BIG_ZONE_ID_REF;
		} else if (zoneId > MED_ZONE_ID_REF) {
			return MED_ZONE_ID_REF;
		} else {
			return SML_ZONE_ID_REF;
		}
	}

	private static int getZoneMeter(long zoneId) {
		if (zoneId > BIG_ZONE_ID_REF) {
			return BIG_ZONE;
		} else if (zoneId > MED_ZONE_ID_REF) {
			return MED_ZONE;
		} else {
			return SML_ZONE;
		}
	}

	public static int parseZoneIdToX(long zoneId) {
		int zoneIdX = (int) ((zoneId % 10000000000l) / 100000);
		return zoneIdX;
	}

	public static int parseZoneIdToY(long zoneId) {
		int zoneIdY = (int) (zoneId % 100000);
		return zoneIdY;
	}

	public static int calculateZoneOffset(long zoneIdA, long zoneIdB) {
		int zoneIdAX = parseZoneIdToX(zoneIdA);
		int zoneIdAY = parseZoneIdToX(zoneIdA);
		int zoneIdBX = parseZoneIdToX(zoneIdB);
		int zoneIdBY = parseZoneIdToX(zoneIdB);
		int offsetX = Math.abs(zoneIdAX - zoneIdBX);
		int offsetY = Math.abs(zoneIdAY - zoneIdBY);
		int offset = Math.max(offsetX, offsetY);
		return offset;
	}

	public static void main(String[] args) {

	}
}
