package com.ui.hooxi.admin.biz.model.active.challenge;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChallengeListReq {


    private int page;
    private int size;

    private String searchName;
    private String searchNameVal;
    private String startDt;
    private String endDt;




}
