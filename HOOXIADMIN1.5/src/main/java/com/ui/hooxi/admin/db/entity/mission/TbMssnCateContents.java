package com.ui.hooxi.admin.db.entity.mission;

import com.ui.hooxi.admin.db.entity.mission.id.TbMssnCateContentsId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_cate_contents")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnCateContentsId.class)
public class TbMssnCateContents {
    @Id
    @Column(name = "cate_id", nullable = true)
    private Long cateId;
    @Id
    @Column(name = "lang_cd", nullable = true)
    private String langCd;
    @Column(name = "cate_title", nullable = true)
    private String cateTitle;
//    @Column(name = "cate_title_dtl", nullable = true)
//    private String cateTitleDtl;
    @Column(name = "cate_contents", nullable = true)
    private String cateContents;

    @Builder
    public TbMssnCateContents(Long cateId, String langCd, String cateTitle, String cateContents) {
        this.cateId = cateId;
        this.langCd = langCd;
        this.cateTitle = cateTitle;
        this.cateContents = cateContents;
    }
}
