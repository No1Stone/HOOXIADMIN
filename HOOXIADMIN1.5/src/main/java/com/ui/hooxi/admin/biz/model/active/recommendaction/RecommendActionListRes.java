package com.ui.hooxi.admin.biz.model.active.recommendaction;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecommendActionListRes {

    private Long mssnId;
    private String tpcTp;
    private String imgUrl;
    private String title;
    private String content;
    private int exp;
    private int point;
    private Long hit;
    private String useYn;
    private Long regId;
    private LocalDateTime regDttm;
    private Long modId;
    private LocalDateTime modDttm;


}
