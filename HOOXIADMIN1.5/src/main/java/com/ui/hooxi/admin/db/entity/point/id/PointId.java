package com.ui.hooxi.admin.db.entity.point.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class PointId implements Serializable {
    private static final long serialVersionUID = 7065982723393036247L;
    private Long pointId;
    private Long mmbrId;

    public PointId() {

    }
    public PointId(Long pointId, Long mmbrId) {
        this.pointId = pointId;
        this.mmbrId = mmbrId;
    }
}
