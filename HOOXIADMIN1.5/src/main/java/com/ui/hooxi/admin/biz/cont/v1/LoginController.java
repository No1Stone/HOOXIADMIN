package com.ui.hooxi.admin.biz.cont.v1;

import com.google.gson.Gson;
import com.ui.hooxi.admin.biz.model.LoginReq;
import com.ui.hooxi.admin.biz.service.common.AdminInfo;
import com.ui.hooxi.admin.biz.service.common.AdminInfoBean;
import com.ui.hooxi.admin.biz.service.login.LoginService;
import com.ui.hooxi.admin.config.type.PMenu;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping(path = "/login")
@Slf4j
public class LoginController {

    private final LoginService loginService;
    private final HttpServletRequest httpServletRequest;
    private final AdminInfoBean adminInfoBean;

    @ModelAttribute("PMenu")
    public List<PMenu> PMENUType() {
        return Arrays.asList(PMenu.ALL);
    }


    @GetMapping(path = "/info")
    public ModelAndView LoginView() {
        ModelAndView mv = new ModelAndView("layout/manage/login/login")
                .addObject("login", new LoginReq());
        return mv;
    }

    @PostMapping(path = "/check")
    public String LoginPost(LoginReq dto) throws Exception {
        String result = loginService.LoginValidService(dto);
//        log.info("result = {}", result);
//        ModelAndView mv = new ModelAndView(result);

//        String result = "/manage/admin/listView";

        return "redirect:" + result;
//        return mv;
    }

    @GetMapping(path = "/top")
    @ResponseBody
    public ModelAndView TopId() {
        Long id = Long.parseLong(httpServletRequest.getSession().getAttribute("seq").toString());
        if(id ==null){
            return new ModelAndView("redirect:/login/info");
        }
        else {
            AdminInfo ai = adminInfoBean.getSeqToAdmin().get(id);

            log.info("seq - {}", id);
            log.info("login top admin info - {}", new Gson().toJson(ai));
            log.info("login top admin info - {}", new Gson().toJson(ai.getAdminName()));
            return new ModelAndView("layout/fragments/topname")
                    .addObject("adminSet",ai);
        }
    }

    @GetMapping(path = "/logout")
    @ResponseBody
    public String LogoutView() {
        log.info("controller");
        loginService.logOutService();
//        return new ModelAndView("redirect:/login/info");
        return "success";
    }

    @GetMapping(path = "/emailCheck")
    public String EmailCheckController(String email){
        return loginService.AdminEmailCheck(email);
    }


}
