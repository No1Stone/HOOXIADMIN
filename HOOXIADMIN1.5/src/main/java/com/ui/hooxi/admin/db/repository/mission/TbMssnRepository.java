package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssn;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnId;
import com.ui.hooxi.admin.db.repository.mission.dsl.TbMssnRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMssnRepository extends JpaRepository<TbMssn, Long> , TbMssnRepositoryDsl {
}
