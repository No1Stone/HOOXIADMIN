package com.ui.hooxi.admin.db.repository.member;

import com.ui.hooxi.admin.db.entity.member.TbMmbrshpSnsLink;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMmbrshpSnsLinkRepository extends JpaRepository<TbMmbrshpSnsLink, Long> {
}