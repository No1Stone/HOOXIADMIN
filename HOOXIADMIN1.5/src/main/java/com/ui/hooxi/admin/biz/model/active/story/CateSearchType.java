package com.ui.hooxi.admin.biz.model.active.story;

public enum CateSearchType {


    NAME("카테고리 이름"),
    META("부문"),
    DATE("일자");

    private String val;

    public static final CateSearchType[] LIST = {NAME, META, DATE};

    CateSearchType(String val) {
        this.val = val;
    }

    public String getVal() {
        return this.val;
    }

    public String getName() {
        return name();
    }

}
