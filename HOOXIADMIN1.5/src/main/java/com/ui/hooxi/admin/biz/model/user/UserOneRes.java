package com.ui.hooxi.admin.biz.model.user;


import lombok.Builder;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

public class UserOneRes {

    private Long mmbrId;
    private String mmbrStatusCd;     //상태코드
    private String cntrCd;           //국가코드
    private String email;            //이메일
    private String kidYn;            //어린이여부
    private String mpno;             //핸드폰번호
    private String profileImg;       //이미지
    private String mmbrNm;           //이름
    private String nickname;         //닉네임
    private Long mmbrshp;
    private LocalDateTime regDt;     //가입일
    private LocalDateTime lastLoginDttm;  //마지막접속일
    private LocalDateTime wtdDttm;        //탈퇴일
    private String oauthTpCd;             //가입경로
    private String mainLang;       //주언어
    private String subLang;        //보조언어
    private String actClausYn;     //액션서약

    public UserOneRes() {
    }

    @Builder
    public UserOneRes(Long mmbrId, String mmbrStatusCd, String cntrCd, String email, String kidYn, String mpno, String profileImg, String mmbrNm, String nickname, Long mmbrshp, LocalDateTime regDt, LocalDateTime lastLoginDttm, LocalDateTime wtdDttm, String oauthTpCd, String mainLang, String subLang, String actClausYn) {
        this.mmbrId = mmbrId;
        this.mmbrStatusCd = mmbrStatusCd;
        this.cntrCd = cntrCd;
        this.email = email;
        this.kidYn = kidYn;
        this.mpno = mpno;
        this.profileImg = profileImg;
        this.mmbrNm = mmbrNm;
        this.nickname = nickname;
        this.mmbrshp = mmbrshp;
        this.regDt =  regDt;
        this.lastLoginDttm = lastLoginDttm;
        this.wtdDttm = wtdDttm;
        this.oauthTpCd = oauthTpCd;
        this.mainLang = mainLang;
        this.subLang = subLang;
        this.actClausYn = actClausYn;
    }

    public Long getMmbrId() {
        return mmbrId;
    }

    public void setMmbrId(Long mmbrId) {
        this.mmbrId = mmbrId;
    }

    public String getMmbrStatusCd() {
        return mmbrStatusCd;
    }

    public void setMmbrStatusCd(String mmbrStatusCd) {
        this.mmbrStatusCd = mmbrStatusCd;
    }

    public String getCntrCd() {
        return cntrCd;
    }

    public void setCntrCd(String cntrCd) {
        this.cntrCd = cntrCd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKidYn() {
        return kidYn;
    }

    public void setKidYn(String kidYn) {
        this.kidYn = kidYn;
    }

    public String getMpno() {
        return mpno;
    }

    public void setMpno(String mpno) {
        this.mpno = mpno;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getMmbrNm() {
        return mmbrNm;
    }

    public void setMmbrNm(String mmbrNm) {
        this.mmbrNm = mmbrNm;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Long getMmbrshp() {
        return mmbrshp;
    }

    public void setMmbrshp(Long mmbrshp) {
        this.mmbrshp = mmbrshp;
    }

    public LocalDateTime getRegDt() {
        return regDt;
    }

    public void setRegDt(LocalDateTime regDt) {
        this.regDt = regDt;
    }

    public LocalDateTime getLastLoginDttm() {
        return lastLoginDttm;
    }

    public void setLastLoginDttm(LocalDateTime lastLoginDttm) {
        this.lastLoginDttm = lastLoginDttm;
    }

    public LocalDateTime getWtdDttm() {
        return wtdDttm;
    }

    public void setWtdDttm(LocalDateTime wtdDttm) {
        this.wtdDttm = wtdDttm;
    }

    public String getOauthTpCd() {
        return oauthTpCd;
    }

    public void setOauthTpCd(String oauthTpCd) {
        this.oauthTpCd = oauthTpCd;
    }

    public String getMainLang() {
        return mainLang;
    }

    public void setMainLang(String mainLang) {
        this.mainLang = mainLang;
    }

    public String getSubLang() {
        return subLang;
    }

    public void setSubLang(String subLang) {
        this.subLang = subLang;
    }

    public String getActClausYn() {
        return actClausYn;
    }

    public void setActClausYn(String actClausYn) {
        this.actClausYn = actClausYn;
    }
}
