package com.ui.hooxi.admin.db.entity.user;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Table(name = "tb_login_hist")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class TbLoginHist {
    @Id
    @Column(name = "mmbr_id", nullable = false)
    private Long id;

    @Column(name = "login_dttm")
    private Instant loginDttm;

    @Column(name = "login_rslt_cd", length = 10)
    private String loginRsltCd;

    @Column(name = "login_tp", length = 10)
    private String loginTp;

    public String getLoginTp() {
        return loginTp;
    }

    public void setLoginTp(String loginTp) {
        this.loginTp = loginTp;
    }

    public String getLoginRsltCd() {
        return loginRsltCd;
    }

    public void setLoginRsltCd(String loginRsltCd) {
        this.loginRsltCd = loginRsltCd;
    }

    public Instant getLoginDttm() {
        return loginDttm;
    }

    public void setLoginDttm(Instant loginDttm) {
        this.loginDttm = loginDttm;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}