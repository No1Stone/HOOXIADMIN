package com.ui.hooxi.admin.db.entity.point.model;

import com.ui.hooxi.admin.db.entity.point.Point;
import com.ui.hooxi.admin.db.entity.point.type.PointStatCd;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor
public class PointCnclRequest {


//    @ApiModelProperty(name = "mmbrId", example = "1", required = true)
    @NotNull(message = "User Id Is Not Null")
    private Long mmbrId;

//    @ApiModelProperty(name = "cnclPointId", example = "1", required = true, notes = "적립/사용 취소할 point Id")
    private Long cnclPointId;


    @Builder
    PointCnclRequest(Long mmbrId, Long cnclPointId) {
        this.mmbrId = mmbrId;
        this.cnclPointId = cnclPointId;
    }

    public Point toCancelEntity(Point point) {
       return Point.builder()
               .mmbrId(mmbrId)
               .pointStateCd(PointStatCd.CANCEL)
               .trstnId(point.getTrstnId())
               .trstnTpCd(point.getTrstnTpCd())
               .point(point.getPoint())
               .build();
    }

}
