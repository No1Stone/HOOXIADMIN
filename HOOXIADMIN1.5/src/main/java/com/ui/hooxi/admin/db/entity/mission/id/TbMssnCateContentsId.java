package com.ui.hooxi.admin.db.entity.mission.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnCateContentsId implements Serializable {

    private Long cateId;
    private String langCd;

}
