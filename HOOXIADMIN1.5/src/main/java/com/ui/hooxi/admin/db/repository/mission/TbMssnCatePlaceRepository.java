package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnCatePlace;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnCatePlaceId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TbMssnCatePlaceRepository extends JpaRepository<TbMssnCatePlace, TbMssnCatePlaceId> {

    List<TbMssnCatePlace> findByCateId(Long seq);

    boolean existsByCateId(Long cateId);

    @Modifying
    @Transactional
    @Query(
            value = "DELETE From tb_mssn_cate_place where cate_id = :cateId "
            ,nativeQuery = true
    )
    void DeletePlace(@Param("cateId")Long cateId);


}
