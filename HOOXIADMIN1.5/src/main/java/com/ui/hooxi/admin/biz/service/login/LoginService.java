package com.ui.hooxi.admin.biz.service.login;

import com.google.gson.Gson;
import com.ui.hooxi.admin.biz.model.LoginReq;
import com.ui.hooxi.admin.biz.service.common.AdminInfoBean;
import com.ui.hooxi.admin.db.entity.admin.Admin;
import com.ui.hooxi.admin.db.repository.admin.AdminRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class LoginService {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final AuthenticationManager authenticationManager;
    private final AdminRepository adminRepository;
    private final PasswordEncoder passwordEncoder;
    private final HttpServletRequest httpServletRequest;
    private final HttpServletResponse response;
    private final AdminInfoBean adminInfoBean;

    public String LoginValidService(LoginReq dto) throws Exception {
//        log.info("pass Encodig - {}", dto.getPasswd(passwordEncoder));
        Optional<Admin> admin = adminRepository.findByAdminEmail(dto.getUserId());
        log.info("adminLog - {}", new Gson().toJson(admin));
//        String result = "layout/manage/login/login";
        String result = "/login/info";
        if (dto.ofValid(passwordEncoder, admin.orElse(null).getAdminPwd())) {
            log.info("Clear");
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(dto.getUserId(), dto.getPasswd()));
            SecurityContextHolder.getContext().setAuthentication(authentication);

            HttpSession session = httpServletRequest.getSession();
            Admin adminResult = adminRepository.findByAdminEmail(dto.getUserId()).get();
            String sessionAdminName = adminResult.getAdminId().toString();
            session.setAttribute("seq", sessionAdminName);

            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(authentication);
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
            result = "/manage/admin/listView";

        } else {
            log.info("Fail!");
        }
        return result;
    }

    public void PasswordUpdate(String password) {
        log.info(" - - - {}", passwordEncoder.encode(password));
    }

    public void logOutService() {
        HttpSession session = httpServletRequest.getSession();
        session.setAttribute("seq", null);
        session.setAttribute("SPRING_SECURITY_CONTEXT", null);
    }

    public String AdminEmailCheck(String email) {
        if (adminRepository.existsByAdminEmail(email)) {
            return "사용불가";
        } else {
            return "사용가능";
        }
    }

}
