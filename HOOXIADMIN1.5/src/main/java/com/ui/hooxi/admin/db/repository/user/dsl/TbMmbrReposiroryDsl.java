package com.ui.hooxi.admin.db.repository.user.dsl;


import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.user.*;
import com.ui.hooxi.admin.config.util.DslResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface TbMmbrReposiroryDsl {

    @Transactional(readOnly = true)
    QueryResults<UserListRes> userListResListSelect(UserListReq dto);
    @Transactional(readOnly = true)
    QueryResults<MemberListRes> MembershipListResListSelect(UserListReq dto);
    @Transactional(readOnly = true)
    UserOneRes UserOneRes(Long seq);
    @Transactional(readOnly = true)
    MemberOneRes MemberShipOneSelect(Long seq);


}
