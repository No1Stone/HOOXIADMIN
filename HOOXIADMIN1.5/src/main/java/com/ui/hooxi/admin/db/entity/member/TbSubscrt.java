package com.ui.hooxi.admin.db.entity.member;

import com.ui.hooxi.admin.db.entity.member.id.TbMmbrshpId;
import com.ui.hooxi.admin.db.entity.member.id.TbSubscrtId;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_subscrt")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbSubscrtId.class)
public class TbSubscrt {

    @Id
    @Column(name = "mmbr_id", nullable = false)
    private Long mmbtId;
    @Id
    @Column(name = "mmbrshp_id", nullable = false)
    private Long mmbtshpId;

    @Column(name = "subscrt_dttn", nullable = false)
    private Long subscrtDttn;

    @Column(name = "alarm_yn", nullable = false)
    private Long alarmYn;


}
