package com.ui.hooxi.admin.biz.model.active.action;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActionCateSelectReq {

    private Long cateId;
    private List<String> place;
    private List<String> prps;
    private String cateTitle;


}
