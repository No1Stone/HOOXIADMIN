package com.ui.hooxi.admin.db.entity.user.id;

import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TbLikeId implements Serializable {
    private static final long serialVersionUID = -7364310220501364791L;
    @Column(name = "story_id", nullable = false)
    private Long storyId;
    @Column(name = "mmbr_id", nullable = false)
    private Long mmbrId;

    public Long getMmbrId() {
        return mmbrId;
    }

    public void setMmbrId(Long mmbrId) {
        this.mmbrId = mmbrId;
    }

    public Long getStoryId() {
        return storyId;
    }

    public void setStoryId(Long storyId) {
        this.storyId = storyId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(storyId, mmbrId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TbLikeId entity = (TbLikeId) o;
        return Objects.equals(this.storyId, entity.storyId) &&
                Objects.equals(this.mmbrId, entity.mmbrId);
    }
}