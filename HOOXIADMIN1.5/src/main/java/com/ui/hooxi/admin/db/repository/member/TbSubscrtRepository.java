package com.ui.hooxi.admin.db.repository.member;

import com.ui.hooxi.admin.db.entity.member.TbSubscrt;
import com.ui.hooxi.admin.db.entity.member.id.TbSubscrtId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbSubscrtRepository extends JpaRepository<TbSubscrt, TbSubscrtId> {


}
