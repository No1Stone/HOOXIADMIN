package com.ui.hooxi.admin.biz.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Testproject {

    String mssnTitle;
    String mssnCtnt;

}
