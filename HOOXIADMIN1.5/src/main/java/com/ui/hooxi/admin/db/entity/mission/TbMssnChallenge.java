package com.ui.hooxi.admin.db.entity.mission;

import com.ui.hooxi.admin.db.entity.mission.id.TbMssnChallengeId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mssn_challenge")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnChallengeId.class)
public class TbMssnChallenge {

    @Id
    @Column(name = "challenge_id", nullable = true)
    private Long challengeId;
    @Id
    @Column(name = "mssn_id", nullable = true)
    private Long mssnId;
    @Column(name = "range_yn", nullable = true)
    private String rangeYn;
    @Column(name = "particptn_tp_cd", nullable = true)
    private String particptnTpCd;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "start_dt", nullable = true)
    private LocalDateTime startDt;
    @Column(name = "end_dt", nullable = true)
    private LocalDateTime endDt;
    @Column(name = "act_dt", nullable = true)
    private LocalDateTime actDt;
    @Column(name = "act_area", nullable = true)
    private String actArea;
    @Column(name = "method", nullable = true)
    private String method;
    @Column(name = "link", nullable = true)
    private String link;

    @Builder
    public TbMssnChallenge(Long challengeId, Long mssnId, String rangeYn, String particptnTpCd, String useYn, LocalDateTime startDt, LocalDateTime endDt, LocalDateTime actDt, String actArea, String method, String link) {
        this.challengeId = challengeId;
        this.mssnId = mssnId;
        this.rangeYn = rangeYn;
        this.particptnTpCd = particptnTpCd;
        this.useYn = useYn;
        this.startDt = startDt;
        this.endDt = endDt;
        this.actDt = actDt;
        this.actArea = actArea;
        this.method = method;
        this.link = link;
    }


}
