package com.ui.hooxi.admin.biz.service.active.pack;

import com.ui.hooxi.admin.db.repository.exp.TbAdminExpRepository;
import com.ui.hooxi.admin.db.repository.exp.TbMmbrExpRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ExpApplication {

    private final ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(ExpApplication.class);

    // repository
    private final TbAdminExpRepository adminExpRepository;
    private final TbAdminExpRepository expRepository;
    private final TbMmbrExpRepository mmbrExpRepository;
//    private final MmbrHistoryExpRepository mmbrHistoryExpRepository;


    /**
     * 경험치 직접 설정 api
     * @param mmbrId
     * @param expSaveRequest
     */
//    @Transactional
//    public void mmbrExpProcessBase(Long mmbrId, ExpSaveRequest expSaveRequest) {
//
//            MmbrExp result = null;
//            if (TrstnDtlTpCd.EVENT_JOIN.equals(expSaveRequest.getExpAcmltDtlTpCd())) {
//
//                AdminExp exp = adminExpRepository.findAdminExpByLvlCd("1");
//                result = mmbrExpRepository.save(expSaveRequest.toSaveEntityMmbrExp(exp.getLvlCd(), exp.getTitle(), exp.getNcssryExp()));
//
//            } else {
//
//                MmbrExp mmbrExp =  mmbrExpRepository.findById(mmbrId)
//                        .orElseThrow(() -> {
//                            throw new MemberException(ErrorCode.NOT_FOUND_MEMBER);
//                        });
//
//                AdminExp info = adminExpRepository.findTop1ByLvlCdGreaterThanOrderByLvlCdAsc(mmbrExp.getLvlCd());
//                mmbrExp.updateExp(expSaveRequest.getExp(), info);
//                result = mmbrExp;
//
//            }
//
//
//            // 히스토리
//            mmbrHistoryExpRepository.save(result.toEntityMmbrExpHistory());
//            expRepository.save(expSaveRequest.toSaveEntityExp());
//    }
//
//    /**
//     * 보상 경험치: api 내부에서 보상 지급시 사용되는 메서드
//     * @param mmbrId
//     * @param targetId
//     * @param expAcmltTpCd
//     */
//    @Transactional
//    public void mmbrExpProcess(Long mmbrId, Long targetId, TrstnTpCd expAcmltTpCd, TrstnDtlTpCd trstnDtlTpCd) {
//        ExpSaveRequest expSaveRequest = ExpSaveRequest.builder()
//                .mmbrId(mmbrId)
//                .expAcmltId(targetId)
//                .expAcmltTpCd(expAcmltTpCd)
//                .expAcmltDtlTpCd(trstnDtlTpCd)
//                .build();
//
//        // TODO: 리워드 정책 들어가야 함
//        if (TrstnDtlTpCd.EVENT_JOIN.equals(expSaveRequest.getExpAcmltDtlTpCd())) {
//            expSaveRequest.updateExp(0);
//        } else {
//            expSaveRequest.updateExp(5);
//        }
//
//        this.mmbrExpProcessBase(mmbrId, expSaveRequest);
//    }
//
//
//    @Transactional
//    public Exp savaExp(ExpSaveRequest expSaveRequest) {
//        return expRepository.save(expSaveRequest.toSaveEntityExp());
//    }
//
//
//    /**
//     * 회원 경험치
//     * @param mmbrId
//     * @return
//     */
//    @Transactional
//    public MmbrExp findMmbrExpList(Long mmbrId) {
//        return mmbrExpRepository.findMmbrExpByMmbrId(mmbrId);
//    }
}
