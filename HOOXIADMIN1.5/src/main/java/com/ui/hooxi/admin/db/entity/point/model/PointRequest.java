package com.ui.hooxi.admin.db.entity.point.model;

import com.ui.hooxi.admin.db.entity.point.Point;
import com.ui.hooxi.admin.db.entity.point.type.PointStatCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnDtlTpCd;
import com.ui.hooxi.admin.db.entity.point.type.TrstnTpCd;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class PointRequest {


//    @ApiModelProperty(name = "mmbrId", example = "1", required = false)
    private Long mmbrId;

//    @ApiModelProperty(name = "trstnId", example = "1", required = true, notes = "거래 ID")
    private Long trstnId;

//    @ApiModelPrope//rty(name = "trstnTpCd", example = "STORY", required = true, notes = "거래유형코드")
    private TrstnTpCd trstnTpCd;

//    @ApiModelProperty(name = "trstnDtlTpCd", example = "VIEW", required = true, notes = "거래상세유형코드")
    private TrstnDtlTpCd trstnDtlTpCd;

//    @ApiModelProperty(name = "point", example = "100", required = true, notes = "포인트 금액")
    @PositiveOrZero
    @NotNull(message = "Point Empty")
    private Long point;


    @Builder
    PointRequest(Long mmbrId, Long trstnId, TrstnTpCd trstnTpCd, TrstnDtlTpCd trstnDtlTpCd, Long point) {
        this.mmbrId = mmbrId;
        this.trstnId = trstnId;
        this.trstnTpCd = trstnTpCd;
        this.trstnDtlTpCd = trstnDtlTpCd;
        this.point = point;
    }


    public boolean validPoint() {
        return this.point >= 0 && this.point != null;
    }

    public Point toSavaEntity(Long mmbrId, Long regId) {
       return Point.builder()
               .mmbrId(mmbrId)
               .pointStateCd(PointStatCd.ACCUMULATE)
               .trstnId(this.trstnId)
               .trstnTpCd(this.trstnTpCd)
               .trstnDtlTpCd(this.trstnDtlTpCd)
               .point(this.point)
               .trstnDt(LocalDateTime.now())
               .regId(regId)
               .regDt(LocalDateTime.now())
               .modId(regId)
               .modDt(LocalDateTime.now())
               .build();
    }

    public Point toUseEntity(Long mmbrId) {
        return Point.builder()
                .mmbrId(mmbrId)
                .pointStateCd(PointStatCd.USE)
                .trstnId(this.trstnId)
                .trstnTpCd(this.trstnTpCd)
                .trstnDtlTpCd(this.trstnDtlTpCd)
                .point(this.point * -1)
                .build();
    }


    public void updatePoint(Long point) {
        this.point = point;
    }
}
