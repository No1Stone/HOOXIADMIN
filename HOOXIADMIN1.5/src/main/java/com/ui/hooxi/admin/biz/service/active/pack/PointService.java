package com.ui.hooxi.admin.biz.service.active.pack;

import com.ui.hooxi.admin.db.entity.point.Point;
import com.ui.hooxi.admin.db.entity.point.id.PointId;
import com.ui.hooxi.admin.db.entity.point.model.PointDtlAmtDTO;
import com.ui.hooxi.admin.db.entity.point.model.PointRequest;
import com.ui.hooxi.admin.db.entity.point.type.PointStatCd;
import com.ui.hooxi.admin.db.entity.user.TbMmbr;
import com.ui.hooxi.admin.db.repository.user.TbMmbrRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class PointService {

    // function
    private final ModelMapper modelMapper;


    /*
          repository
     */

    private final PointApplication pointApplication;
    private final TbMmbrRepository tbMmbrRepository;


    /**
     * 포인트 조회
     * @return
     */
//    @Transactional
//    public Response<Long> getPoint() {
//        HooxiUser hooxiUser = TokenUtil.getTokenUser();
//        Long mmbrId = Long.valueOf(hooxiUser.getMmbrId());
//        return new Response<>(pointApplication.getRemainingPoint(mmbrId));
//    }

    /**
     * get Point List
     * @param
     * @return
     */
//    @Transactional
//    public Response<PageImpl<PointListResponse>> getPointList(Pageable pageable) {
//        HooxiUser hooxiUser = TokenUtil.getTokenUser();
//        Long mmbrId = Long.valueOf(hooxiUser.getMmbrId());
//        PageImpl<PointListResponse> result = pointApplication.getPointList(mmbrId ,pageable);
//        return new Response<>(result);
//    }


    @Transactional
    public void savePoint(PointRequest pointSaveRequest) {
        log.info("savePoint Service");
        TbMmbr hooxiUser = tbMmbrRepository.getById(pointSaveRequest.getMmbrId());
        Long mmbrId = Long.valueOf(hooxiUser.getMmbrId());

        if ( !pointSaveRequest.validPoint() ) {
            throw new IllegalArgumentException("잘못 된 포인트");
        }
        else {
            pointApplication.saveBasePoint(Long.valueOf(hooxiUser.getMmbrId()), pointSaveRequest);
        }
    }

    @Transactional
    public void cancelSavePoint(Long pointId, Long userSeq) {
        log.info("cancelSavePoint");
        TbMmbr hooxiUser = tbMmbrRepository.getById(userSeq);
        Long mmbrId = Long.valueOf(hooxiUser.getMmbrId());

        // 포인트 to 포인트 상세
        Long pointDtlId = pointApplication.getPointDtlIdByKeyAndLegacyCode(mmbrId, pointId, PointStatCd.ACCUMULATE.getLegacyCode());
        // 포인트 적립 금액 체크
        PointDtlAmtDTO pointAmt = pointApplication.getPointDtlAmt(mmbrId, pointDtlId);

        // point 조회
        PointId pointKey = new PointId(pointId, mmbrId);
        Point cancelPoint = pointApplication.findPointByPrimaryKey(pointKey);
        // 포인트 적립 취소
        pointApplication.cancelSavePoint(cancelPoint, pointAmt);

    }

    @Transactional
    public void usePoint(PointRequest pointRequest, Long userSeq) {
        log.info("usePoint Service");
        TbMmbr hooxiUser = tbMmbrRepository.getById(userSeq);
        Long mmbrId = Long.valueOf(hooxiUser.getMmbrId());
        // point가 제대로 들어 왔는지 확인
        pointApplication.getRemainingPoint(mmbrId, pointRequest.getPoint());

        // point 명세 저장
        Point pointDto = pointApplication.useBasePoint(mmbrId, pointRequest);
        // point 상세 저장
        pointApplication.useBasePointDtl(pointRequest.getPoint(), pointDto);

    }


    @Transactional
    public void cancelUsePoint(Long pointId, Long userSeq) {
        log.info("cancel Use Point : {}", pointId);
        TbMmbr hooxiUser = tbMmbrRepository.getById(userSeq);

        PointId keys = new PointId(pointId, Long.valueOf(hooxiUser.getMmbrId()));
        Point getPoint = pointApplication.findPointByPrimaryKey(keys);
        // 2. point 사용취소 insert -> opg_point_id는 사용 point_id
        Point cancelPoint = pointApplication.cancelUseBasePoint(getPoint);
        // 3. pointDtl 사용 취소
        //  - pointId로 조회 list-> expire조건 반대로 insert
        pointApplication.cancelUseBasePointDtl(pointId, cancelPoint);

    }

}
