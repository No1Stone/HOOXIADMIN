package com.ui.hooxi.admin.biz.model.user;

import com.ui.hooxi.admin.biz.service.common.CodeBean;
import com.ui.hooxi.admin.db.entity.code.TbCmnCode;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;


public class UserListRes {

    private Long mmbrId;
    private String mmbrNm;
    private String nickname;
    private String email;
    private String oauthTpCd;
    private Long point;
    private String cntrCd;
    private LocalDateTime regDt;

    public UserListRes(){}
    public UserListRes(Long mmbrId, String mmbrNm, String nickname, String email, String oauthTpCd, Long point, String cntrCd, LocalDateTime regDt) {
        this.mmbrId = mmbrId;
        this.mmbrNm = mmbrNm;
        this.nickname = nickname;
        this.email = email;
        this.oauthTpCd = oauthTpCd;
        this.point = point;
        this.cntrCd = cntrCd;
        this.regDt = regDt;
    }

    public UserListRes setCode(Map<String, TbCmnCode> codes){
        this.cntrCd = codes.get(this.cntrCd).getCdNm();
        this.oauthTpCd = codes.get(this.oauthTpCd).getCdNm();
        return this;
    }

    public Long getMmbrId() {
        return mmbrId;
    }

    public void setMmbrId(Long mmbrId) {
        this.mmbrId = mmbrId;
    }

    public String getMmbrNm() {
        return mmbrNm;
    }

    public void setMmbrNm(String mmbrNm) {
        this.mmbrNm = mmbrNm;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOauthTpCd() {
        return oauthTpCd;
    }

    public void setOauthTpCd(String oauthTpCd) {
        this.oauthTpCd = oauthTpCd;
    }

    public Long getPoint() {
        return point;
    }

    public void setPoint(Long point) {
        this.point = point;
    }

    public String getCntrCd() {
        return cntrCd;
    }

    public void setCntrCd(String cntrCd) {

        this.cntrCd = cntrCd;
    }

    public LocalDateTime getRegDt() {
        return regDt;
    }

    public String getRegDts(){
        DateTimeFormatter dt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return regDt.format(dt);
    }

    public void setRegDt(LocalDateTime regDt) {

        this.regDt = regDt;
    }
}
