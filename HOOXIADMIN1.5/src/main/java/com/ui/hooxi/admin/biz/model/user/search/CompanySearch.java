package com.ui.hooxi.admin.biz.model.user.search;

public enum CompanySearch {

    NAME("회사명"),
    DATE("신청일");
    private String val;

    CompanySearch(String val) {
        this.val = val;
    }

    public static final CompanySearch[] LIST = {NAME, DATE};

    public String getVal() {
        return val;
    }

    public String getName() {
        return name();
    }


}
