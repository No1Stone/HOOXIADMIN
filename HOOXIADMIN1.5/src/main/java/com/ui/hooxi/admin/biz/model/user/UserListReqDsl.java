package com.ui.hooxi.admin.biz.model.user;


import com.ui.hooxi.admin.biz.model.UserSearchType;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UserListReqDsl {

    private int page;
    private int size;
    private LocalDateTime startDt;
    private LocalDateTime endDt;

    private String name;
    private String nickName;
    private String phone;
    private String email;
    private String country;
    private String userStatus;

    private Long userSeq;

    public UserListReqDsl(){}

    public UserListReqDsl(int page, int size, LocalDateTime startDt, LocalDateTime endDt, String name, String nickName, String phone, String email, String country, String userStatus, Long userSeq) {
        this.page = page;
        this.size = size;
        this.startDt = startDt;
        this.endDt = endDt;
        this.name = name;
        this.nickName = nickName;
        this.phone = phone;
        this.email = email;
        this.country = country;
        this.userStatus = userStatus;
        this.userSeq = userSeq;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public LocalDateTime getStartDt() {
        return startDt;
    }

    public void setStartDt(LocalDateTime startDt) {
        this.startDt = startDt;
    }

    public LocalDateTime getEndDt() {
        return endDt;
    }

    public void setEndDt(LocalDateTime endDt) {
        this.endDt = endDt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public Long getUserSeq() {
        return userSeq;
    }

    public void setUserSeq(Long userSeq) {
        this.userSeq = userSeq;
    }
}
