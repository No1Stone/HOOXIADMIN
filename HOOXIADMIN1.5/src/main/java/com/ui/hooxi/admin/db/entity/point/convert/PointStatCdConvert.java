package com.ui.hooxi.admin.db.entity.point.convert;

import com.ui.hooxi.admin.config.util.AbstractLegacyEnumAttributeConverter;
import com.ui.hooxi.admin.db.entity.point.type.PointStatCd;

import javax.persistence.Converter;

@Converter
public class PointStatCdConvert extends AbstractLegacyEnumAttributeConverter<PointStatCd> {
	public static final String ENUM_NAME = "포인트상태코드";

	public PointStatCdConvert() {
		super(PointStatCd.class,false, ENUM_NAME);
	}
}
