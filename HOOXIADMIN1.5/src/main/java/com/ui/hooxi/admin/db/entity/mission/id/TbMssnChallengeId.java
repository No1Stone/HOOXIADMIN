package com.ui.hooxi.admin.db.entity.mission.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnChallengeId implements Serializable {

    private Long challengeId;
    private Long mssnId;

}
