package com.ui.hooxi.admin.biz.restcont;

import com.google.gson.Gson;
import com.ui.hooxi.admin.biz.model.active.story.StoryListReq;
import com.ui.hooxi.admin.biz.model.active.story.StoryPointVo;
import com.ui.hooxi.admin.biz.model.admin.*;
import com.ui.hooxi.admin.biz.service.active.*;
import com.ui.hooxi.admin.biz.service.admin.AdminService;
import com.ui.hooxi.admin.biz.service.login.LoginService;
import com.ui.hooxi.admin.biz.service.user.UserService;
import com.ui.hooxi.admin.biz.service.util.S3AWSService;
import com.ui.hooxi.admin.biz.service.util.UtilService;
import com.ui.hooxi.admin.db.entity.admin.AdminHis;
import com.ui.hooxi.admin.db.entity.admin.AdminPms;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsCmn;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsPmn;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/test")
@RequiredArgsConstructor
@Slf4j
public class TestRestController {

    private final Logger logger = LoggerFactory.getLogger(TestRestController.class);
    private final TestService testService;
    private final AdminService adminService;
    private final LoginService loginService;
    private final UserService userService;
    private final UtilService utilService;
    private final S3AWSService s3AWSService;

    private final StoryService storyService;
    private final ActionService actionService;
    private final SubActionService subActionService;
    private final RecommendActionService recommendActionService;
    private final ChallengeService challengeService;
    private final ComunityService comunityService;


//    @GetMapping(path = "/test1")
//    public Object test1() {
//        return testService.test1();
//    }

//    @GetMapping(path = "/test2")
//    public List<List<String>> test2(){
//        return Arrays.asList(PMenu.values()).stream().map(e -> e.getCMenu()).collect(Collectors.toList());
//    }


    @PostMapping(path = "/test2")
    public void test2(@RequestBody AdminJoinReqTestModel dto) {

        adminService.test2(dto);
    }

    @GetMapping(path = "/test3")
    public AdminHis test3() {
        return adminService.test3();
    }

    @PostMapping(path = "/test4")
    public AdminPms test4(@RequestBody AdminPmsTestModel dto) {
        logger.info("aa - {}", new Gson().toJson(dto));
        return adminService.test4(dto);
    }

    @PostMapping(path = "/test5")
    public AdminPmsPmn test5(@RequestBody AdminPmsPmnReqTestModel dto) {
        logger.info("aa - {}", new Gson().toJson(dto));
        return adminService.test5(dto);
    }

    @PostMapping(path = "/test6")
    public AdminPmsCmn test6(@RequestBody AdminPmsCmnReqTestModel dto) {
        logger.info("aa - {}", new Gson().toJson(dto));
        return adminService.test6(dto);
    }

    @PostMapping(path = "/test7")
    public Object test7(AdminListReq dto) {
        logger.info("data ---- {}", new Gson().toJson(dto));
        return adminService.test7(dto);
    }

    @GetMapping(path = "/test8/{aa}")
    public void test8(@PathVariable(name = "aa") String aa) {
        logger.info("data ---- {}", new Gson().toJson(aa));
        loginService.PasswordUpdate(aa);
    }

    @GetMapping(path = "/test9")
    public void test9() {
        testService.test9();
    }


//    @GetMapping(path = "/test10/{seq}")
//    public Object test10(@PathVariable(name = "seq") int seq) {
//        return userService.test10(seq);
//    }

    @GetMapping(path = "/test11")
    public void test11() {
        testService.MailSendTest();
    }

    @PostMapping(value = "/test12")
    public ResponseEntity<String> uploadFile(MultipartFile file) throws IllegalStateException, IOException {

        if (!file.isEmpty()) {
            log.debug("file org name = {}", file.getOriginalFilename());
            log.debug("file content type = {}", file.getContentType());
            file.transferTo(new File(file.getOriginalFilename()));
        }

        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @GetMapping(value = "/test13/{pa}")
    public ResponseEntity getFile(@PathVariable(name = "pa") String pa) throws IOException {
        log.info("pa - {}", pa);
        return utilService.imgGetService(pa);
    }

    @GetMapping("/test14/{pa}")
    public void download(@PathVariable(name = "pa") String pa, HttpServletResponse response) throws Exception {
        log.info("pa - {}", pa);
        testService.filetest(response, pa);

    }

    @GetMapping("/test15")
    public Object download15() {
        return testService.test15();

    }

    @GetMapping("/test16/{seq}")
    public Object download16(@PathVariable(name = "seq") Long seq) {
        return testService.test16(seq);

    }

    @GetMapping("/test17")
    public Object download17() {
        return testService.test17(new StoryListReq());

    }

//    @GetMapping("/test18")
//    public Object download18()  {
//        return testService.test18();
//    }
//
//    @GetMapping("/test19")
//    public Object download19()  {
//        return testService.test19();
//
//    }
//    @GetMapping("/test20")
//    public Object download20()  {
//        return testService.test20();
//    }
//
//    @GetMapping("/test21")
//    public Object download21()  {
//        return challengeService.ChallengeListService(new ChallengeListReq());
//    }

    @GetMapping("/test22")
    public Object download22() {
        return testService.test22();
    }

    @GetMapping("/test23")
    public Object download23() {
        return testService.test23();
    }

    @GetMapping("/test24")
    public Object download24() {
        return testService.test24();
    }

    @GetMapping("/test25")
    public String test25() {
        return s3AWSService.CreatePresignUrl("", "");
    }

    @GetMapping("/test26")
    public void test26() {
        testService.test26();

    }

    @GetMapping("/test27")
    public void test27() {
        testService.test27();

    }

    @GetMapping("test28/{seq}")
    public void test28(@PathVariable(name = "seq") Long seq) {
        testService.test28(seq);
    }

    @GetMapping("test29/{seq}")
    public void test29(@PathVariable(name = "seq") String seq) {
        testService.test29(seq);
    }


    @PostMapping("test30")
    public void test30(@RequestBody StoryPointVo dto) {
        testService.test30(dto);
    }

    @GetMapping("/test31")
    public void test31() {
        storyService.expSumTest();
    }


    @GetMapping("/test32")
    public void test32() {
        storyService.adminExpFindTest();
    }

    @GetMapping("/test33")
    public void test33() {
        storyService.pointgetTest();
    }


    @GetMapping(path = "/test34/{seq}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public void test35(@PathVariable(name = "seq") Long seq) throws IOException {
        String te = "test34";
        testService.test34(seq, te);
    }

    @GetMapping(path = "/test35/{seq}/{te}")
    public void test36(@PathVariable(name = "seq") Long seq, @PathVariable(name = "te") String te) throws IOException {
        log.info("data - {} , {}", seq, te);
        testService.test35(seq, te);
    }


    @GetMapping(path = "/test37/{seq}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter test37(@PathVariable(name = "seq") Long seq) {
        log.info("data - {} , {}", seq);
        return testService.SseEmitter(seq);
    }

    @GetMapping(path = "/test38/{seq}")
    public void test38(@PathVariable(name = "seq") Long seq) {
        log.info("data - {} , {}", seq);
        testService.sendToSseEmitter(seq, "");
    }

    @GetMapping(path = "/test39")
    public String test39() {
        String path = "https://hooxi-s3.s3.ap-northeast-2.amazonaws.com/view/storyBodyContents.html";
        String con = "<h1><strong><em>전기를 <span class=\"ql-cursor\">\uFEFF\uFEFF</span>올바르게</em></strong></h1><h1> 사용하면 자원을 아끼는 효과가 있어요fdfdf</h1>";
        String bgc = "b"; // b = black w = white
        String brw = "cr"; //brw cr = chrome sa=safari fi = firefox sam = samsung wha = whail
        String devi = "and"; //값 리스트 필요
        return path +
                "?" +
                "bgc=" + bgc +
                "&" +
                "brw=" + brw +
                "&" +
                "devi=" + devi +
                "&"+
                "con=" + URLEncoder.encode(con)
                ;
    }


}
