package com.ui.hooxi.admin.db.entity.mission;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mssn_cate")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class TbMssnCate {

    @Id
    @GeneratedValue(generator = "tb_mssn_id_seq")
    @Column(name = "cate_id", nullable = true)
    private Long cateId;
    @Column(name = "tpc_tp_cd", nullable = true)
    private String tpcTpCd;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "img_url", nullable = true)
    private String imgUrl;
    @Column(name = "reg_id", nullable = true)
    private Long regId;
    @Column(name = "reg_dttm", nullable = true)
    private LocalDateTime regDttm;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dttm", nullable = true)
    private LocalDateTime modDttm;
    @Column(name = "company_id", nullable = true)
    private Long companyId;

    @Builder
    public TbMssnCate(Long cateId, String tpcTpCd, String useYn, String imgUrl, Long regId, LocalDateTime regDttm, Long modId, LocalDateTime modDttm, Long companyId) {
        this.cateId = cateId;
        this.tpcTpCd = tpcTpCd;
        this.useYn = useYn;
        this.imgUrl = imgUrl;
        this.regId = regId;
        this.regDttm = regDttm;
        this.modId = modId;
        this.modDttm = modDttm;
        this.companyId = companyId;
    }


}
