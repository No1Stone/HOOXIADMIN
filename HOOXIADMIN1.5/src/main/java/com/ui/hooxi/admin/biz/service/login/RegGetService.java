package com.ui.hooxi.admin.biz.service.login;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
@Slf4j
public class RegGetService {

    private final HttpServletRequest httpServletRequest;

    public Long getRegId(){
        Long id = Long.parseLong(httpServletRequest.getSession().getAttribute("seq").toString());
        return id;
    }

}
