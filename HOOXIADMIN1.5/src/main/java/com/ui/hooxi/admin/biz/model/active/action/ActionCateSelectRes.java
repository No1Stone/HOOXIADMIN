package com.ui.hooxi.admin.biz.model.active.action;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActionCateSelectRes {

    private Long cateId;
    private String cateTitle;
    private String cateContents;

}
