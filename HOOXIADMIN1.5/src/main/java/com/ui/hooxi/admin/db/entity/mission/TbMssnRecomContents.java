package com.ui.hooxi.admin.db.entity.mission;

import com.ui.hooxi.admin.db.entity.mission.id.TbMssnRecomContentsId;
import lombok.*;

import javax.persistence.*;

@Table(name = "tb_mssn_recom_contents")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnRecomContentsId.class)
public class TbMssnRecomContents {

    @Id
    @Column(name = "recom_id", nullable = true)
    private Long recomId;
    @Id
    @Column(name = "lang_cd", nullable = true)
    private String langCd;
    @Column(name = "recom_title", nullable = true)
    private String recomTitle;
    @Column(name = "recom_contents", nullable = true)
    private String recomContents;

    @Builder
    public TbMssnRecomContents(Long recomId, String langCd, String recomTitle, String recomContents) {
        this.recomId = recomId;
        this.langCd = langCd;
        this.recomTitle = recomTitle;
        this.recomContents = recomContents;
    }
}
