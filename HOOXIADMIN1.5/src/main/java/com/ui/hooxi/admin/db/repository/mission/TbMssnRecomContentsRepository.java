package com.ui.hooxi.admin.db.repository.mission;

import com.ui.hooxi.admin.db.entity.mission.TbMssnRecomContents;
import com.ui.hooxi.admin.db.entity.mission.id.TbMssnRecomContentsId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TbMssnRecomContentsRepository extends JpaRepository<TbMssnRecomContents, TbMssnRecomContentsId> {
}
