package com.ui.hooxi.admin.biz.model.admin.per;


public class AdminPerListReq {
    private int page;
    private int size;
    private String adminEmail;
    private String adminName;
    private String startDt;
    private String endDt;
    private Long adminPmsId;

    public AdminPerListReq() {
    }
    public AdminPerListReq(int page, int size, String adminEmail, String adminName, String startDt, String endDt, Long adminPmsId) {
        this.page = page;
        this.size = size;
        this.adminEmail = adminEmail;
        this.adminName = adminName;
        this.startDt = startDt;
        this.endDt = endDt;
        this.adminPmsId = adminPmsId;
    }

    public Long getAdminPmsId() {
        return this.adminPmsId == null || this.adminPmsId == 0L ? null : this.adminPmsId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getStartDt() {
        return startDt;
    }

    public void setStartDt(String startDt) {
        this.startDt = startDt;
    }

    public String getEndDt() {
        return endDt;
    }

    public void setEndDt(String endDt) {
        this.endDt = endDt;
    }

    public void setAdminPmsId(Long adminPmsId) {
        this.adminPmsId = adminPmsId;
    }

}
