package com.ui.hooxi.admin.biz.service.active;

import com.google.gson.Gson;
import com.querydsl.core.QueryResults;
import com.ui.hooxi.admin.biz.model.active.action.*;
import com.ui.hooxi.admin.biz.model.active.cate.*;
import com.ui.hooxi.admin.biz.model.active.challenge.*;
import com.ui.hooxi.admin.biz.model.active.challengeAction.ChallengeActionViewVo;
import com.ui.hooxi.admin.biz.model.active.subaction.SubActionReq;
import com.ui.hooxi.admin.biz.model.active.subaction.SubActionRes;
import com.ui.hooxi.admin.biz.service.login.RegGetService;
import com.ui.hooxi.admin.biz.service.util.S3AWSService;
import com.ui.hooxi.admin.config.type.CountryCode;
import com.ui.hooxi.admin.db.entity.mission.*;
import com.ui.hooxi.admin.db.repository.company.TbCompanyRepository;
import com.ui.hooxi.admin.db.repository.mission.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ActionService {

    private final TbMssnRepository tbMssnRepository;
    private final TbMssnPlaceRepository tbMssnPlaceRepository;
    private final TbMssnMetaRepository tbMssnMetaRepository;
    private final ModelMapper modelMapper;
    private final TbMssnContentsRepository tbMssnContentsRepository;
    private final TbMssnCateContentsRepositoryr tbMssnCateContentsRepository;
    private final TbMssnCateRepository tbMssnCateRepository;
    private final TbMssnCatePlaceRepository tbMssnCatePlaceRepository;
    private final TbMssnCateMetaRepository tbMssnCateMetaRepository;
    private final S3AWSService s3AWSService;
    private final RegGetService regGetService;
    private final TbCompanyRepository tbCompanyRepository;

    public QueryResults<CateListRes> CateListService(CateListReq dto) {

        QueryResults<CateListRes> results = tbMssnRepository.CateListRes(dto);

        return results;
    }


    public UpdateCreateReqs CateOneDetailService(Long seq) {
        UpdateCreateReqs result = new UpdateCreateReqs();

        if (seq == null) {
            return result;
        } else {
            TbMssnCate select = tbMssnCateRepository.findById(seq).get();
            result.setCateId(select.getCateId());
            result.setTpcTpCd(select.getTpcTpCd());
            result.setUseYn(select.getUseYn());
            result.setImgUrl(select.getImgUrl());
            result.setRegId(select.getRegId());
            result.setRegDttm(select.getRegDttm());
            result.setModId(select.getModId());
            result.setModDttm(select.getModDttm());
            result.setPlace(tbMssnCatePlaceRepository.findByCateId(seq).stream().map(e -> e.getPlaceCd()).collect(Collectors.toList()));
            result.setPrps(tbMssnCateMetaRepository.findByCateId(seq).stream().map(e -> e.getPrpsCd()).collect(Collectors.toList()));

            return result;
        }
    }


    public void CateModalSaveService(MultipartFile[] file, UpdateCreateReqs dto) {
        try {
            s3AWSService.ObjectSaveService(file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Transactional(readOnly = true)
    public CateContentsReq CateLangContentsSelectService(Long seq, String language) {
        CateContentsReq result = new CateContentsReq();
        if (tbMssnCateContentsRepository.existsByCateIdAndLangCd(seq, language)) {
            TbMssnCateContents select = tbMssnCateContentsRepository.findByCateIdAndLangCd(seq, language).get();
            result.setCateId(seq);
            result.setLangCodes(language);
            result.setCateTitle(select.getCateTitle());
            result.setCateContents(select.getCateContents());
            return result;
        } else {
            result.setCateId(seq);
            result.setLangCodes(language);
            return result;
        }
    }

    @Transactional(readOnly = true)
    public ContentsReq LangContentsSelectService(Long seq, String language) {
        ContentsReq result = new ContentsReq();
        if (tbMssnContentsRepository.existsByMssnIdAndLangCd(seq, language)) {
            TbMssnContents select = tbMssnContentsRepository.findByMssnIdAndLangCd(seq, language).get();
            result.setMssnId(seq);
            result.setLangCd(language);
            result.setMssnTitle(select.getMssnTitle());
            result.setMssnTitleDtl(select.getMssnTitleDtl());
            result.setMssnEffect(select.getMssnEffect());
            result.setMssnEtc(select.getMssnEtc());
            return result;
        } else {
            result.setMssnId(seq);
            result.setLangCd(language);
            return result;
        }
    }


    public void CateContentSaveService(CateContentsReq dto) {
        tbMssnCateContentsRepository.save(dto.ofSave());
    }

    public QueryResults<ActionListRes> ActionListService(ActionListReq dto) {
        QueryResults<ActionListRes> result = tbMssnRepository.ActionListRes(dto,"TPC002");
        log.info("mssn List result - {}", new Gson().toJson(result));
        return result;
    }

    public List<ActionCateSelectRes> ActionCateTitleSelect(ActionCateSelectReq dto) {
        List<ActionCateSelectRes> results = tbMssnRepository.ActionCateTitleSelectList(dto,"TPC002");
        log.info("cateListTitleSelect -{}", new Gson().toJson(results));
        return results;
    }

    public List<ActionCateSelectRes> ChalActionCateTitleSelect(ActionCateSelectReq dto) {
        List<ActionCateSelectRes> results = tbMssnRepository.ActionCateTitleSelectList(dto,"TPC003");
        log.info("cateListTitleSelect -{}", new Gson().toJson(results));
        return results;
    }


    @Transactional(readOnly = true)
    public ActionViewVo ActionOneViewService(Long seq) {
        ActionViewVo result = tbMssnRepository.ActionOneViewSelect(seq).get();
//        result.setPlace(tbMssnPlaceRepository.findByMssnId(seq).stream().map(e -> e.getPlaceCd()).collect(Collectors.toList()));
//        result.setPrps(tbMssnMetaRepository.findByMssnId(seq).stream().map(e -> e.getPrpsCd()).collect(Collectors.toList()));
        log.info("ActionOneView - {}", new Gson().toJson(result));
        return result;
    }

    public void ContentSaveService(ContentsReq dto) {
        tbMssnContentsRepository.save(dto.ofSave());
    }

    public TbMssn ActionSaveService(MultipartFile[] file, ActionViewVo dto) {
        TbMssn result = null;
        if (file[0].getOriginalFilename() != null && !file[0].getOriginalFilename().equals("")) {
            try {
                List<String> imgString = s3AWSService.ObjectSaveService(file);
                log.info("file name -= {}", imgString.get(0));
                result = tbMssnRepository.save(dto.ofMssnSave(regGetService.getRegId(), imgString.get(0)));
                log.info("save Result - {}",result);
//                TbMssnMetasSave(dto, result.getMssnId());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return result;
        } else {
            result = tbMssnRepository.save(dto.ofMssnSave(regGetService.getRegId()));
//            TbMssnMetasSave(dto, result.getMssnId());
            log.info("save Result - {}",result);
            return result;
        }
    }

    private void TbMssnMetasSave(ActionViewVo dto, Long seq) {

        log.info("==save matas - {}", new Gson().toJson(dto.getPlace()));
        log.info("==save matas - {}", new Gson().toJson(dto.getPrps()));

        dto.ofPlace(seq).stream().forEach(e -> tbMssnPlaceRepository.save(e));
        dto.ofMeta(seq).stream().forEach(e -> tbMssnMetaRepository.save(e));
    }

    public TbMssnCate ActionCateSaveService(MultipartFile[] file, UpdateCreateReqs dto) {
        TbMssnCate result = null;
        if (file != null && file[0].getOriginalFilename() != null && !file[0].getOriginalFilename().equals("")) {
            try {
                List<String> imgString = s3AWSService.ObjectSaveService(file);
                log.info("file name -= {}", imgString.get(0));
                result = tbMssnCateRepository.save(dto.ofSave(regGetService.getRegId(), imgString.get(0),"TPC002"));
                if(tbMssnCatePlaceRepository.existsByCateId(result.getCateId())){
                    tbMssnCatePlaceRepository.DeletePlace(result.getCateId());
                    dto.ofPlace(result.getCateId()).stream().forEach(e -> tbMssnCatePlaceRepository.save(e));
                }
                else {
                    dto.ofPlace(result.getCateId()).stream().forEach(e -> tbMssnCatePlaceRepository.save(e));
                }

                if(tbMssnCateMetaRepository.existsByCateId(result.getCateId())){
                    tbMssnCateMetaRepository.DeletePlace(result.getCateId());
                    dto.ofPrps(result.getCateId()).stream().forEach(e -> tbMssnCateMetaRepository.save(e));
                }
                else {
                    dto.ofPrps(result.getCateId()).stream().forEach(e -> tbMssnCateMetaRepository.save(e));
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
//            TbMssnCateMetasSave(dto, result.getCateId());
            return result;
        } else {
            result = tbMssnCateRepository.save(dto.ofSave(regGetService.getRegId(), "TPC002"));

            if(tbMssnCatePlaceRepository.existsByCateId(result.getCateId())){
                tbMssnCatePlaceRepository.DeletePlace(result.getCateId());
                dto.ofPlace(result.getCateId()).stream().forEach(e -> tbMssnCatePlaceRepository.save(e));
            }
            else {
                dto.ofPlace(result.getCateId()).stream().forEach(e -> tbMssnCatePlaceRepository.save(e));
            }

            if(tbMssnCateMetaRepository.existsByCateId(result.getCateId())){
                tbMssnCateMetaRepository.DeletePlace(result.getCateId());
                dto.ofPrps(result.getCateId()).stream().forEach(e -> tbMssnCateMetaRepository.save(e));
            }
            else {
                dto.ofPrps(result.getCateId()).stream().forEach(e -> tbMssnCateMetaRepository.save(e));
            }

//            TbMssnCateMetasSave(dto, result.getCateId());

            return result;
        }
    }
//
//    private void TbMssnCateMetasSave(UpdateCreateReqs dto, Long seq) {
//        dto.ofPlace(seq).stream().forEach(e -> tbMssnCatePlaceRepository.save(e));
//        dto.prps(seq).stream().forEach(e -> tbMssnCateMetaRepository.save(e));
//    }


    public QueryResults<ChallengeListRes> ChallengeListSelectService(ChallengeListReq dto) {
        QueryResults<ChallengeListRes> result = null;

        result = tbMssnRepository.ChallengeListSelect(dto);

        return result;
    }

    public List<ChallengeComSelectVo> ChalComSelectService(ChallengeComSelectVo dto) {
        log.info("chalcomSele - {} ", new Gson().toJson(dto));
        List<ChallengeComSelectVo> result = null;
        result = tbCompanyRepository.ChalComSelect(dto);
        return result;
    }

    public ChallengeOneVo ChalSelectOneView(Long seq) {

        ChallengeOneVo result = tbMssnRepository.ChalOneViewSelect(seq).get();

        return result;
    }

    public List<ActionSelectListVo> ChalActionSelectListService(ActionSelectListVo dto) {

        List<ActionSelectListVo> result = null;

        result = tbMssnRepository.ChalActionSelectList(dto);

        log.info("SelectList - {}", new Gson().toJson(result));

        return result;
    }

    public TbMssnCate ChalCreateService(MultipartFile[] file, ChallengeOneVo dto) {
        TbMssnCate result = null;

        if (file !=null && file[0].getOriginalFilename() != null && !file[0].getOriginalFilename().equals("")) {
            try {
                List<String> imgString = s3AWSService.ObjectSaveService(file);
                log.info("file name -= {}", imgString.get(0));
                result = tbMssnCateRepository.save(dto.ofSave(regGetService.getRegId(), imgString.get(0)));




                log.info("challenge save - {}", new Gson().toJson(result));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return result;
        } else {
            result = tbMssnCateRepository.save(dto.ofSave(regGetService.getRegId()));
            if(tbMssnCatePlaceRepository.existsByCateId(result.getCateId())){
                tbMssnCatePlaceRepository.DeletePlace(result.getCateId());
                dto.ofPlace(result.getCateId()).stream().forEach(e -> tbMssnCatePlaceRepository.save(e));
            }
            else {
                dto.ofPlace(result.getCateId()).stream().forEach(e -> tbMssnCatePlaceRepository.save(e));
            }

            if(tbMssnCateMetaRepository.existsByCateId(result.getCateId())){
                tbMssnCateMetaRepository.DeletePlace(result.getCateId());
                dto.ofPrps(result.getCateId()).stream().forEach(e -> tbMssnCateMetaRepository.save(e));
            }
            else {
                dto.ofPrps(result.getCateId()).stream().forEach(e -> tbMssnCateMetaRepository.save(e));
            }
            return result;
        }

    }

    public QueryResults<ActionListRes> ChallengeActionListService(ActionListReq dto) {
        QueryResults<ActionListRes> result = tbMssnRepository.ActionListRes(dto,"TPC003");
        log.info("chal mssn List result - {}", new Gson().toJson(result));
        return result;
    }

    public ChallengeActionViewVo ChalActionOneViewService(Long seq){
        ChallengeActionViewVo result = null;

       result = tbMssnRepository.ChallengeActionOneViewDsl(seq);

        return result;
    }

    public TbMssn ChallengeActionSaveService(MultipartFile[] file, ChallengeActionViewVo dto) {
        TbMssn result = null;

        if (file[0].getOriginalFilename() != null && !file[0].getOriginalFilename().equals("")) {
            try {
                List<String> imgString = s3AWSService.ObjectSaveService(file);
                log.info("file name -= {}", imgString.get(0));
                result = tbMssnRepository.save(dto.ofMssnSave(regGetService.getRegId(), imgString.get(0)));
                log.info("challenge save - {}", new Gson().toJson(result));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return result;
        } else {
            result = tbMssnRepository.save(dto.ofMssnSave(regGetService.getRegId()));
            return result;
        }
    }
}
