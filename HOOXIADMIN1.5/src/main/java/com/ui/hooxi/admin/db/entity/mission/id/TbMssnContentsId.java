package com.ui.hooxi.admin.db.entity.mission.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class TbMssnContentsId implements Serializable {

    private Long mssnId;
    private String langCd;

}
