package com.ui.hooxi.admin.biz.model.active.story;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoryListReq {


    private int page;
    private int size;

    private String searchName;
    private String searchNameVal;


    private String storyStatusSearch;
    private String feedTypeSearch;
    private String privateTypeSearch;
    private String storyTpCd;

    private String startDt;
    private String endDt;
    private Long userSeq;
}
