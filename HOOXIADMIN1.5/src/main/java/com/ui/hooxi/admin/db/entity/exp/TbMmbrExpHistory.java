package com.ui.hooxi.admin.db.entity.exp;

import com.ui.hooxi.admin.db.entity.exp.id.TbMmbrExpHistoryId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "tb_mmbr_exp_history")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMmbrExpHistoryId.class)
public class TbMmbrExpHistory {

    @Id
    @Column(name = "history_id", nullable = true)
    private Long historyId;
    @Id
    @Column(name = "mmbr_id", nullable = true)
    private Long mmbrId;
    @Column(name = "lvl_cd", nullable = true)
    private String lvlCd;
    @Column(name = "title", nullable = true)
    private String title;
    @Column(name = "exp", nullable = true)
    private int exp;
    @Column(name = "next_ncssry_exp", nullable = true)
    private int nextNcssryExp;
    @Column(name = "exp_acmlt_dt", nullable = true)
    private LocalDateTime expAcmltDt;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_id", nullable = true)
    private Long modId;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;
    @Column(name = "reg_id", nullable = true)
    private Long regId;

    @Builder
    public TbMmbrExpHistory(Long historyId, Long mmbrId, String lvlCd, String title, int exp, int nextNcssryExp, LocalDateTime expAcmltDt, LocalDateTime regDt, Long modId, LocalDateTime modDt, Long regId) {
        this.historyId = historyId;
        this.mmbrId = mmbrId;
        this.lvlCd = lvlCd;
        this.title = title;
        this.exp = exp;
        this.nextNcssryExp = nextNcssryExp;
        this.expAcmltDt = expAcmltDt;
        this.regDt = regDt;
        this.modId = modId;
        this.modDt = modDt;
        this.regId = regId;
    }
}
