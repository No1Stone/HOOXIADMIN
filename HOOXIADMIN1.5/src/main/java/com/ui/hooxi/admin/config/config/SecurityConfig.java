package com.ui.hooxi.admin.config.config;


import com.ui.hooxi.admin.biz.service.login.CustomUserDetailsService;
import com.ui.hooxi.admin.config.config.security.LoginFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig
        extends WebSecurityConfigurerAdapter {

    private final CustomUserDetailsService customUserDetailsService;
    private final LoginFilter loginFilter;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
//                .antMatchers("/css/**", "/js/**","/login/**");
                .antMatchers("/**");

    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // CSRF 설정 Disable
        http.csrf().disable()
                .exceptionHandling()

//                .authenticationEntryPoint(authenticationEntryPoint)
//                .accessDeniedHandler(accessDeniedHandler)
                // exception handling 할 때 우리가 만든 클래스를 추가

                // h2-console 을 위한 설정을 추가
                .and()
                .headers()
//                .frameOptions()
//                .sameOrigin()

                // 시큐리티는 기본적으로 세션을 사용
                // 여기서는 세션을 사용하지 않기 때문에 세션 설정을 Stateless 로 설정
                .and()
                .sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .formLogin()
                .loginPage("/login/info")
                .loginProcessingUrl("/login/check")
                .defaultSuccessUrl("/manage/admin/listView")
//                .successHandler(authenticationEntryPoint)
                .failureUrl("/login/info")
                // 로그인, 회원가입 API 는 토큰이 없는 상태에서 요청이 들어오기 때문에 permitAll 설정
                .and()
                .authorizeRequests()

                .antMatchers("/static/img/favicon.ico").permitAll()

//                .antMatchers("/**").permitAll()
                .antMatchers("/admin/manage/admin/listView").hasRole("ADMINLIST_R")
                .and()
                .logout()
                .logoutSuccessUrl("/login/info")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID","jsessionid")
                .permitAll()
                .and()
                .addFilterBefore(loginFilter, UsernamePasswordAuthenticationFilter.class)

        // JwtFilter 를 addFilterBefore 로 등록했던 JwtSecurityConfig 클래스를 적용
//                .and()



        ;
    }


}
