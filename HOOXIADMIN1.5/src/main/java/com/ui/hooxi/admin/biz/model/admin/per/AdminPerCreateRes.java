package com.ui.hooxi.admin.biz.model.admin.per;

import com.ui.hooxi.admin.config.type.CMenu;
import com.ui.hooxi.admin.config.type.PMenu;
import com.ui.hooxi.admin.db.entity.admin.AdminPms;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsCmn;
import com.ui.hooxi.admin.db.entity.admin.AdminPmsPmn;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.tomcat.jni.Local;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminPerCreateRes {

    private String adminPmsNm;
    private List<String> pmenuVal;


    public AdminPms ofPermission(Long regId) {
        AdminPms ap = new AdminPms();
        ap.setAdminPmsNm(this.adminPmsNm);
        ap.setRegId(regId);
        ap.setRegDttm(LocalDateTime.now());
        ap.setModId(regId);
        ap.setModDttm(LocalDateTime.now());

        return ap;
    }


    public List<AdminPmsPmn> ofPmn(Long regId, Long pmsId) {
        List<AdminPmsPmn> apps = new ArrayList<>();
        for (String e : this.pmenuVal) {
            apps.add(AdminPmsPmn.builder()
                    .adminPmsId(pmsId)
                    .adminPmsPmnId(PMenu.valueOf(e).getSeq())
                    .adminPmsNm(this.adminPmsNm)
                    .regId(regId)
                    .regDttm(LocalDateTime.now())
                    .modId(regId)
                    .modDttm(LocalDateTime.now())
                    .build());
        }
        return apps;
    }

    public List<AdminPmsCmn> ofCmn(Long regId, Long pmsId) {
        List<AdminPmsCmn> apcs = new ArrayList<>();
        for (String e : this.pmenuVal) {
            System.out.println("vallllllllllllllll"+e);
            System.out.println(regId);
            System.out.println(pmsId);
            PMenu.valueOf(e).getCMenu().stream().forEach(f -> apcs.add(ofCmns(regId, pmsId, PMenu.valueOf(e).getSeq(), f.getName())));

//            for(CMenu f: PMenu.valueOf(e).getCMenu()){
//                apcs.add(ofCmns(regId, pmsId, PMenu.valueOf(e).getSeq(), f.getName()));
//            }

        }
        return apcs;
    }

    private AdminPmsCmn ofCmns(Long regId, Long pmsId, Long pMenu, String cName) {
//        AdminPmsCmn apc = new AdminPmsCmn();
        return   AdminPmsCmn.builder()
                .adminPmsCmnId(CMenu.valueOf(cName).getSeq())
                .adminPmsPmnId(pMenu)
                .adminPmsId(pmsId)
                .adminCmnNm(CMenu.valueOf(cName).getVal())
                .adminCmnC("Y")
                .adminCmnR("Y")
                .adminCmnU("Y")
                .adminCmnD("Y")
                .regId(regId)
                .regDttm(LocalDateTime.now())
                .modId(regId)
                .modDttm(LocalDateTime.now())
                .build();
    }

    }
