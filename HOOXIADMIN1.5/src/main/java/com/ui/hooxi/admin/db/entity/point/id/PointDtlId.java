package com.ui.hooxi.admin.db.entity.point.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class PointDtlId implements Serializable {
    private static final long serialVersionUID = -5258114456576898577L;
    private Long pointDtlId;
    private Long pointId;
    private Long mmbrId;

    public PointDtlId() {

    }
    public PointDtlId(Long pointDtlId, Long pointId, Long mmbrId) {
        this.pointDtlId = pointDtlId;
        this.pointId = pointId;
        this.mmbrId = mmbrId;
    }

}
