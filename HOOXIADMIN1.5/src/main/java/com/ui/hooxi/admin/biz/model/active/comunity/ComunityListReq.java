package com.ui.hooxi.admin.biz.model.active.comunity;


public class ComunityListReq {


    private int page;
    private int size;

    private String searchName;
    private String searchNameVal;
    private String searchStatus;
    private String searchStatusVal;
    private String language;
    private String startDt;
    private Long userSeq;

    public ComunityListReq(){}

    public ComunityListReq(int page, int size, String searchName, String searchNameVal, String searchStatus, String searchStatusVal, String language, String startDt, Long userSeq) {
        this.page = page;
        this.size = size;
        this.searchName = searchName;
        this.searchNameVal = searchNameVal;
        this.searchStatus = searchStatus;
        this.searchStatusVal = searchStatusVal;
        this.language = language;
        this.startDt = startDt;
        this.userSeq = userSeq;
    }


}
