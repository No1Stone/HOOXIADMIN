package com.ui.hooxi.admin.biz.model.active.challengeAction;

import com.ui.hooxi.admin.config.type.YnType;
import com.ui.hooxi.admin.db.entity.mission.TbMssn;
import com.ui.hooxi.admin.db.entity.mission.TbMssnMeta;
import com.ui.hooxi.admin.db.entity.mission.TbMssnPlace;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChallengeActionViewVo {
    //미션
    private Long mssnId;
    private String tpcTpCd;
    private String imgUrl;
    private int exp;
    private int point;
    private Long hit;
    private String useYn;
    private Long regId;
    private LocalDateTime regDt;
    private Long modId;
    private LocalDateTime modDt;
    private Long cateId;

    private String linkUrl;


    //챌린지
    private Long challengeId;
    private String rangeYn;
    private String particptnTpCd;
    private LocalDateTime startDt;
    private LocalDateTime endDt;
    private LocalDateTime actDt;
    private String actArea;
    private String method;


    //장소
    private List<String> place;
    //메타
    private List<String> prps;


    public TbMssn ofMssnSave(Long seq){
        TbMssn tm = new TbMssn();
        if(this.mssnId == null){
            tm.setRegId(seq);
            tm.setRegDt(LocalDateTime.now());
            tm.setModId(seq);
            tm.setModDt(LocalDateTime.now());
            tm.setCateId(this.cateId);
            tm.setTpcTpCd("TPC003");
            tm.setImgUrl(this.imgUrl);
            tm.setLinkUrl(this.linkUrl);

            tm.setExp(this.exp);
            tm.setPoint(this.point);
            tm.setHit(0L);
            tm.setUseYn(YnType.Y.getName());
        }
        else {
            tm.setMssnId(this.mssnId);
            tm.setCateId(this.cateId);
            tm.setTpcTpCd(this.tpcTpCd);
            tm.setImgUrl(this.imgUrl);
            tm.setLinkUrl(this.linkUrl);

            tm.setExp(this.exp);
            tm.setPoint(this.point);
            tm.setHit(this.hit);
            tm.setUseYn(this.useYn);

            tm.setRegId(this.regId);
            tm.setRegDt(this.regDt);
            tm.setModId(seq);
            tm.setModDt(LocalDateTime.now());
        }
        return tm;
    }
    public TbMssn ofMssnSave(Long seq, String img){
        TbMssn tm = new TbMssn();
        if(this.mssnId == null){
            tm.setRegId(seq);
            tm.setRegDt(LocalDateTime.now());
            tm.setModId(seq);
            tm.setModDt(LocalDateTime.now());
            tm.setCateId(this.cateId);
            tm.setTpcTpCd("TPC003");
            tm.setImgUrl(img);
            tm.setLinkUrl(this.linkUrl);

            tm.setExp(this.exp);
            tm.setPoint(this.point);
            tm.setHit(0L);
            tm.setUseYn(YnType.Y.getName());
        }
        else {
            tm.setMssnId(this.mssnId);
            tm.setCateId(this.cateId);
            tm.setTpcTpCd(this.tpcTpCd);
            tm.setImgUrl(img);
            tm.setLinkUrl(this.linkUrl);

            tm.setExp(this.exp);
            tm.setPoint(this.point);
            tm.setHit(this.hit);
            tm.setUseYn(this.useYn);

            tm.setRegId(this.regId);
            tm.setRegDt(this.regDt);
            tm.setModId(seq);
            tm.setModDt(LocalDateTime.now());
        }
        return tm;
    }

    public List<TbMssnPlace> ofPlace(Long seq){
        List<TbMssnPlace> tmp = new ArrayList<>();
        this.place.stream().forEach(e -> tmp.add(TbMssnPlace.builder().mssnId(seq).placeCd(e).build()));
        return tmp;
    }

    public List<TbMssnMeta> ofMeta(Long seq){
        List<TbMssnMeta> tmp = new ArrayList<>();
        this.prps.stream().forEach(e -> tmp.add(TbMssnMeta.builder().mssnId(seq).prpsCd(e).build()));
        return tmp;
    }



}
