package com.ui.hooxi.admin.db.entity.mission;

import com.ui.hooxi.admin.db.entity.mission.id.TbMssnMetaId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "tb_mssn_meta")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(TbMssnMetaId.class)
public class TbMssnMeta {

    @Id
    @Column(name = "mssn_id", nullable = true)
    private Long mssnId;
    @Id
    @Column(name = "prps_cd", nullable = true)
    private String prpsCd;

    @Builder
    public TbMssnMeta(Long mssnId, String prpsCd) {
        this.mssnId = mssnId;
        this.prpsCd = prpsCd;
    }

}
