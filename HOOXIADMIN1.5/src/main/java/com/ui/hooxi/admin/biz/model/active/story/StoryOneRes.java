package com.ui.hooxi.admin.biz.model.active.story;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoryOneRes {

    private Long storyId;           //스토리 아이디
    private String nickname;            //작성자
    private Long actTlId;           //액션타임라인
    private String storyStatCd;     //스토리 상태 승인여부
    private String storyTpCd;       //스토리타입
    private String feedTpCd;        //피드타입
    private String storyCntn;       //스토리콘텐츠
    private String linkUrl;         //링크
    private String mainImgUrl;      //메인사진
    private String replyYn;         //댓글공개여부
    private String viewsYn;         //조회수공개여부
    private String likeYn;          //추천수공개여부
    private String privateTpCd;     //스토리 공개타입
    private String locat;           //위치
    private Long viewCnt;           //조회수
    private String regId;             //작성자
    private LocalDateTime regDt;    //작성일
    private String modId;             //수정자
    private LocalDateTime modDt;    //수정일
    private Long comentCount;       //댓글개수

    private List<Object> coment;    //댓글


}
